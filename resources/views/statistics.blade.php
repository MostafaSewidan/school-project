@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('الإحصائيات')
                                ])

@section('content')

    @push('styles')
        {{-- ChartStyle --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    @endpush

    @push('scripts')
        {!! $openWOByAge->script() !!}
        {!! $openWOByDept->script() !!}
        {!! $openWOByManufacturer->script() !!}
        {!! $openWOByYear->script() !!}
        {!! $equipmentByClass->script() !!}
        {!! $equipmentByDept->script() !!}
        {!! $cmPerformanceRate->script() !!}
    @endpush


    <style>
        .info-box-number {
            font-family: Trebuchet, Arial, sans-serif;
        }
    </style>


    <section>
        <div class="row">
            <div class="col-md-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('أوامر العمل المفتوحة لكل قسم') }}</h5>
                    </div>
                    <div class="ibox-content">
{{--                        {!!$openWOByDept->container() !!}--}}
                        {!! $openWOByDeptChartJs !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('أيام التأخير لأوامر العمل المفتوحة') }}</h5>
                    </div>
                    <div class="ibox-content">
{{--                        {!! $openWOByAge->container() !!}--}}
                        {!! $openWOByAgeChartJs !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('الأجهزة حسب التصنيف') }}</h5>
                    </div>
                    <div class="ibox-content">
                        {{--                        {!!$equipmentByClass->container() !!}--}}
                        {!! $equipmentByClassesChartJs !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('أوامر العمل المفتوحة لكل مُصنع') }}</h5>
                    </div>
                    <div class="ibox-content">
                        {!!$openWOByManufacturer->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('أوامر العمل لكل سنة') }}</h5>
                    </div>
                    <div class="ibox-content">
                        {!!$openWOByYear->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('الأجهزة حسب القسم') }}</h5>
                    </div>
                    <div class="ibox-content">
                        {!!$equipmentByDept->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('معدل أداء الصيانات') }}</h5>
                    </div>
                    <div class="ibox-content">
                        {!!$cmPerformanceRate->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <section>
        <div class="row">

        </div>
    </section>

@endsection
