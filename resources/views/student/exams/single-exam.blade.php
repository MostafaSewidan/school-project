<div class="{{!empty($col_class) ? $col_class : 'col-lg-12'}} animated fadeInRight">

    <div class="mail-box-header" style="padding: 5px 0px 15px 4px;">

        <label class="label label-lg label-inverse"
               style="float:right;font-size: 1.5rem;">@lang('student.exam_num') #{{$exam->id}} </label>
        <div class="tooltip-demo" style="text-align: left;margin-bottom: -5px;">
            <label class="{{$exam->auth_student_answer_status->label_class}}">{{trans('student.'.$exam->auth_student_answer_status->status)}}</label>
        </div>
    </div>
    <div class="mail-box-header">

        <h5>
            @lang('student.exam_start_date'):
            {{$exam->start_datetime->locale(app()->getLocale())->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  h:mm a')}}
        </h5>

        <h5>
            @lang('student.exam_end_date'):
            {{$exam->end_datetime->locale(app()->getLocale())->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  h:mm a')}}
        </h5>
    </div>
    <div class="mail-box">

        <div class="mail-attachment">
            <p>

           <span><i class="fa fa-paperclip"></i>
               <span class="badge badge-primary"> {{$exam->attachmentRelation()->count()}} </span>
               @lang('student.files') @lang('student.exam') - </span>

            </p>

            @if($exam->attachmentRelation()->count())
                <div class="attachment">

                    @foreach($exam->attachmentRelation as $attachment)
                        <div class="file-box" style="width: 195px;">
                            <div class="file">
                                <a href="#">
                                    <span class="corner"></span>

                                    @if($attachment->type == 'image')
                                        <div class="image">
                                            <img alt="image" class="img-fluid img-responsive"
                                                 src="{{asset($attachment->path)}}">
                                        </div>
                                    @else
                                        <a href="{{asset($attachment->path)}}">
                                            <div class="icon">
                                                <i class="fa fa-file"></i>
                                            </div>
                                        </a>

                                    @endif
                                    <div class="file-name">
                                        <strong>{{$attachment->display_name ?? trans('student.exam_form')}}</strong>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                </div>

            @else
                <label class="label label-danger">@lang('student.files_not_found')</label>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="mail-body">
            <p>
                <strong>@lang('student.what_will_the_exam_cover?') :</strong>
                <br>
                <br>

                @if($exam->description)
                    {!! $exam->description !!}
                @else
                    <label class="label label-danger">@lang('student.no_text')</label>
                @endif
            </p>
        </div>
        <div class="clearfix"></div>
        @if($exam->auth_student_answer_status != 'not_start')
            <div class="mail-attachment">
                <p>
                    @php $student_answer = $exam->AnswerExamStudents()->where('student_id' , auth('student')->user()->id)->first(); @endphp

                    <span><i class="fa fa-paperclip"></i>
               <span class="badge badge-primary"> </span>
               @lang('student.files') @lang('student.exam') - </span>

                </p>
                @if($student_answer && $student_answer->attachmentRelation)

                    <div class="file-box" style="width: 195px;">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>

                                @if($student_answer->attachmentRelation->type == 'image')
                                    <div class="image">
                                        <img alt="image" class="img-fluid img-responsive"
                                             src="{{asset($student_answer->attachmentRelation->path)}}">
                                    </div>
                                @else
                                    <a href="{{asset($student_answer->attachmentRelation->path)}}">
                                        <div class="icon">
                                            <i class="fa fa-file"></i>
                                        </div>
                                    </a>

                                @endif
                                <div class="file-name">
                                    <strong>@lang('student.answer_file')</strong>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div>
                        <strong> @lang('student.written_answer') : </strong>

                        @if($student_answer->answer)
                            <p>
                                {{$student_answer->answer}}
                            </p>
                        @else
                            <label class="label label-danger">@lang('student.written_answer_not_added')</label>
                        @endif

                    </div>

                @else

                    @if(in_array($exam->auth_student_answer_status->status,['started']))
                        <div class="modal fade" id="append-exam-model" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            @lang('student.append_answer')
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    {!! Form::open([
                                        'url' => url('student/exams/append-exam/'.$exam->id),
                                        'id' => 'ajaxForm',
                                        'role' => 'form',
                                        'method' => 'POST',
                                        'files' => true,
                                     ]) !!}

                                    <div class=" modal-body">


                                        <div class="box-body">
                                            {!! \Helper\Field::textarea('answer', trans('student.write_answer')) !!}
                                            {!! \Helper\Field::fileWithPreview('exam_pdf', trans('student.append_answer')) !!}
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <div class="box-footer">
                                            {!! \Helper\Field::ajaxBtn(trans('student.save')) !!}
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">@lang('student.close')
                                            </button>
                                        </div>
                                    </div>

                                    {!! Form::close()!!}
                                </div>
                            </div>
                        </div>
                        <button type="button" data-toggle="modal" data-target="#append-exam-model"
                                class="btn btn-sm btn-primary">
                            <i class="fa fa-paperclip"></i>
                            @lang('student.append_answer')
                        </button>
                    @else
                        <label class="label label-danger">@lang('student.exam_time_passed')</label>
                    @endif

                @endif

                @else
                    <label class="label label-danger">@lang('student.files_not_found')</label>
                @endif
            </div>

            @if(in_array($exam->auth_student_answer_status->status,['corrected']))
                <div class="clearfix"></div>
                <div class="mail-body">
                    <p>
                        <strong>@lang('student.exam_degree') :</strong>

                        @if($student_answer->result)
                            <span class="badge badge-primary"> {!! $student_answer->result !!} </span>

                        @else
                            <label class="label label-danger">@lang('student.no_text')</label>
                        @endif
                    </p>
                    <p>
                        <strong>@lang('student.teacher_comment') :</strong>
                        <br>
                        <br>

                        @if($student_answer->teacher_comment)
                            {!! $student_answer->teacher_comment !!}
                        @else
                            <label class="label label-danger">@lang('student.no_text')</label>
                        @endif
                    </p>
                </div>
            @endif
            <div class="clearfix"></div>

    </div>
</div>

