@extends('layouts.student',[
								'page_header'		=> trans('student.exams'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/exams'),
								'links' => [
								    trans('student.exams') => url('student/exams'),
								    $subject->name.' - '.$exam->name => url('student/exams/'.$exam->id)
								],
								])
@section('content')


    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="file-manager">

                            <h3>@lang('student.subject_details')</h3>

                            <div class="card">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <strong>@lang('student.subject_name') :</strong> {{$subject->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.teacher_name')
                                            :</strong> {{optional($group->teacher)->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.manager_name') :</strong> {{optional($group->teacher->users()->first())->name}}
                                    </li>

                                </ul>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">

                        @include('student.exams.single-exam')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


