@extends('layouts.student',[
								'page_header'		=> trans('student.exams'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/exams'),
								'links' => [
								    trans('student.exams') => url('student/exams')
								],
								])
@section('content')

    @include('layouts.full-schedule' , ['get_res_url' => url('student/exams/get-sessions')])

@stop


