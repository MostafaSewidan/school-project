<div class="{{!empty($col_class) ? $col_class : 'col-lg-12'}} animated fadeInRight">

    <div class="mail-box-header" style="padding: 5px 0px 15px 4px;">
        <label class="label label-lg label-inverse"
               style="float:right;font-size: 1.5rem;">#{{$session->id}}</label>
        <div class="tooltip-demo" style="text-align: left;margin-bottom: -5px;">
            @if($session->auth_student_absent != null)
                @if($session->auth_student_absent == 1)
                    <label class="label label-success">@lang('student.truant')</label>
                @else
                    <label class="label label-danger">@lang('student.truant')</label>
                @endif
            @else
                <label class="label label-warning">@lang('student.absences_not_registered_yet')</label>
            @endif
        </div>
    </div>
    <div class="mail-box-header">

        <h4>
            @lang('student.session_num_title'):
            {{$session->id}}
        </h4>
        <div class="mail-tools tooltip-demo m-t-md">
            <strong> @lang('student.zoom_link'):</strong>
            <a href="{{$session->zoom_link}}" title="test">
                {{$session->zoom_link}}
            </a>
            <br>
            <div class="user-friends">
                @if($session->group->students())
                    <br>
                    <br>
                    <p><strong>@lang('student.students') :</strong></p>
                    @foreach($session->group->students()->get() as $user)
                        <a href="#" title="test">
                            <img alt="image" class="rounded-circle img-circle"
                                 title="{{$user->name}}" src="{{$user->attachment}}">
                        </a>
                    @endforeach
                @endif
            </div>
            <h5>
                <br>
                <span class="float-left font-normal" style="float: left">
       <strong>
            @lang('student.session_datetime'):
       </strong>
       {{$session->datetime->locale(app()->getLocale())->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  h:mm a')}}
   </span>
            </h5>
        </div>
    </div>
    <div class="mail-box">


        <div class="mail-body">
            <p>
                <strong>@lang('student.session_description')</strong>
                <br>
                <br>
                {!! $session->description !!}
            </p>
        </div>

        <div class="mail-attachment">
            <p>

           <span><i class="fa fa-paperclip"></i>
               <span class="badge badge-primary"> {{$session->attachmentRelation()->count()}} </span>
               @lang('student.files') - </span>

            </p>

            @if($session->attachmentRelation()->count())
                <div class="attachment">

                    @if($session->home_work)
                        @if($session->datetime > \Carbon\Carbon::now()->addMinutes($group->duration + 3))

                            <label class="label label-danger">@lang('student.session_download_stop')</label>

                        @else
                            @if(!$session->home_work_answer)
                                <a onclick="openAppendModel({{$session}})" class="btn btn-sm btn-primary">
                                    <i class="fa fa-paperclip"></i>
                                    {{__('إرفاق الواجب كـ pdf')}}
                                </a>
                                <br>
                                <br>
                            @else
                                <div class="file-box" style="width: 195px;">
                                    <div class="file" style="border: 1px solid #43b494;">
                                        <a href="#">
                                            <span class="corner"></span>

                                                <a href="{{$session->datetime < \Carbon\Carbon::now()->addMinutes($group->duration + 3) ? $session->home_work_answer : '#'}}">
                                                    <div class="icon">
                                                        <i class="fa fa-file"></i>
                                                    </div>
                                                </a>

                                            <div class="file-name">
                                                <strong>@lang('student.home_work_answer')</strong>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="file-box" style="width: 195px;">
                            <div class="file">
                                <a href="#">
                                    <span class="corner"></span>

                                    @if($session->home_work == 'image')
                                        <div class="image">
                                            <img alt="image" class="img-fluid img-responsive"
                                                 src="{{$session->datetime < \Carbon\Carbon::now()->addMinutes($group->duration + 3) ?$session->home_work : '#'}}">
                                        </div>
                                    @else
                                        <a href="{{$session->datetime < \Carbon\Carbon::now()->addMinutes($group->duration + 3) ?$session->home_work : '#'}}">
                                            <div class="icon">
                                                <i class="fa fa-file"></i>
                                            </div>
                                        </a>

                                    @endif
                                    <div class="file-name">
                                        <strong>@lang('student.home_work')</strong>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @else
                        <label class="label label-danger">@lang('student.home_work_not_added')</label>
                    @endif
                    <div class="clearfix"></div>
                </div>

            @else
                <label class="label label-danger">@lang('student.files_not_found')</label>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
</div>

