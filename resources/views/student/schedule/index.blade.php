@extends('layouts.student',[
								'page_header'		=> trans('student.schedule'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/schedule'),
								'links' => [
								    trans('student.schedule') => url('student/schedule')
								],
								])
@section('content')

    @include('layouts.full-schedule' , ['get_res_url' => url('student/schedule/get-sessions'),'default_view' => 'agendaWeek'])

@stop


