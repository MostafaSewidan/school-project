@extends('layouts.student',[
								'page_header'		=> trans('student.schedule'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/schedule'),
								'links' => [
								    trans('student.schedule') => url('student/schedule'),
								    $record->name => url('student/schedule/'.$session->id)
								],
								])
@section('content')


    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="file-manager">

                            <h3>@lang('student.subject_details')</h3>

                            <div class="card">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <strong>@lang('student.subject_name') :</strong> {{$record->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.teacher_name')
                                            :</strong> {{optional($group->teacher)->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.manager_name') :</strong> {{optional($group->teacher->users()->first())->name}}
                                    </li>

                                </ul>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <ul class="folder-list" style="padding: 0">
                                <h3>@lang('student.subjects')</h3>
                                @foreach($subjects as $subject)
                                    <li>
                                        <a href="{{url('student/schedule/'.$subject->id)}}">
                                            <i class="fa fa-book"></i> {{$subject->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="text-center">
                                <a href="{{url('student/groups')}}">
                                    @lang('student.show_all')
                                </a>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="modal fade" id="append-homework-model" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            {{__('إرفاق الواجب كـ pdf')}}
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    {!! Form::open([

                                        'id' => 'ajaxForm',
                                        'role' => 'form',
                                        'method' => 'POST',
                                        'files' => true,
                                     ]) !!}

                                    <div class=" modal-body">


                                        <div class="box-body">
                                            {!! \Helper\Field::fileWithPreview('homework_pdf', __('إرفاق الواجب كـ pdf')) !!}
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <div class="box-footer">
                                            {!! \Helper\Field::ajaxBtn(trans('student.save')) !!}
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">@lang('student.close')
                                            </button>
                                        </div>
                                    </div>

                                    {!! Form::close()!!}
                                </div>
                            </div>
                        </div>
                        <script>
                            function openAppendModel(model) {
                                $('#ajaxForm').attr('action', '{{url('student/schedule/'.$record->id.'/append-home-work')}}/' + model.id);
                                $('#append-homework-model').modal('show');
                            }
                        </script>

                        @include('student.schedule.single-session')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


