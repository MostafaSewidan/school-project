@extends('layouts.student',[
                                    'page_header'       => trans('student.profile'),
                                    'page_description'  => '',
                                    'link' => url('student/profile')
                                ])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($student, [
            'url' => url('student/profile'),
            'id' => 'ajaxForm',
            'role' => 'form',
            'method' => 'POST',
            'files' => true,
        ]) !!}

        <div class="ibox-content">
            {!! \Helper\Field::password('old_password' , trans('student.current_password'))!!}
            {!! \Helper\Field::password('password' , trans('student.new_password')) !!}
            {!! \Helper\Field::password('password_confirmation' , trans('student.confirm_new_password')) !!}
            {!! \Helper\Field::fileWithPreview('profile_photo', trans('student.profile_photo')) !!}
            @if($student->attachmentRelation)
                <div class="col-md-4">
                    <img src="{{$student->attachment}}" alt="" class="img-responsive thumbnail">
                </div>
                <div class="clearfix"></div>
            @endif
        </div>

        <div class="ibox-footer">
            {!! \Helper\Field::ajaxBtn(trans('student.save')) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection