<div class="attachment">
    @php $count = 1; @endphp
    @foreach($records as $record)

        <div class="file-box">
            <div class="file">
                <a href="{{url('student/groups/'.$record->id)}}">
                    <span class="corner"></span>

                    <div class="icon" style="height: 15rem;padding: 0px 10px;">
                        <img src="{{asset('photos/book-icon.png')}}" style=" max-width: 15rem;">
{{--                        <i class="fa fa-book" style="color: #ff980099;"></i>--}}
                    </div>
                    <div class="file-name text-center">
                        <span style="font-size: 1.6rem;">{{$record->name}}</span>

                        <small>@lang('student.sections_num')</small>
                        <span class="badge badge-primary">{{\App\Models\Subject::studySchedule($record->id , true)->count()}}</span>
                    </div>
                </a>
            </div>

        </div>

        @php $count ++; @endphp
    @endforeach
</div>