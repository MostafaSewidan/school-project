<div class="col-lg-12">

    <div class="ibox-content">
        <div class="table-responsive">
            <table class="data-table table table-bordered dataTables-example">
                <thead>
                <th class="text-center">#</th>
                <th class="text-center">@lang('student.subject_name')</th>
                <th class="text-center">@lang('student.sections_num')</th>
                <th class="text-center">@lang('student.teacher_name')</th>
                <th class="text-center">@lang('student.show')</th>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach ($records as $record)
                    <tr>
                        <td class="text-center">{{ $count }}</td>
                        <td class="text-center">{{ $record->name }}</td>
                        <td class="text-center">
                            <span class="badge badge-primary">{{\App\Models\Subject::studySchedule($record->id , true)->count()}}</span>
                        </td>
                        <td class="text-center">{{ optional($record->studentGroup()->teacher)->name }}</td>
                        <td class="text-center">
                            <a href="{{ url('student/groups/'.$record->id) }}"
                               class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>

                    @php $count ++; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>