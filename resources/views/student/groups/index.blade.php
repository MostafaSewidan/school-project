@extends('layouts.student',[
								'page_header'		=> trans('student.groups'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/groups'),
								'links' => [
								    trans('student.groups') => url('student/groups')
								],
								])
@section('content')


    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="file-manager">

                            <a href="{{url('student/groups?show=block-show')}}" class="btn btn-info m-r-sm">
                                <i class="fa fa-th-large"></i>
                            </a>
                            <a href="{{url('student/groups?show=layer-show')}}" class="btn btn-info m-r-sm">
                                <i class="fa fa-table"></i>
                            </a>
                            <div class="hr-line-dashed"></div>

                            <h3>@lang('student.search')</h3>

                            {!! Form::open([
                                   'method' => 'get'
                                   ]) !!}
                            {!! Form::hidden('show' , request('show','block-show')) !!}
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        {!! Form::text('name',request()->input('name'),[
                                            'class' => 'form-control',
                                            'placeholder' => trans('student.search_placeholder')
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button class="btn btn-flat btn-block btn-primary"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>

                            </div>

                            {!! Form::close() !!}
                            <div class="hr-line-dashed"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        @if(!empty($records) && count($records)>0)
                            @include('student.groups.'.
                            (in_array(request('show' , 'block-show'),['block-show','layer-show']) ? request('show' , 'block-show') : 'block-show'))


                            {!! $records->render() !!}
                        @else
                            <div>
                                <h3 class="text-info" style="text-align: center"> @lang('student.no_data_to_show') </h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


