@extends('layouts.student',[
								'page_header'		=> trans('student.schedule'),
								'page_description'	=> trans('student.show'),
								'link' => url('student/schedule'),
								'links' => [
								    trans('student.schedule') => url('student/schedule'),
								    $record->name => url('student/schedule/'.$record->id)
								],
								])
@section('content')


    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="file-manager">

                            <h3>@lang('student.subject_details')</h3>

                            <div class="card">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <strong>@lang('student.subject_name') :</strong> {{$record->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.teacher_name')
                                            :</strong> {{optional($group->teacher)->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>@lang('student.manager_name') :</strong> {{optional($group->teacher->users()->first())->name}}
                                    </li>

                                </ul>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <ul class="folder-list" style="padding: 0">
                                <h3>@lang('student.subjects')</h3>
                                @foreach($subjects as $subject)
                                    <li>
                                        <a href="{{url('student/schedule/'.$subject->id)}}">
                                            <i class="fa fa-book"></i> {{$subject->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="text-center">
                                <a href="{{url('student/schedule')}}">
                                    @lang('student.show_all')
                                </a>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox-content">
                            <h3>الحضور</h3>
                            <div class="table-responsive">
                                <table class="data-table table table-bordered dataTables-example">
                                    <thead>
                                    <th class="text-center">@lang('student.session')</th>
                                    <th class="text-center">@lang('student.status')</th>
                                    </thead>
                                    <tbody>
                                    @if(!empty($sessions) && count($sessions)>0)
                                        @foreach($sessions as $session)
                                            <tr>
                                                <td class="text-center">
                                                    <a href="{{url('student/schedule/'.$session->id)}}">
                                                        {{$session->datetime->locale(app()->getLocale())->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  h:mm a')}}
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    @if($session->auth_student_absent != null)
                                                        @if($session->auth_student_absent == 1)
                                                            <label class="label label-success">@lang('student.truant')</label>
                                                        @else
                                                            <label class="label label-danger">@lang('student.truant')</label>
                                                        @endif
                                                    @else
                                                        <label class="label label-warning">@lang('student.absences_not_registered_yet')</label>
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="2">
                                                <h4 class="text-info"
                                                    style="text-align: center"> @lang('student.no_data_to_show') </h4>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox-content">
                            <h3>@lang('student.subject_schedule')</h3>
                            @include('layouts.full-schedule' , ['get_res_url' => url('student/schedule/get-sessions?subject='.$record->id)])
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">


                        <div class="ibox-content">
                            <h3>@lang('student.exams_degree')</h3>
                            <div class="table-responsive">
                                <table class="data-table table table-bordered dataTables-example">
                                    <thead>
                                    <th class="text-center">@lang('student.date')</th>
                                    <th class="text-center">@lang('student.degree')</th>
                                    </thead>
                                    <tbody>

                                    @if(!empty($exams) && count($exams)>0)
                                        @foreach($exams as $exam)
                                            @php $student_exam = $exam->answers()->where('student_id',auth('student')->user()->id)->first(); @endphp
                                            <tr>
                                                <td class="text-center">
                                                    <a href="{{url('student/exams/'.$exam->id)}}">
                                                        {{$exam->name}}
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    @if($student_exam)
                                                        <label class="label label-success">{{ $student_exam->pivot->result }}</label>
                                                    @else
                                                        <label class="label label-warning">@lang('student.not_answered_yet')</label>
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach


                                    @else
                                        <tr>
                                            <td colspan="2">
                                                <h4 class="text-info"
                                                    style="text-align: center"> @lang('student.no_data_to_show') </h4>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


