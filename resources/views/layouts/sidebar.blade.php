<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">

                    {{-- // profile image and display name of user --}}

                    <span>
                        <img alt="image" class="img-circle" style="max-width: 50px;"
                            src="{{ auth()->check() ? auth()->user()->photo : '' }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ auth()->check() ? Auth::user()->name : null }}</strong>
                                {{-- @foreach (auth()->user()->roles as $role) --}}
                                {{-- <span class="label label-success">{{$role->display_name}}</span> --}}
                                {{-- @endforeach --}}
                            </span>
                        </span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{-- <!-- <li><a href="">حسابي</a></li> --}}
                        {{-- <li class="divider"></li> --> --}}

                        <li>
                            <script type="">
                                function submitSidebarSignout() {
                                    document.getElementById('signoutSidebar').submit();

                                }
                            </script>
                            {!! Form::open(['method' => 'post', 'url' => url('logout'), 'id' => 'signoutForm', 'id' => 'signoutSidebar']) !!}

                            {!! Form::close() !!}
                            <a href="#" onclick="submitSidebarSignout()">{{ __('تسجيل الخروج') }}</a>

                        </li>
                    </ul>
                </div>

                {{-- <div class="logo-element">{{ app('settings')->site_name }}</div> --}}
            </li>
            {{-- Home --}}

            @php
                $role = json_decode(
                    auth()
                        ->user()
                        ->getRoleNames(),
                )[0];
            @endphp

            @if ($role)

            @endif

            @if ($role == 'مدير')
                @can('عرض المرحلة الدراسية')
                    <li>
                        <a href="{{ url('manager/level') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('المراحل الدراسية') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير')
                @can('عرض المواد الدراسية')
                    <li>
                        <a href="{{ url('manager/subject') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('المواد الدراسية') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير' || $role == 'مشرف رئيسي')
                @can('عرض المشرف')
                    <li>
                        <a href="{{ url('manager/visor') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('المشرفين') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role)
                @can('عرض المدرس')
                    <li>
                        <a href="{{ url('manager/teacher') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('المدرسين') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير' || $role == 'مشرف')
                @can('عرض المجموعات')
                    <li>
                        <a href="{{ url('manager/group') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('المجموعات') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير' || $role == 'مشرف')
                @can('عرض الجدول الدراسي')
                    <li>
                        <a href="{{ url('manager/study-schedule') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('الجدول الدراسي') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role)
                @can('عرض الطلاب')
                    <li>
                        <a href="{{ url('manager/student') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('الطلاب') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير' || $role == 'مشرف')
                @can('عرض الامتحانات')
                    <li>
                        <a href="{{ url('manager/exam') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('الامتحانات') }}</span>
                        </a>
                    </li>
                @endcan
            @endif

            @if ($role)
                @can('عرض الرسائل')
                    <li>
                        <a href="#">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('الرسائل') }}</span>
                        </a>
                    </li>
                @endcan
            @endif

            @if ($role)
                @can('عرض التقرير')
                    <li>
                        <a href="{{ url('manager/reports') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{ __('التقارير') }}</span>
                        </a>
                    </li>
                @endcan
            @endif
            @if ($role == 'مدير')
                @can('عرض السجلات')
                    <li>
                        <a href="{{ url('manager/logs') }}">
                            <i class="fa fa-copy" aria-hidden="true"></i>
                            <span class="nav-label">{{ __('السجلات') }}</span>
                        </a>
                    </li>
                @endcan
            @endif

            @if ($role == 'مدير')
                <li>
                    <a href="#">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        <span class="nav-label">رتب المستخدمين</span>
                    </a>
                </li>
            @endif

            <li>
                <a href="#">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <span class="nav-label"> اعدادات الحساب</span>
                </a>
            </li>

            <li>
                <script type="">
                    function submitSignout() {
                        document.getElementById('signoutForm').submit();

                    }
                </script>
                {!! Form::open(['method' => 'post', 'url' => url('logout'), 'id' => 'signoutForm']) !!}

                {!! Form::close() !!}

                <a href="#" onclick="submitSignout()">
                    <i class="fa fa-lock"></i> {{ __('تسجيل الخروج') }}
                </a>
            </li>


        </ul>

    </div>
</nav>
