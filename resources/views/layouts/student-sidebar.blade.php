<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">

                    {{-- // profile image and display name of user --}}

                    <span>
                          <img alt="image" class="img-circle" style="       width: 7rem;
    height: 6rem;"
                               src="{{auth('student')->user()->attachment}}">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ auth()->check() ? Auth::user()->name : null }}</strong>
                                {{-- @foreach (auth()->user()->roles as $role) --}}
                                {{-- <span class="label label-success">{{$role->display_name}}</span> --}}
                                {{-- @endforeach --}}
                            </span>
                        </span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{-- <!-- <li><a href="">حسابي</a></li> --}}
                        {{-- <li class="divider"></li> --> --}}

                        <li>
                            <script type="">
                                function submitSidebarSignout() {
                                    document.getElementById('signoutSidebar').submit();

                                }
                            </script>
                            {!! Form::open(['method' => 'post', 'url' => url('logout'), 'id' => 'signoutForm', 'id' => 'signoutSidebar']) !!}

                            {!! Form::close() !!}
                            <a href="#" onclick="submitSidebarSignout()">{{ __('تسجيل الخروج') }}</a>

                        </li>
                    </ul>
                </div>

                <div class="logo-element">{{ app('settings')->site_name }}</div>
            </li>
            {{-- Home --}}

            <li>
                <a href="{{ url('student/home') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">@lang('student.home')</span>
                </a>
            </li>

            <li>
                <a href="{{ url('student/schedule') }}">
                    <i class="fa fa-calendar"></i>
                    <span class="nav-label">@lang('student.schedule')</span>
                </a>
            </li>

            <li>
                <a href="{{ url('student/groups') }}">
                    <i class="fa fa-users"></i>
                    <span class="nav-label">@lang('student.groups')</span>
                </a>
            </li>

            <li>
                <a href="{{ url('student/exams') }}">
                    <i class="fa fa-file"></i>
                    <span class="nav-label">@lang('student.exams')</span>
                </a>
            </li>

            <li>
                <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">@lang('student.settings')</span><span
                            class="fa arrow"></span></a>

                <ul class="nav nav-second-level collapse">

                    <li>
                        <a href="{{url('student/profile')}}">
                            @lang('student.profile')
                        </a>
                    </li>
                </ul>
            </li>

        </ul>

    </div>
</nav>
