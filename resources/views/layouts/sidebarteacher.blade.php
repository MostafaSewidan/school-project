<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">

                    {{-- // profile image and display name of user --}}

                    <span>
                        <img alt="image" class="img-circle" style="max-width: 50px;"
                            src="{{ auth()->check() ? auth()->user()->photo : '' }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ auth()->check() ? Auth::user()->name : null }}</strong>
                                {{-- @foreach (auth()->user()->roles as $role) --}}
                                {{-- <span class="label label-success">{{$role->display_name}}</span> --}}
                                {{-- @endforeach --}}
                            </span>
                        </span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{-- <!-- <li><a href="">حسابي</a></li> --}}
                        {{-- <li class="divider"></li> --> --}}

                        <li>
                            <script type="">
                                function submitSidebarSignout() {
                                    document.getElementById('signoutSidebar').submit();

                                }
                            </script>
                            {!! Form::open(['method' => 'post', 'url' => url('logout'), 'id' => 'signoutForm', 'id' => 'signoutSidebar']) !!}

                            {!! Form::close() !!}
                            <a href="#" onclick="submitSidebarSignout()">{{ __('تسجيل الخروج') }}</a>

                        </li>
                    </ul>
                </div>

                <div class="logo-element">{{ app('settings')->site_name }}</div>
            </li>
            {{-- Home --}}

            <li>
                <a href="{{ url('teacher/study-schedule') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('الجدول الدراسي') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ url('teacher/group') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('المجموعات') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ url('teacher/exam') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('الامتحانات') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ url('teacher/group') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('واجب الحصص') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ url('teacher/group') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('التقارير') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ url('teacher/group') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('الرسائل') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ url('teacher/teacher') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ __('اعدادات المدرس') }}</span>
                </a>
            </li>

            <li>
                <script type="">
                    function submitSignout() {
                        document.getElementById('signoutForm').submit();

                    }
                </script>
                {!! Form::open(['method' => 'post', 'url' => url('logout'), 'id' => 'signoutForm']) !!}

                {!! Form::close() !!}

                <a href="#" onclick="submitSignout()">
                    <i class="fa fa-lock"></i> {{ __('تسجيل الخروج') }}
                </a>
            </li>
            {{-- <li> --}}
            {{-- <a href="{{url('admin/technical-support')}}"> --}}
            {{-- <i class="fa fa-support"></i> --}}
            {{-- <span class="nav-label">{{__('تواصل معنا')}}</span> --}}
            {{-- </a> --}}
            {{-- </li> --}}

            {{-- <li> --}}
            {{-- @if (app()->getLocale() == 'en') --}}
            {{-- <a class="nav-label" href="{{ url('locale/ar') }}"><span class="flag-icon flag-icon-sa"> </span> العربية</a> --}}
            {{-- @elseif(app()->getLocale() == 'ar') --}}
            {{-- <a class="nav-label" href="{{ url('locale/en') }}"><span class="flag-icon flag-icon-us"> </span> English</a> --}}
            {{-- @endif --}}
            {{-- </li> --}}


            {{-- <li> --}}
            {{-- <a href="{{url('admin/role')}}"> --}}
            {{-- <i class="fa fa-user-circle" aria-hidden="true"></i> --}}
            {{-- <span class="nav-label">رتب المستخدمين</span> --}}
            {{-- </a> --}}
            {{-- </li> --}}

        </ul>

    </div>
</nav>
