{{' طلب جديد: ' . '#'. $order->id}}
@foreach($order->products as $product)
{{$product->pivot->quantity.' '}}{{$product->name.' '}}{{$product->pivot->total_price .'ج'}}
@php $productInPivot = \App\Models\OrderProduct::where(['order_id' => $order->id, 'product_id' => $product->id])->first(); @endphp
@foreach($productInPivot->additions as $addition)
{{$addition->pivot->quantity .' ' .$addition->name .' ' .($addition->pivot->quantity * $addition->pivot->price) .'ج'}}
@endforeach
@endforeach