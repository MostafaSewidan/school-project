<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary pull-right" href="#"><i
                        class="fa fa-bars"></i> </a>
            {{-- <form role="search" class="navbar-form-custom" method="post" action="#"> --}}
            {{-- <div class="form-group"> --}}
            {{-- <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search"> --}}
            {{-- </div> --}}
            {{-- </form> --}}
        </div>
        @can('إضافة أوامر العمل')
            <ul class="nav navbar-left">
                <li>
                    <a href="{{ url(route('operations.create')) }}"><i class="fa fa-bolt"></i> {{ __('إنشاء أمر عمل') }}
                    </a>
                </li>
            </ul>
        @endcan


        <ul class="nav navbar-top-links navbar-right">
            @can('عرض أوامر العمل')
                @inject('workingOrders',App\Models\WorkingOrder)
                @php
                    $workCount = $workingOrders
                        ->where('status', '=', 'open')
                        ->pluck('id')
                        ->count();
                @endphp
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-bolt" style="font-size: 1.5em" data-toggle="tooltip"
                           title="{{ __('أوامر العمل') }}"></i>
                        <span class="label label-danger"
                              style="margin-top: -5px;margin-right: -2px">{{ $workCount }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($workingOrders->where('status', '=', 'open')->latest()->paginate(5)
        as $work)
                            <li>
                                <a href="{{ url(route('operations.show', $work->id)) }}" class="dropdown-item">
                                    <div>
                                        {{ __('تم فتح امر عمل علي جهاز') }}
                                        <span class="label label-info">{{ optional($work->device)->code }}</span>
                                        <span class="pull-right text-muted small">
                                            {{ $work->created_at->diffForHumans(Carbon\Carbon::Now()) }}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                        <li class="dropdown-divider"></li>

                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="{{ url('admin/operations?status=open') }}" class="dropdown-item">
                                    <strong>{{ __('مشاهدة الكل') }}</strong>
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

            @endcan
            {{-- contract end --}}

            @can('عرض العقود')
                @inject('contract',App\Models\Contract)
                @php
                    $contractCount = $contract
                        ->Missed()
                        ->pluck('id')
                        ->count();

                @endphp
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-files-o" style="font-size: 1.5em" data-toggle="tooltip"
                           title="{{ __('العقود') }}"></i>
                        <span class="label label-warning"
                              style="margin-top: -5px;margin-right: -2px">{{ $contractCount }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($contract->Missed()->latest()->paginate(5)
        as $work)
                            <li>
                                <a href="{{ url(route('contracts.show', $work->id)) }}" class="dropdown-item">
                                    <div>
                                        {{ __('قارب العقد على الانتهاء') }}
                                        <span class="label label-info">{{ $work->contract_number }}</span>
                                        <span class="pull-right text-muted small">
                                            {{ __('ينتهي في') }} {{ $work->contract_end }}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                        <li class="dropdown-divider"></li>

                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="{{ url('admin/contracts?allMissed=soon') }}" class="dropdown-item">
                                    <strong>{{ __('مشاهدة الكل') }}</strong>
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            @endcan
            {{-- end part contract end --}}
            @can('عرض الصيانات')
                @inject('maintenance',App\Models\Maintenance)
                @php
                    $due = $maintenance->due()->count();
                    $missed = $maintenance->missed()->count();
                @endphp
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-wrench" style="font-size: 1.5em" data-toggle="tooltip"
                           title="{{ __('الصيانات الواجبة') }}"></i>
                        <span class="label label-warning"
                              style="margin-top: -5px;margin-right: -2px">{{ $due }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($maintenance->due()->latest()->paginate(5)
        as $work)
                            <li>
                                <a href="{{ url('admin/maintenance?id=' . $work->id) }}" class="dropdown-item">
                                    <div>
                                        {{ __('صيانة علي جهاز') }}
                                        <span class="label label-warning">{{ optional($work->device)->code }}</span>
                                        <span class="pull-right text-muted small">
                                            {{ \Carbon\Carbon::parse($work->maintenance_date)->diffForHumans(Carbon\Carbon::Now()) }}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                        <li class="dropdown-divider"></li>

                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="{{ url('admin/maintenance?maintenance_status=due') }}" class="dropdown-item">
                                    <strong>{{ __('مشاهدة الكل') }}</strong>
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-wrench" style="font-size: 1.5em" data-toggle="tooltip"
                           title="{{ __('الصيانات الفائتة') }}"></i>
                        <span class="label label-danger"
                              style="margin-top: -5px;margin-right: -2px">{{ $missed }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        @foreach ($maintenance->missed()->latest()->paginate(5)
        as $work)
                            <li>
                                <a href="{{ url('admin/maintenance?id=' . $work->id) }}" class="dropdown-item">
                                    <div>
                                        {{ __('صيانة علي جهاز') }}
                                        <span class="label label-danger">{{ optional($work->device)->code }}</span>
                                        <span class="pull-right text-muted small">
                                            {{ \Carbon\Carbon::parse($work->maintenance_date)->diffForHumans(Carbon\Carbon::Now()) }}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                        <li class="dropdown-divider"></li>

                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="{{ url('admin/maintenance?maintenance_status=missed') }}"
                                   class="dropdown-item">
                                    <strong>{{ __('مشاهدة الكل') }}</strong>
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('عرض البنود')
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        {{-- <i class="fa fa-usd text-green"></i> --}}
                        <img src="{{ asset('photos/dinar.png') }}" data-toggle="tooltip"
                             title="{{ __('البنود المالية') }}" style="height: 24px;margin-top: -16px" alt="">
                    </a>
                    @inject('items',App\Models\Item)


                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="{{ url(route('approved-extract.index')) }}" class="dropdown-item">
                                <div>
                                    <i class="fa fa-usd fa-fw"></i>{{ __('متبقي البند العام') }}:
                                    <span class="float-right text-muted">
                                        {{ \Helper\Helper::remainingBudget() }}
                                    </span>
                                </div>
                            </a>
                            @foreach ($items->get() as $item)

                                <a href="{{ url(route('approved-extract.index')) }}" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-usd fa-fw"></i>{{ __('متبقي بند') }} - {{ $item->name }}
                                        <span class="float-right text-muted">{{ $item->general_item }}</span>
                                    </div>
                                </a>
                            @endforeach
                        </li>
                        <li class="dropdown-divider"></li>
                    </ul>
                </li>
            @endcan


            <li>
                <a href="{{ url('admin/technical-support') }}">
                    {{ __('تواصل معنا') }}
                </a>
            </li>
            <li>
                @if (app()->getLocale() == 'en')
                    <a class="nav-label" href="{{ url('locale/ar') }}"><span class="flag-icon flag-icon-sa"> </span>
                        العربية</a>
                @elseif(app()->getLocale() == 'ar')
                    <a class="nav-label" href="{{ url('locale/en') }}"><span class="flag-icon flag-icon-us"> </span>
                        English</a>
                @endif
            </li>
            <li>
                <script type="">
                    function signOutForm() {
                        document.getElementById('signOutForm').submit();

                    }
                </script>
                {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signOutForm']) !!}
                {!! Form::hidden('guard',$guard ?? '') !!}
                {!! Form::close() !!}

                <a href="#" onclick="signOutForm()">
                    <i class="fa fa-sign-out"></i>@lang('student.logout')
                </a>
            </li>
        </ul>

    </nav>
</div>
