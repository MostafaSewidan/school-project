@extends('layouts.app',[
'page_header' => __('تفاصيل المجموعة'),
'page_description' => __('<br><br> المجوعة: ').$record->group_name,
'link' => url('manager/teacher')
])
@section('content')


    <div class="ibox">
        <div class="ibox-title">
            {{-- <div class="col-xs-6"> --}}
            {{-- <a href="{{ url('manager/visor/' . $record->id . '/edit') }}" class="btn btn-xs btn-success"><i --}}
            {{-- class="fa fa-edit"></i></a> --}}
            {{-- <form method="post" action="{{ route('visor.destroy', $record->id) }}" style="display: inline-block" --}}
            {{-- onsubmit="confirm('{{ __('هل أنت متأكد من الحذف ؟') }}')"> --}}
            {{-- @csrf --}}
            {{-- @method('DELETE') --}}
            {{-- <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button> --}}
            {{-- </form> --}}
            {{-- </div> --}}

            <h3 class="">{{ __(' بيانات الاساسية') }}</h3>


            <div class="clearfix"></div>
        </div>
        <div class="ibox-content">
            @push('styles')
                <style>
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }

                </style>
            @endpush
            @include('flash::message')
            <div class="table-responsive">
                <table class="table m-b-xs">
                    <tbody>
                        <tr>

                            <td>
                                {{ __('اسم المادة') }}: <strong>{{ optional($record->level)->name }}</strong>
                            </td>
                            <td>
                                {{ __('الصف الدراسي') }}: <strong>{{ optional($record->subject)->name }}</strong>
                            </td>

                            <td>
                                {{ __('اسم المعلم') }}: <strong>

                                    <a href="{{ url('manager/teacher/' . optional($record->teacher)->id) }}">
                                        {{ optional($record->teacher)->name }}
                                    </a>
                                </strong>
                            </td>
                            <td>
                                {{ __('اسم المشرف') }}: <strong>
                                    @foreach ($record->teacher->users as $user)
                                        <a href="{{ url('manager/visor/' . optional($user)->id) }}">
                                            <span class="label label-success"
                                                style="display: inline-block;margin-bottom: 5px;">{{ optional($user)->name }}</span>
                                        </a>
                                    @endforeach
                                </strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __(' الطلاب ') }}</h3>
                <table class="table m-b-xs">
                    <tbody>

                        <td>
                            {{ __('الطلاب المشارك فيها') }} : <strong>
                                @foreach ($record->students as $student)
                                                                    <a href="{{ url('manager/student/' . $student->id) }}">
                                        <span class="label label-success"
                                        style="display: inline-block;margin-bottom: 5px;">{{ $student->name }}</span>
                                    </a>

                                @endforeach

                            </strong>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __(' الجدول الدراسي ') }}</h3>
                <table class="table m-b-xs">
                    <tbody>
                        @foreach ($record->StudySchedules as $schedul)
                            <tr>
                            <td>
                                {{ __('ما سيتم شرحه') }}: <strong>{{ optional($schedul)->name }}</strong>
                            </td>
                            <td>
                                {{ __('زوم') }}: <strong>{{ optional($schedul)->zoom_link }}</strong>
                            </td>
                            <td>
                                {{ __('وقت وتاريخ') }}:
                                <strong>{{ Carbon\Carbon::parse(optional($schedul)->start_time)->setTimezone('+2:00')->format('d/m/Y g:ia') }}</strong>
                            </td>

                        </tr>
                        @endforeach
{{--
                        <td>
                            {{ __('الطلاب المشارك فيها') }} : <strong>
                                @foreach ($record->students as $student)
                                    <span class="label label-success"
                                        style="display: inline-block;margin-bottom: 5px;">{{ $student->name }}</span>
                                @endforeach

                            </strong>
                        </td> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
