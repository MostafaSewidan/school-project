{{-- @include('layouts.partials.validation-errors') --}}

@inject('level',App\Models\Level)
@inject('subject',App\Models\Subject)
@inject('teacher',App\Models\Teacher)
@inject('student',App\Models\Student)

@php
$levels = $level->pluck('name', 'id')->toArray();
/* $subjects = $subject->pluck('name', 'id')->toArray();
$teachers = $teacher->pluck('name', 'id')->toArray();
$students = $student->pluck('name', 'id')->toArray(); */
$duration = [90 => 90, 45 => 45];
@endphp
@include('flash::message')

{!! \Helper\Field::text('name', __('الأسم') . '*') !!}
{!! \Helper\Field::select('level_id', 'المرحلة الدراسية', $levels) !!}
{!! \Helper\Field::select('subject_id', 'المادة', []) !!}
{!! \Helper\Field::select('teacher_id', 'المدرس', []) !!}
{!! \Helper\Field::multiselect('student_list', 'الطلاب', []) !!}
{!! \Helper\Field::select('duration', 'مدة المجموعة بالدقائق', $duration) !!}

{!! \Helper\Field::time('break_time', __('وقت الراحة') . '*') !!}

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#level_id').on('change', function() {
                var level_id = this.value;
                $('#subject_id').empty();
                $('#teacher_id').empty();
                $('#student_list').empty();

                if (level_id != null && level_id != "") {
                    $.ajax({
                        url: "{{ url('api/v1/subjects') }}",
                        type: "get",
                        data: {
                            level_id: level_id,
                            _token: '{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(result) {

                            if (result) {
                                $('#subject_id').empty();
                                $('#subject_id').append(
                                    '<option value="">اختر المادة</option>');
                                $.each(result, function(index, subject) {
                                    console.log(subject.id);
                                    $('#subject_id').append('<option value="' + subject
                                        .id + '">' + subject.name + '</option>');
                                });
                            } else {

                                $("#subject_id").empty();
                                $("#subject_id").trigger('change');
                            }
                        }
                    });
                }

            });

            $('#subject_id').on('change', function() {
                var subject_id = this.value;
                $('#teacher_id').empty();
                $('#student_list').empty();
                if (subject_id != null && subject_id != "") {



                    $.ajax({
                        url: "{{ url('api/v1/teachers') }}",
                        type: "get",
                        data: {
                            subject_id: subject_id,
                            _token: '{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(result) {

                            if (result) {
                                $('#teacher_id').empty();
                                $('#teacher_id').append(
                                    '<option value="">اختر المدرس</option>');
                                $.each(result, function(index, teacher) {
                                    console.log(teacher.id);
                                    $('#teacher_id').append('<option value="' + teacher
                                        .id + '">' + teacher.name + '</option>');
                                });
                            } else {
                                $("#teacher_id").empty();
                                $("#teacher_id").trigger('change');
                            }
                        }
                    });

                    $.ajax({
                        url: "{{ url('api/v1/students') }}",
                        type: "get",
                        data: {
                            subject_id: subject_id,
                            _token: '{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(result) {

                            if (result) {
                                console.log(result);
                                $('#student_list').empty();
                                $('#student_list').append(
                                    '<option value="">اختر الطالب</option>');
                                $.each(result, function(index, teacher) {
                                    console.log(teacher.id);
                                    $('#student_list').append('<option value="' +
                                        teacher
                                        .id + '">' + teacher.name + '</option>');
                                });
                            } else {
                                $("#student_list").empty();
                                $("#student_list").trigger('change');
                            }
                        }
                    });




                }

            });

        });

    </script>
@endpush
