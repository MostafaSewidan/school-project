@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __(' الامتحانات')
])

@section('content')
    <div class="ibox-content">
        @can('إضافة امتحان')
        <div class="pull-left">
            <a href="{{ url('manager/exam/create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{ __('إضافة') }}
            </a>
        </div>
        @endcan

        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if (!empty($records) && count($records) > 0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">#</th>
                            <th class="text-center">{{ __('الأسم') }}</th>
                            <th class="text-center">{{ __('المجموعة') }}</th>
                            @can('تعديل الامتحانات')
                            <th class="text-center">{{ __('تعديل') }}</th>
                            @endcan
                            @can('حذف الامتحان')
                            <th class="text-center">{{ __('حذف') }}</th>
                            @endcan
                        </thead>
                        <tbody>
                            @php $count = 1; @endphp
                            @foreach ($records as $record)
                                <tr id="removable{{ $record->id }}">
                                    <td class="text-center">
                                        {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                    <td class="text-center">{{ $record->name }}</td>
                                    <td class="text-center">{{ optional($record->group)->group_name }}</td>

                                    @can('تعديل الامتحانات')
                                    <td class="text-center">
                                        <a href="{{ url('manager/exam/' . $record->id . '/edit') }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                    @endcan

                                    @can('حذف الامتحان')
                                    <td class="text-center">
                                        <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                            data-route="{{ url('manager/exam/' . $record->id) }}" type="button"
                                            class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </td>
                                    @endcan
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>


                @else
                    <div>
                        <h3 class="text-info" style="text-align: center">{{ __(' لا توجد بيانات للعرض ') }}</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
