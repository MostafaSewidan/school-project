{{-- @include('layouts.partials.validation-errors') --}}
@include('flash::message')

@inject('group',App\Models\Group)

@php
$groups = $group
    ->get()
    ->pluck('group_name', 'id')
    ->toArray(); @endphp

{!! \Helper\Field::text('name', __('  الاسم') . '*') !!}
{!! \Helper\Field::select('group_id', 'المجموعة', $groups) !!}
{!! \Helper\Field::fileWithPreview('exam_pdf', __('إرفاق الامتحان كـ pdf')) !!}
