{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')

{!! \Helper\Field::select('overall_evaluation',__('التقيم العام'). '*',[
    'Excellent' => 'ممتاز' ,
    'very_good' => 'جيد جدا' ,
    'good' => 'جيد' ,
    'acceptable' => 'مقبول' ,
    'Bad' => 'سيء' ,
    'very_bad' => 'سيئ جدا' ,
]) !!}


<label>هل واجهتك مشكلة تقنية اليوم؟</label>
<br>
<label for="chkYes">
    <input type="radio" id="chkYes" name="chk" onclick="ShowHideDiv()" />
    نعم
</label>
<label for="chkNo">
    <input type="radio" id="chkNo" name="chk" onclick="ShowHideDiv()" />
    لا
</label>
<div id="dvtext" style="display: none">
    <label>ما هي المشكلة؟</label>
    <input name="technical_problem_note" class="form-control" type="text" id="txtBox" placeholder="ما هي المشكلة؟" />
</div>

<br>
<label>هل قابلتك مشاكل مع الطلاب اليوم؟</label>
<br>
<label for="chkYes">
    <input type="radio" id="chkYes1" name="chk" onclick="ShowHideStudent()" />
    نعم
</label>
<label for="chkNo">
    <input type="radio" id="chkNo1" name="chk" onclick="ShowHideStudent()" />
    لا
</label>
<div id="dvtext1" style="display: none">
    <label>ما هي المشكلة؟</label>
    <input name="student_name" class="form-control" type="text" id="txtBox" placeholder="اسماء الطلاب " />
</div>
<br>

<label>هل قابلتك مشاكل مع أولياء الأمور اليوم ؟</label>
<br>
<label for="chkYes">
    <input type="radio" id="chkYes2" name="chk" onclick="ShowHideParent()" />
    نعم
</label>
<label for="chkNo">
    <input type="radio" id="chkNo2" name="chk" onclick="ShowHideParent()" />
    لا
</label>
<div id="dvtext2" style="display: none">
    <label>ما هي المشكلة؟</label>
    <input name="parent_name" class="form-control" type="text" id="txtBox" placeholder="اسماء اولياء الامور " />
</div>

{!! \Helper\Field::textarea('notes','الملاحظات') !!}

@push('scripts')

    <script>
        function ShowHideDiv()
        {
            var chkYes = document.getElementById("chkYes");
            var dvtext = document.getElementById("dvtext");
            dvtext.style.display = chkYes.checked ? "block" : "none";
        }

        function ShowHideStudent()
        {
            var chkYes = document.getElementById("chkYes1");
            var dvtext = document.getElementById("dvtext1");
            dvtext.style.display = chkYes.checked ? "block" : "none";
        }

        function ShowHideParent()
        {
            var chkYes = document.getElementById("chkYes2");
            var dvtext = document.getElementById("dvtext2");
            dvtext.style.display = chkYes.checked ? "block" : "none";
        }
    </script>
    @endpush

