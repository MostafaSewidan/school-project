@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __('التقارير')
])

@section('content')
    <div class="ibox-content">
        @can('إضافة التقرير')
        <div>
            <a href="{{ url('manager/reports/create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{ __('إضافة') }}
            </a>
        </div>
        @endcan

</div>


        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if (!empty($records) && count($records) > 0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">#</th>
                            <th class="text-center">{{ __('التقيم العام') }}</th>
                            <th class="text-center">{{ __('المشكلة التقنية') }}</th>
                            <th class="text-center">{{ __('اسماء الطلاب') }}</th>
                            <th class="text-center">{{ __('اسماء اولياء الامور') }}</th>
                            <th class="text-center">{{ __('الملاحظات') }}</th>
                            @can('تعديل التقرير')
                            <th class="text-center">{{ __('تعديل') }}</th>
                            @endcan

                            @can('حذف التقرير')
                            <th class="text-center">{{ __('حذف') }}</th>
                            @endcan
                        </thead>
                        <tbody>
                            @php $count = 1; @endphp
                            @foreach ($records as $record)
                                <tr id="removable{{ $record->id }}">
                                    <td class="text-center">

                                        {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                    <td class="text-center">{{ $record->report_statues }}</td>
                                    <td class="text-center">{{ $record->technical_problem_note ?? 'N\A'  }}</td>
                                    <td class="text-center">{{ $record->student_name ?? 'N\A' }}</td>
                                    <td class="text-center">{{ $record->parent_name ?? 'N\A' }}</td>
                                    <td class="text-center">{{ $record->notes ?? 'N\A'}}</td>


                                    @can('تعديل التقرير')
                                        <td class="text-center">
                                            <a href="{{ url('manager/reports/' . $record->id . '/edit') }}"
                                                class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                        </td>
                                    @endcan

                                    @can('حذف التقرير')
                                        <td class="text-center">
                                            <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                                data-route="{{ url('manager/reports/' . $record->id) }}" type="button"
                                                class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </td>
                                    @endcan
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>


                @else
                    <div>
                        <h3 class="text-info" style="text-align: center">{{ __(' لا توجد بيانات للعرض ') }}</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
