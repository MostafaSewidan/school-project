@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('تعديل المشرف')
                                ])

@section('content')
<div class="ibox">
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>['User\UserController@update',$record->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files' => true

                            ])!!}
    <div class="ibox-content">
        @include('manager.visors.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">{{__('حفظ')}}</button>
    </div>
    {!! Form::close()!!}
</div>
@stop

