{{-- @include('layouts.partials.validation-errors') --}}


@include('flash::message')
@inject('role',\Spatie\Permission\Models\Role)
@inject('user',App\User)
@php
    $roles = $role->pluck('name','id')->toArray() ;
    $users = $user->pluck('name', 'id')->toArray();

@endphp
{!! \Helper\Field::text('name', __('الأسم') . '*') !!}
{!! \App\MyHelper\Field::email('email', __('البريد الالكتروني')) !!}
{!! \App\MyHelper\Field::password('password', __('كلمة المرور')) !!}
{!! \App\MyHelper\Field::password('password_confirmation', __('تاكيد كلمة المرور')) !!}
{!! \Helper\Field::text('phone', __('رقم الهاتف') . '*') !!}
{!! \Helper\Field::datePicker('d_o_b', __('تاريخ الميلاد')) !!}
{!! \Helper\Field::text('address', __('العنوان') . '*') !!}
{!! \Helper\Field::select('user_list', 'المشرف', $users) !!}
{!! \Helper\Field::select('role','الرتب',$roles) !!}
{!! \Helper\Field::datePicker('contract_starting_date', __('تاريخ بداية العقد')) !!}
{!! \Helper\Field::datePicker('contract_ending_date', __('تاريخ انتهاء العقد')) !!}
{!! \Helper\Field::fileWithPreview('contract', __('إرفاق العقد كـ pdf')) !!}
{!! \Helper\Field::fileWithPreview('national_id', __('إرفاق صورة البطاقة كـ pdf')) !!}
{!! \Helper\Field::fileWithPreview('criminal_statement', __('إرفاق الفيش والتشبيه كـ pdf')) !!}
