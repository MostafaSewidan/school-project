{{-- @include('layouts.partials.validation-errors') --}}
@include('flash::message')

@inject('group',App\Models\Group)

@php
$groups = $group
    ->get()
    ->pluck('group_name', 'id')
    ->toArray();
@endphp


@if (!isset($edit))

    {!! \Helper\Field::select('group_id', 'المجموعة', $groups) !!}

    <div class="form-group">
        <label for="to">الوقت و التاريخ</label>
        <input type="datetime-local" class="form-control" name="datetime" autocomplete="off" required>
    </div>
@endif
{!! \Helper\Field::text('zoom_link', __('رابط زوم الثابت') . '*') !!}
{!! \Helper\Field::text('description', __('ما سوف يتم شرحه') . '*') !!}
{!! \Helper\Field::text('homework', __('الواجب نصي') . '*') !!}
{!! \Helper\Field::fileWithPreview('homework_pdf', __('إرفاق الواجب كـ pdf')) !!}
