@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __(' الجدول الدراسى')
])

@section('content')
    <div class="ibox-content">
        @can('إضافة الجدول الدراسي')
        <div class="pull-left">
            <a href="{{ url('manager/study-schedule/create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{ __('إضافة') }}
            </a>
        </div>
        @endcan
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if (!empty($records) && count($records) > 0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">#</th>
                            <th class="text-center">{{ __('مجموعة') }}</th>
                            <th class="text-center">{{ __('زوم') }}</th>
                            <th class="text-center">{{ __(' موعد البداية ') }}</th>
                            <th class="text-center">{{ __(' موعد النهاية ') }}</th>
                            <th class="text-center">{{ __('ما سيتم شرحه') }}</th>

                            @can('تعديل الجدول الدراسي')
                            <th class="text-center">{{ __('تعديل') }}</th>
                            @endcan

                            @can('حذف الجدول الدراسي')
                            <th class="text-center">{{ __('حذف') }}</th>
                            @endcan
                        </thead>
                        <tbody>
                            @php $count = 1; @endphp
                            @foreach ($records as $record)
                                <tr id="removable{{ $record->id }}">
                                    <td class="text-center">
                                        {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                    <td class="text-center">{{ $record->group_name }}</td>
                                    <td class="text-center">{{ $record->zoom_link }}</td>
                                    <td class="text-center">
                                        {{ Carbon\Carbon::parse($record->start_time)->setTimezone('+2:00')->format('d/m/Y g:ia') }}
                                    </td>
                                    <td class="text-center">
                                        {{ Carbon\Carbon::parse($record->end_time)->setTimezone('+2:00')->format('d/m/Y g:ia') }}
                                    </td>
                                    <td class="text-center">{{ $record->description }}</td>

                                    @can('تعديل الجدول الدراسي')
                                    <td class="text-center">
                                        <a href="{{ url('manager/study-schedule/' . $record->id . '/edit') }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                    @endcan

                                    @can('حذف الجدول الدراسي')
                                    <td class="text-center">
                                        <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                            data-route="{{ url('manager/study-schedule/' . $record->id) }}" type="button"
                                            class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </td>
                                    @endcan
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>


                @else
                    <div>
                        <h3 class="text-info" style="text-align: center">{{ __(' لا توجد بيانات للعرض ') }}</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
