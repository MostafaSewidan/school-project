{{-- @include('layouts.partials.validation-errors') --}}

@inject('subject',App\Models\Subject)
@inject('user',App\User)
@inject('role',Spatie\Permission\Models\Role);

@php

$subjects = $subject->pluck('name', 'id')->toArray();
$users = $role->findByName('مشرف')->users->pluck('name', 'id')->toArray();
$contract_hours = [80 => 80, 160 => 160];
@endphp
@include('flash::message')

{!! \Helper\Field::text('name', __('الأسم') . '*') !!}
{!! \App\MyHelper\Field::email('email', __('البريد الالكتروني')) !!}
{!! \App\MyHelper\Field::password('password', __('كلمة المرور')) !!}
{!! \App\MyHelper\Field::password('password_confirmation', __('تاكيد كلمة المرور')) !!}
{!! \Helper\Field::text('phone', __('رقم الهاتف') . '*') !!}
{!! \Helper\Field::datePicker('d_o_b', __('تاريخ الميلاد')) !!}
{{-- {!! \Helper\Field::multiselect('subject_list', 'التخصص', $subjects) !!} --}}
{!! \Helper\Field::select('subject_list', 'التخصص', $subjects) !!}
{!! \Helper\Field::multiselect('user_list', 'المشرف', $users) !!}
{!! \Helper\Field::text('address', __('العنوان') . '*') !!}
{!! \Helper\Field::datePicker('contract_starting_date', __('تاريخ بداية العقد')) !!}
{!! \Helper\Field::datePicker('contract_ending_date', __('تاريخ انتهاء العقد')) !!}
{{-- {!! \Helper\Field::number('contract_hours', __('عدد ساعات العقد') . '*') !!} --}}
{!! \Helper\Field::select('contract_hours', 'عدد ساعات العقد', $contract_hours) !!}

{!! \Helper\Field::fileWithPreview('contract', __('إرفاق العقد كـ pdf')) !!}
{!! \Helper\Field::fileWithPreview('national_id', __('إرفاق صورة البطاقة كـ pdf')) !!}
{!! \Helper\Field::fileWithPreview('graduation_certificate', __('إرفاق صورة شهادة التخرج كـ pdf')) !!}
{!! \Helper\Field::fileWithPreview('criminal_statement', __('إرفاق الفيش والتشبيه كـ pdf')) !!}
