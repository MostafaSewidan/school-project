@extends('layouts.app',[
'page_header' => __('تفاصيل المدرس'),
'page_description' => __('<br><br>اسم المدرس: ').$record->name,
'link' => url('manager/teacher')
])
@section('content')

    @php
    $role = json_decode(
        auth()
            ->user()
            ->getRoleNames(),
    )[0];
    @endphp
    <div class="ibox">
        <div class="ibox-title">
            {{-- <div class="col-xs-6"> --}}
            {{-- <a href="{{ url('manager/teacher/' . $record->id . '/edit') }}" class="btn btn-xs btn-success"><i --}}
            {{-- class="fa fa-edit"></i></a> --}}
            {{-- <form method="post" action="{{ route('teacher.destroy', $record->id) }}" style="display: inline-block" --}}
            {{-- onsubmit="confirm('{{ __('هل أنت متأكد من الحذف ؟') }}')"> --}}
            {{-- @csrf --}}
            {{-- @method('DELETE') --}}
            {{-- <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button> --}}
            {{-- </form> --}}
            {{-- </div> --}}

            <h3 class="">{{ __(' بيانات الاساسية') }}</h3>


            <div class="clearfix"></div>
        </div>
        <div class="ibox-content">
            @push('styles')
                <style>
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }

                </style>
            @endpush
            @include('flash::message')
            <div class="table-responsive">
                <table class="table m-b-xs">
                    <tbody>
                        <tr>
                            <td>
                                {{ __('الأسم') }} : <strong>{{ $record->name }}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ الميلاد') }}: <strong>{{ optional($record)->d_o_b }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('بريد') }}: <strong>{{ $record->email }}</strong>
                            </td>
                            <td>
                                {{ __('هاتف') }}: <strong>{{ $record->phone }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('المشرفين') }} : <strong>
                                    @foreach ($record->users as $user)
                                        <a href="{{ url('manager/visor/' . $user->id) }}">
                                            {{ optional($user)->name }}
                                        </a>
                                    @endforeach
                                </strong>
                                @if ($role == 'مشرف رئيسي')
                                    <a href="{{ url('manager/teacher/' . $record->id . '/editvisor') }}"
                                        class="btn btn-xs btn-success"><i class="fa fa-edit"> تعديل المشرفين </i></a>
                                @endif
                            </td>
                            <td>
                                {{ __('التخصص') }} : <strong>
                                    @foreach ($record->subjects as $subject)
                                        <span class="label label-success"
                                            style="display: inline-block;margin-bottom: 5px;">{{ optional($subject)->name }}</span>
                                    @endforeach
                                </strong>
                            </td>
                        <tr>
                            <td>
                                {{ __('العنوان') }}: <strong>{{ $record->address }}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('المجموعات') }}: <strong>
                                    @foreach ($record->groups as $group)
                                        <a href="{{ url('manager/group/' . optional($group)->id) }}">
                                            <span class="label label-success"
                                                style="display: inline-block;margin-bottom: 5px;">{{ optional($group)->group_name }}</span>
                                        </a>
                                    @endforeach

                                </strong>
                            </td>
                        </tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __('تفاصيل العقد') }}</h3>
                <table class="table m-b-xs">
                    <tbody>

                        <tr>
                            <td>
                                {{ __('حاله العقد') }} :
                                <strong>{{ $record->current_status }}</strong>
                            </td>
                            <td>
                                {{ __(' الساعات الاضافية ') }} :
                                <strong>{{ $record->available_hours }}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('تاريخ البداية') }} :
                                <strong>{{ $record->contract_starting_date }}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ النهاية') }} :
                                <strong>{{ $record->contract_ending_date }}</strong>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                {{ __(' عدد ساعات العقد ') }} :
                                <strong>{{ $record->contract_hours }}</strong>
                            </td>
                            <td>
                                {{ __(' عدد ساعات الحالي ') }} :
                                <strong>{{ $record->current_hours }}</strong>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __(' المرفقات') }}</h3>
                <table class="table m-b-xs">
                    <tbody>

                        <tr>
                            <td>
                                {{ __(' صورة البطاقة') }} :
                            </td>
                            <td>
                                <a target="_blank" href="{{ $record->national_id }}"> عرض</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __(' الفيش والتشبيه') }} :
                            </td>
                            <td>
                                <a target="_blank" href="{{ $record->criminal_statement }}"> عرض</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('   العقد ') }} :
                            </td>
                            <td>
                                <a target="_blank" href="{{ $record->contract }}"> عرض</a>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('  شهادة التخرج  ') }} :
                            </td>
                            <td>
                                <a target="_blank" href="{{ $record->graduation_certificate }}"> عرض</a>

                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
