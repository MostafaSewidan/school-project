{{-- @include('layouts.partials.validation-errors') --}}

@inject('subject',App\Models\Subject)
@inject('user',App\User)
@inject('role',Spatie\Permission\Models\Role);

@php

$subjects = $subject->pluck('name', 'id')->toArray();
$users = $role
    ->findByName('مشرف')
    ->users->pluck('name', 'id')
    ->toArray();
$contract_hours = [80 => 80, 160 => 160];
@endphp
@include('flash::message')

{!! \Helper\Field::multiselect('user_list', 'المشرف', $users) !!}
