@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __('تعديل طالب')
])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record, [
    'action' => ['User\StudentController@update', $record->id],
    'id' => 'myForm',
    'role' => 'form',
    'method' => 'PUT',
    'files' => true,
]) !!}
        <div class="ibox-content">
            @include('manager.students.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop
