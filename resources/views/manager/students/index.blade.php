@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __('الطلاب')
])

@section('content')
    <div class="ibox-content">
        @can('إضافة طالب')
            <div class="pull-left">
                <a href="{{ url('manager/student/create') }}" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i> {{ __('إضافة') }}
                </a>
            </div>
        @endcan

        <div>

            <button type="button" class="btn  btn-primary btn-sm" data-toggle="modal" data-target="#myModal"
                style="margin-right: 10px">
                <i class="fa fa-search"></i>
                بحث متقدم
            </button>
            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceInRight text-right">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span
                                    class="sr-only">{{ __('إغلاق') }}</span></button>
                            <h4 class="modal-title">{{ __('بحث') }}</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open([
    'method' => 'GET',
    'id' => 'myForm',
]) !!}
                            <div class="row">
                                <div class="col-sm-4">
                                    {!! \Helper\Field::text('name', __('الاسم'), request('name')) !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! \Helper\Field::text('email', __('البريد الالكتروني'), request('email')) !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! \Helper\Field::text('level', __('المرحلة الدراسية'), request('level')) !!}
                                </div>
                                <div class="clearfix"></div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                            <button type="button" class="btn btn-warning" id="reset_form"><i class="fa fa-recycle"></i>
                                {{ __('تفريغ الحقول') }}</button>
                            <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('إغلاق') }}</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <br>
    @include('layouts.partials.results-count')
    <div class="ibox-content">

        @include('flash::message')
        <div class="table-responsive">
            @if (!empty($records) && count($records) > 0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                        <th class="text-center">#</th>
                        <th class="text-center">{{ __('الأسم') }}</th>
                        <th class="text-center">{{ __('الصف') }}</th>
                        <th class="text-center">{{ __('الهاتف') }}</th>
                        <th class="text-center">{{ __('بريد') }}</th>
                        <th class="text-center">{{ __('المرحلة الدراسية') }}</th>
                        <th class="text-center">{{ __(' اخر ظهور') }}</th>
                        @can('تعديل الطالب')
                            <th class="text-center">{{ __('تعديل') }}</th>
                        @endcan
                        @can('حذف الطالب')
                            <th class="text-center">{{ __('حذف') }}</th>
                        @endcan
                        @can('حظر الطالب')
                            <th class="text-center">{{ __('تفعيل - حظر') }}</th>
                        @endcan
                    </thead>
                    <tbody>
                        @php $count = 1; @endphp
                        @foreach ($records as $record)
                            <tr id="removable{{ $record->id }}">
                                <td class="text-center">
                                    {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                <td class="text-center">
                                    <a href="{{ url('manager/student/' . $record->id) }}">
                                        {{ $record->name }}
                                    </a>
                                </td>
                                <td class="text-center">{{ optional($record->level)->name }}</td>
                                <td class="text-center">{{ $record->phone }}</td>
                                <td class="text-center">{{ $record->email }}</td>
                                <td class="text-center">{{ optional($record->level)->name }}</td>
                                <td class="text-center">
                                    {{-- {{ $record->last_active }} --}}
                                    {{ Carbon\Carbon::parse($record->last_active)->setTimezone('+2:00')->format('d/m/Y g:ia') }}

                                </td>
                                @can('تعديل الطالب')
                                    <td class="text-center">
                                        <a href="{{ url('manager/student/' . $record->id . '/edit') }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                @endcan

                                @can('حذف الطالب')
                                    <td class="text-center">
                                        <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                            data-route="{{ url('manager/student/' . $record->id) }}" type="button"
                                            class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </td>
                                @endcan

                                @can('حظر الطالب')
                                    <td class="text-center">
                                        {!! \App\MyHelper\Helper::toggleBooleanView($record, url('manager/student/toggle-boolean/' . $record->id . '/is_active'), 'is_active') !!}
                                    </td>
                                @endcan
                            </tr>

                        @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $records->render() !!}
                </div>


            @else
                <div>
                    <h3 class="text-info" style="text-align: center">{{ __(' لا توجد بيانات للعرض ') }}</h3>
                </div>
            @endif
        </div>
    </div>
    </div>
@endsection
