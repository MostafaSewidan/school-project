@extends('layouts.app',[
'page_header' => __('تفاصيل الطالب'),
'page_description' => __('<br><br>اسم الطالب: ').$record->name,
'link' => url('manager/teacher')
])
@section('content')
    @php
    $role = json_decode(
        auth()
            ->user()
            ->getRoleNames(),
    )[0];
    @endphp

    <div class="ibox">
        <div class="ibox-title">
            {{-- <div class="col-xs-6"> --}}
            {{-- <a href="{{ url('manager/visor/' . $record->id . '/edit') }}" class="btn btn-xs btn-success"><i --}}
            {{-- class="fa fa-edit"></i></a> --}}
            {{-- <form method="post" action="{{ route('visor.destroy', $record->id) }}" style="display: inline-block" --}}
            {{-- onsubmit="confirm('{{ __('هل أنت متأكد من الحذف ؟') }}')"> --}}
            {{-- @csrf --}}
            {{-- @method('DELETE') --}}
            {{-- <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button> --}}
            {{-- </form> --}}
            {{-- </div> --}}

            <h3 class="">{{ __(' بيانات الاساسية') }}</h3>


            <div class="clearfix"></div>
        </div>
        <div class="ibox-content">
            @push('styles')
                <style>
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }

                </style>
            @endpush
            @include('flash::message')
            <div class="table-responsive">
                <table class="table m-b-xs">
                    <tbody>
                        <tr>
                            <td>
                                {{ __('الأسم') }} : <strong>{{ $record->name }}</strong>
                            </td>
                            <td>
                                {{ __('الصف الدراسي') }}: <strong>{{ optional($record->level)->name }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('بريد') }}: <strong>{{ $record->email }}</strong>
                            </td>
                            <td>
                                {{ __('هاتف') }}: <strong>{{ $record->phone }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('الحالة') }}: <strong>{{ $record->student_status }}</strong>
                            </td>
                            <td>
                                {{ __('اخر ظهور') }}: <strong>{{ $record->last_active }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('العنوان') }}: <strong>{{ $record->address }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __(' ملحوظات') }}: <strong>{{ $record->notes }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('المشرفين') }} : <strong>
                                    @foreach ($record->users as $user)
                                        <a href="{{ url('manager/visor/' . $user->id) }}">
                                            {{ optional($user)->name }}
                                        </a>
                                    @endforeach
                                </strong>
                                @if ($role == 'مشرف رئيسي')
                                    <a href="{{ url('manager/student/' . $record->id . '/edit') }}"
                                        class="btn btn-xs btn-success"><i class="fa fa-edit"> تعديل المشرفين </i></a>
                                @endif
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __(' المجموعات ') }}</h3>
                <table class="table m-b-xs">
                    <tbody>

                        <td>
                            {{ __('المجموعات المشارك فيها') }} : <strong>
                                @foreach ($record->groups as $group)
                                    <span class="label label-success"
                                        style="display: inline-block;margin-bottom: 5px;">{{ $group->group_name }}</span>
                                @endforeach

                            </strong>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
