{{-- @include('layouts.partials.validation-errors') --}}
@inject('level',App\Models\Level)

@php
$levels = $level->pluck('name', 'id')->toArray();

@endphp
@include('flash::message')

{!! \Helper\Field::text('name', __('الأسم') . '*') !!}
{!! \App\MyHelper\Field::email('email' , __('البريد الالكتروني')) !!}
{!! \App\MyHelper\Field::password('password' , __('كلمة المرور')) !!}
{!! \App\MyHelper\Field::password('password_confirmation' , __('تاكيد كلمة المرور')) !!}
{!! \Helper\Field::select('level_id', 'المرحلة الدراسية', $levels) !!}
