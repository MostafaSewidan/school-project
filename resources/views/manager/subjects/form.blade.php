{{-- @include('layouts.partials.validation-errors') --}}

@inject('level',App\Models\Level )

@php
$levels = $level->pluck('name', 'id')->toArray();

@endphp
@include('flash::message')

{!! \Helper\Field::text('name', __('الأسم') . '*') !!}
{!! \Helper\Field::multiselect('level_list', 'المستوي', $levels) !!}
