@extends('layouts.teacher',[
'page_header' => app('settings')->site_name,
'page_description' => __(' الجدول الدراسى')
])

@section('content')
    <div class="ibox-content">
        <div class="pull-left">
            <a href="{{ url('teacher/study-schedule/create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{ __('إضافة') }}
            </a>
        </div>
        <div >

{{--            <button type="button" class="btn  btn-primary hidden-print  btn-sm" data-toggle="modal" data-target="#myModal">--}}
{{--                <i class="fa fa-search"></i>--}}
{{--                بحث متقدم--}}
{{--            </button>--}}
{{--            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">--}}
{{--                <div class="modal-dialog modal-lg">--}}
{{--                    <div class="modal-content animated bounceInRight text-right">--}}
{{--                        <div class="modal-header">--}}
{{--                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span--}}
{{--                                    class="sr-only">{{__('إغلاق')}}</span></button>--}}
{{--                            <h4 class="modal-title">{{__('بحث')}}</h4>--}}
{{--                        </div>--}}
{{--                        <div class="modal-body">--}}
{{--                            {!! Form::open([--}}
{{--                                'method' => 'GET',--}}
{{--                                'id' => 'myForm'--}}
{{--                            ]) !!}--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-sm-4">--}}
{{--                                    {!! \Helper\Field::text('group_name' ,__('المجموعة'),request('group_name')) !!}--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-4">--}}
{{--                                    {!! \Helper\Field::text('zoom' ,__('زوم'),request('zoom')) !!}--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-4">--}}
{{--                                    {!! \Helper\Field::datePicker('datetime' ,__('الوقت والتاريخ'),request('datetime')) !!}--}}
{{--                                </div>--}}
{{--                                <div class="clearfix"></div>--}}


{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="modal-footer">--}}
{{--                            <button class="btn btn-primary"><i class="fa fa-search"></i> {{__('بحث')}}</button>--}}
{{--                            <button type="button" class="btn btn-warning" id="reset_form"><i--}}
{{--                                    class="fa fa-recycle"></i> {{__('تفريغ الحقول')}}</button>--}}
{{--                            <button type="button" class="btn btn-white"--}}
{{--                                    data-dismiss="modal">{{__('إغلاق')}}</button>--}}
{{--                        </div>--}}

{{--                        {!! Form::close() !!}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if (!empty($records) && count($records) > 0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">#</th>
                            <th class="text-center">{{ __('مجموعة') }}</th>
                            <th class="text-center">{{ __('زوم') }}</th>
                            <th class="text-center">{{ __('الوقت و التاريخ') }}</th>
                            <th class="text-center">{{ __('ما سيتم شرحه') }}</th>
                            <th class="text-center">{{ __('تعديل') }}</th>
                            <th class="text-center">{{ __('حذف') }}</th>
                        </thead>
                        <tbody>
                            @php $count = 1; @endphp
                            @foreach ($records as $record)
                                <tr id="removable{{ $record->id }}">
                                    <td class="text-center">
                                        {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                    <td class="text-center">{{ $record->group_name }}</td>
                                    <td class="text-center">{{ $record->zoom_link }}</td>
                                    <td class="text-center">{{ $record->datetime }}</td>
                                    <td class="text-center">{{ $record->description }}</td>

                                    <td class="text-center">
                                        <a href="{{ url('teacher/study-schedule/' . $record->id . '/edit') }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                            data-route="{{ url('teacher/study-schedule/' . $record->id) }}" type="button"
                                            class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>


                @else
                    <div>
                        <h3 class="text-info" style="text-align: center">{{ __(' لا توجد بيانات للعرض ') }}</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
