<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sonqr</title>
    <!--favicons -->
    <link rel="icon" type="image/png" href="{{asset('inspina/img/logo.png')}}"/>
    <link href="{{asset('inspina/img/logo.png')}}" rel="apple-touch-icon">
    {{--favicons--}}
    <div class="text-center">
        <img src="{{asset('inspina/img/logo.png')}}" style="margin-top: 20px; margin-bottom:auto;" height="200"
             alt="logo">
    </div>

    <link href="{{asset('inspina/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/animate.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/inspina-rtl.css')}}" rel="stylesheet">
</head>

<body class="gray-bg" style="background-color: #00488F">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        {{--<div>--}}
        {{--<h3 class="logo-name" style="font-size: 90px;">  {{app('settings')->site_name}} </h3>--}}
        {{--</div>--}}

        <br>
        {{--<div class="text-center">--}}
        {{--<img src="{{asset('uploads/logo.png')}}" style="margin-bottom: 15px;" height="200" alt="logo">--}}
        {{--</div>--}}
        <h2 style="margin-top: -39px; color: white">Sonqr</h2>
        <p style="color: white">استعادة كلمة المرور</p>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group">
                    <input id="email" type="email" placeholder="البريد الإلكتروني" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">
                    إرسال رابط استعادة كلمة المرور
                </button>
            </div>
        </form>
        {{--        <a href="{{url('register')}}" class="btn btn-success block full-width m-b">تسجيل حساب جديد</a>--}}
        <p class="m-t">
            <small style="color: white">{{app('settings')->site_name}} &copy; {{date('Y')}}</small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{asset('inspina/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('inspina/js/bootstrap.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('inspina/js/inspinia.js')}}"></script>
<script src="{{asset('js/enjz.js')}}"></script>
</body>

</html>
