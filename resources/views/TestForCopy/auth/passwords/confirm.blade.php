<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sonqr</title>
    <!--favicons -->
    <link rel="icon" type="image/png" href="{{asset('inspina/img/logo.png')}}"/>
    <link href="{{asset('inspina/img/logo.png')}}" rel="apple-touch-icon">
    {{--favicons--}}
    <div class="text-center">
        <img src="{{asset('inspina/img/logo.png')}}" style="margin-top: 20px; margin-bottom:auto;" height="200"
             alt="logo">
    </div>

    <link href="{{asset('inspina/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/animate.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/inspina-rtl.css')}}" rel="stylesheet">
</head>

<body class="gray-bg" style="background-color: #00488F">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        {{--<div>--}}
        {{--<h3 class="logo-name" style="font-size: 90px;">  {{app('settings')->site_name}} </h3>--}}
        {{--</div>--}}

        <br>
        {{--<div class="text-center">--}}
        {{--<img src="{{asset('uploads/logo.png')}}" style="margin-bottom: 15px;" height="200" alt="logo">--}}
        {{--</div>--}}
        <h2 style="margin-top: -39px; color: white">Sonqr</h2>
        <p style="color: white">تأكيد كلمة المرور</p>
        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">كلمة المرور</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        تأكيد كلمة المرور
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            هل نسيت كلمة المرور؟
                        </a>
                    @endif
                </div>
            </div>
        </form>
        {{--        <a href="{{url('register')}}" class="btn btn-success block full-width m-b">تسجيل حساب جديد</a>--}}
        <p class="m-t">
            <small style="color: white">{{app('settings')->site_name}} &copy; {{date('Y')}}</small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{asset('inspina/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('inspina/js/bootstrap.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('inspina/js/inspinia.js')}}"></script>
<script src="{{asset('js/enjz.js')}}"></script>
</body>

</html>
