@extends('front.master')
@section('content')
    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">عن سُنقر</h2>
            <div class="about-content">
                @isset($settings['about_app']) {!! $settings['about_app'] !!} @endisset
            </div>
            <div class="row mt-5">
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الرؤية</h3>
                    @isset($settings['app_vision']) {!! $settings['app_vision'] !!} @endisset
                </div>
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الرسالة</h3>
                    @isset($settings['app_message']) {!! $settings['app_message'] !!}  @endisset
                </div>
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الأهداف</h3>
                    @isset($settings['app_mission']) {!! $settings['app_mission'] !!}  @endisset
                </div>
            </div>
        </div>
    </section>
    <!-- about us -->
@stop
