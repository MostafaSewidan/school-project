@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">عروض سُنقر</h3>
            </div>
            <div class="offers">
                <div class="row">
                    @foreach($records as $record)
                    <div class="col-sm-4">
                        <div class="offer-post mb-5">
                            <a href="@isset($settings['app_url']) {{$settings['app_url']}} @endisset" class="offer-link" target="_blank"></a>
                            <div class="offer-cover mb-3">
                                <img class="img-fluid" src="{{asset($record->photo? $record->photo->path : 'uploads/thumbnails/products/product2.png')}}" alt="{{$record->name}}">
                                <span class="offer-badge">عرض خاص</span>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h3 class="text-primary">{{$record->name}}</h3>
                                    <h5 class="text-primary"><i class="fas fa-store-alt"></i>{{optional(optional($record->categories()->first())->store)->name}}</h5>
                                </div>
                                <div class="col">
                                    <h3 class="text-right text-muted"><del class="text-red">{{$record->price}}</del> {{$record->offer_price}} <small>ج.م</small></h3>
                                </div>
                            </div>
                            <p>{{$record->description}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex">
                    <div class="mx-auto">
                        {{$records->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- jobs -->
@stop
