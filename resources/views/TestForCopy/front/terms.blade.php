@extends('front.master')
@section('content')
    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">الشروط والأحكام</h2>
            <div class="about-content">
                @isset($settings['terms']) {!! $settings['terms'] !!} @endisset
            </div>
        </div>
    </section>
    <!-- about us -->
@stop
