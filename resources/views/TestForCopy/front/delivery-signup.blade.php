@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">كُن طيار</h3>
            </div>
            <section class="wizard">
                <ul class="nav nav-tabs flex-nowrap nav-justified" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#step1" class="nav-link active" data-toggle="tab" aria-controls="step1" role="tab"
                           title="تأكيد الموبايل">
                            <i class="fas fa-2x fa-phone"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step2" class="nav-link disabled" data-toggle="tab" aria-controls="step2" role="tab"
                           title="بيانات الطيار">
                            <i class="fas fa-2x fa-user"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step3" class="nav-link disabled" data-toggle="tab" aria-controls="step3" role="tab"
                           title="بيانات وسيلة النقل">
                            <i class="fas fa-2x fa-motorcycle"></i>
                        </a>
                    </li>
                </ul>
                {!! Form::open([
                    'action' => 'FrontController@deliverySignupSubmit',
                    'method' => 'post',
                    'files' => true,
                    'id' => 'job-apply-form'
                ]) !!}
                    <div class="tab-content py-5">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            @include('layouts.partials.validation-errors')
                            @include('flash::message')
                            <h3 class="text-primary">تأكيد رقم الموبايل</h3>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">رقم الموبايل</label>
                                <div class="col-sm-9 text-right">
                                    <input type="text" name="phone" class="form-control" placeholder="رقم الموبايل">
                                    <span class="help-text" style="color: darkred;">* مطلوب</span>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary bg-primary next-step float-right">تأكيد رقم
                                الموبايل
                            </button>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <h3 class="text-primary">بيانات الطيار</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_1" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_1"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="photo" id="file_1"
                                                       onchange="loadFile(event,'output_1')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">الصورة الشخصية
                                                {{--<br>--}}
                                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_2" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_2"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="national_id_photo" id="file_2"
                                                       onchange="loadFile(event,'output_2')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة الرقم القومي
                                                {{--<br>--}}
                                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <input type="text" class="form-control" name="name" placeholder="الاسم كاملاً">
                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                            </div>
                            <div class="form-group text-right">
                                <input type="text" class="form-control" name="user_name" placeholder="اسم المستخدم">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="national_id" placeholder="الرقم القومي">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="الإيميل الشخصي">
                            </div>
                            <div class="form-group text-right">
                                <input type="text" class="form-control datepicker" name="d_o_b" placeholder="تاريخ الميلاد" autocomplete="off">
                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    @inject('governorate','App\Models\Governorate')
                                    <div class="form-group">
                                        {!! Form::select('governorate',$governorate->pluck('name','id')->toArray(),null,[
                                            'class' => 'form-control',
                                            'id' => 'governorate',
                                            'placeholder' => 'اختر المحافظة'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::select('city',[],null,[
                                            'class' => 'form-control',
                                            'id' => 'city',
                                            'placeholder' => 'اختر المدينة'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group text-right">
                                        {!! Form::select('region_id',[],null,[
                                            'class' => 'form-control',
                                            'id' => 'region',
                                            'placeholder' => 'اختر المنطقة'
                                        ]) !!}
                                        {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                                    </div>
                                </div>
                            </div>
                            <ul class="float-right">
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-primary bg-primary btn-info-full next-step">التالي</button>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step3">
                            <h3 class="text-primary">بيانات وسيلة النقل</h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_3" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_3"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="transport_photo" id="file_3"
                                                       onchange="loadFile(event,'output_3')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة وسيلة النقل
                                                {{--<br>--}}
                                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                @inject('vehicle_types','App\Models\VehicleType')
                                <div class="form-group">
                                    {!! Form::select('vehicle_type',$vehicle_types->pluck('name','id')->toArray(),null,[
                                        'class' => 'form-control',
                                        'id' => 'vehicle_type',
                                        'placeholder' => 'نوع وسيلة النقل'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="vehicle_year" class="form-control" placeholder="سنة الصنع">
                            </div>
                            <div class="form-group">
                                <input type="text" name="vehicle_model" class="form-control" placeholder="الموديل">
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="">رخصة وسيلة النقل</label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="license_letters" placeholder="حروف">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="license_numbers" placeholder="أرقام">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_4" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_4"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="license_photo" id="file_4"
                                                       onchange="loadFile(event,'output_4')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة رخصة وسيلة النقل
                                                {{--<br>--}}
                                                {{--<span class="help-text" style="color: darkred;">* مطلوب</span>--}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_5" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_5"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="transport_photo2" id="file_5"
                                                       onchange="loadFile(event,'output_5')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة أخرى لوسيلة النقل</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="accept_privacy" type="checkbox" value="1" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    أقر أني قرأت الشروط والطلبات المذكورة وأن البيانات المرفقة صحيحة
                                </label>
                            </div>
                            <ul class="float-right">
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="submit" class="btn btn-primary bg-primary btn-info-full next-step">
                                        أرسل البيانات للإدارة للتسجيل
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                {!! Form::close() !!}
            </section>
        </div>
    </section>
    <!-- jobs -->
    @push('scripts')
        <script>
            $( function() {
                $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd",
                    changeYear: true
                });
            } );
        </script>
    @endpush
@stop
