@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">انضم إلى شركاء النجاح</h3>
            </div>
            @include('layouts.partials.validation-errors')
            @include('flash::message')
            <section class="wizard">
                <ul class="nav nav-tabs flex-nowrap nav-justified" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#step1" class="nav-link active" data-toggle="tab" aria-controls="step1" role="tab"
                           title="الاسم التجاري">
                            <i class="fas fa-store-alt fa-2x"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step2" class="nav-link disabled" data-toggle="tab" aria-controls="step2" role="tab"
                           title="بيانات المحل">
                            <i class="fas fa-info-circle fa-2x"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step3" class="nav-link disabled" data-toggle="tab" aria-controls="step3" role="tab"
                           title="التوصيل">
                            <i class="fas fa-2x fa-motorcycle"></i>
                        </a>
                    </li>
                </ul>
                {!! Form::open([
                    'action' => 'FrontController@storeSignupSubmit',
                    'method' => 'post',
                    'id' => 'job-apply-form',
                    'files' => true
                ]) !!}
                    <div class="tab-content py-5">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <h3 class="text-primary">الاسم التجاري</h3>
                            <div class="form-group text-right">
                                <input type="text" name="name" class="form-control" placeholder="اسم المحل" required>
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group text-right">
                                <input type="text" name="category_name" class="form-control" placeholder="نشاط المحل" required>
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group text-right">
                                <input type="hidden" name="user_name" class="form-control" value="user{{Str::random(6)}}" placeholder="اسم المستخدم">
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group text-right">
                                <input type="text" name="owner_name" class="form-control" placeholder="اسم المفوض" required>
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group text-right">
                                <input type="text" name="phone" class="form-control" placeholder="التليفون" required>
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="whatsapp" placeholder="واتساب">
                            </div>
                            <button type="button" class="btn btn-primary bg-primary next-step float-right">
                                التالي
                            </button>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <h3 class="text-primary">بيانات المحل</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_1" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_1"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="store_photo1" id="file_1"
                                                       onchange="loadFile(event,'output_1')" required>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة المحل
                                                <br>
                                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_2" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_2"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="store_photo2" id="file_2"
                                                       onchange="loadFile(event,'output_2')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة أخرى للمحل</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_3" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_3"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="menu_photo1" id="file_3"
                                                       onchange="loadFile(event,'output_3')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة للمنيو
                                                <br>
                                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10 offset-sm-1">
                                            <div class="upload-image">
                                                <p><label for="file_4" style="cursor: pointer;">
                                                        <img src="{{asset('front/images/camera.png')}}" id="output_4"/>
                                                    </label></p>
                                                <input type="file" accept="image/*" name="menu_photo2" id="file_4"
                                                       onchange="loadFile(event,'output_4')">
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="text-muted text-center">صورة أخرى للمنيو</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                {!! Form::select('category',[
                                'market' => 'محل',
                                'restaurant' => 'مطعم',
                                'pharmacy' => 'صيدلية',
                                ],null,[
                                    'class' => 'form-control',
                                    'placeholder' => 'نوع المحل',
                                    'required' => true
                                ]) !!}
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="commercial_registry" placeholder="السجل التجاري">
                            </div>
                            <div class="form-group">
                                <input type="number" step="1" class="form-control" name="branches_num" placeholder="عدد الفروع">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="fb_url" placeholder="لينك صفحة الفيس بوك ( اختياري )">
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    @inject('governorate','App\Models\Governorate')
                                    <div class="form-group">
                                        {!! Form::select('governorate',$governorate->pluck('name','id')->toArray(),null,[
                                            'class' => 'form-control',
                                            'id' => 'governorate',
                                            'placeholder' => 'اختر المحافظة'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::select('city',[],null,[
                                            'class' => 'form-control',
                                            'id' => 'city',
                                            'placeholder' => 'اختر المدينة'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group text-right">
                                        {!! Form::select('region_id',[],null,[
                                            'class' => 'form-control',
                                            'id' => 'region',
                                            'placeholder' => 'اختر المنطقة',
                                            'required' => true
                                        ]) !!}
                                        <span class="help-text" style="color: darkred;">* مطلوب</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <input type="text" class="form-control" name="full_address" placeholder="العنوان تفصيلي" required>
                                <span class="help-text" style="color: darkred;">* مطلوب</span>
                            </div>
                            <ul class="float-right">
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-primary bg-primary btn-info-full next-step">التالي</button>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step3">
                            <h3 class="text-primary">التوصيل</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">عندك خدمة توصيل؟؟</label>
                                    </div>
                                    <div class="col-sm-2 col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" name="have_delivery" type="radio" value="1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                نعم
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" name="have_delivery" type="radio" value="0" checked>
                                            <label class="form-check-label" for="defaultCheck1">
                                                لا
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">هل بتتعامل مع تطبيقات توصيل تانية؟؟</label>
                                    </div>
                                    <div class="col-sm-2 col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" name="other_app_contracted" type="radio" value="1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                نعم
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-6">
                                        <div class="form-check">
                                            <input class="form-check-input" name="other_app_contracted" type="radio" value="0" checked>
                                            <label class="form-check-label" for="defaultCheck1">
                                                لا
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="accept_privacy" type="checkbox" value="1" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    أقر أني قرأت الشروط والطلبات المذكورة وأن البيانات المرفقة صحيحة
                                </label>
                            </div>
                            <ul class="float-right">
                                <li class="list-inline-item">
                                    <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="submit" class="btn btn-primary bg-primary btn-info-full next-step">
                                        أرسل البيانات للإدارة للتسجيل
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                {!! Form::close() !!}
            </section>
        </div>
    </section>
    <!-- jobs -->
@stop
