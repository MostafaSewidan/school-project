@extends('front.master')
@section('content')
    <!-- main carousel -->
    <section class="carousel-wrapper">
        @inject('sliderModel','App\Models\Slider')
        <div class="owl-carousel main-carousel">
            @foreach($sliderModel->take(10)->get() as $slider)
                <div>
                    <img src="{{asset($slider->image)}}" alt="{{$slider->title}}">
                </div>
            @endforeach
        </div>
        <div class="download-apps">
            <h2 class="text-center mb-5">حمل التطبيق</h2>
            <div class="row">
                <div class="col-sm-6 text-center">
                    <a href="@isset($settings['app_url']) {{$settings['app_url']}} @endisset">
                        <img src="{{asset('front/images/google-play.png')}}" alt="">
                    </a>
                </div>
                <div class="col-sm-6 text-center">
                    <a href="@isset($settings['apple_url']){{$settings['apple_url']}}@endisset">
                        <img src="{{asset('front/images/app-store.png')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- main carousel -->

    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">عن سُنقر</h2>
            <div class="about-content">
                @isset($settings['about_app']) {!! $settings['about_app'] !!} @endisset
            </div>
        </div>
    </section>
    <!-- about us -->

    <!-- statistics -->
    <section class="statistics pt-4">
        <div class="container">
            <div class="row">
                @inject('governorate','App\Models\Governorate')
                @inject('client','App\Models\Client')
                @inject('order','App\Models\Order')
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/governorates.jpg')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">{{\App\MyHelper\Helper::settingValue('gov_number', 0)}}</h1>
                    <h2 class="text-primary">محافظات</h2>
                </div>
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/users.jpg')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">{{\App\MyHelper\Helper::settingValue('client_number', 0)}}</h1>
                    <h2 class="text-primary">مستخدم في مصر</h2>
                </div>
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/orders.jpg')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">{{\App\MyHelper\Helper::settingValue('delivered_orders', 0)}}</h1>
                    <h2 class="text-primary">طلب ناجح</h2>
                </div>
            </div>
        </div>
    </section>
    <!-- statistics -->

    <!-- categories -->
    <section class="categories pt-5 pb-5">
        <div class="container">
            <h3 class="text-primary">تصنيفاتنا</h3>
            @inject('categoryModel','App\Models\Category')
            <div class="owl-carousel categories-carousel">
                @foreach($categoryModel->homeCategories()->get() as $category)
                    <div class="categories-item">
                        <a href="#"></a>
                        <div class="img-box">
                            <img src="{{asset($category->photo ? $category->photo->path : 'front/images/orders.jpg')}}"
                                 alt="{{$category->name}}">
                        </div>
                        <h4>{{$category->name}}</h4>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- categories -->

    <!-- join us -->
    <section class="join-us pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="join-us-box">
                                <h3 class="text-primary"><img src="{{asset('front/images/join-us.png')}}" alt=""> انضم لينا كمحل</h3>
                                <p>ابدأ مرحلة جديدة من النجاح مع سُنقر ، كن من ضمن المميزين ، هدفنا نقل نشاطك التجاري إلى
                                    مرحلة مختلفة من التنافس</p>
                                <h3 class="text-primary text-center">انضم الان</h3>
                                <a href="{{url('become-a-partner')}}" class="join-btn shadow"><i
                                            class="fas fa-2x fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-3">
                    <div class="row">
                        <div class="col-sm-10 offset-sm-2">
                            <div class="join-us-box">
                                <h3 class="text-primary"><img src="{{asset('front/images/join-us.png')}}" alt=""> كُن
                                    طيار</h3>
                                <p>كن طيار وابدأ مرحلة جديدة من تحقيق الذات والأرباح معنا ، الأمر لا يحتاج إلى كثير من
                                    الجهد المبذول </p>
                                <h3 class="text-primary text-center">انضم الان</h3>
                                <a href="{{url('become-a-delivery')}}" class="join-btn shadow"><i
                                            class="fas fa-2x fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- join us -->


    <section class="promo-code pt-4 pb-4">
        <div class="container">
            <h4 class="text-primary text-center"><i class="fas fa-gift"></i> تأكد من كود الهدية من سُنقر ؟</h4>
            <div class="row">
                <div class="col-sm-6 offset-3">
                    <div class="form-group">
                        <input type="text" class="form-control" id="promo" placeholder="اكتب الكود هنا">
                        <span class="help-block" style="color: darkred">عند استخدامك للهدية لا يمكن اعادة استخدامها مرة اخرى</span>
                    </div>
                    <div class="form-group text-center">
                        <button id="promo-check" class="btn btn-primary bg-primary btn-lg pr-5 pl-5" type="submit">تأكد
                            من الكود
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- video -->
    <section class="video-intro pt-5 pb-5 bg-secondary">
        <div class="container">
            {{--<h3 class="text-primary text-center mb-5">--}}
            {{--شرح استخدام التطبيق--}}
            {{--</h3>--}}
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item"
                                src="@isset($settings['home_video']) {{$settings['home_video']}} @endisset"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video -->

    @push('scripts')
        <script>
            $("#promo-check").click(function (e) {
                var promo = $("#promo").val();
                if (!promo) {
                    swal({
                        title: "فشل!",
                        text: "من فضلك اكتب الكود",
                        type: "error",
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else {
                    $.ajax({
                        url: "{{url('promo-check?promo=')}}" + promo,
                        type: "get",
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 1) {
                                Swal.fire({
                                    title: 'التحقق من الهدايا',
                                    text: data.message,
                                    icon: 'success',
                                    showCancelButton: true,
                                    confirmButtonColor: '#00488f',
                                    cancelButtonColor: '#fddd11',
                                    confirmButtonText: 'استخدم الهدية',
                                    cancelButtonText: 'إلغاء'
                                }).then(function (result) {
                                    if (result.value) {
                                        $.ajax({
                                            url: "{{url('promo-check?promo=')}}" + promo + "&use_gift=1",
                                            type: "get",
                                            dataType: 'json',
                                            success: function (data) {
                                                swal({
                                                    title: "تم",
                                                    text: data.message,
                                                    type: "success",
                                                    showConfirmButton: true,
                                                    timer: 0
                                                });
                                            }
                                        });
                                    }
                                });

                            } else if (data.status == 0) {
                                swal({
                                    title: "فشل!",
                                    text: data.message,
                                    type: "error",
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            } else {

                            }
                        }
                    });
                }
            });
        </script>
    @endpush
@stop
