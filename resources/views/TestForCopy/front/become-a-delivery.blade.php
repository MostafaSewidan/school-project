@extends('front.master')
@section('content')
    <!-- become a delivery terms -->
    <section class="become-a-delivery pt-5 pb-5">
        <div class="container">
            <h3 class="text-primary">كُن طيار <small class="text-muted">تعليمات سُنقر</small></h3>
            <h6 class="text-muted">خطوات بسيطة علشان تكون طيار مع سنقر</h6>

            <ul>
                <li>أن لا يقل سن المتقدم عن 20 عام وألا يزيد عن 50 عام</li>
                <li>لبس لائق ، أسلوب راقي ولائق في التعامل</li>
                <li>صورة حديثة لوسيلة النقل المستخدمة</li>
                <li>صورة سيلفي واضحة</li>
            </ul>

            <div class="form-group text-center mt-5">
                <a href="{{url('delivery-signup')}}" class="btn btn-success btn-lg">موافق يلا ندخل البيانات</a>
            </div>
        </div>
    </section>
    <!-- become a delivery terms -->
@stop
