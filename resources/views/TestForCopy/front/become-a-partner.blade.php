@extends('front.master')
@section('content')

    <!-- become a partner -->
    <div class="jumbotron text-center bg-secondary become-a-partner">
        <h3 class="text-primary">خليك واحد من رواد السوق</h3>
    </div>
    <!-- become a partner -->


    <!-- categories -->
    <section class="categoriespb-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3 col-8 offset-2">
                    <h3 class="text-primary shadow mb-5 categories-head">تصنيفاتنا الرئيسية</h3>
                </div>
            </div>
            @inject('categoryModel','App\Models\Category')
            <div class="owl-carousel categories-carousel">
                @foreach($categoryModel->homeCategories()->get() as $category)
                    <div class="categories-item">
                        <a href="#"></a>
                        <div class="img-box">
                            <img src="{{asset($category->photo ? $category->photo->path : 'front/images/orders.jpg')}}"
                                 alt="{{$category->name}}">
                        </div>
                        <h4>{{$category->name}}</h4>
                    </div>
                @endforeach
            </div>
            <div class="form-group text-center mt-5">
                <a href="{{url('store-signup')}}" class="btn btn-success btn-lg">انضم إلينا كشريك نجاح</a>
            </div>
        </div>
    </section>
    <!-- categories -->

@stop
