<!doctype html>
<html lang="en" dir="rtl">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="{{asset('front/images/logo-500.png')}}"/>
    <link rel="stylesheet" href="{{asset('front/plugins/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/plugins/owlcarousel/assets/owl.theme.default.min.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
          integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="{{asset('front/css/hover-min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/wizard.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('inspina/js/plugins/sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    <script>
        window.laravelUrl = "{{url('/')}}";
    </script>

    <title> @isset($settings['site_name']) {{$settings['site_name']}} @endisset</title>
</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v7.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="112609410424887"
     theme_color="#ff7e29"
     logged_in_greeting="أهلا وسهلا بك فى سُنقر . ازاي نقدر نساعدك؟"
     logged_out_greeting="أهلا وسهلا بك فى سُنقر . ازاي نقدر نساعدك؟">
</div>
<!-- Top Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary header-navbar">
    <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{asset('front/images/logo.png')}}" alt="سُنقر" style="height: 70px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item @if(request()->is('/')) active @endif">
                <a class="nav-link" href="{{url('/')}}">الرئيسية <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item @if(request()->is('about')) active @endif">
                <a class="nav-link" href="{{url('about')}}">معلومات عنا</a>
            </li>
            <li class="nav-item @if(request()->is('offers')) active @endif">
                <a class="nav-link" href="{{url('offers')}}">العروض</a>
            </li>
            <li class="nav-item @if(request()->is('jobs')) active @endif">
                <a class="nav-link" href="{{url('jobs')}}">الوظائف</a>
            </li>
            <li class="nav-item @if(request()->is('contact')) active @endif">
                <a class="nav-link" href="{{url('contact')}}">ابقى على تواصل</a>
            </li>
        </ul>
    </div>
</nav>
<!-- Top Navbar -->

@yield('content')

<!-- footer -->
<footer class="footer bg-primary pt-3 pb-3" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mb-3">
                <ul class="nav flex-column footer-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('about')}}">معلومات عنا</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('jobs')}}">الوظائف</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('offers')}}">العروض</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('terms')}}">الشروط والأحكام</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('policies')}}">سياسة الخصوصية</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('contact')}}">ابقى علي تواصل</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 mb-3">
                <h3 class="text-center mb-5 text-white">حمل التطبيق</h3>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <a href="@isset($settings['app_url']) {{$settings['app_url']}} @endisset">
                            <img src="{{asset('front/images/google-play.png')}}" alt="" style="width: 200px">
                        </a>
                    </div>
                    <div class="col-sm-12 text-center">
                        <a href="@isset($settings['apple_url']) {{$settings['apple_url']}} @endisset">
                            <img src="{{asset('front/images/app-store.png')}}" alt="" style="width: 200px">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-3">
                <h3 class="text-center mb-5 text-white">تابعونا على </h3>
                <div class="social-links text-center">
                    <a href="@isset($settings['fb_url']) {{$settings['fb_url']}} @endisset" target="_blank"><i class="fab fa-2x fa-facebook"></i></a>
                    <a href="@isset($settings['insta_url']){{$settings['insta_url']}} @endisset" target="_blank"><i class="fab fa-2x fa-instagram"></i></a>
                    <a href="@isset($settings['tw_url']){{$settings['tw_url']}} @endisset" target="_blank"><i class="fab fa-2x fa-twitter-square"></i></a>
                    <a href="@isset($settings['youtube_url']){{$settings['youtube_url']}} @endisset" target="_blank"><i class="fab fa-2x fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
<script src="{{asset('front/js/popper.min.js')}}"></script>
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"
        integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="{{asset('front/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('front/js/wizard.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
<script src="{{asset('front/js/main.js')}}"></script>
<style>
    .swal2-popup {
        font-size: 1.5rem !important;
    }
</style>
<script>

    @if( session()->get('success'))

    swal({
        title: "تم",
        text: '{{session('success')}}',
        type: "success",
        showConfirmButton: false,
        timer: 2000
    });


    @elseif(session()->get('fail'))

    swal({
        title: "خطأ!",
        text: '{{session('fail')}}',
        type: "error",
        showConfirmButton: false,
        timer: 2000
    });

    @endif
</script>
<script>
    /***
     * ajax request
     * ****/
    $("#governorate").change(function () {
        let governorate   = $("#governorate").val();
        let url   = window.laravelUrl+"/api/v1/cities?governorate_id="+governorate;
        $.ajax({
            url     : url,
            type    : 'get',
            dataType:'json',
            success : function(data){
                $('#city').empty();
                let option = '<option value="">اختر المدينة</option>';
                $("#city").append(option);
                $.each(data.data, function( index, city ) {
                    let option = '<option value="'+city.id+'">'+city.name+'</option>';
                    $("#city").append(option);
                });
            }
        });
    });

    $("#city").change(function () {
        let city   = $("#city").val();
        let url   = window.laravelUrl+"/api/v1/regions?city_id="+city;
        $.ajax({
            url     : url,
            type    : 'get',
            dataType:'json',
            success : function(data){
                $('#region').empty();
                let option = '<option value="">اختر المنطقة</option>';
                $("#region").append(option);
                $.each(data.data, function( index, region ) {
                    let option = '<option value="'+region.id+'">'+region.name+'</option>';
                    $("#region").append(option);
                });
            }
        });
    });

</script>
@stack('scripts')
</body>
</html>