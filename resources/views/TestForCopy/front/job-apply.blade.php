@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">تقدم الآن</h3>
            </div>

            @include('layouts.partials.validation-errors')
            @include('flash::message')
            {!! Form::open([
                'action' => ['FrontController@jobApplySubmit',$job->id],
                'method' => 'post',
                'files' => true,
                'id' => 'job-apply-form'
            ]) !!}
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">الاسم الأول</label>
                    <div class="col-sm-10">
                        <input type="text" name="f_name" class="form-control" placeholder="الاسم الاول">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">الاسم الأخير</label>
                    <div class="col-sm-10">
                        <input type="text" name="l_name" class="form-control" placeholder="الاسم الأخير">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">الوظيفة</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{$job->title}}" class="form-control" placeholder="الوظيفة" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">رقم الموبايل</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" placeholder="رقم الموبايل">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">الإيميل</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" placeholder="الإيميل">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">السيرة الذاتية</label>
                    <div class="col-sm-10">
                        <input type="file" name="cv" class="">
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            ( صيغة الملفات المقبولة doc docx pdf )
                        </small>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success btn-block btn-lg mb-3">إرسال</button>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-default btn-gray btn-block btn-lg mb-3" id="job-apply-reset">تفريغ البيانات</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <!-- jobs -->
@stop
