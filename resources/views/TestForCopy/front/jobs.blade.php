@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="text-primary">الوظائف المتاحة <small class="text-muted">يوجد 25 وظيفة متاحة</small></h3>
                    </div>
                    <div class="col-sm-6">
                        {!! Form::open([
                            'method' => 'get'
                        ]) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    @inject('governorate','App\Models\Governorate')
                                    @inject('department','App\Models\Department')
                                    <div class="form-group">
                                        {!! Form::select('department_id',$department->pluck('name','id')->toArray(),request('department_id'),[
                                            'class' => 'form-control',
                                            'placeholder' => 'كل القطاعات',
                                            'onchange' => 'submit()'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::select('governorate_id',$governorate->pluck('name','id')->toArray(),request('governorate_id'),[
                                            'class' => 'form-control',
                                            'placeholder' => 'كل المحافظات',
                                            'onchange' => 'submit()'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="job-posts">
                <div class="row">
                    @inject('jobModel','App\Models\Job')
                    @foreach($jobs as $job)
                    <div class="col-sm-6">
                        <div class="job-post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="text-primary">الوظيفة</label> : <span class="text-muted">{{$job->title}}</span>
                                </div>
                                <div class="col-sm-6">
                                    <label class="text-primary">المحافظة</label> : <span class="text-muted">{{optional($job->governorate)->name}}</span>
                                </div>
                                <div class="col-sm-6">
                                    <label class="text-primary">الخبرة المطلوبة</label> : <span class="text-muted">{{$job->exp}}</span>
                                </div>
                                <div class="col-sm-6">
                                    <label class="text-primary">القطاع</label> : <span class="text-muted">{{optional($job->department)->name}}</span>
                                </div>
                                <div class="col-sm-6">
                                    <label class="text-primary">المؤهل الدراسي</label> : <span class="text-muted">{{$job->education}}</span>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{url('job-apply/'.$job->id)}}" class="btn btn-primary bg-primary">تقدم الآن</a>
                                </div>
                            </div>
                        </div>
                    </div>
                        @if($loop->iteration % 2 == 0)
                            <div class="clearfix"></div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- jobs -->
@stop
