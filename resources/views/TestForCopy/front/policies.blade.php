@extends('front.master')
@section('content')
    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">سياسة الخصوصية</h2>
            <div class="about-content">
                @isset($settings['privacy_policies']) {!! $settings['privacy_policies'] !!} @endisset
            </div>
        </div>
    </section>
    <!-- about us -->
@stop
