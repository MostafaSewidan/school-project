@extends('front.master')
@section('content')
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">ابقى على تواصل</h3>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <p><label class="text-primary">الهاتف</label> : <span class="text-muted">@isset($settings['phone']) {{$settings['phone']}} @endisset</span></p>
                    <p><label class="text-primary">الايميل</label> : <span class="text-muted">@isset($settings['email']) {{$settings['email']}} @endisset</span></p>
                    <p><label class="text-primary">العنوان</label> : <span class="text-muted">@isset($settings['address']) {{$settings['address']}} @endisset</span></p>
                </div>
                <div class="col-sm-8">
                    @include('layouts.partials.validation-errors')
                    @include('flash::message')
                    {!! Form::open([
                        'action' => 'FrontController@contactSubmit',
                        'method' => 'post',
                        'id' => 'job-apply-form'
                    ]) !!}
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">الاسم</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="اسمك الكريم">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">رقم الموبايل</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="phone" placeholder="رقم الموبايل">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">الموضوع</label>
                            <div class="col-sm-10">
                                @inject('contactReason','App\Models\ContactReason')
                                {!! Form::select('contact_reason_id',
                                $contactReason->pluck('name','id')->toArray(),null,[
                                    'class' => 'form-control',
                                    'placeholder' => 'سبب التواصل'
                                ]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">الرسالة</label>
                            <div class="col-sm-10">
                                <textarea name="contact" class="form-control" rows="5" placeholder="الرسالة"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success btn-block btn-lg">إرسال</button>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-default btn-gray btn-block btn-lg" id="job-apply-reset">تفريغ البيانات</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- jobs -->
@stop
