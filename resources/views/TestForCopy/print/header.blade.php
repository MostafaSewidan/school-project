<div class="print-header">
    <div class="row">
        <div class="col-xs-4 col-4">
            <h4>المملكة العربية السعودية</h4>
            <h4>وزارة الصحة</h4>
            <h4>{{app('settings')->site_name}} </h4>
            <h4>قسم الصيانة الطبية </h4>
            <h4>{{app('settings')->contractor_name}} </h4>
        </div>
        <div class="col-xs-4 col-4">
            <div class="logo text-center">
                <img style="width: 50%;" src="{{asset('photos/health-ministry.png')}}" alt="Logo">
            </div>
        </div>
        <div class="col-xs-4 col-4 text-left">
            <h4>Kingdom of Saudi Arabia</h4>
            <h4>Ministry of Health</h4>
            <h4>{{app('settings')->site_name_en}} </h4>
            <h4>Biomedical Eng. Department</h4>
            <h4>{{app('settings')->contractor_name_en}} </h4>
        </div>
    </div>
    <hr>
</div>