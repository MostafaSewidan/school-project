@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "أضف مستهلك"
                                ])

@section('content')

<div class="ibox">
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>'ExpenseController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}
    <div class="ibox-content">
        @include('admin.expenses.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">حفظ</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
