@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "المستهلكات"
                          ])

@section('content')
    <div class="ibox-content">
        <div class="pull-right">
            <a href="{{url('admin/expenses/create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> إضافة
            </a>
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">التاريخ</th>
                    <th class="text-center">التاريخ بالهجري</th>
                    <th class="text-center"> المبلغ</th>
                    <th class="text-center"> البيان</th>


                    <th class="text-center">تعديل</th>
                    <th class="text-center">حذف</th>
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                    <tr id="removable{{$record->id}}">
                        <td class="text-center">{{$record->expenses_data}}</td>
                        <td class="text-center">{{$record->hijri_expenses_date}}</td>

                        <td class="text-center">{{$record->cost}}</td>
                        <td class="text-center">{{$record->notes}}</td>


                        <td class="text-center">
                            <a href="{{url('admin/expenses/'.$record->id.'/edit')}}"
                               class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="text-center">
                            <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{url('admin/expenses/'.$record->id)}}"
                                    type="button" class="destroy btn btn-danger btn-xs"><i
                                        class="fa fa-trash"></i></button>
                        </td>
                    </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            {!! $records->render() !!}
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                </div>
            @endif
        </div>

    </div>
@endsection
