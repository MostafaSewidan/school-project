{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')

@inject('devices',App\Models\Device)
@inject('deviceType',App\Models\DeviceType)
@inject('manufacturer',App\Models\Manufacturer)
@inject('importer',App\Models\Importer)
{!! \Helper\Field::select('device_id' ,__('كود الجهاز'),$devices->pluck('code','id')->toArray()) !!}
<div id="device-info"></div>
{!! \Helper\Field::text('name' ,__('الأسم').'*') !!}
{!! \Helper\Field::number('available_quantity' ,__('الكمية')) !!}

{!! \Helper\Field::select('device_type_id' ,__('نوع القطعة'),$deviceType->pluck('name','id')->toArray()) !!}
{!! \Helper\Field::text('serial_number' ,__('السريال')) !!}
{!! \Helper\Field::select('manufacturer_id' ,__('الشركة المصنعة'),$manufacturer->pluck('name','id')->toArray()) !!}
{!! \Helper\Field::select('importer_id' ,__('المورد').'*',$importer->pluck('name','id')->toArray()) !!}
{!! \Helper\Field::datePicker('Validity_date' ,__('تاريخ الصلاحية')) !!}
{!! \Helper\Field::hijriDatePicker('hijri_Validity_date' ,__('تاريخ الصلاحية بالهجري')) !!}
{!! \Helper\Field::datePicker('entry_date' ,__('تاريخ الادخال')) !!}
{!! \Helper\Field::hijriDatePicker('hijri_entry_date' ,__('تاريخ الادخال بالهجري')) !!}
{!! \Helper\Field::select('statues' ,__('الحالة'),[
    'valid' => 'صالح' ,
    'not_good' => 'غير صالح ' ,
    'other' => 'أخري' ,
]) !!}
{!! \Helper\Field::textarea('notes',__('الملاحظات')) !!}




