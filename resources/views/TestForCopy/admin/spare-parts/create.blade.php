@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف قطع غيار')
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record,[
                                'action'=>'SparePartsController@store',
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'POST'
                                ])!!}
        <div class="ibox-content">
            @include('admin.spare-parts.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
        </div>
{{--        {!! Form::close()!!}--}}
    </div>
@stop
