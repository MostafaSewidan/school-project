@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('قطع الغيار')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                @can('إضافة قطع الغيار')
                <a href="{{url(route('spare-parts.create'))}}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> {{ __('أضف قطع غيار') }}</a>
                @endcan
                    @can('استيراد قطع الغيار')
                        <button class="btn btn-success hidden-print btn-sm" data-toggle="modal" data-target="#importDevices">
                            <i class="fa fa-file-excel-o"></i> {{__('استيراد قطع الغيار')}}</button>
                    @endcan
                @can('حذف كل قطع الغيار')
                <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                      action="{{route('spare-parts.delete-all')}}" style="display: inline-block">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-danger btn-sm hidden-print"><i
                            class="fa fa-trash"></i>{{__('حذف الكل')}}</button>
                </form>
                    @endcan
                    <div class="modal inmodal" id="importDevices" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight text-right">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                    <h4 class="modal-title">{{__('استيراد قطع الغيار')}}</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'files' => true,
                                        'action' => 'ExcelController@importSpareParts'
                                    ]) !!}
                                    {!! \Helper\Field::fileWithPreview('spare_parts_sheet' ,__('اختر ملف الأكسل')) !!}
                                    <hr>
                                    <a href="{{asset('sheets/add_spare_part.xlsx')}}" class="btn btn-success">
                                        <i class="fa fa-download"></i>
                                        {{__('تحميل الملف القياسي')}}
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary"><i
                                            class="fa fa-file-excel-o"></i> {{__('استيراد قطع الغيار')}}</button>

                                    <button type="button" class="btn btn-white"
                                            data-dismiss="modal">{{__('إغلاق')}}</button>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>

            <div class="btn-group" style="float: left">
                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                   href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل قطع الغيار')
                <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'spareparts'))}}"><i class="fa fa-file-excel-o"></i></a>
                @endcan
                {{--@can('تصدير قطع الغيار pdf')--}}
                {{--<a class="btn btn-danger btn-sm" href="#">--}}
                    {{--<i class="fa fa-file-pdf-o"></i>--}}
                {{--</a>--}}
                {{--@endcan--}}
            </div>
            <div class="col-xs-6 text-left">
                <br>
{{--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
{{--                    <i class="fa fa-search"></i>--}}
{{--                    بحث متقدم--}}
{{--                </button>--}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('بحث متقدم') }}</h4>
                            </div>
                            @inject('models',App\Models\Device)
                            <div class="modal-body">
                                <div class="row">
                                    {!! Form::open([
                                'method' => 'GET'
                            ]) !!}
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('code' ,__('كود القطعة')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('name' ,__('الأسم')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('serial_number' ,__('رقم السريال')) !!}
                                    </div>


                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('model_name' ,__('رقم الموديل'),$models->pluck('model','id')->toArray()) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('manufacturer' ,__('المُصنع')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('importer' ,__('البائع')) !!}
                                    </div>


                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('available_quantity' ,__('الكمية')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('notes' ,__('الملاحظات')) !!}
                                    </div>
{{--                                    <div class="col-sm-4">--}}
{{--                                        {!! \Helper\Field::select('per_page' ,__('عدد النتائج بالصفحة'),[--}}
{{--                                            '10' => '10',--}}
{{--                                            '15' => '15',--}}
{{--                                            '50' => '50',--}}
{{--                                            '75' => '75',--}}
{{--                                            '100' => '100'--}}
{{--                                        ],request('per_page'),'',__('عدد النتائج بالصفحة')) !!}--}}
{{--                                    </div>--}}

                                    @include('layouts.partials.append-per-page-to-filters')
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
     @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
                <div class="table-responsive">
                    @if(!empty($records) && count($records)>0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                        <th class="text-center">#</th>
                        <th class="text-center">{{ __('كود الجهاز') }}</th>
                        <th class="text-center">{{ __('الأسم') }}</th>
                        <th class="text-center">{{ __('الكمية') }}</th>
                        <th class="text-center">{{ __('نوع القطعة') }}</th>
                        <th class="text-center">{{ __('رقم الموديل') }}</th>
                        <th class="text-center">{{ __('رقم السريال') }}</th>
                        <th class="text-center">{{ __('القسم') }}</th>
                        <th class="text-center">{{ __('الغرفة') }}</th>

                        <th class="text-center">{{ __('المُصنع') }}</th>

                        <th class="text-center">{{ __('المورد') }}</th>

                        <th class="text-center">{{ __('تاريخ الصلاحية') }}</th>
                        <th class="text-center">{{ __('تاريخ الصلاحية بالهجري') }}</th>

                        <th class="text-center" >{{ __('تاريخ الادخال') }}</th>
                        <th class="text-center" >{{ __('تاريخ الادخال بالهجري') }}</th>

                        <th class="text-center" >{{ __('الحالة') }}</th>

                        <th class="text-center">{{ __('الملاحظات') }}</th>
                        @can('تعديل قطع الغيار')
                        <th class="text-center">{{ __('تعديل') }}</th>
                        @endcan
                        @can('حذف قطع الغيار')
                        <th class="text-center">{{ __('حذف') }}</th>
                        @endcan
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($records as $record)
                            <tr id="removable">
                                <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                                <td class="text-center">{{optional($record->device)->code}}</td>
                                <td class="text-center">{{$record->name}}</td>
                                <td class="text-center">
                                    {{$record->available_quantity}}
                                </td>
                                <td class="text-center">
                                    {{optional(optional($record->device)->deviceType)->name}}
                                </td>
                                <td class="text-center">{{optional($record->device)->model}}</td>
                                <td class="text-center">{{$record->serial_number}}</td>
                                <td class="text-center">{{optional(optional($record->device)->department)->name}}</td>
                                <td class="text-center">{{optional($record->device)->room_number}}</td>
                                <td class="text-center">{{optional($record->manufacturer)->name}}</td>
                                <td class="text-center">{{optional($record->importer)->name}}</td>
                                <td class="text-center">{{$record->Validity_date}}</td>
                                <td class="text-center">{{$record->hijri_Validity_date}}</td>
                                <td class="text-center">{{$record->entry_date}}</td>
                                <td class="text-center">{{$record->hijri_entry_date}}</td>
                                <td class="text-center">{{$record->statue_type}}</td>

                                <td class="text-center" >{{$record->notes}}</td>
                                @can('تعديل قطع الغيار')
                                <td class="text-center">
                                    <a href="{{url('admin/spare-parts/'.$record->id.'/edit')}}"
                                       class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                @endcan
                                @can('حذف قطع الغيار')
                                <td class="text-center">
                                    <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{url('admin/spare-parts/'.$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                                @endcan
                            </tr>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
@include('layouts.partials.per-page')
                <div class="text-center">
                    {!! $records->appends(request()->query())->render() !!}
                </div>
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> {{ __('لا توجد بيانات للعرض') }} </h3>
                </div>
            @endif
        </div>
        </div>
    </div>
@endsection
