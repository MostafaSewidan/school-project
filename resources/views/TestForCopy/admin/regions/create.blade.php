@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                   'page_description'  => $governorate->name.' - '.$city->name.' - '.'المناطق'
                                ])
@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record,[
                                'action'=>['GovernorateCityRegionController@store',$governorate->id,$city->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'POST'
                                ])!!}
        <div class="ibox-content">
            @include('admin.regions.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>
        {!! Form::close()!!}
    </div>
@stop
