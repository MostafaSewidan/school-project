@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => _("Items")
                                ])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{url(route('restaurant.category.index',$restaurant->id))}}">{{$restaurant->name}}</a>
        </li>
        <li class="breadcrumb-item active">
            <strong>{{$category->name}}</strong>
        </li>
    </ol>
@endsection
@section('content')
    <div class="ibox-title">
        {{$record->name}}
    </div>
    <div class="ibox-content">
        <div class="table-responsive">
            <table class="data-table table table-bordered">
                <thead>

                <th>{{_("Name (Arabic)")}}</th>
                <th>{{_("Name (English)")}}</th>
                <th>{{_("Description (Arabic)")}}</th>
                <th>{{_("Description (English)")}}</th>
                <th>{{_("Price")}}</th>
                <th>{{_("Category")}}</th>
                <th>{{_("Cover Photo")}}</th>
                </thead>
                <tbody>

                    <tr>
                        <td>{{$record->name}}</td>
                        <td>{{$record->name_en}}</td>
                        <td>{{$record->description}}</td>
                        <td>{{$record->description_en}}</td>
                        <td>{{$record->price}}</td>
                        <td>{{optional($record->category)->name}}</td>
                        <td>
                            <img style="height: 70px" src="data:image/{{$record->cover_photo_ext}};base64,{{$record->cover_photo_base64}}" alt="{{$record->name}}" />
                        </td>

                    </tr>


                </tbody>
            </table>
        </div>

    </div>
@stop
