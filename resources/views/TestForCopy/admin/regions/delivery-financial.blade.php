@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => $governorate->name.' - '.$city->name.' - '.'المناطق'
                                ])
@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record,[
                                'url'=>url('/admin/governorates/'.$governorate->id.'/city/'.$city->id.'/region/'.$record->id.'/delivery-financial'),
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'post'
                                ])!!}
        <div class="ibox-content">

            @include('flash::message')
            <div class="col-md-6">
                {!! \Helper\Field::number('store_search_area' ,'نطاق البحث عن المحلات') !!}
            </div>

            <div class="col-md-6">

                {!! \Helper\Field::number('delivery_search_area_in_app_cost' ,'نطاق محاسبة الديلفرى بسعر التطبيق 	') !!}
            </div>
            <div class="col-md-6">

                {!! \Helper\Field::number('extra_km_price' ,'سعر الكيلو متر الزائد عن نظام التطبيق') !!}
            </div>
            <div class="col-md-6">

                {!! \Helper\Field::number('increase_factor' ,'معامل الزيادة لكل محل للطلبات المكتوبة') !!}
            </div>
            <div class="col-md-12">

                {!! \Helper\Field::number('one_store_order_cost' ,'سعر الطلب المكتوب للمحل الأول') !!}
            </div>

            <div class="col-md-3">
                <label>المحل الثاني</label>
                <div class="">
                    {!! Form::number('shop_2', null , [
                    "placeholder" => 'المحل الثاني',
                    "class" => "form-control",
                    "id" => 'shop_2',
                    "readonly" => 'on',
                    "step" => "0.001"
                    ]) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label>المحل الثالث</label>
                <div class="">
                    {!! Form::number('shop_3', null , [
                    "placeholder" => 'المحل الثالث',
                    "class" => "form-control",
                    "id" => 'shop_3',
                    "readonly" => 'on',
                    "step" => "0.001"
                    ]) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label>المحل الرابع</label>
                <div class="">
                    {!! Form::number('shop_4', null , [
                    "placeholder" => 'المحل الرابع',
                    "class" => "form-control",
                    "id" => 'shop_4',
                    "readonly" => 'on',
                    "step" => "0.001"
                    ]) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label>المحل الخامس</label>
                <div class="">
                    {!! Form::number('shop_5', null , [
                    "placeholder" => 'المحل الخامس',
                    "class" => "form-control",
                    "id" => 'shop_5',
                    "readonly" => 'on',
                    "step" => "0.001"
                    ]) !!}
                </div>
            </div>

            <div class="clearfix"></div>

            <br><br>

            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    @inject('vehicle_types',App\Models\VehicleType)
                    <thead>
                    <th>البند</th>
                    @foreach($vehicle_types->get() as $type)

                        <th class="text-center">{{$type->name}}</th>
                    @endforeach
                    </thead>
                    <tbody>
                    <tr>

                        <th style="background-color: #f5f5f6;">
                            ثابت لكل اوردر
                        </th>
                        @foreach($vehicle_types->get() as $type)
                            @php $value = $type->regions()->where('region_id' , $record->id)->first() @endphp
                            <td class="text-center">

                                {!! \Helper\Field::number('every_order_constant_'.$type->id , null , $value && $value->pivot->every_order_constant != null ? $value->pivot->every_order_constant : null) !!}

                            </td>
                        @endforeach

                    </tr>
                    <tr>
                        <th style="background-color: #f5f5f6;">
                            نسبة الطيار
                        </th>
                        @foreach($vehicle_types->get() as $type)
                            @php $value = $type->regions()->where('region_id' , $record->id)->first() @endphp
                            <td class="text-center">

                                {!! \Helper\Field::number('delivery_present_'.$type->id , null , $value && $value->pivot->delivery_present != null ? $value->pivot->delivery_present : null) !!}

                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <th style="background-color: #f5f5f6;">
                            الحد الاقصى
                        </th>
                        @foreach($vehicle_types->get() as $type)
                            @php $value = $type->regions()->where('region_id' , $record->id)->first() @endphp
                            <td class="text-center">

                                {!! \Helper\Field::number('delivery_max_'.$type->id , null , $value && $value->pivot->delivery_max != null ? $value->pivot->delivery_max : null) !!}

                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <th style="background-color: #f5f5f6;">
                            الحد الادنى
                        </th>
                        @foreach($vehicle_types->get() as $type)
                            @php $value = $type->regions()->where('region_id' , $record->id)->first() @endphp
                            <td class="text-center">

                                {!! \Helper\Field::number('delivery_min_'.$type->id , null , $value && $value->pivot->delivery_min != null ? $value->pivot->delivery_min : null) !!}

                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
            <button type="button" class="btn btn-danger" onclick="window.history.back();">الغاء</button>
        </div>
        {!! Form::close()!!}
    </div>


    @push('scripts')
        <script>

            var a = '{{$record->one_store_order_cost ? $record->one_store_order_cost : 0}}';
            var r = '{{$record->increase_factor ? $record->increase_factor : 0}}';

            // {\ a_{n}=a\,r^{n-1}.}

            function f(a, r) {

                a = parseFloat(a);
                r = parseFloat(r);

                a = a == 0 ? $('#one_store_order_cost').val() : a;
                r = r == 0 ? $('#increase_factor').val() : r;

                for (var n = 2; n <= 5; n++) {
                    var result = a * (Math.pow(r, (n - 1)));
                    var input = $('#shop_' + n).val(0);

                    input.val(Math.round(result * 100) / 100);
                }
            }

            f(a, r);

            $("#increase_factor").keyup(function () {
                a = $('#one_store_order_cost').val();
                r = $('#increase_factor').val();
                f(a, r);
            });

            $("#one_store_order_cost").keyup(function () {
                a = $('#one_store_order_cost').val();
                r = $('#increase_factor').val();
                f(a, r);
            });

        </script>
    @endpush
@stop





