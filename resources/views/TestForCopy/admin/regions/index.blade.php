@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => 'المناطق'
                                ])

@section('content')
    <div class="ibox-content">
        <div class="">
            <a href="{{url(route('governorates.city.region.create',[$governorate->id,$city->id]))}}"
               class="btn btn-primary">
                <i class="fa fa-plus"></i> أضف منطقة
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
        @include('flash::message')
        @if(count($records))
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">الأسم</th>
                    <th class="text-center">المدينة - المحافظة</th>
                    <th class="text-center">حسابات الديليفري</th>
                    <th class="text-center">تعديل</th>
                    <th class="text-center">حذف</th>
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                        <tr id="removable{{$record->id}}">
                            <td class="text-center">{{$count}}</td>
                            <td class="text-center">{{$record->name}}</td>
                            <td class="text-center">{{optional($record->city)->name  }} - {{optional($record->city->governorate)->name}}</td>


                            <td class="text-center"><a
                                        href="{{url('/admin/governorates/'.$governorate->id.'/city/'.$city->id.'/region/'.$record->id.'/delivery-financial')}}"
                                        class="btn btn-xs btn-success"><i class="fa fa-money"></i></a>
                            </td>

                            <td class="text-center"><a
                                    href="{{url(route('governorates.city.region.edit',[$governorate->id,$city->id,$record->id]))}}"
                                    class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                            </td>

                            <td class="text-center">
                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                        data-route="{{url(route('governorates.city.region.destroy',[$governorate->id,$city->id,$record->id]))}}"
                                        type="button" class="destroy btn btn-danger btn-xs"><i
                                        class="fa fa-trash"></i></button>
                            </td>

                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {!! $records->render() !!}
            </div>
        @endif
        <div class="clearfix"></div>
    </div>
@stop
