
@include('flash::message')
{!! \Helper\Field::text('name' , 'الاسم')!!}


{!! \Helper\Field::gMap('pick_location',false,true,350,
$record->latitude ? $record->latitude : '31.11098688368921',
$record->longitude ? $record->longitude : '30.938894748687744' ) !!}

{{--<div>--}}
{{--    <label class="label label-danger" id="danger" style="display: none">--}}
{{--       هذه المنطقة موجوده بالفعل--}}
{{--    </label>--}}
{{--    <label class="label label-success" id="success" style="display: none">--}}
{{--       هذه المنطقة جديدة--}}
{{--    </label>--}}
{{--</div>--}}

@push('scripts')
    <script type="text/javascript"
            src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBI69AoVg9i-l6zFosYCUGIXrRx1cmY9MU'></script>
    <script src="{{asset('inspina/js/plugins/locationpicker/locationpicker.jquery.min.js')}}"></script>

    <script>
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var mapFiled = $('#mapField').locationpicker({
            location: {latitude: latitude, longitude: longitude},
            radius: 0,
            inputBinding: {
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#address_en')
            },
            // enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                return false;
            }
        });

        {{--function checkRegion(){--}}
        {{--    var latitude = $('#latitude').val();--}}
        {{--    var longitude = $('#longitude').val();--}}

        {{--    $.ajax({--}}
        {{--        url: "{{url('admin/get-check-region?lat=')}}" + latitude +'&long='+ longitude,--}}
        {{--        type: 'get',--}}
        {{--        dataType: 'json',--}}
        {{--        success: function (data) {--}}
        {{--            console.log(data);--}}
        {{--            if(data.is_added === 'true')--}}
        {{--            {--}}
        {{--                $('#danger').show();--}}
        {{--                $('#success').hide();--}}
        {{--            }else {--}}

        {{--                $('#success').show();--}}
        {{--                $('#danger').hide();--}}
        {{--            }--}}

        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        {{--$('#latitude,#longitude').keyup(function () {--}}
        {{--   checkRegion();--}}
        {{--});--}}

        {{--$('#latitude').change(function () {--}}
        {{--    checkRegion();--}}
        {{--});--}}
    </script>
@endpush






