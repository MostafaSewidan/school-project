@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('الشركات المصنعة')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="pull-right">
            @can('إضافة الشركات المصنعة')
            <a href="{{url('admin/manufacturers/create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{__('إضافة')}}
            </a>
            @endcan
        </div>
        <div class="btn-group" style="float: left">
            @can('تصدير اكسل الشركات المصنعة')
            <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'manufacturers'))}}"><i class="fa fa-file-excel-o"></i></a>
            @endcan
            {{--@can('تصدير الشركات المصنعة pdf')--}}
            {{--<a class="btn btn-danger btn-sm" href="#">--}}
                {{--<i class="fa fa-file-pdf-o"></i>--}}
            {{--</a>--}}
                {{--@endcan--}}
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{__('الأسم')}}</th>
                    @can('تعديل الشركات المصنعة')
                    <th class="text-center">{{__('تعديل')}}</th>
                    @endcan
                    @can('حذف الشركات المصنعة')
                    <th class="text-center">{{__('حذف')}}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                    <tr id="removable{{$record->id}}">
                        <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                        <td class="text-center">{{$record->name}}</td>

@can('تعديل الشركات المصنعة')
                        <td class="text-center">
                            <a href="{{url('admin/manufacturers/'.$record->id.'/edit')}}"
                               class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                        </td>
                        @endcan
@can('حذف الشركات المصنعة')
                        <td class="text-center">
                            <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{url('admin/manufacturers/'.$record->id)}}"
                                    type="button" class="destroy btn btn-danger btn-xs"><i
                                    class="fa fa-trash"></i></button>
                        </td>
                        @endcan
                    </tr>

                    @endforeach
                    </tbody>
                </table>
                @include('layouts.partials.per-page')
     <div class="text-center">
         {!! $records->render() !!}
     </div>

            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> {{__('لا توجد بيانات للعرض')}} </h3>
                </div>
            @endif
        </div>
        </div>
    </div>
@endsection
