@extends('layouts.app',[
								'page_header'		=> 'الرسائل',
								'page_description'	=> 'رسائل التواصل',

								])
@section('content')
    <div class="ibox ibox-primary">


        <div class="">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',old('name'),[
                        'class' => 'form-control',
                        'placeholder' => 'الاسم'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('phone',old('phone'),[
                        'class' => 'form-control',
                        'placeholder' => 'الهاتف'
                    ]) !!}
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('from',old('from'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'تاريخ الاضافة'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('to',old('to'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'تاريخ النهاية'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button class="btn btn-flat btn-block btn-primary">بحث</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="ibox-content">
            @if(!empty($contacts) && count($contacts)>0)
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>الرسالة</th>
                        <th>الهاتف</th>
                        <th>النوع</th>
                        <th>التاريخ</th>
                        <th class="text-center">حذف</th>
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($contacts as $contact)
                            <tr id="removable{{$contact->id}}">
                                <td>{{$count}}</td>
                                <td>{{optional($contact)->name}}</td>
                                <td>{{optional($contact)->contact}}</td>
                                <td>{{optional($contact)->phone}}</td>
                                <td>
                                    @if($contact->contactable_type == 'App\Models\Client') عميل
                                    @elseif($contact->contactable_type == 'App\Models\Store')متجر
                                    @elseif($contact->contactable_type == 'App\Models\Delivery')دليفري
                                    @else عميل
                                    @endif
                                </td>
                                <td>{{$contact->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,   h:mm')}}</td>
                                <td class="text-center">
                                    <button id="{{$contact->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('contacts.destroy',$contact->id)}}" type="button"
                                            class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            @php $count ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center"> {!! $contacts->render() !!}</div>

            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> No data to show </h3>
                </div>
            @endif


        </div>
    </div>
@stop

@section('script')
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true,
            'showImageNumberLabel': false,

        })
    </script>


@stop
