@extends('layouts.app',[
                                'page_header'       => __('المستخدمين'),
                                'page_description'  => __('حسابي')
                                ])
@section('content')
    <!-- general form elements -->
    <div class="ibox">
        <!-- form start -->
        {!! Form::open([
                                'action'=>'UserController@updateProfile',
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'POST',
                                'files' => true
                                ])!!}

        <div class="ibox-content">
            @include('flash::message')
            @include('layouts.partials.validation-errors')


            {!! \Helper\Field::fileWithPreview('photo',__('الصورة الشخصية')) !!}
            <div class="col-sm-12">
                <img src="{{auth()->user()->photo}}" style="height: 100px" alt="">
            </div>

            <div class="clearfix"></div>
            <br><br>

            {!! \Helper\Field::password('old-password',__('كلمة المرور الحالية')) !!}
            {!! \Helper\Field::password('password',__('كلمة المرور الجديدة')) !!}
            {!! \Helper\Field::password('password_confirmation',__('تأكيد كلمة المرور الجديدة')) !!}

            <div class="ibox-footer">
                <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection
