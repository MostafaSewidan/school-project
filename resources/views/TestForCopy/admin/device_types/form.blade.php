{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')

<div class="row">
    <div class="col-md-6">
        {!! \Helper\Field::text('name',__('الأسم').' *') !!}
    </div>
    <div class="col-md-6">
        {!!\Helper\Field::select('category' ,__('الفئة') , [
            'a' => __('أ'),
            'b' => __('ب'),
            'c' => __('ج'),
           'other' => __('اخري'),
        ],null,'selectize') !!}
    </div>
</div>



