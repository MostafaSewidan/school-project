<canvas id="horizontalBarWoPerAge" height="450"></canvas>
<script>
    var ctx = document.getElementById('horizontalBarWoPerAge');
    var data = {
        datasets: [{
            label: '{{$title}}',
            data: {!! json_encode($values) !!},
            backgroundColor: {!! json_encode($bgColors) !!},
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: {!! json_encode($labels) !!}
    };
    var options = {
        responsive: true,
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                },
            }],
        }
    };
    var myHBar = new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: options
    });
</script>