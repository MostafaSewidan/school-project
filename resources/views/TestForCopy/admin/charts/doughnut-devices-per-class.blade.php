<canvas id="doughnutDevicesPerClass" height="450"></canvas>
<script>
    var ctx2 = document.getElementById('doughnutDevicesPerClass');
    var data = {
        datasets: [{
            label: '# of Votes',
            data: {!! json_encode($values) !!},
            backgroundColor: {!! json_encode($bgColors) !!},
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: {!! json_encode($labels) !!}
    };
    var options = {
        responsive: true,
        // scales: {
        //     xAxes: [{
        //         ticks: {
        //             beginAtZero: true
        //         }
        //     }]
        // }
    };
    var myDoughnutChart = new Chart(ctx2, {
        type: 'doughnut',
        data: data,
        options: options
    });
</script>