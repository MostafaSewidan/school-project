@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('طباعة أمر عمل'),
                                'title' => 'WO-'.$record->id
                                ])

@section('content')
    <div class="ibox">
        <div class="ibox-content">
            @include('print.header')
            <h3 class="text-center"> نموذج أمر عمل
                <br>
                Work Order document
            </h3>
            <div class="row">
                <div class="col-xs-4 text-center">
                    <label>تاريخ التعطل <br>
                    Failure Date
                    </label>
                    : {{$record->created_at->toDateString()}}
                </div>
                <div class="col-xs-4 text-center">
                    <label>كود الجهاز <br>
                    Control No.
                    </label>
                    : {{$record->device->code}}
                </div>
                <div class="col-xs-4 text-center">
                    <label>أمر العمل <br>
                    Work Order
                    </label>
                    : {{$record->id}}
                </div>
            </div>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 25%">
                         اسم الجهاز
                        <br>
                        Device Name
                    </th>
                    <td style="width: 25%">{{$record->device->code}}</td>
                    <th style="width: 25%">
                         كود الشركة المصنعة
                        <br>
                        Manuf ID
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->id}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         الموديل
                        <br>
                         Model
                    </th>
                    <td style="width: 25%">{{$record->device->model}}</td>
                    <th style="width: 25%">
                         اسم الشركة المصنعة
                        <br>
                        Manuf Name
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->name}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         الرقم التسلسلي
                        <br>
                         Serial No.
                    </th>
                    <td style="width: 25%">{{$record->device->model}}</td>
                    <th style="width: 25%">
                         فاكس الشركة المصنعة
                        <br>
                        Manuf Fax
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->name}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         القسم
                        <br>
                         Department
                    </th>
                    <td style="width: 25%">{{optional($record->device->department)->name}}</td>
                    <th style="width: 25%">
                         كود المورد
                        <br>
                        Vendor ID
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->name}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         الغرفة
                        <br>
                         Room
                    </th>
                    <td style="width: 25%">{{$record->device->model}}</td>
                    <th style="width: 25%">
                         اسم المورد
                        <br>
                        Vendor Name
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->name}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         نوع الجهاز
                        <br>
                         Device Type
                    </th>
                    <td style="width: 25%">{{$record->device->model}}</td>
                    <th style="width: 25%">
                         فاكس المورد
                        <br>
                        Vendor Fax
                    </th>
                    <td style="width: 25%">{{$record->device->manufacturer->name}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         رقم العقد
                        <br>
                         Contract No.
                    </th>
                    <td style="width: 25%">{{optional($record->device->contract)->contract_number}}</td>
                    <th style="width: 25%">
                          نوع العقد
                        <br>
                        Contract Type
                    </th>
                    <td style="width: 25%">{{optional($record->device->contract)->contract_typ}}</td>
                </tr>
                <tr>
                    <th style="width: 25%">
                         الشكوى
                        <br>
                         Fault
                    </th>
                    <td colspan="3" style="width: 25%;">
                        <div style="min-height: 150px">
                            {{optional($record->complaint)->body}}
                        </div>
                    </td>
                </tr>
            </table>

            <br>
            <div class="row">
                <div class="col-xs-6">
                    <label>اسم المُبلغ <br>
                        Reporter Name
                    </label>
                    : {!! $record->reporter ? $record->reporter->name: '<span class="text-muted">............................................</span>' !!}
                </div>
                <div class="col-xs-6">
                    <label>توقيع المُبلغ <br>
                        Reporter Sign
                    </label>
                    <span class="text-muted">......................................................</span>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print()
    </script>
@endsection
