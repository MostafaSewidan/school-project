<div class="modal fade" id="choose-delivery" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('أضف تعليق')}}</h5>

                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open([
                'url' => route('comment-create'),
                'method' => 'post','ajaxForm','files' => true
            ]) !!}
            <div class="modal-body">

                <div class="form-group">

                    <input type="hidden"   class="form-control" name="working_order_id" value="{{$record->id}}">
                </div>

                {!!\Helper\Field::select('device_status' ,__('حالة الجهاز') , [
                    'up' => 'يعمل',
                    'down' => 'لايعمل',
                    'other' => 'أخرى'
                ],$record->device->status) !!}

                <div class="form-group">
                    <label>{{ __('أضف تعليق') }}</label>
                    <textarea placeholder="{{ __('أضف تعليق') }}"  class="form-control" name="body"></textarea>

                </div>


                {!! \Helper\Field::fileWithPreview('attachments',__('مرفقات'),true) !!}

                @if(!empty($model->attachment))

                    @foreach($model->attachmentRelation()->whereNull('usage')->get() as  $attachment)

                        <div class="col-md-3" id="removable{{$attachment->id}}">
                            <div class="text-center"
                                 style="width: 100%;color: white;background-color: black;font-size: 3rem;font-weight: bolder;">
                                {{$loop->iteration}}
                            </div>
                            <img src="{{asset($attachment->path)}}" class="img-responsive" alt="">
                            <div class="clearfix"></div>
                            <button id="{{$attachment->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('photo.destroy',$attachment->id)}}"
                                    type="button" class="destroy btn btn-danger btn-xs btn-block">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <br>
                        <br>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                {!! \Helper\Field::ajaxBtn(__('حفظ')) !!}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    {{__('إغلاق')}}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="closeWO" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('إغلاق أمر العمل')}}</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open([
                'url' => route('complete-work-order'),
                'method' => 'post','ajaxForm','files' => true
            ]) !!}
            <div class="modal-body">

                <div class="form-group">
                    <input type="hidden"   class="form-control" name="working_order_id" value="{{$record->id}}">
                </div>

                {!!\Helper\Field::select('device_status' ,__('حالة الجهاز') , [
                    'up' => 'يعمل',
                    'down' => 'لايعمل',
                    'other' => 'أخرى'
                ],$record->device->status) !!}

                {!! \Helper\Field::fileWithPreview('attachments',__('مرفقات'),true) !!}
            </div>
            <div class="modal-footer">
                {!! \Helper\Field::ajaxBtn(__('حفظ')) !!}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    {{__('إغلاق')}}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
