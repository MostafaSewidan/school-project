@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('تعديل أمر عمل')
                                ])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>['OperationsController@update',$model->id],
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'PUT'
                                ])!!}
        <div class="ibox-content">
            @include('admin.operations.form')
        </div>
        <div class="ibox-footer">
            {!! \Helper\Field::ajaxBtn(__('حفظ')) !!}
        </div>
        {!! Form::close()!!}
    </div>
@stop
