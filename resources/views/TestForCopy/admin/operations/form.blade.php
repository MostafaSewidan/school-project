@include('flash::message')
@inject('devices',App\Models\Device)

@if($edit)
    {!! \Helper\Field::textarea('comment' ,__('ملحوظة')) !!}
@endif
{!! \Helper\Field::select('device_id' ,__('كود الجهاز').'*',$devices->byUserDepartment()->pluck('code','id')->toArray(),request('device_id')) !!}
<div id="device-info"></div>
@if(request('device_id'))
    @push('scripts')
        <script>
            var deviceInfoEl = $("#device-info");
            var deviceId = "{{request('device_id')}}";
            getDeviceInfo(deviceId, deviceInfoEl);
        </script>
    @endpush
@endif

{!! \Helper\Field::select('type' ,__('النوع').'*', $model->getTypes(),null) !!}
<div style="display: none;">
    {!! \Helper\Field::datePicker('due_to' ,__('التاريخ').'*',\Carbon\Carbon::now()->toDateString()) !!}
{{--    {!! \Helper\Field::hijriDatePicker('hijri_due_to' , __('تاريخ (هجري)').'*') !!}--}}
</div>

{!! \Helper\Field::textarea('complaint' ,__('الشكوى').'*', optional($model->complaint)->body) !!}

{!!\Helper\Field::select('device_status' ,__('حالة الجهاز') , [
                    'up' => 'يعمل',
                    'down' => 'لايعمل',
                    'other' => 'أخرى'
                ]) !!}

<input type="hidden" name="back_to" value="{{request('device_id')}}">
