@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أوامر العمل')
                          ])

@section('content')
    @push('styles')
        <style>
            @media print {
                a[href]:after {
                    content: none !important;
                }
            }
        </style>
    @endpush
    @can('طباعة أوامر العمل')
    <button type="button" class="btn btn-sm btn-success pull-left hidden-print" onclick="window.print()"><i
            class="fa fa-print"></i></button><br>
    @endcan
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                @can('إضافة أوامر العمل')
                <a href="{{url(route('operations.create'))}}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>
                    {{ __('أضف أمر عمل') }}</a>
                @endcan
                @can('حذف أوامر العمل')
                <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                      action="{{route('operations.delete-all')}}" style="display: inline-block">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-danger btn-sm hidden-print"><i
                            class="fa fa-trash"></i>{{__('حذف الكل')}}</button>
                </form>
                    @endcan
            </div>
            <div class="btn-group " style="float: left">
                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                   href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل أوامر العمل')
                <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'operations'))}}"><i
                        class="fa fa-file-excel-o"></i></a>
                @endcan
                {{--@can('تصدير أوامر العمل pdf')--}}
                {{--<a class="btn btn-danger btn-sm" href="#">--}}
                    {{--<i class="fa fa-file-pdf-o"></i>--}}
                {{--</a>--}}
                {{--@endcan--}}
            </div>

            <div class="col-xs-6 text-left">
                <br>

                {{--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
                {{--                    <i class="fa fa-search"></i>--}}
                {{--                    بحث متقدم--}}
                {{--                </button>--}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('بحث متقدم') }}</h4>
                            </div> {!! Form::open([
                                    'method' => 'GET'
                                ]) !!}
                            <div class="modal-body">
                                <div class="row">
                                    @inject('wo' , App\Models\WorkingOrder)
                                    @inject('manufacturers' , App\Models\Manufacturer)
                                    @inject('deviceTypes' , App\Models\DeviceType)
                                    @inject('importers' , App\Models\Importer)
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::number('id' ,__('رقم أمر العمل'),request('id')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('serial_number' ,__('السريال'),request('serial_number')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('device_code' ,__('كود الجهاز'),request('device_code')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!!\Helper\Field::select('device_type' ,__('نوع الجهاز') , $deviceTypes->pluck('name','id')->toArray(),request('device_type')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!!\Helper\Field::select('department' ,__('الأقسام') , $departments,request('department')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!!\Helper\Field::select('manufacturer' ,__('المصنع') , $manufacturers->pluck('name','id')->toArray(),request('manufacturer')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!!\Helper\Field::select('importer' ,__('المورد') , $importers->pluck('name','id')->toArray(),request('importer')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::select('category' ,__('الفئه'),[
                                        'a' => 'أ',
                                        'b' => 'ب',
                                        'c' => 'ج',
                                        'other' => __('اخري'),
],request('category')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!!\Helper\Field::select('contract_type' ,__('نوع العقد') , [
                                            'guaranteed' => 'تحت الضمان',
                                            'inclusive' => 'عقد شامل',
                                            'maintenance_only' => 'عقد صيانات فقط',
                                            'nothing' => 'بدون عقد أو ضمان'
                                        ],request('contract_type')) !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::select('status' ,__('حالة أمر العمل'),$wo->getStatus(),null,'select2','الكل',request('status')) !!}
                                    </div>

                                    <div class="col-sm-3">
                                        {!! \Helper\Field::select('type' ,__('النوع'), $wo->getTypes(),null,'select2','الكل',request('type')) !!}
                                    </div>

                                    <div class="col-sm-3">
                                        {!! \Helper\Field::datePicker('due' ,__('من'),request('due')) !!}
                                    </div>

                                    <div class="col-sm-3">
                                        {!! \Helper\Field::datePicker('due_to' ,__('الي'),request('due_to')) !!}
                                    </div>
                                    @include('layouts.partials.append-per-page-to-filters')
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-warning" id="reset_form"><i
                                        class="fa fa-recycle"></i>{{__('تفريغ الحقول')}}</button>
                                <button type="button" class="btn btn-white"
                                        data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        {{--<div class="row">--}}
        {{--<div class="col-sm-3">--}}
        {{--<input type="text" class="form-control" placeholder="بحث برقم أمر العمل">--}}
        {{--</div>--}}
        {{--<div class="col-sm-3">--}}
        {{--<input type="text" class="form-control" placeholder="الجهاز">--}}
        {{--</div>--}}
        {{--<div class="col-sm-3">--}}
        {{--<input type="text" class="form-control" placeholder="رقم السريال">--}}
        {{--</div>--}}
        {{--<div class="col-sm-3">--}}
        {{--<button class="btn btn-primary btn-block">بحث</button>--}}
        {{--</div>--}}
        {{--</div>--}}

        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{ __('رقم أمر العمل') }}</th>
                    <th class="text-center">{{ __('كود  الجهاز') }}</th>
                    <th class="text-center">{{ __('التاريخ') }}</th>
                    <th class="text-center">{{ __('النوع') }}</th>
                    <th class="text-center">{{ __('الحالة') }}</th>
                    <th class="text-center">{{ __('اسم المبلغ') }}</th>
                    <th class="text-center">{{ __('الشكوى') }}</th>
                    {{--                    <th class="text-center">اضافه تعليق</th>--}}

                    {{--                    <th class="text-center">{{ __('تحديث') }}</th>--}}
                    @can('حذف أوامر العمل')
                    <th class="text-center">{{ __('حذف') }}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                        <tr id="removable{{$record->id}}">
                            <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                            <td class="text-center">
                                <a href="{{url('admin/operations/'. $record->id)}}">
                                    WO-{{$record->id}}
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{url('admin/devices?code='.optional($record->device)->code)}}">
                                    {{optional($record->device)->code}}<br>
                                    {{optional(optional($record->device)->deviceName)->name}}
                                </a>
                            </td>
                            <td class="text-center">{{$record->due_to}}</td>
                            <td class="text-center">
                                @if($record->type == 'maintenance')
                                    {{ __('صيانة') }}
                                @else
                                    {{ __('إصلاح عطل') }}
                                @endif
                            </td>
                            <td class="text-center">
                                @if($record->status == 'open')
                                    {{ __('جديد') }}
                                @elseif($record->status == 'done')
                                    {{ __('مكتمل') }}
                                @else
                                    {{ __('غير مكتمل') }}

                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{url('admin/user?id='.$record->reporter_id)}}">
                                    {{optional($record->reporter)->id}}<br>
                                    {{optional($record->reporter)->name}}
                                </a>
                            </td>
                            <td class="text-center">{{optional($record->complaint)->body}}</td>
                            {{--<td class="text-center">--}}
                            {{--@if($record->status != 'done')--}}
                            {{--<a href="{{url('admin/operations/'. $record->id .'/edit')}}"--}}
                            {{--class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>--}}
                            {{--@endif--}}
                            {{--</td>--}}
                            @can('حذف أوامر العمل')
                            <td class="text-center">
                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                        data-route="{{url('admin/operations/'.$record->id)}}"
                                        type="button" class="destroy btn btn-danger btn-xs"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @include('layouts.partials.per-page')
                <div class="text-center">
                    {!! $records->appends(request()->query())->render() !!}
                </div>
            </div>
        </div>
    </div>






@endsection
