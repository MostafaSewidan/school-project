@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف أمر عمل')
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>'OperationsController@store',
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'POST'
                                ])!!}
        <div class="ibox-content">
            @include('admin.operations.form')
        </div>
        <div class="ibox-footer">

            {!! \Helper\Field::ajaxBtn(__('إنشاء أمر عمل')) !!}
        </div>
        {!! Form::close()!!}
    </div>
@stop
