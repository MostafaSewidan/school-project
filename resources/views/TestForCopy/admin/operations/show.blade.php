@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('عرض أمر عمل')
                                ])

@section('content')

    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <div class="btn-group hidden-print pull-right">
                    <a class="btn btn-default btn-sm text-light-blue hidden-print"
                       href="{{url(route('operations.single-print',$record->id))}}"
                       target="_blank">
                        <i class="fa fa-print"></i>
                    </a>
                    <a class="btn btn-default text-green btn-sm"
                       href="{{url(route('master-export', 'showOperations'))}}"><i
                                class="fa fa-file-excel-o"></i></a>
                    <a class="btn btn-danger btn-sm" href="#">
                        <i class="fa fa-file-pdf-o"></i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="ibox-content">

                {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                {{--<div class="m-b-md">--}}
                {{--<a href="{{url('admin/operations/'. $record->id .'/edit')}}" class="btn btn-success btn-xs float-right">--}}
                {{--<i class="fa fa-edit"></i> تعديل--}}
                {{--</a>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                @include('layouts.partials.validation-errors')
                @include('flash::message')
                <div class="row">
                    <div class="col-xs-6">
                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('رقم أمر العمل') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                <span class="label label-info">
                                    WO-{{$record->id}}
                                </span>
                                </dd>
                            </div>
                        </dl>
                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('الحالة') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                <span class="label label-primary">
                                    {{$record->getStatus()[$record->status]}}
                                </span>
                                </dd>
                            </div>
                        </dl>
                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('انشأ من قبل') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                    {{optional($record->reporter)->name}}
                                </dd>
                            </div>
                        </dl>

                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('النوع') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                    @if($record->type == 'maintenance')
                                        {{ __('صيانة') }}
                                    @else
                                        {{ __('إصلاح عطل') }}
                                    @endif
                                </dd>
                            </div>
                        </dl>
                    </div>
                    <div class="col-xs-6" id="cluster_info">
                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('اخر تحديث') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                    {{$record->updated_at->toDayDateTimeString()}}
                                </dd>
                            </div>
                        </dl>
                        <dl class="row mb-0">
                            <div class="col-xs-4 text-sm-right">
                                <dt>{{ __('تاريخ الإنشاء') }}:</dt>
                            </div>
                            <div class="col-xs-8 text-sm-left">
                                <dd class="mb-1">
                                    {{$record->created_at->toDayDateTimeString()}}
                                </dd>
                            </div>
                        </dl>
                        @if($record->status == 'done')
                            <dl class="row mb-0">
                                <div class="col-xs-4 text-sm-right">
                                    <dt>{{ __('تاريخ الإكتمال') }}:</dt>
                                </div>
                                <div class="col-xs-8 text-sm-left">
                                    <dd class="mb-1">
                                        {{$record->completed_at->toDayDateTimeString()}}
                                    </dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-xs-4 text-sm-right">
                                    <dt>{{ __('المدة') }}:</dt>
                                </div>
                                <div class="col-xs-8 text-sm-left">
                                    <dd class="mb-1">
                                        {{$record->completed_at->diffInDays($record->created_at)}}
                                        {{__('يوم')}}
                                    </dd>
                                </div>
                            </dl>
                        @endif
                    </div>
                    <div class="col-xs-12">
                        <dl class="row mb-0">
                            <div class="col-xs-2 text-sm-right">
                                <dt>{{ __('الشكوى') }}:</dt>
                            </div>
                            <div class="col-xs-10 text-sm-left">
                                <dd class="mb-1">
                                    {{optional($record->complaint)->body}}
                                </dd>
                            </div>
                        </dl>
                    </div>
                </div>
                <div class="row m-t-sm hidden-print">
                    <div class="col-lg-12">
                        <div class="panel blank-panel">
                            <div class="panel-heading">
                                <div class="panel-options">
                                    <ul class="nav nav-tabs">
                                        <li><a class="nav-link active">{{ __('الملاحظات والإجراءات') }}</a></li>
                                        @can('تعديل أوامر العمل')
                                            @if($record->status != 'done')
                                                <div class="text-center">
                                                    <button class="btn btn-info" data-toggle="modal"
                                                            data-target="#choose-delivery">
                                                        <i class="fa fa-edit"></i> {{__('تحديث الحالة')}}
                                                    </button>
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                            data-target="#closeWO">
                                                        <i class="fa fa-check"></i> {{__('إتمام أمر العمل')}}
                                                    </button>
                                                </div>
                                            @endif
                                        @endcan
                                    </ul>

                                    @include('admin.operations.model')
                                </div>
                            </div>

                            <div class="panel-body">

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-1">
                                        <div class="feed-activity-list">
                                            @php $notes = $record->comments()->oldest()->paginate(10); @endphp
                                            @foreach($notes as $note)
                                                <div class="feed-element">
                                                    {{--<a href="{{url('admin/user?id='.$note->user_id)}}" class="float-left">--}}
                                                    {{--<img alt="image" class="rounded-circle" width="50" height="50" src="{{optional($note->user)->photo}}">--}}
                                                    {{--</a>--}}

                                                    <div class="media-body ">
                                                        <small class="float-right">
                                                        </small>
                                                        <strong>{{optional($note->user)->name}}</strong>
                                                        <br>
                                                        <small class="text-muted">
                                                            {{$record->created_at->toDayDateTimeString()}}
                                                        </small>
                                                        <div class="well">
                                                            {{$note->body}}
                                                        </div>
                                                        @foreach($note->attachment as $key => $file)
                                                            @if(isset($file[0]))
                                                                <div class="file-box" style="width: 120px">
                                                                    <div class="file">
                                                                        <a href="{{$file[0]}}">
                                                                            <span class="corner"></span>
                                                                            <div class="icon" style="height: 60px">
                                                                                <i class="fa fa-file"
                                                                                   style="font-size: 30px"></i>
                                                                            </div>
                                                                            <div class="file-name text-center">
                                                                                {{\Helper\Helper::getFileNameByUrl($file[0])}}
                                                                            </div>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                                {{--<a href="{{$file[0]}}" target="_blank"--}}
                                                                {{--class="btn btn-success btn-sm">{{ __('مرفق') }}--}}
                                                                {{--({{$note->id}})</a>--}}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach

                                            <div class="text-center">
                                                {!! $notes->appends(request()->except('page'))->render() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-content">
                @push('styles')
                    <style>
                        @media print {
                            a[href]:after {
                                content: none !important;
                            }
                        }
                    </style>
                @endpush
                <div class="table-responsive">
                    <table class="table m-b-xs">
                        <tbody>
                        <tr>
                            <td>
                                {{ __('كود الجهاز') }} : <strong><a
                                            href="{{url(route('devices.show',$record->device_id))}}">
                                        {{optional($record->device)->code}}
                                    </a></strong>
                            </td>
                            <td>
                                {{ __('الرقم التسلسلي') }}:
                                <strong>{{optional($record->device)->serial_number}}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('القسم') }} : <strong><a
                                            href="{{url(route('devices.index',['department' => optional($record->device)->department_id]))}}">
                                        {{optional(optional($record->device)->department)->name}}
                                    </a></strong>
                            </td>
                            <td>
                                {{ __('الشركات المصنعة') }}: <strong><a
                                            href="{{url(route('devices.index',['manufacturer' => optional($record->device)->manufacturer_id]))}}">
                                        {{optional(optional($record->device)->manufacturer)->name}}
                                    </a></strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('المورد') }} : <strong><a
                                            href="{{url(route('importers.index',['id' => optional($record->device)->importer_id]))}}">
                                        {{optional(optional($record->device)->importer)->name}}
                                    </a></strong>
                            </td>

                            <td>
                                {{ __('نوع الجهاز') }}:
                                <strong>{{optional(optional($record->device)->deviceType)->name}}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('رقم الغرفة') }} : <strong>{{optional($record->device)->room_number}}</strong>
                            </td>
                            <td>
                                {{ __('الموديل') }}: <strong>{{optional($record->device)->model}}</strong>
                            </td>
                        </tr>
                        {{--                        <tr>--}}
                        {{--                            --}}{{--                            <td>--}}
                        {{--                            --}}{{--                                اسم الجهاز: <strong>{{optional($record->device->deviceName)->name}}</strong>--}}
                        {{--                            --}}{{--                            </td>--}}
                        {{--                            <td>--}}
                        {{--                                قيمة الزياره : <strong>{{optional($record->device)->visit_cost}}</strong>--}}

                        {{--                            </td>--}}
                        {{--                        </tr>--}}
                        <tr>
                            <td>
                                {{ __('تاريخ تركيب الجهاز') }} :
                                <strong>{{optional($record->device)->constructing_date}}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ تركيب الجهاز (هجريا)') }} :
                                <strong>{{optional($record->device)->hijri_constructing_date}}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('الحالة') }} : <strong>{{optional($record->device)->statue_type}}</strong>
                            </td>
                            <td>
                                {{ __('الفئة') }} : <strong>{{optional($record->device)->category_type}}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                {{ __('الملاحظات') }} : <strong>{{optional($record->device)->notes}}</strong>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <h3 class="">{{ __('تفاصيل العقد') }}</h3>
                    <table class="table m-b-xs">
                        <tbody>

                        <tr>
                            <td>
                                {{ __('رقم العقد') }} : <strong>
                                    <a href="{{url('admin/contracts?contract_id='.optional($record->device)->contract_id)}}">
                                        {{optional(optional($record->device)->contract)->contract_number}}
                                    </a>
                                </strong>
                            </td>
                            <td>
                                {{ __('حاله العقد') }} :
                                <strong>{{optional(optional($record->device)->contract)->contract_typ}}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('تاريخ البداية') }} :
                                <strong>{{optional(optional($record->device)->contract)->contract_start}}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ البداية الهجري') }} :
                                <strong>{{optional(optional($record->device)->contract)->hijri_contract_start}}</strong>

                            </td>
                        </tr>


                        <tr>
                            <td>
                                {{ __('تاريخ النهاية') }} :
                                <strong>{{optional(optional($record->device)->contract)->contract_end}}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ النهاية الهجري') }} :
                                <strong>{{optional(optional($record->device)->contract)->hijri_contract_end}}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('قيمه العقد') }}:
                                <strong>{{optional(optional($record->device)->contract)->cost}}</strong>
                            </td>
                            <td>
                                {{ __('الخصم عن كل يوم عطل') }} :
                                <strong>{{optional(optional($record->device)->contract)->cut_per_day_down}}</strong>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                {{ __('الملاحظات') }} :
                                <strong>{{optional(optional($record->device)->contract)->notes}}</strong>

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
