@extends('layouts.app',[
                                'page_header'       => 'الاعدادات',
                                'page_description'  => 'اعدادات التطبيق'
                                ])
@section('content')
        <!-- general form elements -->
<div class="ibox">
    <!-- form start -->
    {!! Form::model($model,[
                            'url'=>url('admin/developer/setting/'.$model->id),
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files' => true
                            ])!!}

    <div class="ibox-content">
        <div class="clearfix"></div>
        <br>
        @include('admin.developer_setting.form')

        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
