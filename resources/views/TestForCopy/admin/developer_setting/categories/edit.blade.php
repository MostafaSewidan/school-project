@extends('layouts.app',[
                                'page_header'       => 'اقسام الاعدادات',
                                'page_description'  => "تعديل القسم"
                                ])

@section('content')
<div class="ibox">
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>['SettingCategoryController@update',$record->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files' => true

                            ])!!}
    <div class="ibox-content">
        @include('admin.developer_setting.categories.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">حفظ</button>
    </div>
    {!! Form::close()!!}
</div>
@stop

