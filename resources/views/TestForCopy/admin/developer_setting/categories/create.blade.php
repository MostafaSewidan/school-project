@extends('layouts.app',[
                                'page_header'       => 'اقسام الاعدادات',
                                'page_description'  => "أضف قسم"
                                ])

@section('content')

<div class="ibox">
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>'SettingCategoryController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}
    <div class="ibox-content">
        @include('admin.developer_setting.categories.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">حفظ</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
