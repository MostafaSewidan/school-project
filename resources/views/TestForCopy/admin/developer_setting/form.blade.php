@inject('categories' , App\Models\SettingsCategory)
{!! \Helper\Field::select('settings_category_id' , 'category', $categories->pluck('name','id')->toArray())!!}
{!! \Helper\Field::text('display_name' , 'label name ')!!}
{!! \Helper\Field::text('key' , 'field name') !!}
{!! \Helper\Field::text('value' , 'value') !!}
{!! \Helper\Field::text('validation' , 'validation' , $validation) !!}
{!!  \Helper\Field::select('data_type', 'data type' , [
   'fileWithPreview'=> 'fileWithPreview',
   'mulifileWithPreview'=> 'mulifileWithPreview',
   'editor'  => 'editor',
   'textarea'=> 'textarea',
   'number'  => 'number',
   'email'   => 'email',
   'date'    => 'date',
   'text'    => 'text',
   'radio'    => 'radio',
] ) !!}

<div style="@if(!$model->data_type == 'radio') display: none @endif" id="options-div">

    {!! \Helper\Field::text('options' , 'options EX(key,name|key,name)')!!}

</div>

{!! \Helper\Field::number('level' , 'level')!!}

@push('scripts')
    <script>
        $('#data_type').change(function () {

            if($('#data_type').val() === 'radio'){
                $('#options-div').show();
            }else {

                $('#options-div').hide();
            }

        });
    </script>
@endpush