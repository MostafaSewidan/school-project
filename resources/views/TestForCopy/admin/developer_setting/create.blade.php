@extends('layouts.app',[
                                'page_header'       => 'الاعدادات',
                                'page_description'  => 'اعدادات التطبيق'
                                ])
@section('content')
        <!-- general form elements -->
<div class="ibox">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'DeveloperSetting@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}

    <div class="ibox-content">

        @include('admin.developer_setting.form')

        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
