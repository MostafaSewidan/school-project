@extends('layouts.app',[
                                'page_header'       => __('الدعم الفني'),
                                'page_description'  => ''
                                ])
@section('content')

    <div class="ibox ibox-primary">
        <div class="ibox-content">
{{--        @include('layouts.partials.validation-errors')--}}
        @include('flash::message')

        <!-- general form elements -->

            <!-- form start -->
            {!! Form::open([
                                    'route'=>'admin.save-technical-support',
                                    'role'=>'form',
                                    'method'=>'post'
                                    ])!!}

            {!! \Helper\Field::text('name',__('الأسم')) !!}

            {!! \Helper\Field::text('email',__('البريد الالكتروني')) !!}

            {!! \Helper\Field::text('phone',__('رقم الهاتف')) !!}

            {!! \Helper\Field::text('title',__('العنوان')) !!}

            {!! \Helper\Field::textarea('message' ,__('المحتوى')) !!}

            {!! \Helper\Field::ajaxBtn(__('إرسال')) !!}


            {!! Form::close()!!}
        </div>
    </div>

@endsection
