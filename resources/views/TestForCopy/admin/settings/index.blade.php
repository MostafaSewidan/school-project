@extends('layouts.app',[
                                'page_header'       => __('الإعدادات'),
                                'page_description'  => __('إعدادات عامة')
                                ])
@section('content')

    <div class="ibox ibox-primary">
        <div class="ibox-content">
        @include('layouts.partials.validation-errors')
        @include('flash::message')

        <!-- general form elements -->

            <!-- form start -->
            {!! Form::model($record , [
                                    'url'=>url('admin/settings/'),
                                    'id'=>'ajaxForm',
                                    'role'=>'form',
                                    'method'=>'post',
                                    'files' => true
                                    ])!!}

            {!! \Helper\Field::text('site_name',__('اسم الموقع')) !!}
            {!! \Helper\Field::text('site_name_en',__('اسم الموقع').' EN ') !!}

            {!! \Helper\Field::text('region_name',__('المنطقة')) !!}

            {!! \Helper\Field::number('contract_num',__('رقم التعاقد')) !!}

            {!! \Helper\Field::text('contractor_name',__('اسم المقاول')) !!}
            {!! \Helper\Field::text('contractor_name_en',__('اسم المقاول').' EN') !!}
            {!! \Helper\Field::text('contractor_supervisor',__('مشرف المقاول')) !!}
            {!! \Helper\Field::text('site_manager',__('مدير الصيانة بالموقع')) !!}

{{--            {!! \Helper\Field::number('total_budget','الميزانية الإجمالية') !!}--}}
            <div class="row">
                <div class="col-sm-6">
                    {!! \Helper\Field::datePicker('project_start_date',__('تاريخ بدء المشروع')) !!}
                </div>
                <div class="col-sm-6">
                    {!! \Helper\Field::hijriDatePicker('hijri_project_start_date',__('تاريخ بدء المشروع هجريا')) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!! \Helper\Field::datePicker('project_end_date',__('تاريخ نهاية المشروع')) !!}
                </div>
                <div class="col-sm-6">
                    {!! \Helper\Field::hijriDatePicker('hijri_project_end_date',__('تاريخ نهاية المشروع هجريا')) !!}
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-6">
                    {!! \Helper\Field::number('daily_cut_class_a',__('قيمة الخصم اليومي لأجهزة فئة أ')) !!}
                </div>
                <div class="col-sm-6">
                    {!! \Helper\Field::number('cut_operator_class_a',__('معامل الخصم لأجهزة فئة أ')) !!}
                </div>
                <div class="col-sm-6">
                    {!! \Helper\Field::number('daily_cut_class_b',__('قيمة الخصم اليومي لأجهزة فئة ب')) !!}
                </div>
                <div class="col-sm-6">
                    {!! \Helper\Field::number('cut_operator_class_b',__('معامل الخصم لأجهزة فئة ب')) !!}
                </div>
            </div>

            {!! \Helper\Field::ajaxBtn('حفظ') !!}


            {!! Form::close()!!}
        </div>
    </div>

@endsection
