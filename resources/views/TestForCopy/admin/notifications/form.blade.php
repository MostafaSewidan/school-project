@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "أضف إشعار"
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::open([
                                'action'=>'NotificationController@store',
                                'id'=>'myForm',
                                'role'=>'form',
                                'files'=>true,
                                'method'=>'POST'
                                ])!!}
        <div class="ibox-content">

            @include('flash::message')
            <div class="row">
                <div class="col-sm-4">
                    @inject('governorate','App\Models\Governorate')
                    <div class="form-group">
                        {!! Form::select('governorate',$governorate->pluck('name','id')->toArray(),request('governorate'),[
                            'class' => 'form-control',
                            'id' => 'governorate',
                            'placeholder' => 'كل المحافظات'
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::select('city',[],request('city'),[
                            'class' => 'form-control',
                            'id' => 'city',
                            'placeholder' => 'كل المدن'
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::select('region',[],request('region'),[
                            'class' => 'form-control',
                            'id' => 'region',
                            'placeholder' => 'كل المناطق'
                        ]) !!}
                    </div>
                </div>
            </div>

            {!! \Helper\Field::multiSelect('types','نوع العميل', [
            'clients' => 'عملاء',
            'stores' => 'محلات',
            'deliveries' => 'ديليفاري',
            ]) !!}
            {!! \Helper\Field::text('title','عنوان الإشعار') !!}
            {!! \Helper\Field::textarea('body','تفاصيل الإشعار') !!}

            {!! \Helper\Field::select('send_type','نوع الإرسال', [
            'all' => 'الكل',
            'sms' => 'رسالة',
            'notification' => 'إشعار',
            ]) !!}


            {!! \Helper\Field::fileWithPreview('photo' , 'صورة') !!}
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">إرسال</button>
        </div>
        {!! Form::close()!!}
    </div>
@stop






