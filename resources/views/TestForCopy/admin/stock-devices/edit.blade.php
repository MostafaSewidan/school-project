@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "تعديل أعدادات الإفتراضية الطيارين"
                                ])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record,[
                                'action'=>['VehicleTypeController@update',$record->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT'
                                ])!!}
        <div class="ibox-content">
            @include('admin.vehicle-types.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>
        {!! Form::close()!!}
    </div>
@stop
