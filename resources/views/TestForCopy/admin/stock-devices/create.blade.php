@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "أضف جهاز"
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {{--{!! Form::model($record,[--}}
                                {{--'action'=>'GovernorateController@store',--}}
                                {{--'id'=>'myForm',--}}
                                {{--'role'=>'form',--}}
                                {{--'method'=>'POST'--}}
                                {{--])!!}--}}
        <div class="ibox-content">
            @include('admin.stock-devices.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>
{{--        {!! Form::close()!!}--}}
    </div>
@stop
