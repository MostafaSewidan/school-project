@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "الأجهزة"
                          ])

@section('content')
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                <a href="{{url(route('stock-devices.create'))}}" class="btn btn-info"><i class="fa fa-plus"></i> أضف جهاز</a>
            </div>
            <div class="col-xs-6 text-left">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-search"></i>
                    بحث متقدم
                </button>
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">بحث متقدم</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'كود الجهاز') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'الاسم') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::number('name' ,'رقم العقد') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'الرقم التسلسلي') !!}
                                    </div>

                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'النوع') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'القسم ') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'المُصنع  ') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! \Helper\Field::text('name' ,'البائع') !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('name' ,'المورد') !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('name' ,'الموقع') !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('name' ,'الفئة') !!}
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary"> <i class="fa fa-search"></i> بحث</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal">إغلاق</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        {{--<div class="row">--}}
            {{--<div class="col-sm-3">--}}
                {{--<input type="text" class="form-control" placeholder="بحث الاسم">--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<input type="text" class="form-control" placeholder="رقم العقد">--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<input type="text" class="form-control" placeholder="رقم الموديل">--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<button class="btn btn-primary btn-block">بحث</button>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="ibox-content">
            @include('flash::message')
                <div class="table-responsive">
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                        <th class="text-center">كود الجهاز</th>
                        <th class="text-center">الاسم</th>
                        <th class="text-center">الكمية</th>
                        <th class="text-center">رقم العقد</th>
                        <th class="text-center">نوع العقد</th>
                        <th class="text-center">رقم الموديل</th>
                        <th class="text-center">الرقم التسلسلي</th>
                        <th class="text-center">النوع</th>
                        <th class="text-center">النوع التفصيلي</th>
                        <th class="text-center">القسم</th>
                        <th class="text-center">القسم التفصيلي</th>
                        <th class="text-center">الغرفة</th>
                        <th class="text-center">الموظف المسئول</th>
                        <th class="text-center">المُصنع</th>
                        <th class="text-center">رقم هاتف المُصنع</th>
                        <th class="text-center">رقم فاكس المُصنع</th>
                        <th class="text-center">البريد الإلكتروني الخاص المُصنع</th>
                        <th class="text-center">البائع</th>
                        <th class="text-center">رقم هاتف البائع</th>
                        <th class="text-center">رقم فاكس البائع</th>
                        <th class="text-center">البريد الإلكتروني الخاص البائع</th>
                        <th class="text-center">المورد</th>
                        <th class="text-center">رقم هاتف المورد</th>
                        <th class="text-center">رقم فاكس المورد</th>
                        <th class="text-center">البريد الإلكتروني الخاص المورد</th>
                        <th class="text-center">الموقع</th>
                        <th class="text-center">المبني</th>
                        <th class="text-center">الطابق</th>
                        <th class="text-center">الفئة</th>
                        <th class="text-center">مدة الضمان</th>
                        <th class="text-center">تاريخ أمر الشراء</th>
                        <th class="text-center">تاريخ التنصيب</th>
                        <th class="text-center">تاريخ الإنتهاء</th>
                        <th class="text-center">مدة الحياة</th>
                        <th class="text-center">معامل الخطورة</th>
                        <th class="text-center">تعديل</th>
                        </thead>
                        <tbody>
                            @for($i=1;$i<=10;$i++)
                            <tr id="removable">
                                <td class="text-center">1209{{$i}}</td>
                                <td class="text-center">Opthalmic unit{{$i}}</td>
                                <td class="text-center">
                                    <?php $qnt = rand(1,15) ?>
                                    @if($qnt <= 5) <i class="fa fa-circle text-red"></i> @endif {{$qnt}}
                                </td>
                                <td class="text-center">1209{{$i}}</td>
                                <td class="text-center">Maintenance</td>
                                <td class="text-center">OPD12{{$i}}</td>
                                <td class="text-center">GYC100{{$i}}</td>
                                <td class="text-center">Opthalmic{{$i}}</td>
                                <td class="text-center">Opthalmic{{$i}}</td>
                                <td class="text-center">Out Patient{{$i}}</td>
                                <td class="text-center">Out Patient Clinic 2{{$i}}</td>
                                <td class="text-center">Out Patient Clinic 2{{$i}}</td>
                                <td class="text-center">M.Farouk{{$i}}</td>
                                <td class="text-center">NIDEK{{$i}} CO LTD</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">info@nidek{{$i}}.com</td>
                                <td class="text-center">AMCO{{$i}} Company</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">info@amco{{$i}}.com</td>
                                <td class="text-center">AMICO{{$i}}</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">0{{$i}}000000000</td>
                                <td class="text-center">info@amico{{$i}}.com</td>
                                <td class="text-center">El-eman{{$i}} Hospital</td>
                                <td class="text-center">Main Building{{$i}}</td>
                                <td class="text-center">G1{{$i}}</td>
                                <td class="text-center">Medical</td>
                                <td class="text-center">1 Year</td>
                                <td class="text-center">07-0{{$i}}-2018</td>
                                <td class="text-center">10-0{{$i}}-2018</td>
                                <td class="text-center">10-0{{$i}}-2019</td>
                                <td class="text-center">0</td>
                                <td class="text-center">10</td>
                                <td class="text-center">
                                    <a href="#"
                                       class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@endsection
