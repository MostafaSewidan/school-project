@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "مستخدمى التطبيق"
                          ])
@section('content')
    <div class="ibox-content">
        <div class="ibox-content">
            <div class="ibox-title">
                {{$client->name.' - '.$client->phone}} - <span>الاشعارات</span>
            </div>
        </div>
        @include('flash::message')
        @if(count($records))
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <thead>
                    <th>#</th>
                    <th>العنوان</th>
                    <th>الرسالة</th>
                    <th>صورة</th>
                    <th>تاريخ الارسال</th>
                    <th>حالة الارسال</th>
                    </thead>
                    <tbody>

                    @foreach($records as $record)
                        <tr id="removable{{$record->id}}">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$record->title}}</td>
                            <td>
                                {{$record->body}}
                                @if($record->notifiable instanceof App\Models\Order)
                                    <a href="{{url(route('orders.show',$record->notifiable->id))}}"
                                       class="btn btn-info btn-xs" target="_blank">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                @endif
                            </td>
                            <td>
                                @if($record->photo)
                                    <a href="{{$record->photo}}"
                                       data-lightbox="images">

                                        <img src="{{$record->photo}}" class="img-circle" width="50" height="50" alt="">

                                    </a>
                                @else
                                    لايوجد
                                @endif
                            </td>
                            <td>{{$record->created_at->diffForHumans(Carbon\Carbon::now())}}</td>
                            <td class="text-center">
                                @if($record->clients()->where('clients.id',$client->id)->first()->pivot->is_send)
                                    <span class="text-green"><i class="fa fa-check"></i></span>
                                @else
                                    <span class="text-red"><i class="fa fa-times-circle-o"></i></span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>
    <div class="text-center">
        {!! $records->render() !!}
    </div>
    @else
        <div>
            <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
        </div>
    @endif
@endsection
