<table class="">
    <tbody>
    <tr class="font-bold">
        <td>#</td>
        <td style="width: 20px;">الاسم</td>
        <td>النوع</td>
        <td style="width: 20px;">تاريخ الميلاد</td>
        <td style="width: 20px;">الهاتف</td>
        <td>عدد الطلبات</td>
        <td style="width: 20px;">تاريخ الانضمام</td>
        <td style="width: 20px;">آخر ظهور</td>
        <td>الحالة</td>
    </tr>
    @foreach($records as $record)
        <tr id="removable{{$record->id}}">
            <td>{{$loop->iteration}}</td>
            <td>{{$record->name}}</td>
            <td>
                @if($record->gender == 'male')
                    ذكر
                @elseif($record->gender == 'female')
                    انثى
                @else
                    --
                @endif
            </td>
            <td>{{$record->d_o_b}}</td>
            <td>{{$record->phone}}</td>
            <td>{{$record->delivered_orders_count}}</td>
            <td>{{$record->created_at}}</td>
            <td>{{$record->last_active}}</td>
            <td>
                @if($record->status=='active')
                    مفعل
                @elseif($record->status=='deactivate')
                    موقوف
                @else
                    غير مفعل
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
