@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "مستخدمى التطبيق"
                          ])
@section('content')
    <div class="ibox-content">
        <div class="ibox-content">
            <div class="ibox-title">
                {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('name',request()->input('name'),[
                            'placeholder' => 'الاسم',
                            'class' => 'form-control'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('phone',request()->input('phone'),[
                            'class' => 'form-control',
                            'placeholder' => 'الهاتف'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('from',request()->input('from'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'من تاريخ'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('to',request()->input('to'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'إلى تاريخ'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('status',[
                                'not_confirmed' => 'غير مفعل',
                                'active' => 'مفعل',
                                'deactivate' => 'موقوف',
                            ],request()->input('status'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'كل الحالات'
                            ]) !!}
                        </div>
                    </div>
                    {{--<div class="col-md-3">--}}
                        {{--@inject('governorate','App\Models\Governorate')--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::select('governorate_id',$governorate->pluck('name','id')->toArray(),request()->input('governorate_id'),[--}}
                            {{--'class' => 'form-control',--}}
                            {{--'placeholder' => 'كل المحافظات'--}}
                            {{--]) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    @include('layouts.partials.append-per-page-to-filters')
                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @include('flash::message')
        @if(count($records))
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <thead>
                    <th>#</th>
                    <th>الأسم</th>
                    <th>النوع</th>
                    <th>تاريخ الميلاد</th>
                    <th>الهاتف</th>
                    <th>عدد الطلبات </th>
                    <th>تاريخ الانضمام </th>
                    <th>آخر ظهور </th>
                    <th class="text-center">الحالة</th>
                    <th class="text-center">حذف</th>
                    </thead>
                    <tbody>

                    @foreach($records as $record)
                        <tr id="removable{{$record->id}}">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$record->name}}</td>
                            <td>
                                @if($record->gender == 'male')
                                    ذكر
                                @elseif($record->gender == 'female')
                                    انثى
                                @else
                                    --
                                @endif
                            </td>
                            <td>{{$record->d_o_b}}</td>
                            <td>{{$record->phone}}</td>
                            <td class="text-center">{{$record->delivered_orders_count}}</td>
                            <td>{{$record->created_at}}</td>
                            <td>{{$record->last_active->diffForHumans(Carbon\Carbon::now())}}</td>
                            <td class="text-center">
                                @if($record->status=='active')
                                    <a href="{{url(route('client.deactivate',$record->id))}}"
                                       class="btn btn-xs btn-danger"><i
                                                class="fa fa-close"></i> إيقاف</a>
                                @elseif($record->status=='deactivate')
                                    <a href="{{url(route('client.activate',$record->id))}}"
                                       class="btn btn-xs btn-success"><i
                                                class="fa fa-check"></i> تفعيل</a>
                                @else
                                    <span class="text-center">
                                    <span style="font-size: small" class="label label-primary bg-primary">لم يتم التفعيل بعد</span>
                                </span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if(!in_array($record->status,['active','deactivate']))
                                    <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('client.destroy',$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                                class="fa fa-trash"></i></button>

                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>
    <div class="text-center">
        {!! $records->render() !!}
    </div>

    <div class="text-left">
        <a href="{{url(route('client.export'))}}?from={{request('from')}}&to={{request('to')}}" class="btn btn-success btn-sm">
            <i class="fa fa-file-excel-o"></i> تحميل</a>
    </div>
    @else
        <div>
            <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
        </div>
    @endif
@endsection
