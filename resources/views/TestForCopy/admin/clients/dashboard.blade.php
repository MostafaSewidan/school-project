@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "مستخدمى التطبيق"
                          ])
@section('content')
    <div class="ibox-content">
        <div class="ibox-content">
            <div class="ibox-title">
                {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('name',request()->input('name'),[
                            'placeholder' => 'الاسم',
                            'class' => 'form-control'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('phone',request()->input('phone'),[
                            'class' => 'form-control',
                            'placeholder' => 'الهاتف'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('from',request()->input('from'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'من تاريخ'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('to',request()->input('to'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'إلى تاريخ'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        @inject('governorate','App\Models\Governorate')
                        <div class="form-group">
                            {!! Form::select('governorate',$governorate->pluck('name','id')->toArray(),request('governorate'),[
                                'class' => 'form-control',
                                'id' => 'governorate',
                                'placeholder' => 'كل المحافظات'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::select('city',$cities,request('city'),[
                                'class' => 'form-control',
                                'id' => 'city',
                                'placeholder' => 'كل المدن'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::select('region',$regions,request('region'),[
                                'class' => 'form-control',
                                'id' => 'region',
                                'placeholder' => 'كل المناطق'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('status',[
                                'not_confirmed' => 'غير مفعل',
                                'active' => 'مفعل',
                                'deactivate' => 'موقوف',
                            ],request()->input('status'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'كل الحالات'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('order',[
                                'created_at' => 'تاريخ الإنضمام',
                                'last_active' => 'أحدث ظهور',
                            ],request()->input('order'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'ترتيب حسب'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('app_version',[
                                'updated' => 'احدث إصدار',
                                'not_updated' => 'إصدارات سابقة',
                            ],request()->input('app_version'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'حالة التحديث'
                            ]) !!}
                        </div>
                    </div>
                    {{--<div class="col-md-3">--}}
                    {{--@inject('governorate','App\Models\Governorate')--}}
                    {{--<div class="form-group">--}}
                    {{--{!! Form::select('governorate_id',$governorate->pluck('name','id')->toArray(),request()->input('governorate_id'),[--}}
                    {{--'class' => 'form-control',--}}
                    {{--'placeholder' => 'كل المحافظات'--}}
                    {{--]) !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @include('layouts.partials.validation-errors')
        @include('flash::message')
        @if($records->count())
            @foreach($records as $record)
                <div class="col-lg-4 col-sm-3" id="removable{{$record->id}}">
                    <div class="contact-box center-version text-center">

                        <div class="@if($record->status == 'not_confirmed') bg-red @else bg-green @endif"
                             style="padding: 3px;margin-bottom: 5px"></div>

                        @if($record->app_version == \App\MyHelper\Helper::settingValue('version_client'))
                            <label class="label label-xs label-success label-rounded" style="float: right">
                                تم التحديث
                            </label>
                        @elseif($record->app_version == 'ios')
                            <label class="label label-xs label-success label-rounded" style="float: right">
                                مستخدم ايفون
                            </label>
                        @else
                            <label class="label label-xs label-danger label-rounded" style="float: right">
                                لم يتم التحديث
                            </label>
                        @endif

                        <div class="clearfix"></div>
                        <div class="font-bold">
                            <span style="width: 50px;height: 50px;
                            border-radius: 50px;line-height: 50px;background-color: #f1f1f1;
                            margin-bottom: 15px;margin-top: 15px;display: inline-block">
{{--                                #{{$record->id}}--}}
                                @if($record->gender == 'female')
                                    <i class="fa fa-2x fa-female"></i>
                                @else
                                    <i class="fa fa-2x fa-male"></i>
                                @endif
                            </span>
                        </div>
                        <h3 class="m-b-xs">@if($record->last_active->diffInMinutes(Carbon\Carbon::now()) > 5)<i
                                    class="fa fa-circle text-red"></i> @else<i
                                    class="fa fa-circle text-green"></i>@endif
                            <strong>{{$record->name ?? 'بدون اسم'}}</strong></h3>
                        <div class="font-bold">{{$record->phone}}</div>
                        <address class="m-t-md">
                            <div class="row">
                                <a href="@if($record->orders()->where('status','cart')->first())
                                {{url(route('orders.show',$record->orders()->where('status','cart')->first()->id))}}
                                @else # @endif"
                                   class="btn btn-xs btn-info btn-rounded"
                                   @if(!$record->orders()->where('status','cart')->first()) disabled="disabled" @endif>
                                    <i class="fa fa-shopping-cart"></i>
                                    @if($record->orders()->where('status','cart')->first())<span>السلة</span>
                                    @else<span>السلة</span>@endif
                                </a>

                                <a href="@if($record->currentOrders()->first()) {{url(route('orders.show',$record->currentOrders()->first()->id))}}
                                @else # @endif"
                                   class="btn btn-xs btn-info btn-rounded"
                                   @if(!$record->currentOrders()->first()) disabled="disabled" @endif>
                                    <i class="fa fa-shopping-cart"></i>
                                    @if($record->currentOrders()->first())<span>طلب حالي</span>
                                    @else<span>طلب حالي</span>@endif
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 text-center"><strong><span
                                                class="badge badge-primary">{{$record->wallet_balance}}</span>
                                        رصيد المحفظة</strong></div>
                                <div class="col-xs-6 text-center"><strong><span
                                                class="badge badge-primary">{{$record->points}}</span>
                                        النقاط</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 text-center"><strong><span
                                                class="badge badge-info">{{$record->deliveredOrders()->count()}}</span>
                                        طلب ناجح</strong></div>
                                <div class="col-xs-6 text-center"><strong><span
                                                class="badge badge-danger">{{$record->orders()->where('status','canceled')->count()}}</span>
                                        طلب ملغي</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12"><h4><strong>تاريخ الانضمام: {{$record->created_at}}</strong></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12"><h4><strong>اخر
                                            ظهور: {{$record->last_active->diffForHumans(Carbon\Carbon::now())}}</strong>
                                    </h4></div>
                            </div>
                        </address>
                        {{-- Foooooooooter  --}}
                        <div class="contact-box-footer">
                            @if($record->status == 'active')
                                <a href="{{url(route('client.deactivate',$record->id))}}"
                                   class="btn btn-rounded m-xxs btn-xs btn-success "><i class="fa fa-check"></i> مفعل
                                </a>
                                @if($record->orders()->where('status','!=','cart')->count())
                                    <a href="{{url(route('orders.index'))}}?client_id={{$record->id}}"
                                       class="btn btn-rounded btn-xs btn-primary">
                                        <span class="badge">{{$record->all_orders_count}}</span>
                                        الطلبات</a>
                                @endif
                                <a href="{{url(route('client.notifications',$record->id))}}"
                                   class="btn btn-rounded m-xxs btn-xs btn-info "><i class="fa fa-bell-o"></i> الاشعارات
                                </a>
                                <button data-toggle="modal" data-target="#notify{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-primary"><i
                                            class="fa fa-mobile-phone"></i>
                                    إرسال إشعار
                                </button>
                                <div class="modal fade text-right" id="notify{{$record->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="notify{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">إرسال إشعار</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@notify',$record->id],
                                                    'method' => 'post',
                                                    'files' => true

                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">عنوان الرسالة</label>
                                                    <input type="text" value="" name="title"
                                                           class="form-control" placeholder="عنوان الرسالة">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">محتوى الرسالة</label>
                                                    {!! Form::textarea('message',null,[
                                                        'class' => 'form-control'
                                                    ]) !!}
                                                </div>
                                                {!! \Helper\Field::fileWithPreview('photo' , 'صورة') !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">إرسال</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#send-sms{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-primary"><i class="fa fa-send-o"></i>
                                    إرسال
                                    رسالة
                                </button>
                                <div class="modal fade text-right" id="send-sms{{$record->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="send-sms{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">إرسال إشعار</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@sendSms',$record->id],
                                                    'method' => 'post'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">محتوى الرسالة</label>
                                                    {!! Form::textarea('message',null,[
                                                        'class' => 'form-control'
                                                    ]) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">إرسال</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#add-points{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-primary"><i class="fa fa-gift"></i>
                                    إضافة نقاط
                                </button>
                                <div class="modal fade text-right" id="add-points{{$record->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="add-points{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">إضافة نقاط</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@addPoints',$record->id],
                                                    'method' => 'post'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">نوع العملية</label>
                                                    {!! Form::select('type',[
                                                        'plus' => 'إضافة',
                                                        'minus' => 'خصم',
                                                    ],null,[
                                                        'class' => 'form-control'
                                                    ]) !!}
                                                </div>
                                                <div class="form-group">
                                                    <label for="">عدد النقاط</label>
                                                    {!! Form::number('amount',null,[
                                                        'class' => 'form-control',
                                                        'step' => 1
                                                    ]) !!}
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="send_sms"
                                                                   type="checkbox" value="1"
                                                                   id="defaultCheck1">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                إرسال رسالة قصيرة SMS
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="send_notification"
                                                                   type="checkbox" value="1"
                                                                   id="defaultCheck1">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                إرسال اشعار للتطبيق
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">إضافة</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#add-balance{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-primary"><i class="fa fa-money    "></i>
                                    إضافة رصيد للمحفظة
                                </button>
                                <div class="modal fade text-right" id="add-balance{{$record->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="add-balance{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">إضافة رصيد للمحفظة</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@addBalance',$record->id],
                                                    'method' => 'post'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">نوع العملية</label>
                                                    {!! Form::select('type',[
                                                        'plus' => 'إضافة',
                                                        'minus' => 'خصم',
                                                    ],null,[
                                                        'class' => 'form-control'
                                                    ]) !!}
                                                </div>
                                                <div class="form-group">
                                                    <label for="">المبلغ</label>
                                                    {!! Form::number('amount',null,[
                                                        'class' => 'form-control',
                                                        'step' => 1
                                                    ]) !!}
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="send_sms"
                                                                   type="checkbox" value="1"
                                                                   id="defaultCheck1">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                إرسال رسالة قصيرة SMS
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="send_notification"
                                                                   type="checkbox" value="1"
                                                                   id="defaultCheck1">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                إرسال اشعار للتطبيق
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">إضافة</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#edit{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-success"><i class="fa fa-edit"></i>
                                    تعديل
                                </button>
                                <div class="modal fade text-right" id="edit{{$record->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="edit{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">تعديل بيانات العميل</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@update',$record->id],
                                                    'method' => 'put'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">رقم الموبايل</label>
                                                    <input type="text" value="{{$record->phone}}" name="phone"
                                                           class="form-control" placeholder="رقم الموبايل">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الاسم</label>
                                                    <input type="text" value="{{$record->name}}" name="name"
                                                           class="form-control" placeholder="الاسم">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">تاريخ الميلاد</label>
                                                    <input type="text" value="{{$record->d_o_b}}" name="d_o_b"
                                                           class="form-control datepicker" placeholder="تاريخ الميلاد">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">كلمة السر</label>
                                                    <input type="password" name="password"
                                                           class="form-control" placeholder="كلمة السر">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::select('gender',[
                                                        'male' => 'ذكر',
                                                        'female' => 'أنثى',
                                                    ],null,[
                                                        'class' => 'form-control',
                                                        'placeholder' => 'النوع'
                                                    ]) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">حفظ البيانات</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @elseif($record->status=='deactivate')
                                <a href="{{url(route('client.activate',$record->id))}}"
                                   class="btn btn-rounded m-xxs btn-xs btn-danger"><i class="fa fa-times-circle-o"></i>
                                    موقوف</a>
                                @if($record->orders()->where('status','!=','cart')->count())
                                    <a href="{{url(route('orders.index'))}}?client_id={{$record->id}}"
                                       class="btn btn-rounded m-xxs btn-xs btn-primary">
                                        <span class="badge">{{$record->delivered_orders_count}}</span>
                                        الطلبات</a>
                                @endif
                                <button data-toggle="modal" data-target="#edit{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-success"><i class="fa fa-edit"></i>
                                    تعديل
                                </button>
                                <div class="modal fade text-right" id="edit{{$record->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="edit{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">تعديل بيانات العميل</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@update',$record->id],
                                                    'method' => 'put'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">رقم الموبايل</label>
                                                    <input type="text" value="{{$record->phone}}" name="phone"
                                                           class="form-control" placeholder="رقم الموبايل">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الاسم</label>
                                                    <input type="text" value="{{$record->name}}" name="name"
                                                           class="form-control" placeholder="الاسم">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">تاريخ الميلاد</label>
                                                    <input type="text" value="{{$record->d_o_b}}" name="d_o_b"
                                                           class="form-control datepicker" placeholder="تاريخ الميلاد">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">كلمة السر</label>
                                                    <input type="password" name="password"
                                                           class="form-control" placeholder="كلمة السر">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::select('gender',[
                                                        'male' => 'ذكر',
                                                        'female' => 'أنثى',
                                                    ],null,[
                                                        'class' => 'form-control',
                                                        'placeholder' => 'النوع'
                                                    ]) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">حفظ البيانات</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @else
                                <a href="#" class="btn btn-rounded m-xxs btn-xs btn-default"><i
                                            class="fa fa-warning"></i> غير
                                    مفعل</a>
                                <a href="{{url(route('client.send-pin-code',$record->id))}}"
                                   class="btn btn-rounded m-xxs btn-xs btn-primary"><i class="fa fa-lock"></i>
                                    إرسال كود التفعيل</a>
                                <button data-toggle="modal" data-target="#edit{{$record->id}}"
                                        class="btn btn-rounded m-xxs btn-xs btn-success"><i class="fa fa-edit"></i>
                                    تعديل
                                </button>
                                <div class="modal fade text-right" id="edit{{$record->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="edit{{$record->id}}Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">تعديل بيانات العميل</h4>
                                            </div>
                                            {!! Form::model($record,[
                                                    'action' => ['ClientController@update',$record->id],
                                                    'method' => 'put'
                                                ]) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="">رقم الموبايل</label>
                                                    <input type="text" value="{{$record->phone}}" name="phone"
                                                           class="form-control" placeholder="رقم الموبايل">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الاسم</label>
                                                    <input type="text" value="{{$record->name}}" name="name"
                                                           class="form-control" placeholder="الاسم">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">تاريخ الميلاد</label>
                                                    <input type="text" value="{{$record->d_o_b}}" name="d_o_b"
                                                           class="form-control datepicker" placeholder="تاريخ الميلاد">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">كلمة السر</label>
                                                    <input type="password" name="password"
                                                           class="form-control" placeholder="كلمة السر">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::select('gender',[
                                                        'male' => 'ذكر',
                                                        'female' => 'أنثى',
                                                    ],null,[
                                                        'class' => 'form-control',
                                                        'placeholder' => 'النوع'
                                                    ]) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">حفظ البيانات</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                        data-route="{{URL::route('client.destroy',$record->id)}}"
                                        class="destroy btn btn-rounded m-xxs btn-xs btn-danger"><i
                                            class="fa fa-trash"></i>
                                    حذف
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
                @if($loop->iteration % 3 == 0)
                    <div class="clearfix"></div>
                @endif
            @endforeach
            <div class="clearfix"></div>
            <div class="text-center">
                {!! $records->render() !!}
            </div>
        @else
            <div>
                <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
            </div>
        @endif
    </div>
@endsection
