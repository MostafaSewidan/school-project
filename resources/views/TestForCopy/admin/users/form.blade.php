{{--{!! \Helper\Field::text('name' , 'الاسم' )!!}--}}
{{--{!! \Helper\Field::email('email' , 'البريد الالكترونى' ) !!}--}}
{{--{!! \Helper\Field::password('password' , 'كلمة المرور' ) !!}--}}
{{--{!! \Helper\Field::password('password_confirmation' , 'تاكيد كلمة المرور') !!}--}}
{{--<br>--}}
{{--<div class="form-group" id="permissions_wrap">--}}
{{--    <label for="permissions">الرتب</label>--}}
{{--    <div class="">--}}
{{--        <br>--}}

{{--        @foreach( $roles as $role)--}}

{{--            @if($model->hasRole($role->name))--}}

{{--                {!! \Helper\Field::checkBox('roles', $role  , 'checked') !!}--}}
{{--            @else--}}
{{--                {!! \Helper\Field::checkBox('roles', $role ) !!}--}}
{{--            @endif--}}

{{--        @endforeach--}}
{{--    </div>--}}
{{--    <br>--}}
{{--    <br>--}}

{{--</div>--}}
{{--<div class="clearfix"></div>--}}
{{--<hr>--}}
{{--<div class="form-group" id="governorates_wrap">--}}
{{--    <label for="governorates"> صلاحيات المحافظات</label>--}}
{{--    <div class="">--}}
{{--        <br>--}}

{{--        @foreach( $governorates as $governorate)--}}

{{--            @if($model->governorates()->find($governorate->id))--}}

{{--                {!! \Helper\Field::checkBox('governorates', $governorate  , 'checked','name') !!}--}}
{{--            @else--}}
{{--                {!! \Helper\Field::checkBox('governorates', $governorate ,'','name') !!}--}}
{{--            @endif--}}

{{--        @endforeach--}}
{{--    </div>--}}
{{--    <br>--}}
{{--    <br>--}}

{{--</div>--}}

{{--<div class="clearfix"></div>--}}

@inject('departments' , App\Models\Department)
{!! \App\MyHelper\Field::text('name' , __('الأسم'))!!}
{!! \App\MyHelper\Field::email('email' , __('البريد الالكتروني')) !!}
{!! \App\MyHelper\Field::password('password' , __('كلمة المرور')) !!}
{!! \App\MyHelper\Field::password('password_confirmation' , __('تاكيد كلمة المرور')) !!}
{!!\Helper\Field::select('department_id' , 'ربط المستخدم بقسم معين', $departments->pluck('name','id')->toArray()) !!}
<br>
<div class="form-group" id="permissions_wrap">
    <label for="permissions">{{__('الرتب')}}</label>
    <div class="">
        <br>

        @foreach( $roles as $role)
            {{--{{dd($role->name)}}--}}

            {{--                @if($model->hasRole($role->name))--}}
            {{--                {!! Form::checkBox('roles[]',$role->id) !!}--}}
            {{--                <label for="{{$role->name}}">{{$role->name}}</label>--}}
            {{--                @else--}}

            {{--                @endif--}}

            {{--@if($model->hasRole($role->name))--}}
            {{--{!! Form::checkBox('roles[]',$role->id) !!}--}}
            {{--<label for="{{$role->name}}">{{$role->name}}</label>--}}
            {{--@else--}}
            {{--{!! Form::checkBox('roles[]',$role->id) !!}--}}
            {{--<label for="{{$role->name}}">{{$role->name}}</label>--}}
            {{--@endif--}}

                @if(in_array($role->id,[1,2]) && !auth()->user()->hasRole('Technical Support'))
                    @continue
                @endif
                {!! Form::checkBox('roles[]',$role->id) !!}
                <label for="{{$role->name}}">{{$role->name}}</label>
        @endforeach
    </div>


    <br>
    <br>

</div>
