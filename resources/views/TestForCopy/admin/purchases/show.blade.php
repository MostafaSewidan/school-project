@extends('layouts.app',[
                                'page_header'       => __('التعميدات'),
                                'page_description'  => $record->po_num
                                ])
@section('content')


    <div class="ibox">
        <div class="ibox-content">
            @push('styles')
                <style>
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }
                </style>
            @endpush
            @include('flash::message')
            <div class="table-responsive">
                <table class="table m-b-xs">
                    <tbody>
                    <tr>
                        <td>
                            {{__('رقم التعميد')}} : <strong>{{$record->po_num}}</strong>
                        </td>
                        <td>
                            {{__('معمد الى')}} : <strong>{{$record->starts_at}}</strong>
                        </td>
                    </tr>

                    <tr>

                        <td>
                            {{__('معمد من')}}: <strong>{{optional($record)->by}}</strong>
                        </td>
                        <td>
                            {{__('معمد الى')}}: <strong>{{optional($record)->to}}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        {{__('صلاحية التعميد بالشهر')}} : <strong>{{$record->valid_months}}</strong>
                        </td>

                        <td>
                            {{__('قيمتة بالريال')}}: <strong>{{$record->price}}</strong>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('حالة التعميد')}} : <strong>{{$record->statue_type}}</strong>
                        </td>
                        <td>
                            {{__('أسم القطعة')}}: <strong>{{$record->part_name}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('كود الجهاز')}} : <strong>
                                <a href="{{url('/admin/devices/'.optional(optional($record->workingOrder)->device)->id)}}">
                                    {{optional(optional($record->workingOrder)->device)->code}}
                                </a>
                            </strong>
                        </td>
                        <td>
                            {{__('القسم')}} : <strong>{{optional(optional(optional($record->workingOrder)->device)->department)->name}}</strong>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('رقم الجهاز')}} : <strong>{{optional(optional($record->workingOrder)->device)->serial_number}}</strong>
                        </td>
                        <td>
                            {{__('وصف العطل')}} : <strong>{{optional(optional($record->workingOrder)->device)->notes}}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            {{__('مصدر العرض')}} : <strong>{{$record->offered_by}}</strong>
                        </td>
                        <td>
                            {{__('مدة التوريد')}} : <strong>{{$record->period}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('رقم الفاتورة')}} : <strong>{{$record->bill_num}}</strong>
                        </td>
                        <td>
                            {{__('رقم الفاتورة')}} : <strong></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('تاريخ التوريد')}} : <strong{{$record->import_date}}></strong>
                        </td>
                        <td>
                            {{__('رقم الدفعة')}} : <strong>{{$record->payment_num}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('رقم الرفع للوزارة')}} : <strong>{{$record->ministry_num	}}</strong>
                        </td>
                        <td>
                            {{__('تاريخ الصادر')}} : <strong>{{$record->export_date	}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('حاله التنفيذ')}} : <strong>{{$record->isdone_type}}</strong>
                        </td>
                        <td>
                            {{__('حالة الدفع')}} : <strong>
                                {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/purchases-paid/toggle-boolean/'.$record->id.'/is_paid/'),'is_paid') !!}
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('هل تم التوريد')}} : <strong>
                                {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/purchases-imported/toggle-boolean/'.$record->id.'/is_paid/'),'is_imported') !!}
                            </strong>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

