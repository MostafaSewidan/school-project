@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('تعديل تعميد')
                                ])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($record,[
                                'action'=>['PurchasesController@update',$record->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT',
                                    'files' => true
                                ])!!}
        <div class="ibox-content">
            @include('admin.purchases.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
        </div>
        {!! Form::close()!!}
    </div>
@stop
