{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')
@inject('orders',App\Models\WorkingOrder)
@php
$order = $orders->options();

@endphp

<div class="row">
    <div class="col-sm-4">
        {!! \Helper\Field::text('po_num' ,__('رقم التعميد')) !!}
    </div>
    <div class="col-sm-4">
        {!! \Helper\Field::text('by' ,__('معمد من')) !!}
    </div>
    <div class="col-sm-4">
        {!! \Helper\Field::text('to' ,__('معمد الى')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::datePicker('starts_at' ,__('تاريخ البداية')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::hijriDatePicker('hijri_starts_at' ,__('تاريخ البداية الهجري')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::text('valid_months' ,__('صلاحية التعميد بالشهر')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::text('price' ,__('قيمتة بالريال')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::select('is_done' ,__('الحالة'),
        ['outlet'=>'منفذ' ,
        'not_enforced'=>'غير منفذ',
        'under_procedure'=>'تحت الاجراء',
        'other'=>'اخري',
        ]) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::text('part_name' ,__('أسم القطعة')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::select('working_order_id' ,__('أمر العمل'),$order) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::text('offered_by' ,__('مصدر العرض')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::number('period' ,__('مدة التوريد')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::text('bill_num' ,__('رقم الفاتورة')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::datePicker('import_date' ,__('تاريخ التوريد')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::hijriDatePicker('hijri_import_date' ,__('تاريخ التوريد الهجري')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::text('payment_num' ,__('رقم الدفعة')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::text('ministry_num' ,__('رقم الرفع للوزارة')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::datePicker('export_date' ,__('تاريخ الصادر')) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::hijriDatePicker('hijri_export_date' ,__('تاريخ الصادر الهجرى')) !!}
    </div>
</div>

{!! \Helper\Field::fileWithPreview('attachments',__('مرفقات'),true) !!}

@if(!empty($model->attachment))

    @foreach($model->attachmentRelation()->whereNull('usage')->get() as  $attachment)

        <div class="col-md-3" id="removable{{$attachment->id}}">
            <div class="text-center"
                 style="width: 100%;color: white;background-color: black;font-size: 3rem;font-weight: bolder;">
                {{$loop->iteration}}
            </div>
            <img src="{{asset($attachment->path)}}" class="img-responsive" alt="">
            <div class="clearfix"></div>
            <button id="{{$attachment->id}}" data-token="{{ csrf_token() }}"
                    data-route="{{URL::route('photo.destroy',$attachment->id)}}"
                    type="button" class="destroy btn btn-danger btn-xs btn-block">
                <i class="fa fa-trash"></i>
            </button>
        </div>
        <br>
        <br>
    @endforeach
    <div class="clearfix"></div>
    <br>
@endif
