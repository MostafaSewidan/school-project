@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('التعميدات')
                          ])

@section('content')
    <div class="ibox-content">

        <div class="row">
            <div class="col-xs-6">
                @can('إضافة التعميدات')
                <a href="{{url(route('purchases.create'))}}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> {{ __('أضف تعميد') }}</a>
                @endcan
                @can('حذف كل التعميدات')
                <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                      action="{{route('purchases.delete-all')}}" style="display: inline-block">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-danger btn-sm hidden-print"><i
                            class="fa fa-trash"></i>{{__('حذف الكل')}}</button>
                </form>
                    @endcan
            </div>
            {!! Form::open([
     'method' => 'get'
     ]) !!}
            <div class="btn-group" style="float: left">
                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                   href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل التعميدات')
                <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'purchases'))}}"><i class="fa fa-file-excel-o"></i></a>
                @endcan
                {{--@can('تصدير التعميدات pdf')--}}
                {{--<a class="btn btn-danger btn-sm" href="#">--}}
                    {{--<i class="fa fa-file-pdf-o"></i>--}}
                {{--</a>--}}
                {{--@endcan--}}
            </div>
            <div class="col-xs-6 text-left">
                <br>
{{--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
{{--                    <i class="fa fa-search"></i>--}}
{{--                    بحث متقدم--}}
{{--                </button>--}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('بحث متقدم') }}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('po_num' ,__('رقم التعميد'),request('po_num')) !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('device' ,__('اسم الجهاز'),request('device')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('device_code' ,__('كود الجهاز'),request('device_code')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('by' ,__('تعميد من'),request('by')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('to' ,__('تعميد إلى'),request('to')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('valid_months' ,__('صلاحية التعميد'),request('valid_months')) !!}
                                    </div>
{{--                                    <div class="col-sm-4">--}}
{{--                                        {!! \Helper\Field::select('per_page' ,__('عدد النتائج بالصفحة'),[--}}
{{--                                            '10' => '10',--}}
{{--                                            '15' => '15',--}}
{{--                                            '50' => '50',--}}
{{--                                            '75' => '75',--}}
{{--                                            '100' => '100'--}}
{{--                                        ],request('per_page'),'',__('عدد النتائج بالصفحة')) !!}--}}
{{--                                    </div>--}}
                                    @include('layouts.partials.append-per-page-to-filters')
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-warning" id="reset_form"><i class="fa fa-recycle"></i>{{__('تفريغ الحقول')}}</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="clearfix"></div>
        <br>
     @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">

                @if(!empty($records) && count($records)>0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{ __('رقم التعميد') }} </th>
                    <th class="text-center">{{ __('معمد من') }} </th>
                    <th class="text-center"> {{ __('معمد الى') }} </th>
                    <th class="text-center">{{ __('تاريخ البداية') }}</th>
                    <th class="text-center">{{ __('صلاحية التعميد بالشهر') }}</th>
                    <th class="text-center">{{ __('قيمتة بالريال') }}</th>
                    <th class="text-center">{{ __('حالة التعميد') }}</th>
                    <th class="text-center">{{ __('أسم القطعة') }} </th>

                    <th class="text-center">{{ __('كود الجهاز') }}</th>
                    <th class="text-center">{{ __('القسم') }}</th>
                    <th class="text-center">{{ __('رقم الجهاز') }}</th>
                    <th class="text-center">{{ __('وصف العطل') }} </th>
                    <th class="text-center"> {{ __('مصدر العرض') }} </th>
                    <th class="text-center"> {{ __('مدة التوريد') }}  </th>


                    <th class="text-center"> {{ __('رقم الفاتورة') }} </th>
                    <th class="text-center">{{ __('تاريخ التوريد') }}</th>
                    <th class="text-center">{{ __('رقم الدفعة') }}</th>
                    <th class="text-center">{{ __('رقم الرفع للوزارة') }}</th>
                    <th class="text-center">{{ __('تاريخ الصادر') }}</th>
                    <th class="text-center">{{ __('حاله التنفيذ') }}</th>
                    <th class="text-center"> {{ __('حالة الدفع') }}  </th>
                    <th class="text-center"> {{ __('هل تم التوريد') }} </th>
                    @can('تعديل التعميدات')
                    <th class="text-center">{{ __('تعديل') }}</th>
                    @endcan
                    @can('حذف التعميدات')
                    <th class="text-center">{{ __('حذف') }}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)

                        <tr id="removable{{$record->id}}">
                            <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                            <td class="text-center">
                                <a href="{{url(route('purchases.show',$record->id))}}">
                                    {{$record->po_num}}
                                </a>
                            </td>
                            <td class="text-center">{{$record->by}}</td>
                            <td class="text-center">{{$record->to}}</td>
                            <td class="text-center">{{$record->starts_at}}</td>
                            <td class="text-center">{{$record->valid_months}} </td>
                            <td class="text-center">{{$record->price}} </td>
                            <td class="text-center">{{$record->statue_type}} </td>
                            <td class="text-center">{{$record->part_name}} </td>
                            <td class="text-center">
                                <a href="{{url('/admin/devices/'.optional(optional($record->workingOrder)->device)->id)}}">
                                    {{optional(optional($record->workingOrder)->device)->code}}
                                </a>
                            </td>
                            <td class="text-center">{{optional(optional(optional($record->workingOrder)->device)->department)->name}} </td>
                            <td class="text-center">{{optional(optional($record->workingOrder)->device)->serial_number}} </td>
                            <td class="text-center">{{optional(optional($record->workingOrder)->device)->notes}} </td>
                            <td class="text-center">{{$record->offered_by}} </td>
                            <td class="text-center">{{$record->period}} </td>


                            <td class="text-center">{{$record->bill_num}} </td>
                            <td class="text-center">{{$record->import_date}} </td>
                            <td class="text-center">{{$record->payment_num}} </td>
                            <td class="text-center">{{$record->ministry_num	}} </td>
                            <td class="text-center">{{$record->export_date	}} </td>
                            <td class="text-center">
                                    {{$record->isdone_type}}
                            </td>
                            <td class="text-center">

                                {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/purchases-paid/toggle-boolean/'.$record->id.'/is_paid/'),'is_paid') !!}
                            </td>
                            <td class="text-center">                                {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/purchases-imported/toggle-boolean/'.$record->id.'/is_paid/'),'is_imported') !!}
                            </td>
                            @can('تعديل التعميدات')
                            <td class="text-center">
                                <a href="{{url('admin/purchases/'.$record->id.'/edit')}}"
                                   class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                            </td>
                            @endcan
                            @can('حذف التعميدات')
                            <td class="text-center">
                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                        data-route="{{url('admin/purchases/'.$record->id)}}"
                                        type="button" class="destroy btn btn-danger btn-xs"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                            @endcan
                        </tr>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @include('layouts.partials.per-page')
           <div class="text-center">
               {!! $records->appends(request()->query())->render() !!}
           </div>
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> {{ __('لا توجد بيانات للعرض') }} </h3>
                </div>
            @endif
        </div>
        </div>

    </div>
@endsection
