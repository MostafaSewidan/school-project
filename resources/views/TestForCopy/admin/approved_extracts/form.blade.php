@include('flash::message')


{!! \Helper\Field::datePicker('extract_month',__('الشهر')) !!}


<table id="items" class="data-table table table-bordered">
    <tbody>
    @if($edit ?? '' == true)
        @foreach($record->approvedItems()->get() as $item)
            <tr id="removable{{$item->item_id}}">
                <td class="text-center">{{$item->item->name}}</td>
                <td class="">

                    {!! \Helper\Field::number('total_amount[]',__('القيمه'),$item->amount   ) !!}
                    <input type="hidden" name="item_id[]" value="{{$item->item_id}}">
                </td>
        @endforeach
    @else
        @foreach($records as $record)
            <tr id="removable{{$record->id}}">
                <td class="text-center">{{$record->name}}</td>
                <td class="">

                    {!! \Helper\Field::number('total_amount[]',__('القيمه'), 0) !!}
                    <input type="hidden" name="item_id[]" value="{{$record->id}}">
                </td>
        @endforeach
    @endif
    </tbody>
</table>
{{--<div class="form-group">--}}
    {{--<label for="total">الاجمالي</label>--}}
    {{--<input type="number" id="total" name="total" value="{{$record->value}}" class="form-control" placeholder="الاجمالي"--}}
           {{--readonly>--}}

{{--</div>--}}

@push('scripts')

    <script>

    </script>
@endpush
