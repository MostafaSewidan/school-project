@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('المستخلصات المعتمدة')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="pull-left">
            @can('إضافة المستخلصات المعتمدة')
            <a href="{{url('admin/approved-extract/create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> {{ __('إضافة') }}
            </a>
            @endcan
        </div>
        <div class="clearfix"></div>
        <hr>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">{{ __('التاريخ') }}</th>
                    <th class="text-center">{{ __('الاجمالي') }}</th>
                    <th class="text-center">{{ __('البنود') }}</th>
                    @can('تعديل المستخلصات المعتمدة')
                    <th class="text-center">{{ __('تعديل') }}</th>
                    @endcan
                    @can('حذف المستخلصات المعتمدة')
                    <th class="text-center">{{ __('حذف') }}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                    <tr id="removable{{$record->id}}">
                        <td class="text-center">{{$record->extract_month}}</td>
                        <td class="text-center">{{$record->total}}</td>
                        <td>
                            <table class="table">
                                @foreach($record->items as $item)
{{--                                    @php dd($item) @endphp--}}
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->pivot->amount}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </td>
                        @can('تعديل المستخلصات المعتمدة')
                        <td class="text-center">
                            <a href="{{ route('approved-extract.edit', $record->id) }}"
                               class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                        </td>
                        @endcan
                        @can('حذف المستخلصات المعتمدة')
                        <td class="text-center">
                            <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{url('admin/approved-extract/'.$record->id)}}"
                                    type="button" class="destroy btn btn-danger btn-xs"><i
                                    class="fa fa-trash"></i></button>
                        </td>
                        @endcan
                    </tr>

                    @endforeach
                    </tbody>
                </table>
                    @include('layouts.partials.per-page')
                    <div class="text-center">{!! $records->render() !!}</div>
            </div>

            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> {{ __('لا توجد بيانات للعرض') }} </h3>
                </div>
            @endif
        </div>

    </div>
@endsection
