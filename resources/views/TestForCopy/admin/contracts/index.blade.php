@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('العقود')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                @can('إضافة العقود')
                <a href="{{url(route('contracts.create'))}}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> {{ __('أضف عقد') }}</a>
                @endcan
                    @can('استيراد العقود')
                        <button class="btn btn-success hidden-print btn-sm" data-toggle="modal" data-target="#importDevices">
                            <i class="fa fa-file-excel-o"></i> {{__('استيراد العقود')}}</button>
                    @endcan
                @can('حذف كل العقود')
                <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                      action="{{route('contracts.delete-all')}}" style="display: inline-block">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-danger btn-sm hidden-print"><i
                            class="fa fa-trash"></i>{{__('حذف الكل')}}</button>
                </form>
                    @endcan
                    <div class="modal inmodal" id="importDevices" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight text-right">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                    <h4 class="modal-title">{{__('استيراد العقود')}}</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'files' => true,
                                        'action' => 'ExcelController@importContracts'
                                    ]) !!}
                                    {!! \Helper\Field::fileWithPreview('contracts_sheet' ,__('اختر ملف الأكسل')) !!}
                                    <hr>
                                    <a href="{{asset('sheets/create_contract.xlsx')}}" class="btn btn-success">
                                        <i class="fa fa-download"></i>
                                        {{__('تحميل الملف القياسي')}}
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary"><i
                                            class="fa fa-file-excel-o"></i> {{__('استيراد العقود')}}</button>

                                    <button type="button" class="btn btn-white"
                                            data-dismiss="modal">{{__('إغلاق')}}</button>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>
            <div class="btn-group" style="float: left">
                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                   href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل العقود')
                <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'contracts'))}}"><i class="fa fa-file-excel-o"></i></a>
                @endcan
                {{--@can('تصدير العقود pdf')--}}
                {{--<a class="btn btn-danger btn-sm" href="#">--}}
                    {{--<i class="fa fa-file-pdf-o"></i>--}}
                {{--</a>--}}
                {{--@endcan--}}
            </div>
            <div class="col-xs-6 text-left">
                <br>
{{--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
{{--                    <i class="fa fa-search"></i>--}}
{{--                    بحث متقدم--}}
{{--                </button>--}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('بحث متقدم') }}</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open([
                                  'method' => 'GET'
                              ]) !!}
                                <div class="row">
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('manufacturer_id' ,__('الشركة')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('contract_type' ,__('نوع العقد'),[
                        'guaranteed' => 'تحت الضمان' ,
                        'inclusive' => 'عقد شامل' ,
                        'maintenance_only' => 'عقد صيانات فقط' ,
                        'nothing' => 'بدون عقد أو ضمان' ,
                        'other' => 'أخري' ,
],request('contract_type')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('cost' ,__('قيمة العقد')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::datePicker('contract_start' ,__('تاريخ بداية العقد')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::datePicker('contract_end' ,__('تاريخ نهاية العقد')) !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('code' ,__('كود الجهاز')) !!}
                                    </div>
{{--                                    <div class="col-sm-4">--}}
{{--                                        {!! \Helper\Field::select('per_page' ,__('عدد النتائج بالصفحة'),[--}}
{{--                                            '10' => '10',--}}
{{--                                            '15' => '15',--}}
{{--                                            '50' => '50',--}}
{{--                                            '75' => '75',--}}
{{--                                            '100' => '100'--}}
{{--                                        ],request('per_page'),'',__('عدد النتائج بالصفحة')) !!}--}}
{{--                                    </div>--}}

                                    @include('layouts.partials.append-per-page-to-filters')
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-warning" id="reset_form"><i class="fa fa-recycle"></i>{{__('تفريغ الحقول')}}</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{ __('رقم العقد') }}</th>
                    <th class="text-center">{{ __('الخصم عن كل يوم عطل') }}</th>
                    <th class="text-center">{{ __('نوع العقد') }}</th>

                    <th class="text-center">{{ __('تاريخ بداية العقد') }}</th>
                    <th class="text-center">{{ __('تاريخ نهاية العقد') }}</th>
                    <th class="text-center">{{ __('قيمة العقد') }}</th>
{{--                    <th class="text-center">{{ __('ملاحظات علي العقد') }}</th>--}}
                    <th class="text-center">{{ __('عدد الأجهزة') }} </th>
                    @can('تعديل العقود')
                    <th class="text-center">{{ __('تعديل') }}</th>
                    @endcan
                    @can('حذف العقود')
                    <th class="text-center">{{ __('حذف') }}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                        <tr id="removable{{$record->id}}">
                            <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                            <td class="text-center">
                                <a href="{{url(route('contracts.show',$record->id))}}">
                                    {{$record->contract_number}}
                                </a>
                            </td>
                            <td class="text-center">{{$record->cut_per_day_down}}</td>
                            <td class="text-center">{{$record->contract_typ}}</td>


                            <td class="text-center">{{$record->contract_start}}</td>
                            <td class="text-center">{{$record->contract_end}}</td>
                            <td class="text-center">{{$record->cost}}</td>
{{--                            <td class="text-center">{{$record->notes}}</td>--}}
                            <td class="text-center">
                                <span class="label label-info">{{$record->devices()->count()}}</span>
                            </td>
                            @can('تعديل العقود')
                            <td class="text-center">
                                <a href="{{url('admin/contracts/'. $record->id .'/edit')}}"
                                   class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                            </td>
                            @endcan
                            @can('حذف العقود')
                            <td class="text-center">
                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                        data-route="{{url('admin/contracts/'.$record->id)}}"
                                        type="button" class="destroy btn btn-danger btn-xs"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                            @endcan
                        </tr>
                    @endforeach


                    </tbody>

                </table>
                @include('layouts.partials.per-page')
                <div class="text-center">
                    {!! $records->appends(request()->query())->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
