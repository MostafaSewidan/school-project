@extends('layouts.app',[
                                'page_header'       => __('عرض العقود'),
                                'page_description'  => __('عقد رقم #').$record->id,
                                'link' => url('admin/contracts')
                                ])
@section('content')

    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{__('تفاصيل العقد')}}</h3>
                <table class="table m-b-xs">
                    <tbody>
                    <tr>
                        <td>
                            {{__('رقم العقد')}} : <strong>
                                {{$record->contract_number}}
                            </strong>
                        </td>
                        <td>
                            {{__('حاله العقد')}} : <strong>{{$record->contract_typ}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('تاريخ البداية')}} : <strong>{{$record->contract_start}}</strong>
                        </td>
                        <td>
                            {{__('تاريخ البداية الهجري')}} :
                            <strong>{{$record->hijri_contract_start}}</strong>

                        </td>
                    </tr>


                    <tr>
                        <td>
                            {{__('تاريخ النهاية')}} : <strong>{{$record->contract_end}}</strong>
                        </td>
                        <td>
                            {{__('تاريخ النهاية الهجري ')}}: <strong>{{$record->hijri_contract_end}}</strong>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('قيمه العقد')}}: <strong>{{$record->cost}}</strong>
                        </td>
                        <td>
                            {{__('الخصم عن كل يوم عطل')}}: <strong>{{$record->cut_per_day_down}}</strong>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            {{__('الملاحظات')}} : <strong>{{$record->notes}}</strong>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if($record->devices()->count())
    <div class="ibox">
        <div class="ibox-title">
            <h4>{{__('الأجهزة')}}</h4>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{__('كود الجهاز')}}</th>
                    <th class="text-center">{{__('الرقم التسلسلي')}}</th>
                    <th class="text-center">{{__('الفئة')}}</th>
                    <th class="text-center">{{__('القسم')}}</th>
                    <th class="text-center">{{__('الشركة المصنعة')}}</th>
                    <th class="text-center">{{__('المورد')}}</th>
                    <th class="text-center">{{__('نوع الجهاز')}}</th>
                    <th class="text-center"> {{__('الموديل')}}</th>
                    <th class="text-center"> {{__('حالة الجهاز')}}</th>
                    </thead>
                    <tbody>
                    @foreach($record->devices()->get() as $device)
                        <tr id="removable{{$device->id}}">
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td class="text-center">
                                <a href="{{url('admin/devices/'.$device->id)}}">
                                    {{optional($device)->code}}
                                </a>
                            </td>
                            <td class="text-center">{{optional($device)->serial_number}}</td>
                            <td class="text-center">{{$device->category_type}}</td>
                            <td class="text-center">
                                <a href="{{url(route('devices.index',['department' => $device->department_id]))}}">
                                    {{optional($device->department)->name}}
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{url(route('devices.index',['manufacturer' => $device->manufacturer_id]))}}">
                                    {{optional($device->manufacturer)->name}}
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{url(route('importers.index',['id' => $device->importer_id]))}}">
                                    {{optional($device->importer)->name}}
                                </a>
                            </td>
                            <td class="text-center">{{optional($device->deviceType)->name}}</td>
                            <td class="text-center">{{optional($device)->model}}</td>
                            <td class="text-center">
                                {{$device->statue_type}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    @endif
@stop

