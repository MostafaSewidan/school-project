{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')

<div class="row">
    <div class="col-sm-6">
        {!! \Helper\Field::text('contract_number',__('رقم العقد').'*') !!}

    </div>
    <div class="col-sm-6">
        <div style="@if($model->cut_per_day_down) display: none @endif" id="cut_per_day_down_div">
            {!!\Helper\Field::number('cut_per_day_down' ,__('الخصم عن كل يوم عطل')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!\Helper\Field::select('contract_type' ,__('نوع العقد') , [
    'guaranteed' => __('تحت الضمان'),
    'inclusive' => __('عقد شامل'),
    'maintenance_only' => __('عقد صيانات فقط'),
    'nothing' => __('بدون عقد أو ضمان'),
    'other' => __('اخري')
]) !!}
    </div>
    <div class="col-sm-6">
        {!! \Helper\Field::number('contract_cost',__('قيمة العقد').' *', $model->cost??'') !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!\Helper\Field::datePicker('contract_start' ,__('تاريخ بداية العقد/الضمان')) !!}
    </div>
    <div class="col-sm-6">
        {!!\Helper\Field::hijriDatePicker('hijri_contract_start' ,__('تاريخ بداية العقد/الضمان (هجريا)')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!\Helper\Field::datePicker('contract_end' ,__('تاريخ نهاية العقد/الضمان')) !!}
    </div>
    <div class="col-sm-6">
        {!!\Helper\Field::hijriDatePicker('hijri_contract_end' ,__('تاريخ نهاية العقد/الضمان (هجريا)')) !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        {!! \Helper\Field::textarea('notes',__('الملاحظات').' *') !!}
    </div>
</div>

{!! \Helper\Field::fileWithPreview('contract_pdf',__('إرفاق العقد كـ pdf')) !!}

@if(!empty($model->attachment) && array_key_exists('bdf-file' ,$model->attachment) && !empty($model->attachment['bdf-file']))
    <div class="col-md-4">
        <a href="{{$model->attachment['bdf-file'][0]}}" target="_blank">{{__('عرض الملف')}}</a>
        <img src="" alt="" class="img-responsive thumbnail">
    </div>
    <div class="clearfix"></div>
@endif

@push('scripts')

    <script>

        $('#category').change(function () {
            var value = $('#category').val();

            if (value === 'a' || value === 'b') {
                $('#cut_per_day_down_div').show();

            } else {

                $('#cut_per_day_down_div').hide();
            }
        });
    </script>

@endpush
