@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('تعديل أعدادات الإفتراضية الطيارين')
                                ])

@section('content')
    <div class="ibox">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>['ContractsController@update',$model->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT',
                                 'files' => true
                                ])!!}
        <div class="ibox-content">
            @include('admin.contracts.form')
        </div>
        <div class="ibox-footer">
            <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
        </div>
        {!! Form::close()!!}
    </div>
@stop
