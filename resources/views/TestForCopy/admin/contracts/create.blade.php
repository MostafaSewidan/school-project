@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف عقد')
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>'ContractsController@store',
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'POST',
                                'files' => true
                                ])!!}
        <div class="ibox-content">
            @include('admin.contracts.form')
        </div>
{{--        <div class="ibox-footer">--}}
{{--            <button type="submit" class="btn btn-primary">حفظ</button>--}}
{{--        </div>--}}
        <div class="ibox-footer">
            {!! \Helper\Field::ajaxBtn(__('حفظ')) !!}
        </div>
{{--        {!! Form::close()!!}--}}
    </div>
@stop
