@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "طباعة الباركود"
                          ])
@section('content')
    <div>
        <div class="text-center">
            @if($record->qr_code)
                <img src="{{$record->qr_code}}">
                <h3>كود الجهاز : {{$record->code}}</h3>
            @else
                <h3 class="text-info" style="text-align: center"> لا يوجد </h3>
            @endif
        </div>
    </div>
    <script>window.print()</script>
@stop


