<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>{{ __('الرقم التسلسلي') }}</th>
            <td>{{optional($device)->serial_number}}</td>
            <th>{{ __('رقم الموديل') }}</th>
            <td>{{optional($device)->model}}</td>
        </tr>
        <tr>
            <th>{{ __('نوع الجهاز') }}</th>
            <td>{{optional($device->deviceType)->name}}</td>
            <th>{{ __('فئة الجهاز') }}</th>
            <td>{{$device->category_type}}</td>
        </tr>
        <tr>
            <th>{{ __('القسم') }}</th>
            <td>{{optional($device->department)->name}}</td>
            <th> {{ __('الشركة المصنعة') }}</th>
            <td>{{optional($device->manufacturer)->name}}</td>
        </tr>
    </table>
</div>

@if($device->maintenances()->count())
    <div class="ibox">
        <div class="ibox-title">
            <h4>{{__('خطة الصيانة')}}</h4>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">{{__('الرقم التسلسلي')}}</th>
                    <th class="text-center">{{__('كود الجهاز')}}</th>
                    <th class="text-center">{{__('القسم')}}</th>
                    <th class="text-center">{{__('تاريخ الصيانة')}}</th>
                    </thead>
                    <tbody>
                    @foreach($device->maintenances()->oldest('maintenance_date')->get() as $maintenance)
                        <tr id="removable{{$maintenance->id}}">
                            <td class="text-center">{{optional($maintenance->device)->serial_number}}</td>
                            <td class="text-center"><a href="{{url('/admin/devices/'.optional($maintenance->device)->id)}}">{{optional($maintenance->device)->code}}</a> </td>
                            <td class="text-center">{{optional(optional($maintenance->device)->department)->name}}</td>
                            <td class="text-center">{{$maintenance->maintenance_date}}</td>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
