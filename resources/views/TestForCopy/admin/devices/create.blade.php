@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف جهاز')
                                ])

@section('content')

    <div class="ibox">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>'DevicesController@store',
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'POST'
                                ])!!}
        <div class="ibox-content">
            @include('admin.devices.form')
        </div>
        <div class="ibox-footer">
            {!! \Helper\Field::ajaxBtn(__('حفظ')) !!}
        </div>
        {!! Form::close()!!}
    </div>
@stop
