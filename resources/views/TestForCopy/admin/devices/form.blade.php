{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')
@inject('departments' , App\Models\Department)
@inject('manufacturers' , App\Models\Manufacturer)
{{--@inject('deviceNames' , App\Models\DeviceName)--}}
@inject('deviceTypes' , App\Models\DeviceType)
@inject('importers' , App\Models\Importer)
@inject('contracts',App\Models\Contract)
<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::text('code' ,__('كود الجهاز').' *') !!}
    </div>

    <div class="col-md-6">

        {!!\Helper\Field::select('department_id' ,__('القسم') , $departments->pluck('name','id')->toArray()) !!}
    </div>
</div>
<div class="row">

    <div class="col-md-6">

        {!!\Helper\Field::select('manufacturer_id' ,__('الشركات المصنعة').' *' ,
        $manufacturers->pluck('name','id')->toArray()) !!}
    </div>

    <div class="col-md-6">
        {!!\Helper\Field::select('importer_id' ,__('المورد') , $importers->pluck('name','id')->toArray()) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::select('device_type_id' ,__('نوع الجهاز'). ' *' , $deviceTypes->pluck('name','id')->toArray())
        !!}
    </div>
    <div class="col-md-6">
        {!!\Helper\Field::select('status' ,__('الحالة') , [
        'up' => __('يعمل'),
        'down' => __('لايعمل'),
        'under_construction' => __('تحت التركيب'),
        'in_stock' => __('في المستودع'),
        'other' => __('أخرى')
        ]) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::text('serial_number' ,__('الرقم التسلسلي').'*') !!}
    </div>
    <div class="col-md-6">
        {!!\Helper\Field::text('model' ,__('الموديل').' *') !!}
    </div>
</div>
{{--<div class="row">--}}
{{--    --}}{{--    <div class="col-md-6">--}}

{{--    --}}{{--        {!!\Helper\Field::select('device_name_id' ,'الاسم' , $deviceNames->pluck('name','id')->toArray()) !!}--}}
{{--    --}}{{--    </div>--}}
{{--    <div class="col-md-6">--}}
{{--        {!!\Helper\Field::number('visit_cost' ,'قيمة الزيارة') !!}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::text('location' ,__('موقع الجهاز')) !!}
    </div>
    <div class="col-md-6">
        {!!\Helper\Field::text('room_number' ,__('رقم الغرفة')) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::text('location2' ,__('موقع الجهاز الفرعي')) !!}
    </div>
    <div class="col-md-6">
        {!!\Helper\Field::text('price' ,__('السعر')) !!}
    </div>
</div>

{!! \Helper\Field::select('contract_id',__('العقد'),$contracts->pluck('contract_number','id')->toArray()) !!}



<div class="row">
    <div class="col-md-6">
        {!!\Helper\Field::datePicker('constructing_date' ,__('تاريخ تركيب الجهاز')) !!}
    </div>

    <div class="col-md-6">
        {!!\Helper\Field::hijriDatePicker('hijri_constructing_date' ,__('تاريخ تركيب الجهاز (هجريا)')) !!}
    </div>
</div>

{!!\Helper\Field::textarea('notes' ,__('الملاحظات')) !!}

{{--{!! \Helper\Field::fileWithPreview('attachments',__('مرفقات'),true) !!}--}}

{{--@if(!empty($model->attachment))--}}

{{--@foreach($model->attachmentRelation()->whereNull('usage')->get() as  $attachment)--}}

{{--<div class="col-md-3" id="removable{{$attachment->id}}">--}}
{{--<div class="text-center"--}}
{{--style="width: 100%;color: white;background-color: black;font-size: 3rem;font-weight: bolder;">--}}
{{--{{$loop->iteration}}--}}
{{--</div>--}}
{{--<img src="{{asset($attachment->path)}}" class="img-responsive" alt="">--}}
{{--<div class="clearfix"></div>--}}
{{--<button id="{{$attachment->id}}" data-token="{{ csrf_token() }}"--}}
{{--data-route="{{URL::route('photo.destroy',$attachment->id)}}"--}}
{{--type="button" class="destroy btn btn-danger btn-xs btn-block">--}}
{{--<i class="fa fa-trash"></i>--}}
{{--</button>--}}
{{--</div>--}}
{{--<br>--}}
{{--<br>--}}
{{--@endforeach--}}
{{--<div class="clearfix"></div>--}}
{{--<br>--}}
{{--@endif--}}