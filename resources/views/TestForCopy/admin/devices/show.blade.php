@extends('layouts.app',[
'page_header' => __('عرض الأجهزة'),
'page_description' => __('جهاز رقم #').$record->id,
'link' => url('admin/orders')
])
@section('content')


    <div class="ibox">
        <div class="ibox-title">
            <div class="col-xs-6">
                @can('تعديل الأجهزة')
                    <a href="{{ url('admin/devices/' . $record->id . '/edit') }}" class="btn btn-xs btn-success"><i
                            class="fa fa-edit"></i></a>
                @endcan
                @can('حذف الأجهزة')
                    {{-- <button id="{{$record->id}}" data-token="{{ csrf_token() }}" --}}
                    {{-- data-route="{{url('admin/devices/'.$record->id)}}" --}}
                    {{-- type="button" class="destroy btn btn-danger btn-xs"><i --}}
                    {{-- class="fa fa-trash"></i></button> --}}
                    <form method="post" action="{{ route('devices.destroy', $record->id) }}" style="display: inline-block"
                        onsubmit="confirm('{{ __('هل أنت متأكد من الحذف ؟') }}')">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </form>
                @endcan
                <div class="modal inmodal" id="importDevices" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('استيراد مجموعة من الأجهزة') }}</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open([
    'method' => 'post',
    'files' => true,
    'action' => 'ExcelController@importDevices',
]) !!}
                                {!! \Helper\Field::fileWithPreview('devices_sheet', __('اختر ملف الأكسل')) !!}
                                <hr>
                                <a href="{{ asset('sheets/devices-sample-2.xlsx') }}" class="btn btn-success">
                                    <i class="fa fa-download"></i>
                                    {{ __('تحميل الملف القياسي') }}
                                </a>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary"><i class="fa fa-file-excel-o"></i>
                                    {{ __('استيراد الأجهزة') }}</button>

                                <button type="button" class="btn btn-white"
                                    data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group pull-right">
                <a class="btn btn-default text-green btn-sm" href="{{ url(route('show-device', [$record->id])) }}"><i
                        class="fa fa-file-excel-o"></i>
                </a>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="ibox-content">
            @push('styles')
                <style>
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }

                </style>
            @endpush
            @include('flash::message')
            <div class="table-responsive">
                <table class="table m-b-xs">
                    <tbody>
                        <tr>
                            <td>
                                {{ __('كود الجهاز') }} : <strong>{{ $record->code }}</strong>
                            </td>
                            <td>
                                {{ __('الرقم التسلسلي') }}: <strong>{{ optional($record)->serial_number }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('القسم') }} : <strong><a
                                        href="{{ url(route('devices.index', ['department' => $record->department_id])) }}">
                                        {{ optional($record->department)->name }}
                                    </a></strong>
                            </td>
                            <td>
                                {{ __('الشركة المصنعة') }}: <strong><a
                                        href="{{ url(route('devices.index', ['manufacturer' => $record->manufacturer_id])) }}">
                                        {{ optional($record->manufacturer)->name }}
                                    </a></strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('المورد') }} : <strong><a
                                        href="{{ url(route('importers.index', ['id' => $record->importer_id])) }}">
                                        {{ optional($record->importer)->name }}
                                    </a></strong>
                            </td>

                            <td>
                                {{ __('نوع الجهاز') }}: <strong>{{ optional($record->deviceType)->name }}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('رقم الغرفة') }} : <strong>{{ $record->room_number }}</strong>
                            </td>
                            <td>
                                {{ __('الموديل') }}: <strong>{{ optional($record)->model }}</strong>
                            </td>
                        </tr>
                        {{-- <tr> --}}
                        {{--  --}}{{-- <td> --}}
                        {{--  --}}{{-- اسم الجهاز: <strong>{{optional($record->deviceName)->name}}</strong> --}}
                        {{--  --}}{{-- </td> --}}
                        {{-- <td> --}}
                        {{-- قيمة الزياره : <strong>{{optional($record)->visit_cost}}</strong> --}}

                        {{-- </td> --}}
                        {{-- </tr> --}}
                        <tr>
                            <td>
                                {{ __('تاريخ تركيب الجهاز') }} :
                                <strong>{{ optional($record)->constructing_date }}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ تركيب الجهاز (هجريا)') }} :
                                <strong>{{ optional($record)->hijri_constructing_date }}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('حالة الجهاز') }} : <strong>{{ $record->statue_type }}</strong>
                            </td>
                            <td>
                                {{ __('الفئة') }} : <strong>{{ $record->category_type }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                {{ __('الملاحظات') }} : <strong>{{ $record->notes }}</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                {{ __('موقع الجهاز الفرعى') }} : <strong>{{ $record->location2 }}</strong>
                            </td>
                            <td>
                                {{ __('السعر') }} : <strong>{{ $record->price }}</strong>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <h3 class="">{{ __('تفاصيل العقد') }}</h3>
                <table class="table m-b-xs">
                    <tbody>

                        <tr>
                            <td>
                                {{ __('رقم العقد') }} : <strong>
                                    <a href="{{ url('admin/contracts?contract_id=' . $record->contract_id) }}">
                                        {{ optional($record->contract)->contract_number }}
                                    </a>
                                </strong>
                            </td>
                            <td>
                                {{ __('حاله العقد') }} :
                                <strong>{{ optional($record->contract)->contract_typ }}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('تاريخ البداية') }} :
                                <strong>{{ optional($record->contract)->contract_start }}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ البداية الهجري') }} :
                                <strong>{{ optional($record->contract)->hijri_contract_start }}</strong>

                            </td>
                        </tr>


                        <tr>
                            <td>
                                {{ __('تاريخ النهاية') }} :
                                <strong>{{ optional($record->contract)->contract_end }}</strong>
                            </td>
                            <td>
                                {{ __('تاريخ النهاية الهجري ') }}:
                                <strong>{{ optional($record->contract)->hijri_contract_end }}</strong>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ __('قيمه العقد') }}: <strong>{{ optional($record->contract)->cost }}</strong>
                            </td>
                            <td>
                                {{ __('الخصم عن كل يوم عطل') }}:
                                <strong>{{ optional($record->contract)->cut_per_day_down }}</strong>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                {{ __('الملاحظات') }} : <strong>{{ optional($record->contract)->notes }}</strong>

                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if ($record->working_orders()->count())
        <div class="ibox">
            <div class="ibox-title">
                <h4>{{ __('أوامر العمل') }}</h4>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">{{ __('رقم أمر العمل') }}</th>
                            <th class="text-center">{{ __('كود الجهاز') }}</th>
                            <th class="text-center">{{ __('التاريخ') }}</th>
                            <th class="text-center">{{ __('التاريخ') }}</th>
                            <th class="text-center">{{ __('اسم المبلغ') }}</th>
                            <th class="text-center">{{ __('الشكوى') }}</th>
                            {{-- <th class="text-center">اضافه تعليق</th> --}}

                            {{-- <th class="text-center">{{__('التفاصيل')}}</th> --}}

                        </thead>
                        <tbody>
                            @foreach ($record->working_orders()->take(10)->get()
        as $wo)
                                <tr id="removable{{ $wo->id }}">
                                    <td class="text-center">

                                        <a href="{{ url('admin/operations/' . $wo->id) }}">
                                            WO-{{ $wo->id }}
                                        </a>

                                    </td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/devices?code=' . optional($wo->device)->code) }}">
                                            {{ optional($wo->device)->code }}<br>
                                            {{ optional(optional($wo->device)->deviceName)->name }}
                                        </a>
                                    </td>
                                    <td class="text-center">{{ $wo->due_to }}</td>
                                    <td class="text-center">{{ $wo->created_at }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/user?id=' . $wo->reporter_id) }}">
                                            {{ optional($wo->reporter)->id }}<br>
                                            {{ optional($wo->reporter)->name }}
                                        </a>
                                    </td>
                                    <td class="text-center">{{ optional($wo->complaint)->body }}</td>


                                    {{-- <td class="text-center"> --}}
                                    {{-- <a href="{{url('admin/operations/'. $wo->id)}}" --}}
                                    {{-- class="btn btn-xs btn-warning"><i class="fa fa-eye"></i></a> --}}
                                    {{-- </td> --}}

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    @endif
    @if ($record->maintenances()->count())
        <div class="ibox">
            <div class="ibox-title">
                <h4>{{ __('خطة الصيانة') }}</h4>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">{{ __('الرقم التسلسلي') }}</th>
                            <th class="text-center">{{ __('كود الجهاز') }}</th>
                            <th class="text-center">{{ __('القسم') }}</th>
                            <th class="text-center">{{ __('تاريخ الصيانة') }}</th>
                            <th class="text-center">{{ __('قيمة زيارة الصيانة') }}</th>
                            <th class="text-center">{{ __('الحالة') }}</th>
                            <th class="text-center">{{ __('هل تمت ؟') }}</th>
                            <th class="text-center">{{ __('تعديل') }}</th>
                        </thead>
                        <tbody>
                            @foreach ($record->maintenances()->oldest('maintenance_date')->get()
        as $maintenance)
                                <tr id="removable{{ $maintenance->id }}">
                                    <td class="text-center">{{ optional($maintenance->device)->serial_number }}</td>
                                    <td class="text-center"><a
                                            href="{{ url('/admin/devices/' . optional($maintenance->device)->id) }}">{{ optional($maintenance->device)->code }}</a>
                                    </td>
                                    <td class="text-center">
                                        {{ optional(optional($maintenance->device)->department)->name }}</td>
                                    <td class="text-center">{{ $maintenance->maintenance_date }}</td>
                                    <td class="text-center">{{ $maintenance->cost }}</td>
                                    <td class="text-center">{{ $maintenance->statue_type }}</td>
                                    <td class="text-center">
                                        @if ($maintenance->is_done)
                                            <i class="fa fa-check text-success"></i>
                                        @else
                                            <i class="fa fa-times text-danger"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/maintenance/' . $maintenance->id . '/edit') }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
    @if ($record->purchases()->count())
        <div class="ibox">
            <div class="ibox-title">
                <h4>{{ __('التعميدات') }}</h4>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">{{ __('رقم التعميد') }}</th>
                            <th class="text-center">{{ __('معمد من') }}</th>
                            <th class="text-center">{{ __('معمد الى') }}</th>
                            <th class="text-center">{{ __('تاريخ البداية') }}</th>
                            <th class="text-center">{{ __('حالة التعميد') }}</th>
                            <th class="text-center">{{ __('أسم القطعة') }}</th>
                            <th class="text-center">{{ __('حاله التنفيذ') }}</th>
                        </thead>
                        <tbody>
                            @foreach ($record->purchases()->take(10)->get()
        as $purchase)
                                <tr id="removable{{ $purchase->id }}">
                                    <td class="text-center">{{ $purchase->po_num }}</td>
                                    <td class="text-center">{{ $purchase->by }}</td>
                                    <td class="text-center">{{ $purchase->to }}</td>
                                    <td class="text-center">{{ $purchase->starts_at }}</td>
                                    <td class="text-center">{{ $purchase->statue_type }}</td>
                                    <td class="text-center">{{ $purchase->part_name }}</td>
                                    <td class="text-center">{{ $purchase->isdone_type }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-2 col-sm-offset-2">

            <a class="btn btn-danger btn-block" style="border-radius: 10px"
                href="{{ url('admin/maintenance/create?device_id=' . $record->id) }}">
                <i class="fa fa-wrench"></i>
                {{ __('خطة الصيانة') }}
            </a>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-info btn-block" style="border-radius: 10px"
                href="{{ url('admin/contracts?contract_id=' . $record->contract_id) }}">
                <i class="fa fa-file"></i>
                {{ __('تفاصيل العقد') }}
            </a>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-warning btn-block" style="border-radius: 10px"
                href="{{ url(route('operations.create', ['device_id' => $record->id])) }}">
                <i class="fa fa-wrench"></i>
                {{ __('أنشئ أمر عمل') }}
            </a>
        </div>

        <div class="col-sm-2">
            <a class="btn btn-success btn-block" style="border-radius: 10px"
                href="{{ url('admin/qrcode-device/' . $record->id) }}">
                <i class="fa fa-qrcode"></i>
                Qr Code
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
    </div>
@stop
