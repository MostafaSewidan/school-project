@extends('layouts.app',[
'page_header' => app()->getLocale() == 'en' ? app('settings')->site_name_en : app('settings')->site_name,
'page_description' => __('الأجهزة')
])
@section('content')
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                @can('إضافة الأجهزة')
                    <a href="{{ url(route('devices.create')) }}" class="btn btn-info btn-sm hidden-print">
                        <i class="fa fa-plus"></i> {{ __('أضف جهاز') }}</a>
                @endcan
                @can('استيراد الأجهزة')
                    <button class="btn btn-success hidden-print btn-sm" data-toggle="modal" data-target="#importDevices">
                        <i class="fa fa-file-excel-o"></i> {{ __('استيراد الأجهزة') }}</button>
                @endcan
                @can('حذف كل الأجهزة')
                    <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                        action="{{ route('devices.delete-all') }}" style="display: inline-block">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm hidden-print"><i
                                class="fa fa-trash"></i>{{ __('حذف الكل') }}</button>
                    </form>
                @endcan
                <div class="modal inmodal" id="importDevices" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('استيراد مجموعة من الأجهزة') }}</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open([
    'method' => 'post',
    'files' => true,
    'action' => 'ExcelController@importDevices',
]) !!}
                                {!! \Helper\Field::fileWithPreview('devices_sheet', __('اختر ملف الأكسل')) !!}
                                <hr>
                                <a href="{{ asset('sheets/devices-sample-2.xlsx') }}" class="btn btn-success">
                                    <i class="fa fa-download"></i>
                                    {{ __('تحميل الملف القياسي') }}
                                </a>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary"><i class="fa fa-file-excel-o"></i>
                                    {{ __('استيراد الأجهزة') }}</button>

                                <button type="button" class="btn btn-white"
                                    data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group pull-right">

                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                    href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل الأجهزة')
                    <a class="btn btn-default text-green btn-sm hidden-print"
                        href="{{ url(route('master-export', 'devices')) }}"><i class="fa fa-file-excel-o"></i></a>
                @endcan

            </div>
            <div class="col-xs-6 text-left">
                {{-- <br> --}}
                {{-- <button type="button" class="btn btn-primary hidden-print" data-toggle="modal" data-target="#myModal"> --}}
                {{-- <i class="fa fa-search"></i> --}}
                {{-- بحث متقدم --}}
                {{-- </button> --}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span><span
                                        class="sr-only">{{ __('إغلاق') }}</span></button>
                                <h4 class="modal-title">{{ __('بحث') }}</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open([
    'method' => 'GET',
    'id' => 'myForm',
]) !!}
                                <div class="row">
                                    @inject('device' , App\Models\Device)
                                    @inject('contract' , App\Models\Contract)
                                    @inject('departments' , App\Models\Department)
                                    @inject('manufacturers' , App\Models\Manufacturer)
                                    {{-- @inject('deviceNames' , App\Models\DeviceName) --}}
                                    @inject('deviceTypes' , App\Models\DeviceType)
                                    @inject('models' , App\Models\Device)
                                    @inject('importers' , App\Models\Importer)
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('code', __('كود الجهاز'), request('code')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('serial_number', __('الرقم التسلسلي'), request('serial_number')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('contract_number', __('رقم العقد'), request('contract_number')) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('contract_type', __('نوع العقد'), $contract->getType(), request('contract_type')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('department', __('الأقسام'), $departments->pluck('name', 'id')->toArray(), request('department')) !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('importer', __('المورد'), $importers->pluck('name', 'id')->toArray(), request('importer')) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('category', __('الفئة'), $deviceTypes->getCategory(), request('category')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('manufacturer', __('الشركة المصنعة'), $manufacturers->pluck('name', 'id')->toArray(), request('manufacturer')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('type', __('نوع الجهاز'), $deviceTypes->pluck('name', 'id')->toArray(), request('type')) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('status', __('حالة الجهاز'), $device->getStatus(), request('status'), 'select2', __('الكل')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('model', __('الموديل'), $models->pluck('model', 'model')->toArray(), request('model')) !!}
                                    </div>
                                    @include('layouts.partials.append-per-page-to-filters')
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-warning" id="reset_form"><i class="fa fa-recycle"></i>
                                    {{ __('تفريغ الحقول') }}</button>
                                <button type="button" class="btn btn-white"
                                    data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">
            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                        <th class="text-center">#</th>
                        <th class="text-center">
                            {{ __('كود الجهاز') }}
                        </th>
                        <th class="text-center">{{ __('الرقم التسلسلي') }}</th>
                        {{-- <th class="text-center">الاسم</th> --}}
                        <th class="text-center">{{ __('الفئة') }}</th>
                        <th class="text-center">{{ __('القسم') }}</th>
                        <th class="text-center">{{ __('الشركة المصنعة') }}</th>
                        <th class="text-center">{{ __('المورد') }}</th>
                        <th class="text-center">{{ __('نوع الجهاز') }}</th>
                        <th class="text-center"> {{ __('الموديل') }}</th>
                        <th class="text-center"> {{ __('العقد') }}</th>
                        <th class="text-center"> {{ __('حالة الجهاز') }}</th>
                        {{-- <th class="text-center">Qr Code</th> --}}
                        {{-- @can('تعديل الأجهزة') --}}
                        {{-- <th class="text-center">{{__('تعديل')}}</th> --}}
                        {{-- @endcan --}}
                        {{-- @can('حذف الأجهزة') --}}
                        {{-- <th class="text-center"> {{__('حذف')}} </th> --}}
                        {{-- @endcan --}}
                    </thead>
                    <tbody>
                        @php $count = 1; @endphp
                        @foreach ($records as $record)
                            <tr id="removable{{ $record->id }}">
                                <td class="text-center">
                                    {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                <td class="text-center">
                                    <a href="{{ url('admin/devices/' . $record->id) }}">
                                        {{ optional($record)->code }}
                                    </a>
                                </td>
                                <td class="text-center">{{ optional($record)->serial_number }}</td>
                                {{-- <td class="text-center">{{optional($record->deviceName)->name}}</td> --}}
                                <td class="text-center">{{ $record->category_type }}</td>
                                <td class="text-center">
                                    <a
                                        href="{{ url(route('devices.index', ['department' => $record->department_id])) }}">
                                        {{ optional($record->department)->name }}
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a
                                        href="{{ url(route('devices.index', ['manufacturer' => $record->manufacturer_id])) }}">
                                        {{ optional($record->manufacturer)->name }}
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a href="{{ url(route('importers.index', ['id' => $record->importer_id])) }}">
                                        {{ optional($record->importer)->name }}
                                    </a>
                                </td>
                                <td class="text-center">{{ optional($record->deviceType)->name }}</td>
                                <td class="text-center">{{ optional($record)->model }}</td>
                                <td class="text-center">
                                    <a href="{{ url(route('contracts.index', ['id' => $record->contract_id])) }}">
                                        {{ optional($record->contract)->contract_number }}
                                    </a>
                                </td>
                                <td class="text-center">
                                    {{ $record->statue_type }}
                                </td>

                                {{-- <td class="text-center"> --}}
                                {{-- <button style="background-color: white;border: none" --}}
                                {{-- data-toggle="modal" data-target="#client{{$record->id}}"> --}}
                                {{-- <i class="btn btn-xs btn-info fa fa-qrcode" style="padding: 4px 6px"></i> --}}
                                {{-- </button> --}}
                                {{-- <!-- Modal --> --}}
                                {{-- <div class="modal fade" id="client{{$record->id}}" tabindex="-1" role="dialog" --}}
                                {{-- aria-labelledby="myModalLabel"> --}}

                                {{-- <div class="modal-dialog" role="document"> --}}
                                {{-- <div class="modal-content"> --}}
                                {{-- <div class="modal-header"> --}}
                                {{-- <button type="button" class="close" data-dismiss="modal" --}}
                                {{-- aria-label="Close"><span aria-hidden="true">&times;</span> --}}
                                {{-- </button> --}}


                                {{-- <h2 style="font-weight: bold" class="modal-title" --}}
                                {{-- id="myModalLabel">{{$record->title}}</h2> --}}
                                {{-- </div> --}}

                                {{-- <div class="modal-body"> --}}
                                {{-- <center> --}}
                                {{-- @if ($record->qr_code) --}}
                                {{-- <img src="{{$record->qr_code}}"> --}}
                                {{-- @else --}}
                                {{-- <h3 class="text-info" style="text-align: center"> لا يوجد </h3> --}}
                                {{-- @endif --}}
                                {{-- </center> --}}
                                {{-- </div> --}}
                                {{-- <div class="modal-footer"> --}}

                                {{-- <button type="button" class="btn btn-default" data-dismiss="modal" --}}
                                {{-- style="background-color: #00c0ef;color: white"> --}}
                                {{-- إغلاق --}}
                                {{-- </button> --}}
                                {{-- </div> --}}
                                {{-- </div> --}}
                                {{-- </div> --}}
                                {{-- </div> --}}
                                {{-- </td> --}}

                                {{-- @can('تعديل الأجهزة') --}}
                                {{-- <td class="text-center"> --}}
                                {{-- <a href="{{url('admin/devices/'. $record->id .'/edit')}}" --}}
                                {{-- class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a> --}}
                                {{-- </td> --}}
                                {{-- @endcan --}}
                                {{-- @can('حذف الأجهزة') --}}
                                {{-- <td class="text-center"> --}}
                                {{-- <button id="{{$record->id}}" data-token="{{ csrf_token() }}" --}}
                                {{-- data-route="{{url('admin/devices/'.$record->id)}}" --}}
                                {{-- type="button" class="destroy btn btn-danger btn-xs"><i --}}
                                {{-- class="fa fa-trash"></i></button> --}}
                                {{-- </td> --}}
                                {{-- @endcan --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @include('layouts.partials.per-page')
                <div class="text-center">
                    {!! $records->appends(request()->query())->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
