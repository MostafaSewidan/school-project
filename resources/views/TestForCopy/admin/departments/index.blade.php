@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('الأقسام')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="pull-right">
            @can('إضافة الأقسام')
            <a href="{{url('admin/department/create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> {{__('إضافة')}}
            </a>
            @endcan
        </div>

        <div class="btn-group" style="float: left">
            @can('تصدير اكسل الأقسام')
            <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'departments'))}}"><i class="fa fa-file-excel-o"></i></a>
            @endcan
            {{--@can('تصدير الأقسام pdf')--}}
            {{--<a class="btn btn-danger btn-sm" href="#">--}}
                {{--<i class="fa fa-file-pdf-o"></i>--}}
            {{--</a>--}}
                {{--@endcan--}}
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                <table class="data-table table table-bordered dataTables-example">
                    <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">{{__('الأسم')}}</th>
                    <th class="text-center">{{__('الأسم المختصر')}}</th>
                    @can('تعديل الأقسام')
                    <th class="text-center">{{__('تعديل')}}</th>
                    @endcan
                    @can('حذف الأقسام')
                    <th class="text-center">{{__('حذف')}}</th>
                    @endcan
                    </thead>
                    <tbody>
                    @php $count = 1; @endphp
                    @foreach($records as $record)
                    <tr id="removable{{$record->id}}">
                        <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                        <td class="text-center">{{$record->name}}</td>
                        <td class="text-center">{{$record->shorten_name}}</td>
                        @can('تعديل الأقسام')
                        <td class="text-center">
                            <a href="{{url('admin/department/'.$record->id.'/edit')}}"
                               class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                        </td>
                        @endcan
                        @can('حذف الأقسام')
                        <td class="text-center">
                            <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{url('admin/department/'.$record->id)}}"
                                    type="button" class="destroy btn btn-danger btn-xs"><i
                                    class="fa fa-trash"></i></button>
                        </td>
                        @endcan
                    </tr>

                    @endforeach
                    </tbody>
                </table>
                    @include('layouts.partials.per-page')
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>


            @else
                <div>
                    <h3 class="text-info" style="text-align: center">{{__(' لا توجد بيانات للعرض ')}}</h3>
                </div>
            @endif
        </div>
        </div>
    </div>
@endsection
