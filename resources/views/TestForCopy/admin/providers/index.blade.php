@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "طلبات مقدمى الخدمة"
                          ])
@section('content')
    <div class="ibox-content">
        <div class="ibox-content">
            <div class="ibox-title">
                {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('name',request()->input('name'),[
                            'placeholder' => 'الاسم',
                            'class' => 'form-control'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @include('flash::message')
        @if(count($records))
            <div class="row">
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>الأسم</th>
                        <th>المنطقة/ المدينة</th>
                        <th>صورة بطاقة الرقم القومى</th>
                        <th>صورة شخصية حديثة</th>
                        <th>المؤهل الدراسى</th>
                        <th>فيش جنائى حديث</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center">قبول / رفض</th>
                        <th>حذف</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{optional($record->region)->name}}
                                    - {{ $record->region ? optional($record->region->city)->name : ''}}</td>
                                @foreach($record->attachments as $attachment)
                                    <td class="text-center">
                                        <a href="{{asset(optional($attachment)->national_id)}}"
                                           data-lightbox="{{$attachment->id}}"
                                        ><img src="{{asset(optional($attachment)->national_id)}}"
                                              alt="" style="height: 50px;"></a>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{asset(optional($attachment)->personal_photo)}}"
                                           data-lightbox="{{$attachment->id}}"
                                        ><img src="{{asset(optional($attachment)->personal_photo)}}"
                                              alt="" style="height: 50px;"></a>
                                    </td>

                                    <td class="text-center">
                                        <a href="{{asset(optional($attachment)->qualification)}}"
                                           data-lightbox="{{$attachment->id}}"
                                        ><img src="{{asset(optional($attachment)->qualification)}}"
                                              alt="" style="height: 50px;"></a>
                                    </td>

                                    <td class="text-center">
                                        <a href="{{asset(optional($attachment)->criminal_chips)}}"
                                           data-lightbox="{{$attachment->id}}"
                                        ><img src="{{asset(optional($attachment)->criminal_chips)}}"
                                              alt="" style="height: 50px;"></a>
                                    </td>
                                @endforeach
                                @if($record->status == 'pending')
                                    <td class="text-center">
                                        <span class="label label-primary">الطلب قيد الانتظار</span>
                                    </td>
                                @elseif($record->status == 'accepted')
                                    <td class="text-center">
                                        <span class="label label-success ">الطلب مقبول</span>
                                    </td>
                                @else
                                    <td class="text-center">
                                        <span class="label label-danger">الطلب مرفوض</span>
                                    </td>
                                @endif
                                <td class="text-center">
                                    <a href="{{route('provider.accept',$record->id)}}"
                                       class="btn btn-outline btn-success dim"><i
                                            class="fa fa-check"></i> قبول</a>
                                    <a href="{{route('provider.reject',$record->id)}}"
                                       class="btn btn-outline btn-danger dim"><i
                                            class="fa fa-times"></i> رفض</a>
                                </td>
                                <td class="text-center">
                                    <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('providers.destroy',$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <div class="text-center">
        {!! $records->render() !!}
    </div>
    @else
        <div class="col-md-4 col-md-offset-4">
            <div class="alert alert-info md-blue text-center">لا يوجد سجلات</div>
        </div>
    @endif
@endsection
