@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('الصيانات')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-6">
                @can('إضافة الصيانات')
                <a href="{{url(route('maintenance.create'))}}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> {{ __('أضف صيانة') }}</a>
                @endcan
                    @can('استيراد الصيانات')
                        <button class="btn btn-success hidden-print btn-sm" data-toggle="modal" data-target="#importDevices">
                            <i class="fa fa-file-excel-o"></i> {{__('استيراد الصيانات')}}</button>
                    @endcan
                @can('حذف كل الصيانات')
                <form onsubmit="return confirm('{{ __('هل أنت متأكد من حذف الكل؟') }}')" method="post"
                      action="{{route('maintenance.delete-all')}}" style="display: inline-block">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-danger btn-sm hidden-print"><i
                            class="fa fa-trash"></i>{{__('حذف الكل')}}</button>
                </form>
                    @endcan
                    <div class="modal inmodal" id="importDevices" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight text-right">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                    <h4 class="modal-title">{{__('استيراد خطة الصيانات')}}</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'files' => true,
                                        'action' => 'ExcelController@importMaintenance'
                                    ]) !!}
                                    {!! \Helper\Field::fileWithPreview('maintenance_sheet' ,__('اختر ملف الأكسل')) !!}
                                    <hr>
                                    <a href="{{asset('sheets/create_maintenance.xlsx')}}" class="btn btn-success">
                                        <i class="fa fa-download"></i>
                                        {{__('تحميل الملف القياسي')}}
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary"><i
                                            class="fa fa-file-excel-o"></i> {{__('استيراد خطة الصيانات')}}</button>

                                    <button type="button" class="btn btn-white"
                                            data-dismiss="modal">{{__('إغلاق')}}</button>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>
            <div class="btn-group" style="float: left">
                <a class="btn btn-default text-blue btn-sm hidden-print" data-toggle="modal" data-target="#myModal"
                   href="#"><i class="fa fa-search"></i></a>
                @can('تصدير اكسل الصيانات')
                <a class="btn btn-default text-green btn-sm" href="{{url(route('master-export', 'maintenance'))}}">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                @endcan
                {{--@can('تصدير الصيانات pdf')--}}
                {{--<a class="btn btn-danger btn-sm" href="#">--}}
                    {{--<i class="fa fa-file-pdf-o"></i>--}}
                {{--</a>--}}
                {{--@endcan--}}
            </div>
            <div class="col-xs-6 text-left">
                <br>
                {{--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
                {{--                    <i class="fa fa-search"></i>--}}
                {{--                    بحث متقدم--}}
                {{--                </button>--}}
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content animated bounceInRight text-right">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span></button>
                                <h4 class="modal-title">{{ __('بحث متقدم') }}</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['method' => 'get']) !!}
                                <div class="row">
                                    @inject('manufacturers' , App\Models\Manufacturer)
                                    @inject('deviceTypes' , App\Models\DeviceType)
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('serial_number' ,__('اسم الجهاز'),request('serial_number')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::datePicker('maintenance_date' ,__('من'),request('maintenance_date')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::datePicker('maintenance_date_to' ,__('الي'),request('maintenance_date_to')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::text('device_code' ,__('كود الجهاز'),request('device_code')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!!\Helper\Field::select('department' ,__('القسم') , $departments,request('department')) !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!!\Helper\Field::select('device_type' ,__('نوع الجهاز') , $deviceTypes->pluck('name','id')->toArray(),request('device_type')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! \Helper\Field::select('maintenance_status' ,__('حالة الصيانة'),

[
    'due' => 'واجبة',
    'missed' => 'فائتة',
    'coming' => 'قادمة',
]) !!}
                                    </div>

                                    <div class="col-sm-4">
                                        {!!\Helper\Field::select('manufacturer' ,__('الشركات المصنعة') , $manufacturers->pluck('name','id')->toArray(),request('manufacturer')) !!}
                                    </div>
{{--                                    <div class="col-sm-4">--}}
{{--                                        {!! \Helper\Field::select('per_page' ,__('عدد النتائج بالصفحة'),[--}}
{{--                                            '10' => '10',--}}
{{--                                            '15' => '15',--}}
{{--                                            '50' => '50',--}}
{{--                                            '75' => '75',--}}
{{--                                            '100' => '100'--}}
{{--                                        ],request('per_page'),'',__('عدد النتائج بالصفحة')) !!}--}}
{{--                                    </div>--}}
                                    @include('layouts.partials.append-per-page-to-filters')

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ __('بحث') }}</button>
                                <button type="button" class="btn btn-warning" id="reset_form"><i class="fa fa-recycle"></i>{{__('تفريغ الحقول')}}</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal">{{ __('إغلاق') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">
            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                        <th class="text-center">#</th>
                        <th class="text-center">{{ __('كود الجهاز') }}</th>
                        <th class="text-center">{{ __('سريال الجهاز') }}</th>
                        <th class="text-center">{{ __('القسم') }}</th>
                        <th class="text-center">{{ __('تاريخ الصيانة') }}</th>
                        <th class="text-center">{{ __('قيمة زيارة الصيانة') }}</th>
                        <th class="text-center">{{ __('الحالة') }}</th>
                        <th class="text-center">{{ __('أمر العمل') }}</th>
                        <th class="text-center">{{ __('هل تمت ؟') }}</th>
                        @can('تعديل الصيانات')
                        <th class="text-center">{{ __('تعديل') }}</th>
                        @endcan
                        @can('حذف الصيانات')
                        <th class="text-center">{{ __('حذف') }}</th>
                        @endcan
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td class="text-center">{{($records->perPage() * ($records->currentPage() - 1)) + $loop->iteration}}</td>
                                <td class="text-center">
                                    <a href="{{url(route('devices.show',$record->device_id))}}">
                                        {{optional($record->device)->code}}
                                    </a>
                                </td>
                                <td class="text-center">{{optional($record->device)->serial_number}}</td>
                                <td class="text-center">{{optional(optional($record->device)->department)->name}}</td>
                                <td class="text-center">{{$record->maintenance_date}}</td>
                                <td class="text-center">{{$record->cost}}</td>
                                <td class="text-center">{{$record->statue_type}}</td>
                                <td class="text-center">
                                    @if($record->workingOrder()->count())
                                        <a href="{{url(route('operations.show',$record->workingOrder->id))}}">
                                            #WO-{{optional($record->workingOrder)->id}}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($record->is_done)
                                        <i class="fa fa-check text-success"></i>
                                    @else
                                        <i class="fa fa-times text-danger"></i>
                                    @endif
                                </td>
                                @can('تعديل الصيانات')
                                <td class="text-center">
                                    <a href="{{url('admin/maintenance/'.$record->id.'/edit')}}"
                                       class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                @endcan
@can('حذف الصيانات')
                                <td class="text-center">
                                    <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{url('admin/maintenance/'.$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            @endcan
                        @endforeach
                        </tbody>
                    </table>
                    @include('layouts.partials.per-page')
                   <div class="text-center">
                       {!! $records->appends(request()->query())->render() !!}
                   </div>
            </div>
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> {{ __('لا توجد بيانات للعرض') }} </h3>
                </div>
            @endif
        </div>

    </div>
@endsection
