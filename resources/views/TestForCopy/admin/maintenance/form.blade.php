{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')
@inject('devices',App\Models\Device)

{!! \Helper\Field::multiSelect('device_id' ,__('كود الجهاز').' *',$devices->pluck('code','id')->toArray(),request('device_id')) !!}
<div id="device-info"></div>
@if(request('device_id'))
    @push('scripts')
        <script>
            var deviceInfoEl = $("#device-info");
            var deviceId = "{{request('device_id')}}";
            getDeviceInfo(deviceId, deviceInfoEl);
        </script>
    @endpush
@endif
@if($record->device_id)
    @push('scripts')
        <script>
            $('#device_id')[0].selectize.disable();
            var deviceInfoEl = $("#device-info");
            var deviceId = "{{$record->device_id}}";
            getDeviceInfo(deviceId, deviceInfoEl);
        </script>
    @endpush
@endif
{!! \Helper\Field::text('cost' ,__('قيمة زيارة الصيانة').' *') !!}
{!! \Helper\Field::select('by' ,__('بواسطة'),[
'main_contractor' => 'المقاول الرئيسي',
'importer' => 'مقاول الباطن'
]) !!}

{{--{!! \Helper\Field::select('all_contract' ,__('خطة صيانة مجمعة لعقد'),[--}}
{{--'yes' => 'نعم',--}}
{{--' no' => 'لا'--}}
{{--]) !!}--}}
{{--for redirect--}}
<input type="hidden" name="back_to" value="{{request('device_id')}}">

@if($edit)
    {!! \Helper\Field::datePicker('maintenance_date', __('تاريخ صيانة').' *') !!}
@endif
@if($edit == false)
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('maintenance_date_input',null,[
                            'class' => 'form-control datepicker',
                            'id' => 'maintenance_date_input',
                            'placeholder' => __('تاريخ صيانة').' *',
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-info btn-block" id="add_maintenance"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody id="maintenance_dates_table">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif

@push('scripts')
    <script>
        $("#add_maintenance").click(function (e) {
            e.preventDefault();
            var maintenance_date = $("#maintenance_date_input").val();
            if (maintenance_date === null || maintenance_date === "")
            {
                alert("Validation");
            }
            var maintenance_dates = $("input[name='maintenance_dates[]']")
                .map(function(){return $(this).val();}).get();
            console.log(maintenance_dates)
            if (maintenance_dates.includes(maintenance_date))
            {
                alert("Repeated");
            }else {
                $("#maintenance_dates_table").append
                (
                    '<tr>' +
                    '<td class="text-center">'+maintenance_date+'</td>' +
                    '<td class="text-center">' +
                    '<button class="btn btn-xs btn-danger" onclick="deleteThisRow(this)"><i class="fa fa-trash"></i></button>' +
                    '<input type="hidden" name="maintenance_dates[]" value="'+maintenance_date+'">' +
                    '</td>' +
                    '</tr>'
                );
                $("#maintenance_date_input").val("");
            }
        });

        function deleteThisRow(el) {
            $(el).parent().parent().remove();
        }
    </script>
@endpush
