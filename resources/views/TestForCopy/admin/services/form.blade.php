@include('layouts.partials.validation-errors')
@include('flash::message')

{!! field()->text('title','الاسم') !!}
{!! field()->text('description','الوصف') !!}
{!! Field()->fileWithPreview('url','اختر صورة') !!}
@if(isset($record->photo->url))
    <div class="col-md-4">
        <img src="{{url(optional($record->photo)->url)}}" alt="" class="img-responsive thumbnail" data-lty>
    </div>
    <div class="clearfix"></div>
@endif
<?php
$units = \App\Models\Unit::all();
?>
<label>اختر الوحدة </label>
{!! Form::select('unit_id',$units->pluck('singular_name','id')->toArray(),null,[
    'class' => 'form-control select2',
    'placeholder' => 'اختر الوحدة'
]) !!}

<?php
$categories = \App\Models\Category::all();
?>
<br><br>
<label>اختر القسم </label>
{!! Form::select('category_id',$categories->pluck('name','id')->toArray(),null,[
    'class' => 'form-control select2',
    'placeholder' => 'اختر القسم'
]) !!}


