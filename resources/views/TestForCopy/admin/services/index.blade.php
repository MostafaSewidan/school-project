@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => "الخدمات"
                          ])
{{--@inject('group','App\Models\Group')
@php
$groups = $group->pluck('title','id')->toArray();
@endphp--}}
@section('content')
    <div class="ibox-content">
        <div class="pull-right">
            <a href="{{url('admin/service/create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> إضافة
            </a>
        </div>
        <hr>
        <div class="ibox-content">

            @include('flash::message')
            @if(count($records))
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>#</th>
                        <th class="text-center">الاسم</th>
                        <th class="text-center">الوصف</th>
                        <th class="text-center">القسم</th>
                        <th class="text-center">اسم الوحدة بالمفرد</th>
                        <th class="text-center">اسم الوحدة بالجمع</th>
                        <th class="text-center">سعر الوحدة</th>
                        <th class="text-center">بداية المدى</th>
                        <th class="text-center">نهاية المدى</th>
                        <th class="text-center">الصورة</th>
                        <th class="text-center">تعديل</th>
                        <th class="text-center">حذف</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">{{$record->title}}</td>
                                <td class="text-center">{{$record->description}}</td>
                                <td class="text-center">{{optional($record->category)->name}}</td>
                                <td class="text-center">{{optional($record->unit)->singular_name}}</td>
                                <td class="text-center">{{optional($record->unit)->plural_name}}</td>
                                <td class="text-center">{{optional($record->unit)->price}}</td>
                                <td class="text-center">{{optional($record->unit)->start_time}}</td>
                                <td class="text-center">{{optional($record->unit)->end_time}}</td>
                                <td class="text-center">
                                    <a href="{{asset(optional($record->photo)->url)}}" data-lightbox="{{$record->id}}"
                                    ><img src="{{asset(optional($record->photo)->url)}}"
                                          alt="" style="height: 50px;"></a>
                                </td>
                                <td class="text-center">
                                    <a href="service/{{$record->id}}/edit"
                                       class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="text-center">
                                    <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('service.destroy',$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

        </div>
        <div class="text-center">
            {!! $records->render() !!}
        </div>
        @else
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-info md-blue text-center">لا يوجد بيانات</div>
            </div>
        @endif
    </div>
@endsection
