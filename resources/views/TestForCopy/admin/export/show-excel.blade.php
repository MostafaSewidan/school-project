@extends('excel_layouts.index-for-excel')
@section('content')

{{--    @if(!empty($records))--}}
{{--        <div class="table-responsive">--}}
{{--            <table id="chargesTable" class="table table-bordered table-heading">--}}
{{--                <tbody>--}}
{{--                <tr style="background-color: #337ab7;color: #FFF;">--}}
{{--                    @foreach($cols as $col)--}}
{{--                        <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">{{$col}}</th>--}}
{{--                    @endforeach--}}
{{--                </tr>--}}
{{--                @if(!empty($records))--}}

{{--                    @foreach($records as $record)--}}
{{--                        <tr>--}}
{{--                            @foreach($values as $val)--}}
{{--                                <td style="text-align: center">{{$record[$val]}}</td>--}}
{{--                            @endforeach--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                @else--}}
{{--                    <tr>--}}
{{--                        <td style="text-align: center" colspan="5">لا يوجد نتائج</td>--}}
{{--                    </tr>--}}
{{--                @endif--}}

{{--                </tbody>--}}
{{--            </table>--}}

{{--        </div>--}}
{{--    @endif--}}


@if(!empty($records))
<div class="table-responsive">
    <table id="chargesTable" class="table table-bordered table-heading">
        <tbody>

            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">#</th>
                <td style="text-align: center">{{$records->id}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">كود الجهاز</th>
                <td style="text-align: center">{{$records->code}}</td>
            </tr>
            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">رقم الغرفة</th>
                <td style="text-align: center">{{optional($records)->room_number}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">الشركات المصنعة</th>
                <td style="text-align: center">{{optional($records->manufacturer)->name}}</td>
            </tr>
            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">الموديل</th>
                <td style="text-align: center">{{optional($records)->model}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">المورد</th>
                <td style="text-align: center">{{optional($records->importer)->name}}</td>
            </tr>


            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">قيمه الزياره</th>
                <td style="text-align: center">{{optional($records)->visit_cost}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">نوع الجهاز</th>
                <td style="text-align: center">{{optional($records->deviceType)->name}}</td>
            </tr>


            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">تاريخ تركيب الجهاز (هجريا)</th>
                <td style="text-align: center">{{optional($records)->hijri_constructing_date}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;">تاريخ تركيب الجهاز</th>
                <td style="text-align: center">{{optional($records)->constructing_date}}</td>
            </tr>

            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> العنوان </th>
                <td style="text-align: center">{{optional($records)->location}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> العنوان الفرعي </th>
                <td style="text-align: center">{{optional($records)->location2}}</td>
            </tr>

            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> serial_no </th>
                <td style="text-align: center">{{optional($records)->serial_number}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> السعر </th>
                <td style="text-align: center">{{optional($records)->price}}</td>
            </tr>

            <tr style="background-color: #337ab7;color: #FFF;">
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> القسم </th>
                <td style="text-align: center">{{optional($records->department)->name}}</td>
                <th style="text-align: center;background-color: #0a6ebd;color: #ffffff;"> رقم العقد </th>
                <td style="text-align: center">{{optional($records)->contract_id}}</td>
            </tr>



        </tbody>
    </table>
</div>
@endif
@stop