@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('لوحة التحكم')
                                ])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ __('المستخلصات') }}</h5>
                        <br>
                        @include('layouts.partials.results-count')
                    </div>
                    <div class="ibox-content table-responsive">
                        @include('flash::message')
                        <table class="table table-hover no-margins  table-bordered dataTables-example">
                            <thead>

                            <tr>
                                <th class="text-center">{{ __('رقم النموذج') }}</th>
                                <th>{{ __('اسم النموذج') }}</th>
                                <th class="text-center">{{ __('استخراج') }}</th>
                                <th class="text-center">{{ __('طباعة') }}</th>
                                @can('المستخلص الذكي')
                                <th class="text-center">{{ __('النموذج الذكي') }}</th>
                                @endcan
                                {{--<th class="text-center">{{ __('تم') }}</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            @php $count = 1; @endphp
                            @foreach($records as $record)
                                <tr id="removable{{$record->id}}">
                                    <td class="text-center">{{__("نموذج رقم")}} {{$record->code}}</td>
                                    <td><strong>{{$record->name}}</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                                data-target="#exampleModal-{{$record->id}}"><i
                                                    class="fa fa-file"></i></button>

                                        <!-- Modal -->
                                        <div class="modal fade text-right" id="exampleModal-{{$record->id}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">{{$record->name}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    {{Form::open([
                                                        'method' => 'get',
                                                        'action' => ['ExtractController@export',$record->id]
                                                    ])}}
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="">{{ __('من تاريخ') }}</label>
                                                            <input type="text" name="from"
                                                                   class="datepicker form-control"
                                                                   placeholder="{{ __('من تاريخ') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">{{ __('إلى تاريخ') }}</label>
                                                            <input type="text" name="to" class="datepicker form-control"
                                                                   placeholder="{{ __('إلى تاريخ') }}">
                                                        </div>
                                                        @if(in_array($record->id,[21,5,6,7,8,10,11,12,13,14,15,17]))
                                                            <div class="form-group">
                                                                <label for="">{{ __('رقم الدفعة') }}</label>
                                                                <input type="text" name="payment_num"
                                                                       class="form-control"
                                                                       placeholder="{{ __('رقم الدفعة') }}">
                                                            </div>
                                                        @endif
                                                        @if(in_array($record->id,[21]))
                                                            <div class="form-group">
                                                                <label for="">{{ __('تاريخ الدفعة') }}</label>
                                                                <input type="text" name="payment_date"
                                                                       class="datepicker form-control"
                                                                       placeholder="{{ __('تاريخ الدفعة') }}">
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit"
                                                                class="btn btn-primary">{{ __('طباعة') }}</button>
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('إغلاق') }}
                                                        </button>
                                                    </div>
                                                    {{Form::close()}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info" data-toggle="tooltip" title="{{__('نموذج فارغ')}}">
                                            <i class="fa fa-print"></i></button>
                                    </td>
                                    @can('المستخلص الذكي')
                                    <td class="text-center">
                                        {{Form::open([
                                                        'method' => 'get',
                                                        'action' => ['ExtractController@export',$record->id]
                                                    ])}}
                                        <button class="btn btn-info" type="submit" data-toggle="tooltip"
                                                title="{{__('النموذج الذكي')}}"><i class="fa fa-calculator"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                    @endcan
                                    {{--<td class="text-center">--}}
                                    {{--@if(rand(0,1))--}}
                                    {{--<i class="fa fa-check text-green"></i>--}}
                                    {{--@endif--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                        <br>
                        @include('layouts.partials.per-page')
                        <div class="text-center">
                            <button class="btn btn-success">{{ __('المستخلص النهائي') }}</button>
                        </div>

                    </div>

                </div>
            </div>

    </section>
    <br>
    <section>

    </section>

@endsection
