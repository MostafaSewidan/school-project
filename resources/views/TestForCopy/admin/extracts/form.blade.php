{{--@include('layouts.partials.validation-errors')--}}
@include('flash::message')
@inject('devices',App\Models\Device)

{!! \Helper\Field::select('device_id' ,'أسم الجهاز',$devices->pluck('code','id')->toArray()) !!}
{!! \Helper\Field::datePicker('maintenance_date' ,'التاريخ') !!}
{!! \Helper\Field::text('location' ,'الموقع') !!}
{!! \Helper\Field::text('cost' ,'التكلفة') !!}
