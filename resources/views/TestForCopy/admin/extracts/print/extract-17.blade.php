@extends('admin.extracts.print.extract-layout',[
    'modelName' => 'نموذج 17'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 17 ) </h5>
        <h5><u>مشهد</u></h5>
        <h5> عن الدفعة رقم ( الدفعة الشهرية رقم ({{app('helper')->extractPaymentNum()}} )</h5>
        <div class="row">
            <div class=" col">
                <h5>تشهد مستشفى {{app('settings')->site_name}} أن
                    شركة {{app('settings')->contractor_name}} </h5>
                <p class="text-center ">
                    قد قامت بصرف رواتب العمالة الموضحة بالكشف الشهرى لرواتب العاملين للدفعة رقم
                    ( {{app('helper')->extractPaymentNum()}} )
                    خلال الفترة من من {{app('helper')->extractStart()}}
                    إلى {{app('helper')->extractStart()}}
                    ومدتها شهر ميلادى كامل. </p>
                <h5> ولقد أعطيت لهم هذة الشهادة لكى يتم صرف المستحقات لهم . </h5>
                <h5>والله الموفق ؛؛؛</h5>

                <br><br><br><br><br><br><br><br><br><br><br><br>
                <br><br><br><br><br><br><br><br><br><br><br><br>
                <br><br><br><br><br><br><br><br><br><br><br><br>
            </div>

        </div>

    </div>
@stop
