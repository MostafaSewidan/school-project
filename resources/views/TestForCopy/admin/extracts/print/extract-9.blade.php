@extends('admin.extracts.print.extract-layout',[
    'modelName' => '9'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 9 ) </h5>
        <h5>بيان قطع الغيار التى وصلت للموقع حسب التعاميد الصادرة للمقاول وتم تركيبها على الأجهزة  </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center">تاريخ أمر العمل  </td>
            <td scope="col" class="text-center">  إسم الجهاز </td>
            <td scope="col" class="text-center">  مكان الجهاز بالموقع  </td>
            <td scope="col" class="text-center"> رقم التعميد  </td>
            <td scope="col" class="text-center">  تاريخ التعميد  </td>
            <td scope="col" class="text-center"> تاريخ وصول القطعه  </td>
            <td scope="col" class="text-center">  تاريخ التركيب  </td>
        </tr>
        @inject('purchases','App\Models\PurchaseOrder')
        @foreach($purchases->where(function($query){
            $query->where('starts_at','>=',app('helper')->extractStart());
            $query->where('starts_at','<=',app('helper')->extractEnd());
        })->orWhere(function($query){
            $query->where('starts_at','<',app('helper')->extractStart());
        })->with('workingOrder.device.deviceType')->get() as $purchase)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>WO-{{ optional($purchase->workingOrder)->id }}</td>
                <td>{{ optional($purchase->workingOrder)->due_to }}</td>
                <td>{{ $purchase->workingOrder->device->deviceType->name??'' }}</td>
                <td>{{ optional($purchase->workingOrder->device)->location }}</td>
                <td>{{ $purchase->po_num }}</td>
                <td>{{ $purchase->starts_at }}</td>
                <td>{{ $purchase->import_date }}</td>
                <td>{{ optional($purchase->workingOrder)->completed_at }}</td>
            </tr>
        @endforeach

    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop
