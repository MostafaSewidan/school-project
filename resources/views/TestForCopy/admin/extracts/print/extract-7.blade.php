@extends('admin.extracts.print.extract-layout',[
    'modelName' => '7'
])
@section('content')


    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 7 ) </h5>
        <h5>بيان أوامر العمل التى تم إنجازها خلال فترة المستخلص | الدفعة الشهرية رقم (  للدفعة الشهرى رقم ({{app('helper')->extractPaymentNum()}} )  </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">
        @inject('wo','App\Models\WorkingOrder')
        <tr>
            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center"> تاريخه  </td>
            <td scope="col" class="text-center">  إسم الجهاز </td>
            <td scope="col" class="text-center">  مكان الجهاز بالموقع  </td>
            <td scope="col" class="text-center">  تاريخ الإنتهاء  </td>
            <td scope="col" class="text-center">  صيانة / إصلاح  </td>
        </tr>
        @foreach($wo->completed()->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())->get() as $workOrder)
        <tr>
            <td class="text-center">{{$loop->iteration}}</td>
            <td class="text-center">WO-{{$workOrder->id}}</td>
            <td class="text-center">{{$workOrder->due_to}}</td>
            <td class="text-center">{{$workOrder->device->deviceType->name ?? ""}}</td>
            <td class="text-center">{{$workOrder->device->location ?? ""}}</td>
            <td class="text-center">{{$workOrder->completed_at}}</td>
            <td class="text-center">
                @if($workOrder->type == 'maintenance')
                    {{ __('صيانة') }}
                @else
                    {{ __('إصلاح عطل') }}
                @endif
            </td>
        </tr>
        @endforeach
    </table>
@stop