@extends('admin.extracts.print.extract-layout',[
    'modelName' => '2'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 2 ) </h5>
        <h5>جـدول الاستحقاق الشهري للدفعـة الشهرية رقم (  للدفعة الشهرية رقم ({{app('helper')->extractPaymentNum()}} ) </h5>
        <h5>{{app('helper')->extractStart()}}
            وإلى {{app('helper')->extractEnd()}} ومدتها شهر ميلادي كامل </h5>
    </div>
    <table class="table table-bordered">

        <tr>
            <td scope="col" rowspan="2" class="text-center align-middle"> رقم البند</td>
            <td scope="col" rowspan="2" class="text-center align-middle">البيان</td>
            <td scope="col" rowspan="2" class="text-center align-middle"> القيمة الشهرية حسب العقد</td>
            <td scope="col" colspan="2" class="text-center"> الحسميات إن وجدت</td>
            <td scope="col" rowspan="2" class="text-center"> المبلغ ملستحق بعد الحسميات</td>
            <td scope="col" rowspan="2" class="text-center align-middle">ملاحظات</td>
        </tr>
        <tr>
            <td class="text-center">كمقابل</td>
            <td class="text-center"> كغرامة</td>
        </tr>
        @for($i=1;$i<=23;$i++)
            <tr>
                <td>{{$i}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endfor
        <tr>
            <td scope="col" colspan="2" class="text-center">الإجمالى</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
    {{--<br>--}}
    {{--@endfor--}}
@stop