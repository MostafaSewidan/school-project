@extends('admin.extracts.print.extract-layout',[
    'modelName' => '19'
])
@section('content')


    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 19 ) </h5>
        <h5>  جدول مواعيد زيارات مقاولى الباطن لأجهزة التخصصية للفئتين { أ-ب }  </h5>
        <h5>  عن   الفترة  من  {{$request->from}}
            إلى {{$request->to}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>
            <td scope="col" class="text-center">كود الجهاز</td>
            <td scope="col" class="text-center">سريال الجهاز</td>
            <td scope="col" class="text-center">نوع الجهاز</td>
            <td scope="col" class="text-center">يناير</td>
            <td scope="col" class="text-center">فبراير</td>
            <td scope="col" class="text-center">مارس</td>
            <td scope="col" class="text-center">أبريل</td>
            <td scope="col" class="text-center">مايو</td>
            <td scope="col" class="text-center">يونيو </td>
            <td scope="col" class="text-center">يوليو</td>
            <td scope="col" class="text-center">أغسطس</td>
            <td scope="col" class="text-center">سبتمبر</td>
            <td scope="col" class="text-center">أكتوبر</td>
            <td scope="col" class="text-center">نوفمبر</td>
            <td scope="col" class="text-center">ديسمبر</td>
        </tr>

        <tr>
            <td class="text-center" colspan="15">
                فئه { أ }
            </td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>

        <tr>
            <td class="text-center" colspan="15">
                فئه { ب }
            </td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU1288</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td>AKU12887758381</td>
            <td> 1223-125-12 </td>
            <td>BIO</td>
            <td>check</td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>check  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td>check</td>
            <td></td>
            <td> check</td>
        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>
        <tr>
            <td></td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>

        </tr>

    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop