@extends('admin.extracts.print.extract-layout',[
    'modelName' => 'نموذج 15'
])
@section('content')
        <div class="text-center mt-2 mb-4">
            <h5> المبرم مع : {{app('settings')->contractor_name}}</h5>
            <h5> نموذج ( 15 ) </h5>
            <h5> الدفعة الشهرية رقم ({{app('helper')->extractPaymentNum()}})</h5>
            <h5><U>شهادة إنجاز الحاسب الآلى </U></h5>
            <h5> عن الفترة من {{app('helper')->extractStart()}} إلى {{app('helper')->extractEnd()}}  </h5>
        </div>
        <br><br>

        <p>
            يشهد مستشفي بأنه قد تم تامين كافه مستهلكات الحاسب الالي و ملحقاتها التي يحتاجها الموقع و ذلك خلال فتره
            من {{app('helper')->extractStart()}} إلى {{app('helper')->extractEnd()}} ومدتها شهر ميلادي كامل .
        </p>

        <p class="text-center"> والله الموفق ،،،، </p>
        <br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br>

@stop
