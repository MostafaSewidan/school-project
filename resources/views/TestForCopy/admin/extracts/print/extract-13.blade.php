@extends('admin.extracts.print.extract-layout',[
    'modelName' => '13'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 13 )  </h5>
        <h5> الدفعة الشهرية رقم ( {{$request->payment_num}} )</h5>
        <h5>بيان أداء أعمال الصيانة الوقائية خلال فترة المستخلص لكافة الأجهزة الطبية الفئة ( أ + ب ) الموجودة بالموقع </h5>
        <h5>  عن   الفترة  من  {{$request->from}}
            إلى {{$request->to}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center">إسم الجهاز  </td>
            <td scope="col" class="text-center">إسم مقاول الباطن  </td>
            <td scope="col" class="text-center"> عدد الزيارات المطلوبة حسب العقد  </td>
            <td scope="col" class="text-center">  هل تمت الزيارة الوقائية كاملة </td>
            <td scope="col" class="text-center">تاريخ أداء هذة الزيارة وترتيبها حسب جدول مواعيد الزيارات </td>
            <td scope="col" class="text-center">تاريخ أخر زيارة سابقة وترتيبها  </td>
        </tr>


        <tr>
            <td>1</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td></td>
            <td> </td>
        </tr>
        <tr>
            <td>2</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td></td>
            <td> </td>
        </tr>
        <tr>
            <td>3</td>
            <td> </td>
            <td></td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>4</td>
            <td> </td>
            <td></td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>

        <tr>
            <td>5</td>
            <td> </td>
            <td></td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>6</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>7</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>8</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>

    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop