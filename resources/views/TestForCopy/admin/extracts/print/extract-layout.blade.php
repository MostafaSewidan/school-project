<!DOCTYPE html>
<html dir="rtl">
<head>
	<title>{{app('settings')->site_name}} @isset($modelName) - {{$modelName}} @endisset</title>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('extracts/css/bootstrap.min.css')}}" >
    <link rel="stylesheet" href="{{asset('extracts/css/bootstrap.rtl.min.css')}}" >
	<link rel="stylesheet" href="{{asset('extracts/css/style.css')}}" >
	<style>
        {{--  why i had to write this ??!! --}}
		@media print {
            .hidden-print{
                display: none;
            }
        }
	</style>
</head>
<body>
	<!-- start section header-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
                <div class="text-center p-2 hidden-print">
                    <a href="{{url(route('extracts.index'))}}" class="btn btn-primary hidden-print">
                        {{__('عودة')}}
                    </a>
                </div>
				<div class="card">
					@include('admin.extracts.print.header')
					<div class="card-body">
						@yield('content')
					</div>
					@include('admin.extracts.print.footer')
				</div>
			</div>
		</div>
	</div>
	{{--<script>window.print()</script>--}}
</body>
</html>