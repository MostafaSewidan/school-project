@extends('admin.extracts.print.extract-layout',[
    'modelName' => '4'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 4 ) </h5>
        <h5> تفاصيل تطبيق جدول حسميات التقصير في أعمال الصيانة الوقائية للأجهزة الطبية   </h5>
        <h5>  عن   الفترة  من   {{app('helper')->extractStart()}}
            إلى   {{app('helper')->extractEnd()}}
            ومدتها شهر ميلادى واحد  </h5>
    </div>
    <table class="table table-bordered">

        <tr>
            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> إسم   الجهاز  </td>
            <td scope="col" class="text-center"> فئته  </td>
            <td scope="col" class="text-center">  عدد أيام التاخير  </td>
            <td scope="col" class="text-center">  القيمة اليومية للحسم كاغرامه  </td>
            <td scope="col" class="text-center">  القيمة الإجمالية للجهاز الواحد  </td>
            <td scope="col" class="text-center">   عدد الأجهزة </td>
            <td scope="col" class="text-center">  القيمة الإجمالية  الأجهزة  </td>
            <td scope="col" class="text-center">  ملاحظات </td>
        </tr>


        <tr>
            <td>1</td>
            <td>  </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>2</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>3</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>4</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>

        <tr>
            <td>5</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>6</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td>7</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
            <td scope="col" colspan="7" class="text-center">الإجمالى</td>
            <td scope="col" colspan="1" class="text-center"></td>
            <td scope="col" colspan="1" class="text-center"></td>
        </tr>
    </table>

    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop