<div class="print-header card-header">
    <div class="row">
        <div class="col-xs-4 col-4">
            <h4>المملكة العربية السعودية</h4>
            <h4>وزارة الصحة</h4>
            <h4>{{app('settings')->site_name}} </h4>
            <h4>قسم الصيانة الطبية </h4>
            <h4>{{app('settings')->contractor_name}} </h4>
        </div>
        <div class="col-xs-4 col-4">
            <div class="logo text-center">
                <img style="width: 50%;" src="{{asset('photos/health-ministry.png')}}" alt="Logo">
            </div>
        </div>
        <div class="col-xs-4 col-4 text-left">
            <h5> أعمال إصلاح و صيانة الأجهزة و المعدات الطبية  </h5>
            <h5>  الموجودة فى المواقع التابعة لصحة  : {{app('settings')->region_name}}</h5>
            <h5>  بمقتضى العقد رقم  : {{app('settings')->contract_num}} </h5>
            <h5>  التاريخ : {{\Carbon\Carbon::today()->toDateString()}} </h5>
        </div>
    </div>
</div>