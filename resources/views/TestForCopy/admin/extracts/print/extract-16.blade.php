@extends('admin.extracts.print.extract-layout',[
    'modelName' => '16'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 16 ) </h5>
        <h5> شهادة أداء أعمال الصيانة الوقائية المخططة من قبل مقاولى الباطن </h5>
        <h5> خلال الفترة من {{$request->from}}
            إلى {{$request->to}}
            ومدتها شهر ميلادى كامل</h5>
        <div class="row">
            <div class=" col">
                <h5>1.بيان مقاولى الباطن الذين لهم صيانة وقائية وقد حضروا إلى الموقع خلال فترة هذا
                    المستخلص وهم :</h5>
            </div>
        </div>
        <div class="row mt-5">
            <div class=" col-4">
                <h3>إسم مقاول الباطن:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
            <div class=" col-4">
                <h3>إسم الجهاز:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
            <div class=" col-4">
                <h3>تاريخ آخر زيارة:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class=" col">
                <h5>2.بيان بأسماء مقاولى الباطن الذين لهم صيانة وقائية ولم يحضروا خلال فترة هذا المستخلص
                    :</h5>
            </div>
        </div>
        <div class="row mt-5">
            <div class=" col-3">
                <h3 class="mb-4">إسم مقاول الباطن:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
            <div class=" col-3">
                <h3 class="mb-4">إسم الجهاز:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
            <div class=" col-3">
                <h3 class="mb-4">تاريخ آخر زيارة:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
            <div class=" col-3">
                <h3 class="">أسباب عدم تنفيذ الزيارة فى موعدها:- </h3>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>
            </div>
        </div>
    </div>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop