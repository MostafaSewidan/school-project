@extends('admin.extracts.print.extract-layout',[
    'modelName' => 'نموذج 3'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 3 ) </h5>
        <h5> جدول الحسميات على الأجهزة المتعطلة  </h5>
        <h5> عن الفترة {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}
            والفترات السابقة  </h5>
    </div>
    <table class="table table-bordered">

        <tr>
            <td scope="col" rowspan="2" class="text-center align-middle"> م  </td>
            <td scope="col" rowspan="2" class="text-center align-middle">إسم الجهاز </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> فئته  </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> رقم أمر العمل وتاريخه </td>
            <td scope="col" colspan="3" class="text-center"> يطبق عليه  الغرامة  </td>
            <td scope="col" colspan="2" class="text-center"> لايطبق عليه  الغرامة  </td>
        </tr>

        <tr>

            <td class="text-center">عدد الايام  </td>
            <td class="text-center"> القيمة اليومية  </td>
            <td class="text-center">الإجمالي </td>
            <td class="text-center"> طبقت سابقا  </td>
            <td class="text-center">  يوجد مبررات </td>

        </tr>
        @inject('workingOrderModel','App\Models\WorkingOrder')
        @foreach($workingOrderModel->where(function($query){
            $query->where('due_to','>=',app('helper')->extractStart());
            $query->where('due_to','<=',app('helper')->extractEnd());
        })->orWhere(function($query){
            $query->where('due_to','<',app('helper')->extractStart());
        })->latest('due_to')->get() as $wo)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td> {{$wo->device->deviceType->name ?? ""}}</td>
            <td class="text-center">{{$wo->device->deviceType->category_type ?? ""}} </td>
            <td class="text-center">(WO-{{$wo->id}}) - ({{$wo->due_to}})</td>
            <td class="text-center">{{$wo->down_days}} </td>
            <td class="text-center">{{$wo->cut_per_day_down ?? 0}} </td>
            <td class="text-center">{{ $wo->down_days *  $wo->cut_per_day_down}}</td>
            <td> </td>
            <td> </td>
        </tr>
        @endforeach
    </table>

    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop