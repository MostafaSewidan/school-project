@extends('admin.extracts.print.extract-layout',[
    'modelName' => 'نموذج أ'
])
@section('content')
    <div class="">
        <div class="text-center">
            <h3> نموذج رقم (أ) الخاص بدفعات قطع الغيار </h3>
            <h3> بيان بالمبلغ المصروف من بند قطع الغيار </h3>
        </div>
        <h4> تاريخ بدايه العقد {{app('settings')->hijri_project_start_date}} هـ </h4>
        <h4 class="text-center"> دفعة رقم ({{request('payment_num')}}) </h4>
        <h6> تاريخ الدفعة : {{$request->payment_date}}  </h6>
    </div>
    <table class="table table-bordered">
        <tr>
            <td scope="col" class="text-center"> إجمالي المبلغ المعتمد في العقد لقطع الغيار</td>
            <td scope="col" class="text-center"> ماسبق صرفه عن طريق المقاول</td>
            <td scope="col" class="text-center"> ما سبق صرفه عن طريق التعاميد المباشرة من الموقع</td>
            <td scope="col" class="text-center"> إجمالي ما سبق صرفه</td>
            <td scope="col" class="text-center"> قيمة التعاميد المباشرة لهذه الدفعة</td>
            <td scope="col" class="text-center"> صافي المستحق</td>
            <td scope="col" class="text-center"> المبلغ المتبقي من البند</td>
        </tr>
        @for($i=1;$i<=1;$i++)
            <tr>
                <td><br><br></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endfor
    </table>

    @for($i=1;$i<=37;$i++)
        <br>
    @endfor
@stop