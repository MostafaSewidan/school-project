<!--	Start Footer	 -->
<div class="card-footer">

    <div class="row">
        <div class="col-6">
            <h4> المشرف على أعمال المقاول(مهندس الوزارة) بالموقع </h4>
            <h4> الإسم : {{app('settings')->contractor_supervisor}}</h4>
            <div class="text-center">
                <h6> التاريخ : </h6>

                <h6> ختم الموقع : </h6>
            </div>
        </div>

        <div class="col-6">
            <h4> مدير الصيانة بالموقع </h4>
            <h4>  الإسم : {{app('settings')->site_manager}}</h4>
            <div class="text-center">
                <h6> التاريخ : </h6>

                <h6> ادارة الموقع / </h6>
            </div>
        </div>
    </div>
</div>
<!--	End Footer	 -->