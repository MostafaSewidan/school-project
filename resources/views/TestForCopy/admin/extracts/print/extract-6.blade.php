@extends('admin.extracts.print.extract-layout',[
    'modelName' => '6'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 6 ) </h5>
        <h5> للدفعة الشهرى رقم ({{app('helper')->extractPaymentNum()}}) </h5>
        <h5> التقرير الشهرى لإصلاح وصيانة الأجهزة الطبية بالموقع </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    @inject('wo','App\Models\WorkingOrder')
    @inject('po','App\Models\PurchaseOrder')
    <h5 class="text-center mt-2 mb-4">  يجب تعبئة بيانات الجدول التالى بحيث تطابق البيانات بالنماذج التوضحيه المرفقة وكذلك مخرجات الحاسب الآلى المرفقة أيضا</h5>
    <table class="table table-bordered">
        <tr>
            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center">بيان بأوامر العمل والتعاميد الصادرة للمقاول الرئيسي  </td>
            <td scope="col" class="text-center"> العدد الكلى     </td>
            <td scope="col" class="text-center">  العدد الذى تم إنجازه </td>
            <td scope="col" class="text-center"> العدد الذى لم يتم إنجازه  </td>
        </tr>
        <tr>
            <td>1</td>
            <td> أوامر الإصلاح</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','repair')
            ->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','repair')
            ->completed()->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','repair')
            ->notCompleted()->count()}}</td>

        </tr>
        <tr>
            <td>2</td>
            <td> أوامر صيانة وقائية ( تنفذ بواسطة المقاول الرئيسي)</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','main_contractor');
            })
            ->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','main_contractor');
            })
            ->completed()->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','main_contractor');
            })
            ->notCompleted()->count()}}</td>
        </tr>
        <tr>
            <td>3</td>
            <td>أوامر صيانة وقائيه ( تنفذ بواسطة المقاول الباطن)</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','main_contractor');
            })
            ->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->completed()->count()}}</td>
            <td class="text-center">{{$wo
            ->where('due_to','>=',app('helper')->extractStart())
            ->where('due_to','<=',app('helper')->extractEnd())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->notCompleted()->count()}}</td>
        </tr>
        <tr>
            <td>4</td>
            <td>أوامر إصلاح مفتوحه من فترات سابقة</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())->count()}}</td>
            <td class="text-center">0</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())->count()}}</td>
        </tr>

        <tr>
            <td>5</td>
            <td>أوامر الصيانة الوقائية المفتوحة من فترات سابقة ( تنفذ بواسطة المقاول الرئيسي )</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->count()}}</td>
            <td class="text-center">0</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->count()}}</td>
        </tr>
        <tr>
            <td>6</td>
            <td>أوامر الصيانة الوقائية المفتوحة من فترات سابقة ( تنفذ بواسطة المقاول الباطن )</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->count()}}</td>
            <td class="text-center">0</td>
            <td class="text-center">{{$wo->notCompleted()
            ->where('due_to','<',app('helper')->extractStart())
            ->where('type','maintenance')->whereHas('maintenance',function($query){
                $query->where('by','importer');
            })
            ->count()}}</td>
        </tr>
        <tr>
            <td>7</td>
            <td>تعاميد قطع الغيار الصادرة للمقاول الرئيسي ( خلال الفترة )</td>
            <td class="text-center">{{$po
            ->where('starts_at','>=',app('helper')->extractStart())
            ->where('starts_at','<=',app('helper')->extractEnd())
            ->count()}}</td>
            <td class="text-center"> {{$po
            ->where('starts_at','>=',app('helper')->extractStart())
            ->where('starts_at','<=',app('helper')->extractEnd())
            ->where('is_imported',1)
            ->count()}}</td>
            <td class="text-center"> {{$po
            ->where('starts_at','>=',app('helper')->extractStart())
            ->where('starts_at','<=',app('helper')->extractEnd())
            ->where('is_imported',0)
            ->count()}}</td>
        </tr>
        <tr>
            <td>8</td>
            <td>تعاميد قطع الغيار الصادرة للمقاول الرئيسي ( خلال الفترات السابقة  )</td>
            <td class="text-center">{{$po
            ->where('starts_at','<',app('helper')->extractStart())
            ->count()}}</td>
            <td class="text-center"> {{$po
            ->where('starts_at','<',app('helper')->extractStart())
            ->where('is_imported',1)
            ->count()}}</td>
            <td class="text-center"> {{$po
            ->where('starts_at','<',app('helper')->extractStart())
            ->where('is_imported',0)
            ->count()}}</td>
        </tr>
    </table>

    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop