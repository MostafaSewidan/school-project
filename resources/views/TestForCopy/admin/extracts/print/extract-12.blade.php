@extends('admin.extracts.print.extract-layout',[
    'modelName' => '12'
])
@section('content')


    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 12 )  </h5>
        <h5> الدفعة الشهرية رقم ( {{app('helper')->extractPaymentNum()}} )</h5>
        <h5>بيان أوامر العمل المفتوحة والتى لم يتم إنجازها <u >خلال الفترات الماضية </u>   </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center">تاريخه  </td>
            <td scope="col" class="text-center"> إسم الجهاز </td>
            <td scope="col" class="text-center">  مكان الجهاز بالموقع </td>
            <td scope="col" class="text-center">رقم التعميد  </td>
            <td scope="col" class="text-center">تاريخه  </td>
            <td scope="col" class="text-center"> صيانة / إصلاح </td>
        </tr>
        @inject('workingOrderModel','App\Models\WorkingOrder')
        @foreach($workingOrderModel->where(function($query){
            $query->where('due_to','>=',app('helper')->extractStart());
            $query->where('due_to','<=',app('helper')->extractEnd());
        })->orWhere(function($query){
            $query->where('due_to','<',app('helper')->extractStart());
        })->where('status', 'open')->latest('due_to')->with('device.deviceType', 'purchases')->get() as $wo)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>WO-{{ $wo->id }}</td>
                <td>{{ $wo->due_to }}</td>
                <td>{{ $wo->device->deviceType->name??'' }}</td>
                <td>{{ optional($wo->device)->location }}</td>
                <td>
                    @if(count($wo->purchases()->get()))
                    @foreach($wo->purchases()->get() as $purchase)
                        {{ $purchase->id }}
                        @if(!$loop->last)
                            ,
                        @endif
                    @endforeach
                    @endif
                </td>
                <td>
                    @if(count($wo->purchases()->get()))
                    @foreach($wo->purchases()->get() as $purchase)
                        {{ $purchase->starts_at }}
                        @if(!$loop->last)
                            ,
                        @endif
                    @endforeach
                    @endif
                </td>
                <td>
                    @if($wo->type == 'maintenance')
                        {{ __('صيانة') }}
                    @else
                        {{ __('إصلاح عطل') }}
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop
