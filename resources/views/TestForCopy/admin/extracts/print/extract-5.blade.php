@extends('admin.extracts.print.extract-layout',[
    'modelName' => '5'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 5 ) </h5>
        <h5>  جدول تكاليف مقاولى الباطن للأجهزة الطبية التخصصية بالموقع للدفعة رقم (  {{app('helper')->extractPaymentNum()}}  )
            للعقود التى تصرف بنظام الزيارات  </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">
        <tr>
            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> إسم   الجهاز  </td>
            <td scope="col" class="text-center">  عدد الأجهزة بالموقع    </td>
            <td scope="col" class="text-center"> عدد الأجهزة المشموله بالزيارة  </td>
            <td scope="col" class="text-center">  قيمة الزيارة للجهاز الواحد حسب العقد  </td>
            <td scope="col" class="text-center">  القيمة المستحقه لما تم  صيانته  </td>
            <td scope="col" class="text-center">      إسم مقاول  الباطن   </td>
            <td scope="col" class="text-center">  ملاحظات  سبب  عدم صيانة  / صرف قيمة باقي الأجهزة   </td>
        </tr>

        <tr>
            <td class="text-center" colspan="8">
                فئه { أ }
            </td>
        </tr>
        @inject('deviceTypeModel','App\Models\DeviceType')
        @php
            $totalVisitCost = 0;
        @endphp
        @foreach($deviceTypeModel->where('category','a')->get() as $deviceType)
            @php
                $notGuaranteedCount = $deviceType->devices()->whereHas('contract',function ($contract){
                    $contract->whereNotIn('contract_type',['guaranteed']);
                })
                // to be continued
                ->whereHas('maintenances',function($query){
                    $query->due();
                })->count();
                $visitCost = $deviceType->devices()->first()->contract->cost ?? 0 ;
                $totalVisitCost += $visitCost;
            @endphp
        <tr>
            <td>{{$loop->iteration}}</td>
            <td> {{$deviceType->name}}  </td>
            <td> {{$deviceType->devices()->count()}} </td>
            <td> {{ $notGuaranteedCount }}</td>
            <td>{{$visitCost}}</td>
            <td>{{$visitCost * $notGuaranteedCount}}</td>
            <td></td>
            <td> </td>
        </tr>
        @endforeach

        <tr>
            <td>#</td>
            <td>إجمالى تكاليف مقاولى الباطن</td>
            <td class="text-center" colspan="3"></td>
            <td>{{$totalVisitCost}}</td>
            <td colspan="2"></td>
        </tr>
    </table>

    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop