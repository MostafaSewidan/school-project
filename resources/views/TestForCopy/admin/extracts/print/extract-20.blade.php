@extends('admin.extracts.print.extract-layout',[
    'modelName' => '20'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 20 ) </h5>
        <h5>الأستحقاق الشهرى لرواتب العاملين  </h5>
        <h5> للفترة من  {{app('helper')->extractStart()}}
            وإلى   {{app('helper')->extractEnd()}} </h5>
    </div>
    <table class="table table-bordered">

        <tr>
            <td scope="col" rowspan="2" class="text-center align-middle"> م  </td>
            <td scope="col" rowspan="2" class="text-center align-middle">الإسم </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> الوظيفة \ الجنسية  </td>

            <td scope="col" colspan="2" class="text-center"> الإستحقاق حسب العقد </td>
            <td scope="col" colspan="5" class="text-center"> الغياب والغرامة  </td>
            <td scope="col" rowspan="2" class="text-center align-middle">إجمالى الإستحقاق </td>
        </tr>

        <tr>

            <td class="text-center">شهرياً  </td>
            <td class="text-center"> يومياً  </td>
            <td class="text-center">عدد أيام الغياب</td>
            <td class="text-center"> أسباب الغياب  </td>
            <td class="text-center">  قيمة الغياب </td>
            <td class="text-center"> غرامة الغياب  </td>
            <td class="text-center">  قيمة الغياب والغرامة </td>

        </tr>
        @inject('salaries', 'App\Models\Salary')
        @php
         $total_monthly = 0;
         $total_daily = 0;
        @endphp
        @foreach($salaries->where(function($query){
            $query->where('month','>=',app('helper')->extractStart());
            $query->where('month','<=',app('helper')->extractEnd());
        })->orWhere(function($query){
            $query->where('month','<',app('helper')->extractStart());
        })->with('employee')->get() as $salary)
            @php
                $total_monthly += $salary->employee->monthly_maturity??0;
                $total_daily += $salary->employee->daily_benefit??0;;
            @endphp
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ optional($salary->employee)->name }}</td>
            <td>{{ optional($salary->employee)->job.' / '.optional($salary->employee)->nationality }}</td>
            <td>{{ optional($salary->employee)->monthly_maturity }}</td>
            <td>{{ optional($salary->employee)->daily_benefit }}</td>
            <td>{{ $salary->absence_day }}</td>
            <td>{{ $salary->absence_reason }}</td>
            <td>{{ $salary->absence_value }}</td>
            <td>{{ $salary->fine_value }}</td>
            <td>{{ $salary->absence_value + $salary->fine_value }}</td>
            <td>{{ $salary->total_accrual }}</td>
        </tr>
        @endforeach
        <tr>
            <td scope="col" colspan="3" class="text-center">الإجمالى</td>
            <td>{{ $total_monthly }}</td>
            <td>{{ $total_daily }}</td>
            <td>{{ $salaries->sum('absence_day') }}</td>
            <td></td>
            <td>{{ $salaries->sum('absence_value') }}</td>
            <td>{{ $salaries->sum('fine_value') }}</td>
            <td>{{ $salaries->sum('absence_value') + $salaries->sum('fine_value') }}</td>
            <td>{{ $salaries->sum('total_accrual') }}</td>
        </tr>
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop
