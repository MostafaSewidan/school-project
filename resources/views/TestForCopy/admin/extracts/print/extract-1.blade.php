@extends('admin.extracts.print.extract-layout',[
    'modelName' => 'نموذج 1'
])
@section('content')
        <div class="text-center mt-2 mb-4">
            <h5> المبرم مع : {{app('settings')->contractor_name}}</h5>
            <h5> نموذج ( 1 ) </h5>
            <h5><U>شهادة إنجاز </U></h5>
            <h5> للدفعة الشهرية رقم ({{app('helper')->extractPaymentNum()}})</h5>
            <h5> قام المتعهد : {{app('settings')->contractor_name}}</h5>
            <h5> في المدة من {{app('helper')->extractStart()}} إلى {{app('helper')->extractEnd()}} ومدتها شهر ميلادي كامل </h5>
        </div>
        <br><br>

        <p>
            وذلك بأعمال الإصلاحات والصيانة للأجهزة والمعدات الطبية الموجودة في :ـ
            موقــــع مستشفى :  {{app('settings')->site_name}}
            بموجب ما هو موضح بالأوراق الإثباتية المشفوعة بهذه الشهادة والتي تم إعدادها لتقديمها مع المستندات الأخرى اللازمة لصرف مستحقات المتعهد عن المدة المذكورة بمقتضى العقد المبرم معه وحسب النظام.
        </p>

        <p class="text-center"> والله الموفق ،،،، </p>
        <br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br>

@stop
