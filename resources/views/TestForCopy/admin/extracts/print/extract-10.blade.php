@extends('admin.extracts.print.extract-layout',[
    'modelName' => '10'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 10 )  </h5>
        <h5> الدفعة الشهرية رقم ( {{app('helper')->extractPaymentNum()}} )</h5>
        <h5>بيان قطع الغيار التى لم يتم تأمينها او توريدها   </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center">تاريخ أمر العمل  </td>
            <td scope="col" class="text-center">  إسم الجهاز </td>
            <td scope="col" class="text-center">  مكان الجهاز بالموقع  </td>
            <td scope="col" class="text-center"> وصف قطعة الغيار </td>
            <td scope="col" class="text-center">  تاريخ التعميد  </td>
            <td scope="col" class="text-center"> مدة التوريد </td>
            <td scope="col" class="text-center"> أسباب عدم التوريد </td>
        </tr>
        @inject('purchases','App\Models\PurchaseOrder')
        @foreach($purchases->where(function($query){
                    $query->where('starts_at','>=',app('helper')->extractStart());
                    $query->where('starts_at','<=',app('helper')->extractEnd());
                })->orWhere(function($query){
                    $query->where('starts_at','<=',app('helper')->extractStart());
                })->latest('starts_at')->with('workingOrder.device')->get() as $part)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>WO-{{ $part->working_order_id }}</td>
                <td>{{ optional($part->workingOrder)->due_to }}</td>
                <td>{{ optional($part->workingOrder->device)->code }}</td>
                <td>{{ optional($part->workingOrder->device)->location }}</td>
                <td>{{ $part->part_name }}</td>
                <td>{{ $part->starts_at }}</td>
                <td>{{ $part->period }}</td>
                <td></td>
            </tr>
        @endforeach
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop
