@extends('admin.extracts.print.extract-layout',[
    'modelName' => '14'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 14 )  </h5>
        <h5> الدفعة الشهرية رقم ( {{$request->payment_num}} )</h5>
        <h5><U>بيان بأعمال الصيانة الوقائية للأجهزة فئة أ + ب التى لم تتم فى موعدها خلال الفترات السابقة </U> </h5>
        <h5>  عن   الفترة  من  {{$request->from}}
            إلى {{$request->to}}  </h5>
    </div>

    <table class="table table-bordered">

        <tr>
            <td scope="col" rowspan="2" class="text-center align-middle"> م  </td>
            <td scope="col" rowspan="2" class="text-center align-middle">إسم الجهاز </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> مقاول الباطن </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> تاريخ استحقاق الزيارة الوقائية</td>
            <td scope="col" colspan="3" class="text-center"> تطبق الغرامة </td>
            <td scope="col" colspan="2" class="text-center"> لا تطبق الغرامة  </td>
            <td scope="col" rowspan="2" class="text-center align-middle"> ملاحظات</td>
        </tr>

        <tr>

            <td class="text-center">عدد الايام  </td>
            <td class="text-center"> القيمة اليومية  </td>
            <td class="text-center">الإجمالى </td>
            <td class="text-center"> طبقت سابقا  </td>
            <td class="text-center">  يوجد مبررات </td>

        </tr>
        <tr>
            <td>1</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck">
                    <label class="form-check-label" for="gridCheck">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-1">
                    <label class="form-check-label" for="gridCheck-1">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-2">
                    <label class="form-check-label" for="gridCheck-2">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-3">
                    <label class="form-check-label" for="gridCheck-3">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-4">
                    <label class="form-check-label" for="gridCheck-4">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-5">
                    <label class="form-check-label" for="gridCheck-5">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-6">
                    <label class="form-check-label" for="gridCheck-6">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-7">
                    <label class="form-check-label" for="gridCheck-7">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-8">
                    <label class="form-check-label" for="gridCheck-8">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>4</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-9">
                    <label class="form-check-label" for="gridCheck-9">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-10">
                    <label class="form-check-label" for="gridCheck-10">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-11">
                    <label class="form-check-label" for="gridCheck-11">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>

        <tr>
            <td>5</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-12">
                    <label class="form-check-label" for="gridCheck-12">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-13">
                    <label class="form-check-label" for="gridCheck-13">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-14">
                    <label class="form-check-label" for="gridCheck-14">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>6</td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-15">
                    <label class="form-check-label" for="gridCheck-15">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-16">
                    <label class="form-check-label" for="gridCheck-16">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-17">
                    <label class="form-check-label" for="gridCheck-17">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>7</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> </td>
            <td> </td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-18">
                    <label class="form-check-label" for="gridCheck-18">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-19">
                    <label class="form-check-label" for="gridCheck-19">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-20">
                    <label class="form-check-label" for="gridCheck-20">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>8</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-21">
                    <label class="form-check-label" for="gridCheck-21">
                        سيتم عمل الزيارة فى موعد لاحق
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-22">
                    <label class="form-check-label" for="gridCheck-22">
                        سيتم عمل الزيارة بالتعميد المباشر
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck-23">
                    <label class="form-check-label" for="gridCheck-23">
                        تم عمل الزيارة فى موعد متأخر
                    </label>
                </div>
            </td>
        </tr>
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop