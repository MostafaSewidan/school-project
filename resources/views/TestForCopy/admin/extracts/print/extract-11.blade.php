@extends('admin.extracts.print.extract-layout',[
    'modelName' => '11'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 11 )  </h5>
        <h5> الدفعة الشهرية رقم ( {{app('helper')->extractPaymentNum()}} )</h5>
        <h5>مبررات المتعهد والإجراءات التى إتخذها بشأن أوامر العمل التى لم يتم إنجازها   </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center">تاريخ أمر العمل  </td>
            <td scope="col" class="text-center"> أسباب ومبررات المتعهد فى التأخير وعدم الإنجاز </td>
            <td scope="col" class="text-center">  الإجراءات المتخذة من قبل المقاول </td>
        </tr>
        @inject('workingOrderModel','App\Models\WorkingOrder')
        @foreach($workingOrderModel->where(function($query){
            $query->where('due_to','>=',app('helper')->extractStart());
            $query->where('due_to','<=',app('helper')->extractEnd());
        })->orWhere(function($query){
            $query->where('due_to','<',app('helper')->extractStart());
        })->where('status', 'open')->latest('due_to')->get() as $wo)

        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>WO-{{ $wo->id }}</td>
            <td>{{ $wo->due_to }}</td>
            <td></td>
            <td></td>
        </tr>
        @endforeach
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop
