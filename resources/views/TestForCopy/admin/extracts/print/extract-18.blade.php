@extends('admin.extracts.print.extract-layout',[
    'modelName' => '18'
])
@section('content')

    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 18 ) </h5>
        <h5> شهادة أداء صيانة/ إصلاح لجهاز تم فحصه من قبل مقاول الباطن  </h5>
    </div>
    <div class="row">
        <div class="text-center col-6">
            <p> تاريخ الزيارة: / / 2م </p>
        </div>
        <div class="text-center col-6">
            <p>   الموقع : </p>
        </div>
        <div class="text-center col-6">
            <p>رقم أمر العمل:  </p>
        </div>
        <div class="text-center col-6">
            <p> القسم: المقاول: مؤسسة / شركة  </p>
        </div>

        <div class="text-center col-6">
            <p>  بداية عقد الصيانة (الباطن)  </p>
        </div>
        <div class="text-center col-6">
            <p>   نهاية عقد الصيانة (الباطن)    </p>
        </div>
        <div class="text-center col-6">
            <p>الجهاز : </p>
        </div>
        <div class="text-center col-6">
            <p> نوعه  </p>
        </div>


        <div class="text-center col-6">
            <p>  رقمه  </p>
        </div>
        <div class="text-center col-6">
            <p>   رقم الحاسب الآلي:  </p>
        </div>
        <div class="text-center col-6">
            <p>انوع الصيانة/ إصلاح  :  </p>
        </div>
        <div class="text-center col-6">
            <p> مجدولة وقائية ( )   طارئة ( )   </p>
        </div>

        <hr class="hr">
        <div class="text-center col-12">
            <p>  تاريخ الصيانة المجدولة (الوقائية القادمة)    </p>
        </div>
        <div class="text-center col-6">
            <p>  التاريخ السابق للصيانة  </p>
        </div>
        <div class="text-center col-6">
            <p>سبب الصيانة السابق وقائية( ) طارئة ( )   </p>
        </div>
        <div class="text-center col-12">
            <p>التاريخ السابق للصيانة الوقائية ( )  </p>
        </div>
        <div class="text-center col-12">
            <p>اهل تمت أعمال الصيانة/ الإصلاح نعم ( ) لا ( )   </p>
        </div>
        <div class="text-center col-12">
            <p>  ي حالة عدم أدائها تذكر الأسباب ـ وفي حالة أدائها يرفق اصل تقرير مقاول الباطن : </p>
        </div>



        <div class="text-center col-6">
            <p>القائم بالصيانة من قبل مقاول الباطن   :  </p>
        </div>
        <div class="text-center col-6">
            <p> وظيفته    </p>
        </div>


        <div class="text-center col-12">
            <p>ااعتماد مهندس مقاول التشغيل بالموقع :   </p>

        </div>
        <div class="text-center col-6">
            <p>  الإسم  : </p>
        </div>

        <div class="text-center col-6">
            <p>  الوظيفة : </p>
        </div>
        <div class="text-center col-12">
            <p>
                في حالة وجود ملاحظات على مقاول الباطن يتم ذكرها :
            </p>
        </div>

    </div>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop