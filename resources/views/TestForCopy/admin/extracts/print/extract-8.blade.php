@extends('admin.extracts.print.extract-layout',[
    'modelName' => '8'
])
@section('content')
    <div class="text-center mt-2 mb-4">
        <h5> المبرم مع : {{app('settings')->contractor_name}} </h5>
        <h5> نموذج ( 8 ) </h5>
        <h5>بيان أوامر العمل التى لم يتم إنجازها خلال فترة المستخلص | الدفعة الشهرية رقم ( {{app('helper')->extractPaymentNum()}} )  </h5>
        <h5>  عن   الفترة  من  {{app('helper')->extractStart()}}
            إلى {{app('helper')->extractEnd()}}  </h5>
    </div>
    <table class="table table-bordered">

        <tr>

            <td scope="col" class="text-center"> م  </td>
            <td scope="col" class="text-center"> رقم أمر العمل  </td>
            <td scope="col" class="text-center"> تاريخه  </td>
            <td scope="col" class="text-center">  إسم الجهاز </td>
            <td scope="col" class="text-center">  مكان الجهاز بالموقع  </td>
            <td scope="col" class="text-center"> رقم التعميد  </td>
            <td scope="col" class="text-center">  مدة التوريد  </td>
        </tr>
        @inject('wo','App\Models\WorkingOrder')
        @foreach($wo->notCompleted()->where('due_to','>=',app('helper')->extractStart())
                    ->where('due_to','<=',app('helper')->extractEnd())->has('purchase')->get() as $workOrder)
            <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td class="text-center">WO-{{$workOrder->id}}</td>
                <td class="text-center">{{$workOrder->due_to}}</td>
                <td class="text-center">{{$workOrder->device->deviceType->name ?? ""}}</td>
                <td class="text-center">{{$workOrder->device->location ?? ""}}</td>
                <td class="text-center">{{$workOrder->purchase->po_num ?? ""}}</td>
                <td class="text-center">
                    {{$workOrder->purchase->period ?? ""}}
                </td>
            </tr>
        @endforeach
    </table>
    {{--@for($i=1;$i<=37;$i++)--}}
        {{--<br>--}}
    {{--@endfor--}}
@stop