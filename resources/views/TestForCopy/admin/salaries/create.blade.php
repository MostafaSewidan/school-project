@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف راتب')
                                ])

@section('content')

<div class="ibox">
@include('flash::message')
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>'SalaryController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}
    <div class="ibox-content">
        @include('admin.salaries.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">{{ __('حفظ') }}</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
