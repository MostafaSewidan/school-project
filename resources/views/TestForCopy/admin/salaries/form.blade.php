@include('layouts.partials.validation-errors')
@include('flash::message')
@inject('employees',App\Models\Employee)
{!! \Helper\Field::datePicker('month',__('التاريخ').' *') !!}
{!! \Helper\Field::select('employee_id',__('الموظف').' *',$employees->pluck('name','id')->toArray()) !!}
<div class="form-group">
    <label for="">{{__('الاستحقاق')}}</label>
    {!! Form::text('merit',null,[
        'class' => 'form-control',
        'id' => 'merit',
        'readonly' => true
    ]) !!}
</div>
{!! \Helper\Field::number('absence_day',__('ايام الغياب').' *') !!}
{!! \Helper\Field::text('absence_reason',__('اسباب الغياب').' *') !!}
<div class="form-group">
    <label for="">{{__('قيمة الغياب')}}</label>
    {!! Form::text('absence_value',null,[
        'class' => 'form-control',
        'id' => 'absence_value',
        'readonly' => true
    ]) !!}
</div>
{!! \Helper\Field::number('fine_value',__('قيمة الغرامة').' *', 0) !!}
<div class="form-group">
    <label for="">{{__('إجمالي الاستحقاق')}}</label>
    {!! Form::text('total_accrual',null,[
        'class' => 'form-control',
        'id' => 'total_accrual',
        'readonly' => true
    ]) !!}
</div>

@push('scripts')
    <script>
        var salaryPerDay = 0;
        function calculateSalaryDetails()
        {
            var absence_day  = Math.round($("#absence_day").val() * 100) / 100;
            var merit  = Math.round($("#merit").val() * 100) / 100;
            var fine_value  = Math.round($("#fine_value").val() * 100) / 100;
            var totalAbsence = salaryPerDay * absence_day;
            $("#absence_value").val(Math.round(totalAbsence * 100) / 100);
            var netSalary = merit - (fine_value + totalAbsence);
            $("#total_accrual").val(Math.round(netSalary * 100) / 100);
        }
        $("#absence_day , #fine_value").keyup(function () {
            calculateSalaryDetails();
        });
        $("#employee_id").change(function(){
            var employeeId = $("#employee_id").val();
            $.ajax({
                url: "{{url(route('api.employee-info'))}}?employee_id="+employeeId,
                type: 'get',
                success: function (data) {
                    if (data.status === 1)
                    {
                        console.log(data.data);
                        $("#merit").val(data.data.monthly_maturity);
                        salaryPerDay = data.data.daily_benefit;
                        calculateSalaryDetails();
                    }else{
                        alert("{{__('حدث خطأ')}}");
                    }
                },
                error: function () {
                    alert("{{__('حدث خطأ')}}");
                }
            });
        });
    </script>
@endpush



@push('scripts')
    <script>
        $('#month').on('change', function () {
            {{--var month = this.value;--}}
            {{--var employee_id = $("#employee_id").val();--}}
            {{--$.ajax({--}}
                {{--url: "{{url('api/v1/check-salary-month')}}",--}}
                {{--type: "get",--}}
                {{--data: {--}}
                    {{--month: month,--}}
                    {{--employee_id: employee_id,--}}
                    {{--_token: '{{csrf_token()}}'--}}
                {{--},--}}
                {{--dataType: 'json',--}}

                {{--success: function (result)--}}
                {{--{--}}
                    {{--if (result.status === 0)--}}
                    {{--{--}}
                        {{--alert(result.message);--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#employee_id').on('change', function () {

                var employee_id = this.value;

                $("#merit").html('');
                $.ajax({
                    url: "{{url('api/v1/employee')}}",
                    type: "get",
                    data: {
                        employee_id: employee_id,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',

                    success: function (result)
                    {

                        // $('#state-dd').html('<option value="">المحافظة</option>');
                        $.each(result, function (key, value) {

                            $("#merit").append('<option value="' + value
                                .id + '">' + value.name + '</option>');
                        });
                        // $('#merit').html('<option value="">Select City</option>');
                    }
                });
            });
        });
    </script>
@endpush
