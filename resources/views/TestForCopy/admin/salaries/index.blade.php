@extends('layouts.app',[
'page_header' => app('settings')->site_name,
'page_description' => __('الرواتب')
])

@section('content')
    <div class="ibox-content">
        <div class="pull-left">
            @can('إضافة الرواتب')
                <a href="{{ url('admin/salaries/create') }}" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i> {{ __('إضافة') }}
                </a>
            @endcan
        </div>
        <div class="btn-group pull-right">
            @can('تصدير اكسل الرواتب')
                <a class="btn btn-default text-green btn-sm" href="{{ url(route('master-export', 'salaries')) }}"><i
                        class="fa fa-file-excel-o"></i></a>
            @endcan
            {{-- @can('تصدير الرواتب pdf') --}}
            {{-- <a class="btn btn-danger btn-sm" href="#"> --}}
            {{-- <i class="fa fa-file-pdf-o"></i> --}}
            {{-- </a> --}}
            {{-- @endcan --}}
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if (!empty($records) && count($records) > 0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                            <th class="text-center">#</th>
                            <th class="text-center">{{ __('الشهر') }}</th>
                            <th class="text-center">{{ __('الموظف') }}</th>
                            <th class="text-center">{{ __('الاستحقاق') }}</th>
                            <th class="text-center">{{ __('ايام الغياب') }}</th>
                            <th class="text-center">{{ __('اسباب الغياب') }}</th>
                            <th class="text-center">{{ __('قيمة الغياب') }}</th>
                            <th class="text-center">{{ __('قيمة الغرامة') }}</th>
                            <th class="text-center">{{ __('إجمالي الاستحقاق') }} </th>
                            @can('تعديل الرواتب')
                                <th class="text-center">{{ __('تعديل') }}</th>
                            @endcan
                            @can('حذف الرواتب')
                                <th class="text-center">{{ __('حذف') }}</th>
                            @endcan
                        </thead>
                        <tbody>
                            @php $count = 1; @endphp
                            @foreach ($records as $record)
                                <tr id="removable{{ $record->id }}">
                                    <td class="text-center">
                                        {{ $records->perPage() * ($records->currentPage() - 1) + $loop->iteration }}</td>
                                    <td class="text-center">{{ $record->month_display }}</td>
                                    <td class="text-center">{{ optional($record->employee)->name }}</td>
                                    <td class="text-center">{{ $record->merit }}</td>
                                    <td class="text-center">{{ $record->absence_day }}</td>
                                    <td class="text-center">{{ $record->absence_reason }}</td>
                                    <td class="text-center">{{ $record->absence_value }}</td>
                                    <td class="text-center">{{ $record->fine_value }}</td>
                                    <td class="text-center">{{ $record->total_accrual }}</td>
                                    @can('تعديل الرواتب')
                                        <td class="text-center">
                                            <a href="{{ url('admin/salaries/' . $record->id . '/edit') }}"
                                                class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                        </td>
                                    @endcan
                                    @can('حذف الرواتب')
                                        <td class="text-center">
                                            <button id="{{ $record->id }}" data-token="{{ csrf_token() }}"
                                                data-route="{{ url('admin/salaries/' . $record->id) }}" type="button"
                                                class="destroy btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </td>
                                    @endcan
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    @include('layouts.partials.per-page')
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>
                @else
                    <div>
                        <h3 class="text-info" style="text-align: center"> {{ __('لا توجد بيانات للعرض') }} </h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
