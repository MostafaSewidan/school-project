@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('أضف بند')
                                ])

@section('content')

<div class="ibox">
    <!-- form start -->
    {!! Form::model($record,[
                            'action'=>'ItemController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}
    <div class="ibox-content">
        @include('admin.items.form')
    </div>
    <div class="ibox-footer">
        <button type="submit" class="btn btn-primary">{{__('حفظ')}}</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
