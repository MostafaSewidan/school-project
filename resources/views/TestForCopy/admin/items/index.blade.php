@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('البنود المالية')
                          ])

@section('content')
    <div class="ibox-content">
        <div class="pull-right">
            @can('إضافة البنود')
                <a href="{{url('admin/items/create')}}" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i> {{__('إضافة')}}
                </a>
            @endcan
        </div>
        <div class="clearfix"></div>
        <br>
        @include('layouts.partials.results-count')
        <div class="ibox-content">

            @include('flash::message')
            <div class="table-responsive">
                @if(!empty($records) && count($records)>0)
                    <table class="data-table table table-bordered dataTables-example">
                        <thead>
                        <th class="text-center">{{__('الاسم')}}</th>
                        <th class="text-center">{{__('القيمة')}}</th>
                        <th class="text-center">{{__('المعتمد')}}</th>
                        <th class="text-center">{{__('المتبقي')}}</th>

                        @can('تعديل البنود')
                            <th class="text-center">{{__('تعديل')}}</th>
                        @endcan
                        @can('حذف البنود')
                            <th class="text-center">{{__('حذف')}}</th>
                        @endcan
                        </thead>
                        <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach($records as $record)
                            @php
                                $totalRest += $record->rest_item;
                                $totalBalance += $record->general_item;
                            @endphp
                            <tr id="removable{{$record->id}}">
                                <td class="text-center">{{$record->name}}</td>
                                <td class="text-center">{{$record->total_amount}}</td>
                                <td class="text-center">{{$record->rest_item}}</td>
                                <td class="text-center">{{$record->general_item}}</td>

                                @can('تعديل البنود')
                                    <td class="text-center">
                                        <a href="{{url('admin/items/'.$record->id.'/edit')}}"
                                           class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                @endcan
                                @can('حذف البنود')
                                    <td class="text-center">
                                        <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                                data-route="{{url('admin/items/'.$record->id)}}"
                                                type="button" class="destroy btn btn-danger btn-xs"><i
                                                    class="fa fa-trash"></i></button>
                                    </td>
                                @endcan
                            </tr>

                        @endforeach
                        <tr class="bg-success">
                            <th class="text-center">{{__('الاجماليات')}}</th>
                            <th class="text-center">{{ $records->sum('total_amount') }}</th>
                            <th class="text-center">{{ $totalRest }}</th>
                            <th class="text-center">{{ $totalBalance }}</th>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    @include('layouts.partials.per-page')
                    <div class="text-center">
                        {!! $records->render() !!}
                    </div>
                @else
                    <div>
                        <h3 class="text-info" style="text-align: center"> {{__('لا توجد بيانات للعرض')}} </h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
