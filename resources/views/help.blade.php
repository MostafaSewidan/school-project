@extends('layouts.app',[
'page_header' => app()->getLocale() == 'en' ? app('settings')->site_name_en : app('settings')->site_name,
'page_description' => "لوحة التحكم"
])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>الدعم والمساعدة</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {{-- @include('layouts.partials.validation-errors') --}}
                        @include('flash::message')

                        {!! \Helper\Field::text('name', 'الاسم') !!}
                        {!! \Helper\Field::text('name', 'الجوال') !!}
                        {!! \Helper\Field::text('name', 'الإيميل') !!}
                        {!! \Helper\Field::text('name', 'العنوان ') !!}
                        {!! \Helper\Field::textarea('name', 'الرسالة ') !!}
                    </div>
                    <div class="ibox-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <br>
    <section>

    </section>

@endsection
