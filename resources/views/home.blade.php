@extends('layouts.app',[
                                'page_header'       => app('settings')->site_name,
                                'page_description'  => __('لوحة التحكم')
                                ])
@inject('department','App\Models\Department')
@section('content')

    @push('styles')
        {{-- ChartStyle --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    @endpush

    @push('scripts')
        {!! $pie->script() !!}
        {!! $line1->script() !!}
        {!! $line2->script() !!}
        {!! $equipmentByClass->script() !!}
    @endpush


    <style>
        .info-box-number {
            font-family: Trebuchet, Arial, sans-serif;
        }
    </style>


    <section>
        <div class="text-center">
            @can('عرض الأجهزة')
                <a class="btn btn-success " style="border-radius: 10px" href="{{url('admin/devices')}}">
                    <i class="fa  fa-stethoscope"></i>

                    {{__('الأجهزة')}}
                </a>
            @endcan
            @can('عرض أوامر العمل')
                <a class="btn btn-warning " style="border-radius: 10px" href="{{url('admin/operations')}}">
                    <i class="fa  fa-wrench"></i>

                    {{__('أوامر العمل')}}
                </a>
            @endcan
            @can('عرض الصيانات')
                <a class="btn btn-danger " style="border-radius: 10px" href="{{url('admin/maintenance')}}">
                    <i class="fa  fa-wrench"></i>

                    {{__('خطة الصيانات')}}
                </a>
            @endcan
            @can('عرض العقود')
                <a class="btn btn-info" style="border-radius: 10px" href="{{url('admin/contracts')}}">
                    <i class="fa  fa-file"></i>

                    {{__('عقود الصيانة')}}
                </a>
            @endcan
            @can('عرض التعميدات')
                <a class="btn btn-success " style="border-radius: 10px" href="{{url('admin/purchases')}}">
                    <i class="fa  fa-money"></i>

                    {{__('التعميدات')}}
                </a>
            @endcan
            @can('عرض الإعدادات')
                <a class="btn btn-primary" style="border-radius: 10px" href="{{url('admin/settings')}}">
                    <i class="fa fa-cogs"></i>

                    {{__('الإعدادات')}}
                </a>
        @endcan
        <div class="clearfix"></div>
        <br>
        </div>
        @can('عرض الرسوم البيانية')
            <div class="row">
                <div class="col-md-4">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>{{__('رسم بياني للأجهزة')}}</h5>
                        </div>
                        <div class="ibox-content">
                            {!!$pie->container() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>{{__('الأقسام')}}</h5>
                        </div>
                        <div class="ibox-content table-responsive">
                            <table class="table table-hover no-margins">
                                <thead>
                                <tr>
                                    <th>{{__('الأسم')}}</th>
                                    <th class="text-center">{{__('عدد الأجهزة')}}</th>
                                    <th class="text-center">{{__('الحالة')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($department->has('devices')->withCount('devices')
                                ->orderBy('devices_count','desc')->take(10)->get() as $department)
                                    <tr>
                                        <td><strong>{{$department->name}}</strong></td>
                                        <td class="text-center"> {{$department->devices()->count()}}</td>
                                        <td class="text-green text-center">
                                            <a href="#" data-toggle="tooltip"
                                               title="{{$department->devices()->whereNotIn('status',[])->count() ?
                                          number_format($department->devices()->where('status','up')->count() / $department->devices()->whereNotIn('status',[])->count() * 100,2) :
                                          0}} %">
                                                <span class="pie">{{$department->devices()->where('status','up')->count()}}/{{$department->devices()->whereNotIn('status',[])->count()}}</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>{{__('رسم بياني لأوامر العمل المفتوحة لكل قسم')}}</h5>
                            </div>
                            <div class="ibox-content">
                                {{--                            {!!$line2->container() !!}--}}
                                {!! $openWOByDeptChartJs !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>{{ __('الأجهزة حسب التصنيف') }}</h5>
                            </div>
                            <div class="ibox-content">
                                {{--                            {!!$equipmentByClass->container() !!}--}}
                                {!! $equipmentByClassesChartJs !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>{{__('رسم بياني بأيام التأخير لأوامر العمل المفتوحة')}}</h5>
                            </div>
                            <div class="ibox-content">
                                {{--                            {!!$line1->container() !!}--}}
                                {!! $openWOByAgeChartJs !!}
                            </div>
                        </div>
                    </div>
                    {{-- dounut--}}
                </div>
                {{--<div class="col-sm-4">--}}
                {{--<div class="ibox ">--}}
                {{--<div class="ibox-title">--}}
                {{--<h5>{{ __('الأجهزة حسب التصنيف') }}</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                {{--{!! $equipmentByClassesChartJs !!}--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<canvas id="myChart2" height="400"></canvas>--}}
                {{--<script>--}}
                {{--var ctx2 = document.getElementById('myChart2');--}}
                {{--var data = {--}}
                {{--datasets: [{--}}
                {{--label: '# of Votes',--}}
                {{--data: [10, 20, 30],--}}
                {{--backgroundColor: [--}}
                {{--'rgba(255, 99, 132, 1)',--}}
                {{--'rgba(54, 162, 235, 1)',--}}
                {{--'rgba(255, 206, 86, 1)'--}}
                {{--],--}}
                {{--}],--}}

                {{--// These labels appear in the legend and in the tooltips when hovering different arcs--}}
                {{--labels: [--}}
                {{--'Red',--}}
                {{--'Yellow',--}}
                {{--'Blue'--}}
                {{--]--}}
                {{--};--}}
                {{--var options = {--}}
                {{--responsive: true,--}}
                {{--// scales: {--}}
                {{--//     xAxes: [{--}}
                {{--//         ticks: {--}}
                {{--//             beginAtZero: true--}}
                {{--//         }--}}
                {{--//     }]--}}
                {{--// }--}}
                {{--};--}}
                {{--var myDoughnutChart = new Chart(ctx2, {--}}
                {{--type: 'doughnut',--}}
                {{--data: data,--}}
                {{--options: options--}}
                {{--});--}}
                {{--</script>--}}
                {{--</div>--}}

            </div>
        @endcan
    </section>
    <br>
    <section>
        <div class="row">

        </div>
    </section>

@endsection
