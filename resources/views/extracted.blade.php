@extends('layouts.app',[
'page_header' => app()->getLocale() == 'en' ? app('settings')->site_name_en : app('settings')->site_name,
'page_description' => "لوحة التحكم"
])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>المستخلصات السابقة</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content table-responsive">
                        <table class="table table-hover no-margins  table-bordered dataTables-example">
                            <thead>
                                <tr>
                                    <th>المستخلص</th>
                                    <th class="text-center">طباعة</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for ($i = 1; $i <= 10; $i++)
                                    <tr>
                                        <td> مستخلص شهر {{ $i }}-2021</td>
                                        <td class="text-center">
                                            <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <br>
    <section>

    </section>

@endsection
