@extends('layouts.app',[
'page_header' => app()->getLocale() == 'en' ? app('settings')->site_name_en : app('settings')->site_name,
'page_description' => "لوحة التحكم"
])

@section('content')
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>المستخلصات</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content table-responsive">
                        <table class="table table-hover no-margins  table-bordered dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center">رقم النموذج</th>
                                    <th>اسم النموذج</th>
                                    <th class="text-center">استخراج</th>
                                    <th class="text-center">طباعة</th>
                                    {{-- <th class="text-center">تم</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> نموذج رقم 1</td>
                                    <td><strong>شهادة إنجاز</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1"><i
                                                class="fa fa-file"></i></button>

                                        <!-- Modal -->
                                        <div class="modal fade text-right" id="exampleModal1" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">شهادة إنجاز</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="">من تاريخ</label>
                                                            <input type="text" class="datepicker form-control"
                                                                placeholder="من تاريخ">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">إلى تاريخ</label>
                                                            <input type="text" class="datepicker form-control"
                                                                placeholder="إلى تاريخ">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary">استخراج</button>
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">إغلاق
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 2</td>
                                    <td><strong>جدول الاستحقاق الشهري</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 3</td>
                                    <td><strong>جدول الحسميات علي الأجهزة المتعطلة</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 4</td>
                                    <td><strong>تكاليف تطبيق جدول حسميات التقصير في أعمال الصيانة الوقائية</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 5أ/ب</td>
                                    <td><strong>تكاليف مقاولي الباطن للأجهزة الطبية للعقود التي تصرف بنظام الزيارات</strong>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 6</td>
                                    <td><strong>التقرير الشهري لإصلاح وصيانة الأجهزة الطبية بالموقع</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 7</td>
                                    <td><strong>بيان أوامر العمل التي تم إنجازها خلال فترة المستخلص</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 8</td>
                                    <td><strong>بيان أوامر العمل التي لم يتم إنجازها خلال فترة المستخلص</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 9</td>
                                    <td><strong>بيان قطع الغيار التي وصلت الموقع حسب التعاميد وتم تركيبها بالأجهزة</strong>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 10</td>
                                    <td><strong>بيان قطع الغيار التي لم يتم تأمينها أو توريدها</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 11</td>
                                    <td><strong>مبررات المتعهد والإجراءات التي اتخذها بشأن أوامر العمل التي لم يتم
                                            إنجازها</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 12</td>
                                    <td><strong>بيان أوامر العمل المفتوحة والتي لم يتم انجازها خلال الفترات الماضية</strong>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 13</td>
                                    <td><strong>بيان بأعمال الصيانة الوقائية لأجهزة فئة أ+ب خلال فترة المستخلص</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 14</td>
                                    <td><strong>بيان بأعمال الصيانة الوقائية للأجهزة فئة أ+ب التي لم تتم خلال الفترات
                                            السابقة</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 15</td>
                                    <td><strong>شهادة إنجاز الحاسب الآلي</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 16</td>
                                    <td><strong>شهادة أداء أعمال الصيانة الوقائية المخططة من قبل المقاول</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 17</td>
                                    <td><strong>مشهد بصرف الرواتب</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 18</td>
                                    <td><strong>شهادة أداء صيانة / إصلاح لجهاز تم فحصه من قبل مقاول الباطن </strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 19</td>
                                    <td><strong>زيارات مقاولي الباطن للأجهزة التخصصية أ-ب</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> نموذج رقم 20</td>
                                    <td><strong>الاستحقاق الشهري لرواتب العاملين</strong></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i
                                                class="fa fa-file"></i></button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-info"><i class="fa fa-print"></i></button>
                                    </td>
                                    <td class="text-center">
                                        @if (rand(0, 1))
                                            <i class="fa fa-check text-green"></i>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center">
                            <button class="btn btn-success">المستخلص النهائي</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <br>
    <section>

    </section>

@endsection
