<?php

namespace App\Imports;

use App\Models\Device;
use App\Models\Maintenance;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MaintenanceImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
//        dd($row);
        $device_code = trim($row['device_code']);
        $visit_cost = trim($row['visit_cost']);
        $device = Device::where('code', $device_code)->select('id')->first();
        if ($device) {
            for ($i = 1; $i <= 60; $i++) {
                if (isset($row['date' . $i])) {

                    info($row['date' . $i] . ' -- ' . $device);
                    $maintenance_date = trim($row['date' . $i]);
                    $maintenance_date = Carbon::parse($maintenance_date)->format('Y-m-d');
                    $check = Maintenance::where([
                        'device_id' => $device->id,
                        'maintenance_date' => $maintenance_date
                    ])->first();
                    if ($check) {
                        info($check->id);
                        $check->update([
                            'cost' => $visit_cost,
                        ]);
                    } else {
                        $maintenance = Maintenance::create([
                            'device_id' => $device->id,
                            'cost' => $visit_cost,
                            'maintenance_date' => $maintenance_date
                        ]);
                        info($maintenance->id);
                    }
                }
            }
        }
    }
}
