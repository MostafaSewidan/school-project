<?php

namespace App\Imports;

use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Importer;
use App\Models\Manufacturer;
use App\Models\SparePart;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SparePartsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
//        dd($row);
        $device_code = trim($row['device_code']);
        $name = trim($row['name']);
        $quantity = trim($row['quantity']);
        $part_type = trim($row['part_type']);
        $serial_number = trim($row['serial_number']);
        $manufacturer = trim($row['manufacturer']);
        $importer = trim($row['importer']);
        $status = trim($row['status']);
        $notes = trim($row['notes']);
        $expire_date = trim($row['expire_date']);
        $entry_date = trim($row['entry_date']);
        $device = Device::where('code', $device_code)->select('id')->first();
        $type = DeviceType::where('name', $part_type)->select('id')->first();
        $company = Manufacturer::where('name', $manufacturer)->select('id')->first();
        $supplier = Importer::where('name', $importer)->select('id')->first();
        if ($device && $type && $company && $supplier) {
            $expire_date = Carbon::parse($expire_date)->format('Y-m-d');
            $expire_date = Carbon::createFromFormat('Y-m-d', $expire_date);
            $hijri_expire_date = \Helper\Helper::georgianToHijri($expire_date->day, $expire_date->month, $expire_date->year, $expire_date);
            $entry_date =Carbon::parse($entry_date)->format('Y-m-d');
            $entry_date = Carbon::createFromFormat('Y-m-d', $entry_date);
            $hijri_entry_date = \Helper\Helper::georgianToHijri($entry_date->day, $entry_date->month, $entry_date->year, $entry_date);
//            dd($expire_date, $hijri_expire_date, $entry_date, $hijri_entry_date);
            $arr = [
                'صالح' => 'valid',
                'غير صالح' => 'not_good',
                'أخرى' => 'other'
            ];
            if (array_key_exists($status, $arr)) {
                $status = $arr[$status];
            } else {
                $status = 'other';
            }
            $check = SparePart::where([
                'name' => $name,
                'device_id' => $device->id,
                'serial_number' => $serial_number,
                'device_type_id' => $type->id,
                'manufacturer_id' => $company->id,
                'importer_id' => $supplier->id
            ])->first();
            if ($check) {
                $check->update([
                    'notes' => $notes,
                    'status' => $status,
                    'quantity' => $quantity,
                    'Validity_date' => $expire_date,
                    'hijri_Validity_date' => $hijri_expire_date,
                    'entry_date' => $entry_date,
                    'hijri_entry_date' => $hijri_entry_date,
                ]);
                return $check;
            } else {
                $sapre_part = new SparePart([
                    'name' => $name,
                    'device_id' => $device->id,
                    'serial_number' => $serial_number,
                    'device_type_id' => $type->id,
                    'manufacturer_id' => $company->id,
                    'importer_id' => $supplier->id,
                    'notes' => $notes,
                    'status' => $status,
                    'quantity' => $quantity,
                    'Validity_date' => $expire_date,
                    'hijri_Validity_date' => $hijri_expire_date,
                    'entry_date' => $entry_date,
                    'hijri_entry_date' => $hijri_entry_date,
                ]);
                return $sapre_part;
            }
        }
        return null;
    }
}
