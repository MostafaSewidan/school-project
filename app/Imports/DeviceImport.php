<?php

namespace App\Imports;

use App\Models\Contract;
use App\Models\Department;
use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Importer;
use App\Models\Manufacturer;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DeviceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        info(json_encode($row));
        $row["install_date"] = trim($row["install_date"]);
        $exploded = explode('/', $row["install_date"]);
        if (count($exploded) == 3) {
            $row["install_date"] = Carbon::createFromDate($exploded[2], $exploded[1], $exploded[0])->toDateString();
        // convert to hijri
        } else {
            $row["install_date"] = null;
        }
        $check = Device::where('code', $row["device_code"])->first();
        if (!$check) {
            if ($row["device_code"]) {
                $serialCheck = Device::where('serial_number', $row["serial_no"])->first();
                $deviceType = DeviceType::firstOrCreate(['name'=>$row["device_type"]]);
                if (isset($row["class"]) && $deviceType->wasRecentlyCreated) {
                    $deviceType->category = $row["class"];
                    $deviceType->save();
                }
                $device = new Device([
                    'code' => $row["device_code"],
                    'serial_number' => ($serialCheck || !$row["serial_no"]) ? 'NO-SN-'.Str::random(6) : $row["serial_no"],// NSN-random
                    'manufacturer_id' => $row["manufacturer"] ? Manufacturer::firstOrCreate(['name' => $row["manufacturer"]])->id : null,
                    'device_type_id' => $row["device_type"] ?$deviceType->id : null,
                    'model' => $row["model"],
                    'department_id' => $row["department"] ? Department::firstOrCreate(['name' => $row["department"]])->id : null,
                    'importer_id' => $row["vendor"] ? Importer::firstOrCreate(['name' => $row["vendor"]])->id : null,
                    'contract_id' => $row["contract_number"] ? Contract::firstOrCreate(['contract_number' => $row["contract_number"]])->id : null,
                    'room_name' => $row["room_name"],
                    'location' => $row["site_name"] ?? null,
                    'location2' => $row["location"] ?? null,
                    'price' => $row["price"],
                    'status' => $row["status"] ?? 'up',
                    'constructing_date' => $row["install_date"],
                ]);
                return $device;
            } else {
                info("no code or serial");
            }


//            info(json_encode($device));
        } else {
            if ($row["device_code"]) {
                $serialCheck = Device::where('serial_number', $row["serial_no"])->where('id', '!=', $check->id)->first();
                $deviceType = DeviceType::firstOrCreate(['name'=>$row["device_type"]]);
                if (isset($row["class"]) && $deviceType->wasRecentlyCreated) {
                    $deviceType->category = $row["class"];
                    $deviceType->save();
                }
                $check->update([
                        'serial_number' => ($serialCheck  || !$row["serial_no"]) ? 'NO-SN-'.Str::random(6) : $row["serial_no"],
                        'manufacturer_id' => $row["manufacturer"] ? Manufacturer::firstOrCreate(['name' => $row["manufacturer"]])->id : null,
                        'device_type_id' => $row["device_type"] ? $deviceType->id : null,
                        'model' => $row["model"],
                        'department_id' => $row["department"] ? Department::firstOrCreate(['name' => $row["department"]])->id : null,
                        'importer_id' => $row["vendor"] ? Importer::firstOrCreate(['name' => $row["vendor"]])->id : null,
                        'contract_id' => $row["contract_number"] ? Contract::firstOrCreate(['contract_number' => $row["contract_number"]])->id : null,
                        'room_name' => $row["room_name"],
                        'location' => $row["location"],
                        'location2' => $row["location2"] ?? null,
                        'price' => $row["price"] ?? null,
                        'status' => $row["status"] ?? 'up',
                        'constructing_date' => $row["install_date"],
                    ]);
            } else {
                info("no code or serial [update]");
            }
            return $check;
        }
    }
}