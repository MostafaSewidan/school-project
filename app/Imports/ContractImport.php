<?php

namespace App\Imports;

use App\Models\Contract;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContractImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $contract_number = trim($row['contract_number']);
        $discount = $row['deduction_for_each_down_day'];
        $contract_type = $row['contract_type'];
        $contract_value = $row['contract_value'];
        $contract_start_date = trim($row['contract_start_date']);
        $contract_end_date = trim($row['contract_end_date']);
        $notes = trim($row['notes']);
        $arr = [
            'تحت الضمان'  => 'guaranteed',
            'عقد شامل' => 'inclusive',
            'عقد صيانات فقط' => 'maintenance_only',
            'بدون عقد أو ضمان' => 'nothing',
            'اخري' => 'other'
        ];
        if (array_key_exists($contract_type, $arr)) {
            $contract_type = $arr[$contract_type];
        } else {
            $contract_type = 'other';
        }
        $check = Contract::where('contract_number', $contract_number)->select('id')->first();
        $contract_start_date = Carbon::parse($contract_start_date)->format('Y-m-d');
        $contract_start_date = Carbon::createFromFormat('Y-m-d', $contract_start_date);
        $hijri_start_date = \Helper\Helper::georgianToHijri($contract_start_date->day, $contract_start_date->month, $contract_start_date->year,true);
        $contract_end_date = Carbon::parse($contract_end_date)->format('Y-m-d');
        $contract_end_date = Carbon::createFromFormat('Y-m-d', $contract_end_date);
        $hijri_end_date = \Helper\Helper::georgianToHijri($contract_end_date->day, $contract_end_date->month, $contract_end_date->year, true);
        if (!$check) {
            $contract = new Contract([
                'contract_number' => $contract_number,
                'contract_type' => $contract_type,
                'cost' => $contract_value,
                'cut_per_day_down' => $discount,
                'contract_start' => $contract_start_date,
                'hijri_contract_start' =>$hijri_start_date ,
                'contract_end' => $contract_end_date,
                'hijri_contract_end' => $hijri_end_date,
                'notes' => $notes
            ]);
            return $contract;
        } else {
            $check->update([
                'contract_type' => $contract_type,
                'cost' => $contract_value,
                'cut_per_day_down' => $discount,
                'contract_start' => $contract_start_date,
                'hijri_contract_start' => $hijri_start_date,
                'contract_end' => $contract_end_date,
                'hijri_contract_end' => $hijri_end_date,
                'notes' => $notes
            ]);
            return $check;
        }
    }
}
