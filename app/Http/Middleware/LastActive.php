<?php

namespace App\Http\Middleware;

use App\User ;
use App\Models\Student;
use App\Models\Teacher;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Closure;
use Illuminate\Auth\AuthenticationException;

class LastActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guard = null;

        if (auth()->check()) {
            if (auth()->user() instanceof Teacher) {
                $guard = 'teacher';
            } elseif (auth()->user() instanceof Student) {
                $guard = 'student';
            } elseif (auth()->user() instanceof User) {
                $guard = 'web';
            }
        }

        if ($guard != null) {
            $user = auth($guard)->user();
            $user->last_active = Carbon::now();
            $user->save();
        }

        return $next($request);
    }
}