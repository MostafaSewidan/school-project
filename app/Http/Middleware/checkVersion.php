<?php

namespace App\Http\Middleware;


use App\MyHelper\Helper;
use Closure;


class checkVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $helper = new Helper();
        if(!in_array(auth('api_client')->user()->app_version , [Helper::settingValue('version_client'),'ios']))
        {
            //return $helper->responseJson(0, 'الرجاء تحميل أحدث إصدار من التطبيق ');
        }

        return $next($request);
    }
}
