<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use Closure;

class autoCheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route()->getName();
        $permission = \Spatie\Permission\Models\Permission::whereRaw("FIND_IN_SET('$route',routes)")->where('guard_name', 'web')->first();

        if ($permission) {

            if (!auth('web')->user()->hasPermissionTo($permission)) {
                 abort(503);
//                session()->flash("error", __("ليس لديك الصلاحية"));
//                return back();
            }
        }
        return $next($request);
    }
}
