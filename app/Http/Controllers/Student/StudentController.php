<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Traits\Master;
use Helper\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    use Master;


    public function __construct()
    {
        $this->model = new Student();
        $this->viewsDomain = 'student.home.';
        $this->viewsUrl = 'student/home';
    }
    // public function viewLogin()
    // {
    //     return view('auth.login');
    // }

    public function home(Request $request)
    {
        return $this->view('index');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profileView()
    {
        $student = auth('student')->user();
        return $this->view('profile-form',compact('student'));
    }

    /**
     * @param Request $request
     */
    public function updateProfile(Request $request)
    {
        $user = auth('student')->user();

        $rules =
        [
            'old_password' => 'nullable',
            'password' => 'required_with:old_password|confirmed',
            'profile_photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails())
            return $this->returnError($data);

        if($request->password && $request->input('password')) {
            if(Hash::check($request->old_password , $user->password))
            {
                $user->update(['password'=> Hash::make($request->password)]);

                session()->flash('success' , 'تمت تحديث بنجاح');
                return redirect('student/reset-password');

            }else{
                return $this->returnError(['old_password' => 'الرجاء ادخال كلمة المرور الحالية ']);
            }
        }

        if($request->hasFile('profile_photo')) {
            Attachment::updateAttachment(
                $request->file('profile_photo'),
                $user->attachmentRelation,
                $user,
                'student-profile'
                );
        }

        $this->viewsUrl = 'student/profile';
        session()->flash('success', trans('student.added_successfully'));
        return $this->returnSuccess();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    }
}