<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Resources\Student\Schedule;
use App\Models\Subject;
use App\Traits\Master;
use Helper\Attachment;
use Helper\Helper;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Subject();
        $this->viewsDomain = 'student.schedule.';
        $this->viewsUrl = 'student/schedule';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->view('index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSessions(Request $request)
    {
        $records = Schedule::collection($this->model->studySchedule(($request->subject ?? null) , true)->get());
        return response()->json([
            'status' => 1,
            'success' => true,
            'records' => $records
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $subjects = $this->model->studentQuery()->latest()->take(5)->get();
        $session = $this->model->studySchedule(null , true)->findOrFail($id);
        $group = optional($session->group);
        $record = optional($session->group->subject);

        return $this->view('show', compact('subjects', 'record', 'session','group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }

    /**
     * @param $subject
     * @param $session
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function appendHomeWork($subject , $session ,Request $request)
    {
        $user = auth('student')->user();
        $record = $this->model->studentQuery()->find($subject);
        $session = $this->model->studySchedule($record->id, true)->find($session);
        $this->viewsUrl = 'student/schedule/'.$record->id;

        $rules =
            [
                'homework_pdf' => 'required|mimes:pdf,PDF',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails())
            return $this->returnError($data);

        if(!$session->home_work_answer) {

            $homeWork = $session->AnswerHomeworks()->create(['student_id' => $user->id]);

            Attachment::addAttachment($request->file('homework_pdf'),$homeWork,'home-work',[
                'save' => 'original',
            ]);

            session()->flash('success', 'تمت الاضافة بنجاح');
            return $this->returnSuccess();
        }

        session()->flash('error', 'حدث خطأ ما حاول مرة أخري');
        return $this->returnSuccess();

    }
}