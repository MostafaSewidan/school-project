<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Resources\Student\Exam as ExamResource;
use App\Models\Exam;
use App\Traits\Master;
use Helper\Attachment;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Exam();
        $this->viewsDomain = 'student.exams.';
        $this->viewsUrl = 'student/exams';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->view('index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExams(Request $request)
    {
        $records = ExamResource::collection($this->model->getExams(($request->subject ?? null) , true)->get());

        return response()->json([
            'status' => 1,
            'success' => true,
            'records' => $records
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $exam = $this->model->getExams(null , true)->findOrFail($id);
        $group = optional($exam->group);
        $subject = optional($exam->group->subject);

        return $this->view('show', compact( 'subject', 'exam','group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }

    /**
     * @param $subject
     * @param $exam
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function appendExam($exam ,Request $request)
    {
        $user = auth('student')->user();
        $exam = $this->model->getExams(null , true)->findOrFail($exam);
        $this->viewsUrl = 'student/exams/'.$exam->id;

        $rules =
            [
                'exam_pdf' => 'required|mimes:pdf,PDF',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails())
            return $this->returnError($data);

        if(in_array($exam->auth_student_answer_status->status,['started'])) {

            $request->merge(['student_id' => $user->id]);
            $answer = $exam->AnswerExamStudents()->create($request->all());

            Attachment::addAttachment($request->file('exam_pdf'),$answer,'exam-answer',[
                'save' => 'original',
            ]);

            session()->flash('success', trans('student.added_successfully'));
            return $this->returnSuccess();
        }

        session()->flash('error', trans('student.can\'t_write_answer_in_this_status'));
        return $this->returnSuccess();

    }
}