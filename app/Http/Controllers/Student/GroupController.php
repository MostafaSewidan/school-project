<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Traits\Master;
use Helper\Attachment;
use Helper\Helper;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Subject();
        $this->viewsDomain = 'student.groups.';
        $this->viewsUrl = 'student/groups';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $records = $this->model->studentQuery()->where(function ($q) use ($request) {

            if ($request->name) {

                $q->where('name', 'LIKE', '%' . $request->name . '%');
            }

        })->latest()->paginate(10);

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $record = $this->model->studentQuery()->findOrFail($id);
        $subjects = $this->model->studentQuery()->latest()->take(3)->get();
        $group = $record->studentGroup();
        $exams = $group->exams()->oldest()->get();
        $sessions = $this->model->studySchedule($record->id, true)->oldest()->get();

        return $this->view('show', compact('subjects', 'record', 'sessions', 'group','exams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }
}