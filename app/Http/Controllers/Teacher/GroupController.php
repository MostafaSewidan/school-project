<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Log;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected $model;
    protected $viewsDomain = 'teacher/groups.';

    public function __construct()
    {
        $this->model =new Group();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $records = $this->model->where('teacher_id', auth()->user()->id)->paginate(10);
        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'        => 'required',
        ];
        $messages = [
            'name.required'        => 'الاسم مطلوب',
        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->create($request->all());
        $record->students()->attach($request->student_list);
        Log::createLog($record, auth()->user(), 'عملية اضافة', 'إضافة  مجموعة #' . $record->id);
        session()->flash('success', __('تم الإضافة'));
        return redirect(route('group.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'        => 'required'
        ];
        $messages = [
        'name.required'        => 'الاسم مطلوب'

        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->findOrFail($id);
        $record->update($request->all());
        $record->levels()->sync($request->level_list);
        Log::createLog($record, auth()->user(), 'عملية تعديل', 'تعديل مادة دراسية #' . $record->id);
        session()->flash('success', __('تم التعديل'));
        return redirect(route('group.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $record = $this->model->find($id);

        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => __('تعذر الحصول على البيانات')
            ]);
        }
        if ($record->students()->count()) {
            return response()->json([
                'status'  => 0,
                'message' => __('يوجد طلاب مرتبطة بهذه المجموعة')
            ]);
        }


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف مادة دراسية ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}