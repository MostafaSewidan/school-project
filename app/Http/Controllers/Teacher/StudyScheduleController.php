<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\StudySchedule;
use App\Models\Log;
use Illuminate\Http\Request;
use Helper\Attachment;

class StudyScheduleController extends Controller
{
    protected $model;
    protected $viewsDomain = 'teacher/study_schedule.';

    public function __construct()
    {
        $this->model =new StudySchedule();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $records = $this->model->whereHas('group', function ($q)
        {

            $q->where('teacher_id', auth()->user()->id);
        })->paginate(10);
        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'group_id' => 'required|exists:groups,id',
                'zoom_link' => 'required',
                'homework' => 'string',
                'datetime' => 'required|date',
                'homework_pdf' => 'mimes:pdf,PDF',
            ];

        // $error_sms =
        //     [
        //         //'datetime.required' => 'الرجاء ادخال تاريخ ',
        //         // 'email.unique' => ' البريد الالكتروني موجود بالفعل',
        //         // 'email.required' => 'الرجاء ادخال البريد الالكتروني',
        //         // 'password.required' => 'الرجاء ادخال كلمة المرور',
        //         // 'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
        //         // 'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
        //         // 'contract.required' => 'الرجاء ارفاق العقد ',
        //         // 'graduation_certificate.required' => 'الرجاء ارفاق شهادة التخرج ',
        //         // 'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
        //         // 'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',

        //     ];

        $this->validate($request, $rules);



        $record = $this->model->create($request->all());
        if ($request->hasFile('homework_pdf')) {
            Attachment::addAttachment($request->file('homework_pdf'), $record, 'teachers/homework_pdf', ['save' => 'original','usage' => 'homework_pdf']);
        }

        Log::createLog($record, auth()->user(), 'عملية اضافة', 'إضافة حصة دراسية #' . $record->id);
        session()->flash('success', __('تم الإضافة'));
        return redirect(route('study-schedule.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'        => 'required',
            'homework_pdf' => 'mimes:pdf,PDF',
        ];
        $messages = [
        'name.required'        => 'الاسم مطلوب'

        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->findOrFail($id);
        $record->update($request->all());
        if ($request->hasFile('homework_pdf')) {
            Attachment::updateAttachment(
                $request->file('homework_pdf'),
                $record->attachmentRelation()->where('usage', 'homework_pdf')->first(),
                $record,
                'teachers/homework_pdf',
                ['save' => 'original','usage' => 'homework_pdf']
            );
        }
        Log::createLog($record, auth()->user(), 'عملية تعديل', 'تعديل مرحلة دراسية #' . $record->id);
        session()->flash('success', __('تم التعديل'));
        return redirect(route('study-schedule.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $record = $this->model->find($id);

        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => __('تعذر الحصول على البيانات')
            ]);
        }
        /*  if ($record->groups()->count()|| $record->students()->count()||$record->subjects()->count()) {
             return response()->json([
                 'status'  => 0,
                 'message' => __('يوجد مجموعات او طلاب او مواد دراسية مرتبطة بهذا المرحلة الدراسية')
             ]);
         } */


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف حصة دراسية ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}
