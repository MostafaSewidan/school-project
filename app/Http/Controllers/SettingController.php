<?php

namespace App\Http\Controllers;

use App\Mail\technicalSupport;
use App\Models\Category;
use App\Models\Log;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SettingController extends Controller
{
    protected $model;
    protected $viewsDomain = 'admin/settings.';
    protected $viewsUrl = 'admin/settings';

    public function __construct()
    {
        $this->model = new Setting();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $record = $this->model->first();

        return view('admin.settings.index', compact('record'));
    }


    public function update(Request $request)
    {
        $rules = [
            'site_name' => 'nullable|max:200',
            'region_name' => 'nullable|max:200',
            'contract_num' => 'nullable',
            'contractor_name' => 'nullable|max:200',
            'contractor_supervisor' => 'nullable',
            'project_start_date' => 'nullable|date',
//            'total_budget' => 'nullable',
            'hijri_project_start_date' => 'nullable|date',
            'project_end_date' => 'nullable|date',
            'hijri_project_end_date' => 'nullable|date',
        ];

        $message = [
            'site_name.required' => 'اسم الموقع مطلوب',
            'site_name.max' => 'الإسم اطول من اللازم',

            'region_name.required' => 'اسم الحي مطلوب',
            'region_name.max' => 'الإسم اطول من اللازم',

            'contract_num.required' => ' ',

            'contractor_name.required' => ' ',
            'contractor_name.max' => 'الإسم اطول من اللازم',

            'contractor_supervisor.required' => ' ',

            'project_start_date.required' => ' ',
            'project_start_date.date' => ' ',

//            'total_budget.required' => ' ',

            'hijri_project_start_date.required' => ' ',
            'project_end_date.required' => ' ',
            'hijri_project_end_date.required' => ' ',
        ];

        $this->validate($request, $rules, $message);

        $settings = $this->model->first();

        if ($settings)
            $settings->update($request->all());
        else
            $this->model->create($request->all());

        session()->flash('success', __('تم الإضافة'));

        return response()->json([
            'success' => true,
            'url' => url($this->viewsUrl)
        ]);
    }

    public function getSupport()
    {
        return view('admin.settings.technical-support');
    }

    public function postSupport(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'nullable|numeric',
            'title' => 'required|string',
            'message' => 'required|string'
        ];
        $messages = [
//            'title.required' => 'العنوان مطلوب',
//            'message.required' => 'المحتوى مطلوب',
        ];
        $this->validate($request, $rules, $messages);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'title' => $request->title,
            'message' => $request->message
        ];
        \Mail::to('hamsupport@zamred.com')
            ->send(new technicalSupport($data));
        session()->flash('success', __('تم الارسال بنجاح'));
        return back();
    }
}
