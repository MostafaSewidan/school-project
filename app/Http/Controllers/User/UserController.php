<?php

namespace App\Http\Controllers\User;

use App\Models\Log;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Helper\Attachment;

use App\Http\Requests;

use App\User;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/visors.';

    public function __construct()
    {
        $this->model =new User();
    }

    public function home()
    {
        return redirect('manager/level');
    }
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $role = json_decode(auth()->user()->getRoleNames())[0];
        $managerId = auth()->user()->id;
        
        if ($role == 'مشرف رئيسي') {
            $records = $this->model->where('parent_id',$managerId )->where(
                function ($query) use ($request) {
                            if ($request->name) {
                                $query->where('name', 'LIKE', '%'.$request->name.'%');
                            }
                            if ($request->email) {
                                $query->where('email', 'LIKE', '%'.$request->email.'%');
                            }
                            if ($request->phone) {
                                $query->where('phone', 'LIKE', '%'.$request->phone.'%');
                            }
                        }
            )->paginate(10);
        } else {
            $records = $this->model->where(
                function ($query) use ($request) {
                if ($request->name) {
                    $query->where('name', 'LIKE', '%'.$request->name.'%');
                }
                if ($request->email) {
                    $query->where('email', 'LIKE', '%'.$request->email.'%');
                }
                if ($request->phone) {
                    $query->where('phone', 'LIKE', '%'.$request->phone.'%');
                }
            }
            )->paginate(10);
        }


        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
                'd_o_b' => 'required|date',
                'national_id' => 'required|mimes:pdf,PDF',
                'criminal_statement' => 'required|mimes:pdf,PDF',
                'contract' => 'required|mimes:pdf,PDF',
                'phone' => 'required|numeric',
                'role' => 'required'
            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'password.required' => 'الرجاء ادخال كلمة المرور',
                'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
                'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
                'contract.required' => 'الرجاء ارفاق العقد ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
                'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',
                'role.required' => 'الرجاء التاكد من  الرتبة ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }
        $user = User::create(request()->all());
        $user->users()->attach($request->user_list);
        $user->update(['password' => Hash::make($request->password)]);

        if ($request->hasFile('national_id')) {
            Attachment::addAttachment($request->file('national_id'), $user, 'visors/national_id', ['save' => 'original','usage' => 'national_id']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('criminal_statement')) {
            Attachment::addAttachment($request->file('criminal_statement'), $user, 'visors/criminal_statement', ['save' => 'original','usage' => 'criminal_statement']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('contract')) {
            Attachment::addAttachment($request->file('contract'), $user, 'visors/contract', ['save' => 'original','usage' => 'contract']);
        }
        // Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);
        $user->assignRole($request->role);
        Log::createLog($user, auth()->user(), 'إضافة مستخدم لوحة تحكم #' . $user->id);
        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect(route('visor.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $record = $this->model->findOrFail($id);

        return $this->view('show', compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $record = User::findOrFail($id);

        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $record->id . '',
                'password' => 'confirmed',
                'd_o_b' => 'required|date',
                'national_id' => 'mimes:pdf,PDF',
                'criminal_statement' => 'mimes:pdf,PDF',
                'contract' => 'mimes:pdf,PDF',
                'phone' => 'required|numeric',
            ];

        $error_sms =
            [
               'name.required' => 'الرجاء ادخال الاسم ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'password.required' => 'الرجاء ادخال كلمة المرور',
                // 'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
                // 'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
                // 'contract.required' => 'الرجاء ارفاق العقد ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
                'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',

            ];
        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect('/manager/visor/' . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));
        $record->users()->sync($request->user_list);
        if ($request->password) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        if ($request->hasFile('national_id')) {
            Attachment::addAttachment($request->file('national_id'), $record, 'visors/national_id', ['save' => 'original','usage' => 'national_id']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('criminal_statement')) {
            Attachment::addAttachment($request->file('criminal_statement'), $record, 'visors/criminal_statement', ['save' => 'original','usage' => 'criminal_statement']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('contract')) {
            Attachment::addAttachment($request->file('contract'), $record, 'visors/contract', ['save' => 'original','usage' => 'contract']);
        }

        Log::createLog($record, auth()->user(), 'تعديل مستخدم لوحة تحكم #' . $record->id);
        \DB::table('model_has_roles')->where('model_id', $id)->delete();

        $record->assignRole($request->role);
        session()->flash('success', __('تم التعديل بنجاح'));

        return redirect(route('visor.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $record = $this->model->findOrFail($id);

        if (auth('web')->user()->id == $record->id) {
            return response()->json([
                'status'  => 0,
                'message' => __('This email, you cannot deactivate it')
            ]);
        }

        if ($record->visor()->count()|| $record->teachers()->count()) {
            return response()->json([
                'status'  => 0,
                'message' => __('يوجد مجموعات او مدرسين او مراحل دراسية مرتبطة بهذا المادة الدراسية')
            ]);
        }


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف مستخدم  ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}