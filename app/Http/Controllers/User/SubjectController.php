<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Models\Log;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/subjects.';

    public function __construct()
    {
        $this->model =new Subject();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $records = $this->model->where(function ($query) use ($request){
            if ($request->name)
            {
                $query->where('name','LIKE','%'.$request->name.'%');
            }
            $query->whereHas('levels',function ($q) use ($request){
                if ($request->level)
                {
                    $q->where('name','LIKE','%'.$request->level.'%');
                }
            });
        })->paginate(10);
        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'        => 'required',
        ];
        $messages = [
            'name.required'        => 'الاسم مطلوب',
        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->create($request->all());
        $record->levels()->attach($request->level_list);
        Log::createLog($record, auth()->user(), 'عملية اضافة', 'إضافة مادة دراسية #' . $record->id);
        session()->flash('success', __('تم الإضافة'));
        return redirect(route('subject.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'        => 'required'
        ];
        $messages = [
        'name.required'        => 'الاسم مطلوب'

        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->findOrFail($id);
        $record->update($request->all());
        $record->levels()->sync($request->level_list);
        Log::createLog($record, auth()->user(), 'عملية تعديل', 'تعديل مادة دراسية #' . $record->id);
        session()->flash('success', __('تم التعديل'));
        return redirect(route('subject.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $record = $this->model->find($id);

        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => __('تعذر الحصول على البيانات')
            ]);
        }
        if ($record->groups()->count()|| $record->teachers()->count()||$record->levels()->count()) {
            return response()->json([
                'status'  => 0,
                'message' => __('يوجد مجموعات او مدرسين او مراحل دراسية مرتبطة بهذا المادة الدراسية')
            ]);
        }


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف مادة دراسية ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}
