<?php

namespace App\Http\Controllers\User;

use App\Models\Log;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MyHelper\Helper;
use App\Models\Student;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class StudentController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/students.';

    public function __construct()
    {
        $this->model =new Student();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $records = $this->model
            ->where(function ($query) use($request)
            {
                if ($request->name)
                {
                    $query->where('name','LIKE','%'.$request->name.'%');
                }

                if ($request->email)
                {
                    $query->where('email','LIKE','%'.$request->email.'%');
                }

                $query->whereHas('groups',function ($query) use ($request)
                {
                    if ($request->level)
                    {
                        $query->where('name','LIKE','%'.$request->level.'%');

                    }
                });
            })
            ->paginate(10);
        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
            ];

        $error_sms =
            [
               'name.required' => 'الرجاء ادخال الاسم ',
               'email.unique' => ' البريد الالكتروني موجود بالفعل',
               'email.required' => 'الرجاء ادخال البريد الالكتروني',
               'password.required' => 'الرجاء ادخال كلمة المرور',
               'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }
        $user = $this->model->create(request()->all());

        $user->update(['password' => Hash::make($request->password)]);

        Log::createLog($user, auth()->user(), 'إضافة مستخدم لوحة تحكم #' . $user->id);
        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect(route('student.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $record = $this->model->findOrFail($id);

        return $this->view('show', compact('record'));
    }

    public function toggleBoolean($id, $action)
    {
        $record = $this->model->findOrFail($id);
        Helper::toggleBoolean($record);
        if ($record->is_active == 1) {
            Log::createLog($record, auth()->user(), 'عملية تفعيل', 'تفعيل  طالب #' . $record->id);
        } else {
            Log::createLog($record, auth()->user(), 'عملية الغاء تفعيل ', 'الغاء تفعيل  طالب #' . $record->id);
        }
        return Helper::responseJson(1, 'تمت العملية بنجاح');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $record = $this->model->findOrFail($id);

        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $record->id . '',
                'password' => 'confirmed',
            ];

        $error_sms =
            [
               'name.required' => 'الرجاء ادخال الاسم ',
               'email.required' => 'الرجاء ادخال البريد الالكتروني',
               'email.unique' => ' البريد الالكتروني موجود بالفعل',
               'password.required' => 'الرجاء ادخال كلمة المرور ',
               'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];
        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect('/manager/students/' . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));

        if ($request->password) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        session()->flash('success', __('تم التعديل بنجاح'));

        return redirect(route('student.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $record = $this->model->findOrFail($id);

        if (auth('web')->user()->id == $record->id) {
            return response()->json([
                'status'  => 0,
                'message' => __('This email, you cannot deactivate it')
            ]);
        }

        /*   if ($record->visor()->count()|| $record->teachers()->count()||$record->supervisor()->count()) {
              return response()->json([
                  'status'  => 0,
                  'message' => __('يوجد مجموعات او مدرسين او مراحل دراسية مرتبطة بهذا المادة الدراسية')
              ]);
          } */


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف طالب  ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}
