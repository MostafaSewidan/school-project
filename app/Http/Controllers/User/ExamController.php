<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Log;
use Illuminate\Http\Request;
use Helper\Attachment;

class ExamController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/exams.';

    public function __construct()
    {
        $this->model =new Exam();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $records = $this->model->paginate(10);
        $totalRecords = $records->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name'     => 'required',
                'group_id' => 'required|exists:groups,id',
                'exam_pdf' => 'mimes:pdf,PDF',
            ];

        $error_sms =
            [
                //'datetime.required' => 'الرجاء ادخال تاريخ ',
                // 'email.unique' => ' البريد الالكتروني موجود بالفعل',
                // 'email.required' => 'الرجاء ادخال البريد الالكتروني',
                // 'password.required' => 'الرجاء ادخال كلمة المرور',
                // 'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
                // 'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
                // 'contract.required' => 'الرجاء ارفاق العقد ',
                // 'graduation_certificate.required' => 'الرجاء ارفاق شهادة التخرج ',
                // 'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
                // 'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',

            ];

        $this->validate($request, $rules, $error_sms);



        $record = $this->model->create($request->all());
        if ($request->hasFile('exam_pdf')) {
            Attachment::addAttachment($request->file('exam_pdf'), $record, 'teachers/exam_pdf', ['save' => 'original','usage' => 'exam_pdf']);
        }

        Log::createLog($record, auth()->user(), 'عملية اضافة', 'إضافة  امتحان #' . $record->id);
        session()->flash('success', __('تم الإضافة'));
        return redirect(route('exam.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
              'name'     => 'required',
                'group_id' => 'required|exists:groups,id',
                'exam_pdf' => 'mimes:pdf,PDF',
        ];
        $messages = [
        //'name.required'        => 'الاسم مطلوب'

        ];
        $this->validate($request, $rules, $messages);

        $record = $this->model->findOrFail($id);
        $record->update($request->all());
        Log::createLog($record, auth()->user(), 'عملية تعديل', 'تعديل  امتحان #' . $record->id);
        session()->flash('success', __('تم التعديل'));
        return redirect(route('exam.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $record = $this->model->find($id);

        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => __('تعذر الحصول على البيانات')
            ]);
        }
        /*  if ($record->groups()->count()|| $record->students()->count()||$record->subjects()->count()) {
             return response()->json([
                 'status'  => 0,
                 'message' => __('يوجد مجموعات او طلاب او مواد دراسية مرتبطة بهذا المرحلة الدراسية')
             ]);
         } */


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف حصة دراسية ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}