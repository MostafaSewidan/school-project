<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    //
    public function viewLogin()
    {
        return view('manager.auth.login');
    }


    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ];

        $message = [
            'email.required' => 'البريد الإلكترني مطلوب',
            'email.email' => 'الرجاء ادخال البريد الإلكتروني بشكل صحيح',
            'email.exists' => 'البريد الاكتروني غير مسجل بقواعد البيانات',
            'password.required' => 'كلمة المرور مطلوبة'
        ];


        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        } else {
            $remember = $request->input('remember') && $request->remember == 1 ? $request->remember : 0;

            if (auth()->guard('web')->attempt(['email' => $request->email , 'password' => $request->password], $remember)) {
                return redirect(route('manager.home'));
            } else {
                return back()->withInput()->withErrors(['email' => 'خطأ في البريد الإلكتروني أو كلمة المرور']);
            }
        }
    }


    public function managerLogout(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return back();
    }
}