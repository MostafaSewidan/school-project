<?php

namespace App\Http\Controllers\User;

use App\Models\Log;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Helper\Attachment;

use App\Http\Requests;

use App\Models\Teacher;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class TeacherController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/teachers.';

    public function __construct()
    {
        $this->model = new Teacher();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $records = $this->model->with('subjects')
            ->where(function ($query) use ($request) {
                if ($request->name) {
                    $query->where('name', 'LIKE', '%'.$request->name.'%');
                }
                if ($request->email) {
                    $query->where('email', 'LIKE', '%'.$request->email.'%');
                }
                if ($request->phone) {
                    $query->where('phone', 'LIKE', '%'.$request->phone.'%');
                }
                $query->whereHas('subjects', function ($query) use ($request) {
                    if ($request->subject) {
                        $query->where('name', 'LIKE', '%'.$request->subject.'%');
                    }
                });
                $query->whereHas('users', function ($query) use ($request) {
                    if ($request->users) {
                        $query->where('name', 'LIKE', '%'.$request->users.'%');
                    }
                });
            })->paginate(10);
        $records = $this->model->paginate(10);
        $totalRecords = $this->model->count();

        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
                'd_o_b' => 'required|date',
                'national_id' => 'required|mimes:pdf,PDF',
                'criminal_statement' => 'required|mimes:pdf,PDF',
                'graduation_certificate' => 'required|mimes:pdf,PDF',
                'contract' => 'required|mimes:pdf,PDF',
                'phone' => 'required|numeric',

            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'password.required' => 'الرجاء ادخال كلمة المرور',
                'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
                'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
                'contract.required' => 'الرجاء ارفاق العقد ',
                'graduation_certificate.required' => 'الرجاء ارفاق شهادة التخرج ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
                'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }
        $user = $this->model->create(request()->all());
        $user->subjects()->attach($request->subject_list);
        $user->users()->attach($request->user_list);


        $user->update(['password' => Hash::make($request->password)]);

        if ($request->hasFile('national_id')) {
            Attachment::addAttachment($request->file('national_id'), $user, 'teachers/national_id', ['save' => 'original','usage' => 'national_id']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('criminal_statement')) {
            Attachment::addAttachment($request->file('criminal_statement'), $user, 'teachers/criminal_statement', ['save' => 'original','usage' => 'criminal_statement']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('contract')) {
            Attachment::addAttachment($request->file('contract'), $user, 'teachers/contract', ['save' => 'original','usage' => 'contract']);
        }
        // Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('graduation_certificate')) {
            Attachment::addAttachment($request->file('graduation_certificate'), $user, 'teachers/graduation_certificate', ['save' => 'original','usage' => 'graduation_certificate']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);




        Log::createLog($user, auth()->user(), 'إضافة مدرس الي لوحة تحكم #' . $user->id);
        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect(route('teacher.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $record = $this->model->findOrFail($id);

        return $this->view('show', compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    public function editvisor($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('editvisor', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $record = $this->model->findOrFail($id);

        $rules =
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
                'd_o_b' => 'required|date',
                'national_id' => 'mimes:pdf,PDF',
                'criminal_statement' => 'mimes:pdf,PDF',
                'graduation_certificate' => 'mimes:pdf,PDF',
                'contract' => 'mimes:pdf,PDF',
                'phone' => 'required|numeric',

            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'password.required' => 'الرجاء ادخال كلمة المرور',
                // 'national_id.required' => 'الرجاء ارفاق صورة البطاقة ',
                // 'criminal_statement.required' => ' الرجاء ارفاق الفيش و التشبيه ',
                // 'contract.required' => 'الرجاء ارفاق العقد ',
                // 'graduation_certificate.required' => 'الرجاء ارفاق شهادة التخرج ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
                'phone.required' => 'الرجاء التاكد من  رقم الهاتف ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect('/manager/teacher/' . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));
        $record->subjects()->sync($request->subject_list);
        $record->users()->sync($request->user_list);

        if ($request->password) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        if ($request->hasFile('national_id')) {
            Attachment::addAttachment($request->file('national_id'), $record, 'teachers/national_id', ['save' => 'original','usage' => 'national_id']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('criminal_statement')) {
            Attachment::addAttachment($request->file('criminal_statement'), $record, 'teachers/criminal_statement', ['save' => 'original','usage' => 'criminal_statement']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('contract')) {
            Attachment::addAttachment($request->file('contract'), $record, 'teachers/contract', ['save' => 'original','usage' => 'contract']);
        }
        // Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);

        if ($request->hasFile('graduation_certificate')) {
            Attachment::addAttachment($request->file('graduation_certificate'), $record, 'teachers/graduation_certificate', ['save' => 'original','usage' => 'graduation_certificate']);
        }
        //Log::createLog($user, auth()->user(), 'عملية اضافة', 'إضافة صورة بطاقة #' . $user->id);


        session()->flash('success', __('تم التعديل بنجاح'));

        return redirect(route('teacher.index'));
    }

    public function updateVisor(Request $request, $id)
    {
        $record = $this->model->findOrFail($id);

        $record->users()->sync($request->user_list);

        session()->flash('success', __('تم التعديل بنجاح'));

        return  back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $record = $this->model->findOrFail($id);

        if (auth('web')->user()->id == $record->id) {
            return response()->json([
                'status'  => 0,
                'message' => __('This email, you cannot deactivate it')
            ]);
        }

        if ($record->subjects()->count() || $record->groups()->count() || $record->users()->count()) {
            return response()->json([
                'status'  => 0,
                'message' => __('يوجد مجموعات او حصص او  طلاب مرتبطة بهذا المدرس')
            ]);
        }


        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف مستخدم  ' . $record->name);


        return response()->json([
            'status'  => 1,
            'message' => __('تم الحذف بنجاح'),
            'id'      => $id
        ]);
    }
}