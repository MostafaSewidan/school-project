<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\StudySchedule;
use App\Models\Log;
use App\Models\Group;
use App\Models\GroupStudent;
use Illuminate\Http\Request;
use Helper\Attachment;
use Carbon\Carbon;

class StudyScheduleController extends Controller
{
    protected $model;
    protected $viewsDomain = 'manager/study_schedule.';

    public function __construct()
    {
        $this->model = new StudySchedule();
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $records = $this->model->with('group')->paginate(10);
        $totalRecords = count($records);
        // dd($records);
        return $this->view('index', compact('records', 'totalRecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $record = $this->model->get();
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'group_id' => 'required|exists:groups,id',
                'zoom_link' => 'url',
                'homework' => 'string',
                'datetime' => 'required',
                'homework_pdf' => 'mimes:pdf,PDF',
            ];
        $this->validate($request, $rules);

        /**
         * 1) get student in current group
         * 2) get all groups for any studant in current group
         * 3) check if currunt time between any start and end scheduleData
         * 4)
         */

        $CurrentGroupStudents = GroupStudent::where('group_id', $request->group_id)->select('student_id')->get()->toArray();
        $Allstudentincurrentroup = Group::whereHas('students', function ($q) use ($CurrentGroupStudents) {
            $q->whereIn('student_id', $CurrentGroupStudents);
        })->select('id')->get()->toArray();


        $old = $this->model->whereIN('group_id', $Allstudentincurrentroup)->get();
        foreach ($old as $scheduleData) {
            if (strtotime($request->datetime) > $scheduleData->start_time && strtotime($request->datetime) < $scheduleData->end_time) {
                session()->flash('error', 'احد طلاب المجموعة او المجموعة كاملة لديه معاد محجوز بالفعل');
                return back();
            }
        }

        $request->merge(['datetime' => Carbon::parse($request->datetime)->toDateTimeString()]);
        $record = $this->model->create($request->all());
        if ($request->hasFile('homework_pdf')) {
            Attachment::addAttachment($request->file('homework_pdf'), $record, 'teachers/homework_pdf', ['save' => 'original', 'usage' => 'homework_pdf']);
        }
        Log::createLog($record, auth()->user(), 'عملية اضافة', 'إضافة حصة دراسية #' . $record->id);
        session()->flash('success', __('تم الإضافة'));
        return redirect(route('study-schedule.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->model->findOrFail($id);
        $edit = true;
        return $this->view('edit', compact('record', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'zoom_link' => 'url',
            'homework' => 'string',
            'homework_pdf' => 'mimes:pdf,PDF',
        ];
        $messages = [
            'zoom_link.required' => 'رابط زوم مطلوب'
        ];

        $this->validate($request, $rules, $messages);

        $record = $this->model->findOrFail($id);
        $record->update($request->all());
        if ($request->hasFile('homework_pdf')) {
            Attachment::updateAttachment(
                $request->file('homework_pdf'),
                $record->attachmentRelation()->where('usage', 'homework_pdf')->first(),
                $record,
                'teachers/homework_pdf',
                ['save' => 'original','usage' => 'homework_pdf']
            );
        }
        Log::createLog($record, auth()->user(), 'عملية تعديل', 'تعديل مرحلة دراسية #' . $record->id);
        session()->flash('success', __('تم التعديل'));
        return redirect(route('study-schedule.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $record = $this->model->find($id);
        if (!$record) {
            return response()->json([
                'status' => 0,
                'message' => __('تعذر الحصول على البيانات')
            ]);
        }
        $record->delete();
        Log::createLog($record, auth()->user(), 'عملية حذف', 'حذف حصة دراسية ' . $record->name);

        return response()->json([
            'status' => 1,
            'message' => __('تم الحذف بنجاح'),
            'id' => $id
        ]);
    }
}