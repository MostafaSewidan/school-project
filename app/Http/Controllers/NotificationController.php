<?php

namespace App\Http\Controllers;

use App\Models\CharitableActor;
use App\Models\City;
use App\Models\Client;
use App\Models\Delivery;
use App\Models\Governorate;
use App\Models\Job;
use App\Models\Notification;
use App\Models\PointsTransaction;
use App\Models\Store;
use App\MyHelper\Helper;
use App\MyHelper\Sms;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $model;
    protected $viewsDomain = 'admin/notifications.';
    protected $url = 'admin/notifications';

    public function __construct()
    {
        $this->model = new Notification();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $govs = Governorate::pluck('name', 'id')->toArray();
        return $this->view('form', compact('govs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $record = $this->model;
        return $this->view('create', compact('record'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'body' => 'required',
            'types' => 'required',
            'send_type' => 'required|in:all,sms,notification',
            'types.*' => 'required|in:clients,stores,deliveries',
        ];
        $messages = [
            'title.required' => ' ',
            'body.required' => ' ',
            'send_type.required' => 'اختر نوع الإرسال ',
            'types.required' => 'اختر نوع واحد من أنواع العملاء علي الأقل ',
            'types.in' => 'اختر نوع واحد من أنواع العملاء علي الأقل ',
        ];
        $this->validate($request, $rules, $messages);

            if (in_array('clients', $request->types)) {
                $clients = Client::where('status', 'active')->where(function($query) use($request){
                    if ($request->region) {

                        $query->whereHas('addresses', function ($q) use ($request) {
                            $q->whereHas('region', function ($q) use ($request) {
                                $q->where('id', $request->region);
                            });
                        });

                    } elseif ($request->city) {

                        $query->whereHas('addresses', function ($q) use ($request) {
                            $q->whereHas('region', function ($q) use ($request) {
                                $q->whereHas('city', function ($q) use ($request) {
                                    $q->where('id', $request->city);
                                });
                            });
                        });

                    } elseif ($request->governorate) {

                        $query->whereHas('addresses', function ($q) use ($request) {
                            $q->whereHas('region', function ($q) use ($request) {
                                $q->whereHas('city', function ($q) use ($request) {
                                    $q->whereHas('governorate', function ($q) use ($request) {
                                        $q->where('id', $request->governorate);
                                    });
                                });
                            });
                        });
                    }
                });

                $this->sendNotifyAndSms($request, $clients, 'clients');
            }

            if (in_array('stores', $request->types)) {
                $stores = Store::Check()->where(function($query) use($request){
                    if ($request->region) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->where('id', $request->region);
                        });

                    } elseif ($request->city) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->whereHas('city', function ($q) use ($request) {
                                $q->where('id', $request->city);
                            });
                        });

                    } elseif ($request->governorate) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->whereHas('city', function ($q) use ($request) {
                                $q->whereHas('governorate', function ($q) use ($request) {
                                    $q->where('id', $request->governorate);
                                });
                            });
                        });
                    }
                });

                $this->sendNotifyAndSms($request, $stores, 'stores');
            }

            if (in_array('deliveries', $request->types)) {
                $deliveries = Delivery::where('status', 'active')->where(function ($query) use($request){
                    if ($request->region) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->where('id', $request->region);
                        });

                    } elseif ($request->city) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->whereHas('city', function ($q) use ($request) {
                                $q->where('id', $request->city);
                            });
                        });

                    } elseif ($request->governorate) {

                        $query->whereHas('region', function ($q) use ($request) {
                            $q->whereHas('city', function ($q) use ($request) {
                                $q->whereHas('governorate', function ($q) use ($request) {
                                    $q->where('id', $request->governorate);
                                });
                            });
                        });
                    }
                });

                $this->sendNotifyAndSms($request, $deliveries, 'deliveries');
            }


        session()->flash('success', __('تم الاضافة بنجاح'));
        return redirect($this->url);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $notifyRelation
     * @param $records
     * @return mixed
     */
    public function sendNotifyAndSms(Request $request, $records, $notifyRelation)
    {
        if ($request->send_type == 'all' || $request->send_type == 'notification')
        {
            $file = $request->hasFile('photo') ? $request->file('photo') : null;
            Helper::sendNotification(auth()->user(), $records->pluck('id')->toArray(), $notifyRelation, $request->title, $request->body,'admin',[],$file);
        }

        if ($request->send_type == 'all' || $request->send_type == 'sms') {

            if(in_array(auth()->user()->id , [10,1]))
            {
                $message = $this->view('sms' , compact('request'))->render();

                $records->whereNotNull('phone')->chunk(50, function ($record) use ($message){

                    $phones = $record->pluck('phone')->toArray();

                    (new Sms)->send($phones, $message);
                });
            }

        }

    }


}
