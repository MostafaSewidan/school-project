<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller ;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Student;


use Illuminate\Http\Request;

class MainController extends Controller
{
    public function subjects(Request $request)
    {
        $data = Subject::whereHas('levels', function ($q) use ($request) {
            $q->where('level_id', $request->level_id);
        })->get();
        return response()->json($data);
    }
    public function teachers(Request $request)
    {
        $data = Teacher::whereHas('subjects', function ($q) use ($request) {
            $q->where('subject_id', $request->subject_id);
        })->get();
        return response()->json($data);
    }
    public function students(Request $request)
    {
        $data = Student::whereHas('groups', function ($query) use ($request) {
            $query->whereNotIn('subject_id', [$request->subject_id]);
        })->get();

        return response()->json($data);
    }
}