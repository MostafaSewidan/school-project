<?php

namespace App\Http\Controllers;

use App\Models\CharitableActor;
use App\Models\City;
use App\Models\Client;
use App\Models\Delivery;
use App\Models\Governorate;
use App\Models\Job;
use App\Models\Log;
use App\Models\Notification;
use App\Models\PointsTransaction;
use App\Models\Store;
use App\MyHelper\Helper;
use App\MyHelper\Sms;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Route;

class LogController extends Controller
{
    protected $model;
    protected $viewsDomain = 'admin/logs.';
    protected $url = 'admin/logs';

    public function __construct()
    {
        $this->model = new Log();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $records = Log::where(function ($q) use ($request){

            if ($request->logable_type) {
                $q->where('logable_type', 'App\Models\\' . $request->logable_type);
            }

            if ($request->logable_id) {
                $q->where('logable_id',  $request->logable_id);
            }

            if ($request->type) {
                $q->where('type',  $request->type);
            }

            if ($request->user_id) {
                $q->where('user_id',  $request->user_id);
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', $request->from);
            }

            if ( $request->to) {

                $q->whereDate('created_at', '<=', $request->to);
            }

        })->latest()->paginate($request->input('paginate' , 15));

        return $this->view('index',compact('records'));
    }


}
