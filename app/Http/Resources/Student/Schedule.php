<?php

namespace App\Http\Resources\Student;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Schedule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'title'         => optional($this->group->subject)->name,
            'start'         => Carbon::parse($this->datetime)->toDateTimeString(),
            'end'         => Carbon::parse($this->datetime)->addMinutes(optional($this->group)->duration)->toDateTimeString(),
            'allDay'         => false,
            'url'         => url('student/schedule/'.$this->id),
        ];
    }
}
