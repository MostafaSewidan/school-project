<?php

namespace App\Http\Resources\Student;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Exam extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'title'         => $this->name,
            'start'         => Carbon::parse($this->start_datetime)->toDateTimeString(),
            'end'         => Carbon::parse($this->end_datetime)->toDateTimeString(),
            'allDay'         => false,
            'url'         => url('student/exams/'.$this->id),
        ];
    }
}
