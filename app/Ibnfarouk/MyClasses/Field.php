<?php

namespace App\Ibnfarouk\MyClasses;

use Form;

/**
 * to create dynamic fields for modules
 */
class Field
{

    function __construct()
    {
        # code...
    }

    /**
     * @param $name
     * @param $label
     * @return string
     */
    public static function text($name, $label, $error = null, $old = null)
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::text($name, $old, [
                    "class" => "form-control",
                    "id" => $name
                ]) . '
                </div>
                
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::text($name, null, [
                    "class" => "form-control",
                    "style" => "border : 1px solid red",
                    "id" => $name
                ]) . '
                </div>
                
                <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';
        }

    }


    public static function checkBox($name, $model , $check = '',$column = 'display_name')
    {
        return ' 
            <div class="form-group col-lg-4" id="' . $name . '_wrap">
                
                <div class="">
                     <input type="checkbox" name="' . $name . '[]" value="' . $model->id . '" '.$check.'>
                     
                    <label for="' . $name . '">' . $model->$column . '</label>
                </div>
            </div>
        ';


    }


    public static function validationError($errors, $name)
    {
        if ($errors)
        {
            if ($errors->any()) {
                $error = $errors->toArray();
                if (array_key_exists($name, $error)) {
                    return implode('', $error[$name]);
                }
            }
        }
        return null;

    }

    /**
     * @param $name
     * @param $label
     * @param float $step
     * @return string
     */
    public static function number($name, $label, $error = null, $step = 0.01)
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::number($name, null, [
                    "class" => "form-control",
                    "id" => $name,
                    "step" => $step
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::number($name, null, [
                    "class" => "form-control",
                    "id" => $name,
                    "step" => $step
                ]) . '
                </div>
                   <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';


        }

    }

    /**
     * @param $name
     * @param $label
     * @return string
     */
    public static function email($name, $label, $error = null)
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::email($name, null, [
                    "class" => "form-control",
                    "id" => $name
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::email($name, null, [
                    "class" => "form-control",
                    "id" => $name
                ]) . '
                </div>
                     <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
            
        ';

        }


    }

    /**
     * @param $name
     * @param $label
     * @return string
     */
    public static function password($name, $label, $error = null)
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::password($name, [
                    "class" => "form-control",
                    "id" => $name
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::password($name, [
                    "class" => "form-control",
                    "id" => $name
                ]) . '
                </div>
                 <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';

        }

    }

    /**
     * @param $name
     * @param $label
     * @param $plugin
     * @return string
     */
    public static function date($name, $label, $error = null, $plugin = 'datepicker')
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::text($name, null, [
                    "class" => "form-control " . $plugin,
                    "id" => $name
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::text($name, null, [
                    "class" => "form-control " . $plugin,
                    "id" => $name
                ]) . '
                </div>
                <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';

        }

    }


    /**
     * @param $name
     * @param $label
     * @param $options
     * @param string $plugin
     * @param string $placeholder
     * @param null $selected
     * @return string
     */
    public static function select($name, $label, $options, $error = null, $plugin = null, $placeholder = 'Choose', $selected = null)
    {
        if (self::validationError($error, $name) == null) {
            return '  
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>                              
                <div class="">
                     ' . Form::select($name, $options, $selected, [
                    "class" => "form-control ",
                    "id" => $name,
                    "placeholder" => $placeholder
                ]) . '
                </div>
            </div>
        ';
        } else {
            return '  
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>                              
                <div class="">
                     ' . Form::select($name, $options, $selected, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "placeholder" => $placeholder
                ]) . '
                </div>
                <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';

        }


    }


    public static function multiSelect($name, $label, $options, $error = null, $selected = null, $plugin = 'select2', $placeholder = 'قم بالاختيار')
    {
        if (self::validationError($error, $name) == null) {
            return '  
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>                              
                <div class="">
                     ' . Form::select($name . '[]', $options, $selected, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "multiple" => "multiple",
                    //            "placeholder"=> $placeholder
                ]) . '
                </div>
            </div>
        ';
        } else {
            return '  
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>                              
                <div class="">
                     ' . Form::select($name . '[]', $options, $selected, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "multiple" => "multiple",
                    //            "placeholder"=> $placeholder
                ]) . '
                </div>
                 <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';

        }

    }

    /**
     * @param $name
     * @param $label
     * @return string
     */
    public static function textarea($name, $label, $error = null)
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::textarea($name, null, [
                    "class" => "form-control",
                    "id" => $name,
                    "rows" => 5
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group" id="' . $name . '_wrap">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::textarea($name, null, [
                    "class" => "form-control",
                    "id" => $name,
                    "rows" => 5
                ]) . '
                </div>
                   <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';

        }

    }

    /**
     * @param $id
     * @param bool $address
     * @param bool $coordinates
     * @param string $height
     * @param string $latitude
     * @param string $longitude
     * @return string
     */
    public static function gMap($id, $address = true, $coordinates = true, $height = '350',$latitude=null,$longitude=null )
    {
        $field = new static;

        $addressField = '';
        if ($address) {
            $addressField = $field->text('address_en', 'ابحث عن عنوان',null);
        }

        $coordinatesFields = '';
        if ($coordinates) {
            $coordinatesFields = '
                <div class="row">
                    <div class="col-xs-6">
                        ' . $field->text('latitude', 'خط الطول',null,$latitude) . '
                    </div>
                    <div class="col-xs-6">
                        ' . $field->text('longitude', 'خط العرض',null,$longitude) . '
                    </div>
                </div>
            ';
        }

        return '<div id="' . $id . '">' . $addressField . '<div id="mapField" style="width: 100%; height: ' . $height . 'px;"></div>' . $coordinatesFields . '</div>';

    }

    /**
     * @param $name
     * @param $label
     * @param string $plugin
     * @return string
     */
    public static function fileWithPreview($name, $label, $error = null, $plugin = "file_upload_preview")
    {
        if (self::validationError($error, $name) == null) {
            return ' 
            <div class="form-group">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::file($name, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "data-preview-file-type" => "text"
                ]) . '
                </div>
            </div>
        ';
        } else {
            return ' 
            <div class="form-group">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::file($name, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "data-preview-file-type" => "text"
                ]) . '
                </div>
                 <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
            </div>
        ';
        }


    }

    /**
     * @param $name
     * @param $label
     * @param string $plugin
     * @return string
     */
    public static function multiFileUpload($name, $label, $plugin = "file_upload_preview")
    {
        return ' 
            <div class="form-group">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::file($name, [
                "class" => "form-control " . $plugin,
                "id" => $name,
                "multiple" => "multiple",
                "data-preview-file-type" => "text"
            ]) . '
                </div>
            </div>
        ';
    }

    /**
     * @param $name
     * @param $label
     * @return string
     */
    public static function tagsInput($name, $label)
    {
        return ' 
            <div class="form-group">
                <label for="' . $name . '">' . $label . '</label>
                <div class="">
                     ' . Form::text($name, null, [
                "class" => "form-control tagsinput",
                "id" => $name,
                "data-role" => "tagsinput",
                "onchange" => "return false"
            ]) . '
                </div>
            </div>
        ';
    }

    /**
     * summernote editor
     *
     * @param $name
     * @param $label
     * @param null $error
     * @param string $plugin
     * @return string
     */
    public static function editor($name, $label, $error = null, $plugin = 'summernote')
    {

        if (self::validationError($error, $name) == null) {

            return '<div class="form-group"><label for="' . $name . '">' . $label . '</label>
                  <div class=""> ' . Form::textarea($name, null, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "rows" => 10
                ]) . ' </div></div>';
        } else {

            return '<div class="form-group"><label for="' . $name . '">' . $label . '</label>
                  <div class=""> ' . Form::textarea($name, null, [
                    "class" => "form-control " . $plugin,
                    "id" => $name,
                    "rows" => 10
                ]) . ' </div>
                   <label id=' . $name . ' class="error_sms">
                <i class="fa fa-exclamation-circle" style="padding-left: 4px"></i>
                ' . self::validationError($error, $name) . '
                </label>
                  </div>';

        }


    }
}
