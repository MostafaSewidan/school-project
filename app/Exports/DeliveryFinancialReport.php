<?php

namespace App\Exports;

use App\Models\Order;
use App\MyHelper\Helper;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;

class DeliveryFinancialReport implements FromView,ShouldAutoSize, WithHeadings, WithEvents
{
    private $request;
    protected $model;
    protected $helper;

    public function __construct($request)
    {
        $this->request = $request;

        $this->model = new Order();
        $this->helper = new Helper();
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }

    public function view(): View
    {
        $request = new Request();
        $request->merge($this->request);

        $records = $this->model->where(function ($query) use ($request) {

            $query->whereIn('status',['delivery_handed','client_handed','canceled','rejected','returned']);

            if ($request->order_id) {
                $query->where('id', $request->order_id);
            }

            if ($request->client_id) {
                $query->where('client_id', $request->client_id);
            }

            if ($request->status) {
                $query->where('status', $request->status);
            }

            if ($request->delivery_id) {
                $query->where('delivery_id', $request->delivery_id);
            }

            if ($request->store_id) {
                $query->whereHas('stores', function ($query) use ($request) {
                    $query->where('stores.id', $request->store_id);
                });
            }

            if ($request->has_coupon == 1) {
                $query->has('coupon');
            }

            if ($request->region) {

                $query->whereHas('address', function ($q) use ($request) {
                    $q->whereHas('region', function ($q) use ($request) {
                        $q->where('id', $request->region);
                    });
                });

            } elseif ($request->city) {

                $query->whereHas('address', function ($q) use ($request) {
                    $q->whereHas('region', function ($q) use ($request) {
                        $q->whereHas('city', function ($q) use ($request) {
                            $q->where('id', $request->city);
                        });
                    });
                });

            } elseif ($request->governorate) {

                $query->whereHas('address', function ($q) use ($request) {
                    $q->whereHas('region', function ($q) use ($request) {
                        $q->whereHas('city', function ($q) use ($request) {
                            $q->whereHas('governorate', function ($q) use ($request) {
                                $q->where('id', $request->governorate);
                            });
                        });
                    });
                });
            }

            if ($request->from && $request->to) {
                $query->whereDate('created_at', '>=', $request->from);
                $query->whereDate('created_at', '<=', $request->to);
            }
        })->latest('asked_at')->take(3000)->get();

        return view('admin.reports.delivery-financial-excel', compact('records'));
    }


    /**
     * @return Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
    }
}
