<?php


namespace App\my_helper;


use Illuminate\Support\Facades\File;
use \Intervention\Image\Facades\Image;

class Ipda3Cms
{


    static function addPhoto($file, $model, $folder_name)
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);

        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        $model->photo()->create($input);
    }

    static function addTeacherPhoto($file, $model, $folder_name)
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(225, 225, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);

        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        $model->photo()->create($input);
    }

    static function addGalleryPhoto($file, $model, $folder_name)
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(360, 275, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);

        $input =
            [
                'category_id' => $model->id,
                'extension'   => $extension,
                'original'    => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400'   => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        $model->galleries()->create($input);
    }

    static function addClassRoomPhoto($file, $model, $folder_name)
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(360, 220.86, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);

        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        $model->photo()->create($input);
    }

    static function updatePhoto($file, $oldFiles, $model, $folder_name)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->original);
            File::delete(public_path() . '/' . $oldFiles->photo_400);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);
        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        if ($oldFiles) {
            $model->photo()->update($input);
        } else {

            $model->photo()->create($input);
        }

    }

    static function updateLogoPhoto($file, $oldFiles, $model, $folder_name)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->original);
            File::delete(public_path() . '/' . $oldFiles->photo_400);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);
        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        if ($oldFiles) {
            $model->photo()->update($input);
        } else {

            $model->photo()->create($input);
        }

    }

    static function updateTeacherPhoto($file, $oldFiles, $model, $folder_name)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->original);
            File::delete(public_path() . '/' . $oldFiles->photo_400);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(225, 225, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);
        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        if ($oldFiles) {
            $model->photo()->update($input);
        } else {

            $model->photo()->create($input);
        }

    }

    static function updateGalleryPhoto($file, $oldFiles, $model, $folder_name)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->original);
            File::delete(public_path() . '/' . $oldFiles->photo_400);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(360, 275, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);
        $input =
            [
                'category_id' => $model->id,
                'extension'   => $extension,
                'original'    => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400'   => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        if ($oldFiles) {
            $oldFiles->update($input);
        } else {

            $oldFiles->create($input);
        }

    }

    static function updateClassRoomPhoto($file, $oldFiles, $model, $folder_name)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->original);
            File::delete(public_path() . '/' . $oldFiles->photo_400);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;
        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(360, 220.86, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);
        $input =
            [
                'extension' => $extension,
                'original'  => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'photo_400' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,
            ];

        if ($oldFiles) {
            $model->photo()->update($input);
        } else {

            $model->photo()->create($input);
        }

    }

    static function deletePhoto($model)
    {
        $photo = $model->photo;

        File::delete(public_path() . '/' . $photo->original);
        File::delete(public_path() . '/' . $photo->photo_400);
        $model->photo()->delete();
    }

    static function deleteGalleryPhotos($model)
    {
        $photo = $model;
//         dd($photo);
        File::delete(public_path() . '/' . $photo->original);
        File::delete(public_path() . '/' . $photo->photo_400);
        $model->delete();
    }
}