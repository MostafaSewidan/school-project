<?php


namespace App\MyHelper;


use App\Models\Notification;
use App\Models\Setting;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class Helper
{

    public $error = ['error' => true, 'status' => 400];
    public $success = ['success' => true, 'status' => 200];
    public $status = ['error' => 400, 'success' => 200];

    protected $responseTo;

    public function __construct($responseTo = 'api')
    {
        $this->responseTo = $responseTo;
    }

    //////////////////////////////////////////////////////////////////////
    ///

    public function getResponseTo()
    {
        echo $this->responseTo;
    }

    public function getDownload(Request $request)
    {
        $file = $request->file_path;
        $fileName = $request->file_name . '.' . explode('.', $file)[count(explode('.', $file)) - 1];

        try {

            return \Response::download($file, $fileName);

        } catch (\Exception $e) {

            session()->flash('fail', 'لم تم العثور علي الملف');
            return back();
        }

    }

    static function responseJson($status, $massage, $data = null, $newAttr = [])
    {

        if ($data == null) {
            $response =
                [
                    'status' => $status,
                    'massage' => $massage
                ];
        } else {
            $response =
                [
                    'status' => $status,
                    'massage' => $massage,
                    'data' => $data
                ];
        }

        if (count($newAttr)) {
            $response += $newAttr;
        }

        return response()->json($response);
    }

    public function switchResponseJson($status = 'success', $message = null, $data = [])
    {
        $responseData = $this->$status;
        $responseData += ['message' => $message, 'data' => $data];
        $status_code = $this->status[$status];

        if ($this->responseTo == 'web') {

            return response()->json($responseData, $status_code);
        }

        $response =
            [
                'code' => $status_code,
                'status' => $status == 'success' ? 1 : 0,
                'massage' => $message,
                'data' => $data
            ];

        return response()->json($response);
    }


    static function notifyByFirebase($title, $body, $tokens, $data = [], $imageUrl = null)        // paramete 5 =>>>> $type
    {

        $registrationIDs = $tokens;

        $fcmMsg = array(
            'body' => $body,
            'title' => $title,
            'sound' => "default",
            'color' => "#203E78",
        );

        $imageUrl ? $fcmMsg += ['image' => $imageUrl] : null;
        $fcmFields = array(
            'registration_ids' => $registrationIDs,
            'priority' => 'high',
            'notification' => $fcmMsg,
            'data' => $data
        );

        $headers = array(
            'Authorization: key=AAAAJLpL5Zw:APA91bHYZqv53_BvSdEy9tqb264LN9uSYavAwXA_DgfG_NiD7-de_c5gXWIgT6GioNDkYHcBtnSFvpuhhsSTvSmQbCeNsn8NPEfRpPOYWA7krgGunYfGD1uvgz9oOM5PaWPv2-HM45NF',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }

    static function sendNotification($model, array $notifierIds, $relation, $title, $body, $data_type = 'admin', $data = [], $image = null): void
    {
        if (count($notifierIds)) {
            if ($relation == 'deliveries')
            {
                info($relation);
                info(json_encode($notifierIds));
            }
            $notification = $model->notifications()->create([
                'title' => $title,
                'body' => $body
            ]);

            if ($image) {
                Photo::addPhotoV2($image, $notification, 'notifications', ['size' => 600, 'quality' => 50, 'relation' => 'image']);
            }

            $notification->$relation()->attach($notifierIds);

            if (Token::CheckType($relation)->whereIn('tokenable_id', $notifierIds)->count()) {

                Token::CheckType($relation)->whereIn('tokenable_id', $notifierIds)->chunk(999, function ($records) use ($notification, $data, $data_type) {

                    $tokens = $records->pluck('token')->toArray();

                    $data =
                        [
                            $data_type => $data
                        ];

                    //send notification for client tokens
                    $send = helper::notifyByFirebase($notification->title, $notification->body, $tokens, $data, $notification->photo);
                    info($send);
                });

            }
        }
    }


    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                //احنا ضربنا في معامل عشان نقترب من المسافة الحقيقية
                return ($miles * 1.609344) * 1.2;
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    static function getLocationComponents($location)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . $location . "&sensor=false&key=AIzaSyDS9LYOd1LeyHK-FMOVzdvYCbbYGzJ_bM8");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $decodedResponse = json_decode($response);
        $components = $decodedResponse->results;

        $result = (object)[
            'locality' => null,
            'sub_locality' => null,
            'country' => (object)[
                'key' => 'country',
                'place_id' => null,
                'formatted_address' => null,
            ],
            'governorate' => (object)[
                'key' => 'administrative_area_level_1',
                'place_id' => null,
                'formatted_address' => null,
            ],
            'city' => (object)[
                'key' => 'administrative_area_level_2',
                'place_id' => null,
                'formatted_address' => null,
            ],
            'region' => (object)[
                'key' => 'administrative_area_level_3',
                'place_id' => null,
                'formatted_address' => null,
            ],
            'point' => (object)[
                'key' => 'point_of_interest',
                'place_id' => null,
                'formatted_address' => null,
            ],
        ];

        foreach ($components as $component) {
            if (in_array("country", $component->types)) {
                $result->country->place_id = $component->place_id;
                $result->country->formatted_address = $component->formatted_address;
            }

            if (in_array("administrative_area_level_1", $component->types)) {

                $result->governorate->place_id = $component->place_id;
                $result->governorate->formatted_address = $component->formatted_address;
            }

            if (in_array("administrative_area_level_2", $component->types)) {

                $result->city->place_id = $component->place_id;
                $result->city->formatted_address = $component->formatted_address;
            }

            if (in_array("administrative_area_level_3", $component->types)) {

                $result->region->place_id = $component->place_id;
                $result->region->formatted_address = $component->formatted_address;
            }

            if (in_array("point_of_interest", $component->types)) {

                $result->point->place_id = $component->place_id;
                $result->point->formatted_address = $component->formatted_address;
            }

            if (in_array("locality", $component->types)) {
//                return $component->types;
                foreach ($component->address_components as $address_component) {
                    if (in_array("locality", $address_component->types)) {
                        $result->locality = $address_component->short_name;
                    }
                }
            }


            if (in_array("sublocality", $component->types)) {
                foreach ($component->address_components as $address_component) {
                    if (in_array("sublocality", $address_component->types)) {
                        $result->sub_locality = $address_component->short_name;
                    }
                }
            }
        }

        return $result;
    }

    static function f($a, $r, $endCount, $startCount = 1)
    {

        for ($n = $startCount; $n <= $endCount; $n++) {

            $result = $a * (pow($r, ($n - 1)));
        }

        return $result;
    }


///
//////////////////////////////////////////////////////////////////////

    static function generateCode($tableModel, $record, $rowName = 'code')
    {
        $code = '#';
        for ($i = 0; $i < 5; $i++) {
            $code .= self::generateChar();
        }

        $test_record = $tableModel->where($rowName, $code)->first();

        if ($test_record) {
            self::generateCode($tableModel, $record, $rowName);
        } else {
            $record->$rowName = $code;
            $record->save();
            return true;
        }

    }

    static function generateSlug($model, $from, $id = null, $separator = '-')
    {
        $slug = '';

        foreach ($from as $word) {
            $slug .= str_replace(' ', $separator, $word) . $separator;
        }

        $record = $model->where('slug', 'LIKE', $slug . '%')->where('id', '!=', $id)->latest('updated_at')->first();

        if ($record) {

            $number = intval(str_replace($slug, '', $record->slug)) + 1;
            return $slug . $number;
        } else {
            if ($id == null || $model->find($id)->slug == null) {
                return $slug . 1;
            }

            return $model->find($id)->slug;
        }

    }

    static function generateChar()
    {
        $array = [Str::random(1), rand(0, 9)];
        return $array[rand(0, 1)];
    }


//////////////////////////////////////////////////////////////////////
///
    static function ResetPassword($model, $password)
    {
        $model->password = Hash::make($password);
        $model->save();
        return true;
    }

    static function readNotification($notification_id, $user_id, $relation = 'users')
    {
        $notification = Notification::find($notification_id);

        if ($notification) {
            $notification->$relation()->updateExistingPivot($user_id, ['is_read' => 1]);
        }

    }


    static function removeToken($token)
    {
        Token::where('token', $token)->delete();
    }

    static function is_read($model)
    {
        if ($model->is_read == 0) {
            $model->is_read = 1;
            $model->save();
            return true;
        } else {
            return false;
        }
    }


    static function convertDateTime($dateTime)
    {
        $date = Carbon::parse($dateTime)->toDateTimeString();

        return $date;
    }

    static function convertDateTimeNotString($dateTime)
    {
        $date = Carbon::parse($dateTime);

        return $date;
    }

    static function toggleBoolean($model, $name = 'is_active', $open = 1, $close = 0)
    {
        if ($model->$name == $open) {
            $model->$name = $close;
            $model->save();

        } elseif ($model->$name == $close) {
            $model->$name = $open;
            $model->save();
        } else {
            return false;
        }

        return true;
    }


    static function toggleBooleanView($model, $url, $switch = 'is_active', $open = 1, $close = 0)
    {
        return view('admin.my-helper-partials.toggle-boolean-view', compact('model', 'url', 'switch', 'open', 'close'))->render();
    }

    static function ratStars($gold_stars, $style = '', $class = 'label-primary')
    {
        $style .= 'color : gold !important;';
        $empty_star = '<span> <i class="fa fa-star-o"></i> </span>';
        $gold_star = '<span> <i class="fa fa-star"></i> </span>';
        $half_star = '<span> <i class="fa fa-star-half-o" style="transform: rotateY(180deg);"></i> </span>';
        $html = '<label class="' . $class . '" style="' . $style . '">';

        for ($i = 1; $i <= 5; $i++) {
            if ($gold_stars > 0 && $gold_stars >= 1) {
                $html .= $gold_star;
                $gold_stars--;

            } elseif ($gold_stars > 0 && $gold_stars < 1) {
                $html .= $half_star;
                $gold_stars--;

            } elseif ($gold_stars <= 0) {
                $html .= $empty_star;
            }
        }
        $html .= '</label>';
        return $html;
    }

    static function frontRate($gold_stars)
    {
        $empty_star = '☆';
        $gold_star = '★';
        $html = '';

        for ($i = 1; $i <= 5; $i++) {
            if ($gold_stars > 0 && $gold_stars >= 1) {
                $html .= $gold_star;
                $gold_stars--;

            } elseif ($gold_stars <= 0) {
                $html .= $empty_star;
            }
        }
        $html .= '';
        return $html;
    }

    static function activation($model, $name = 'activation')
    {
        if ($model->$name == 1) {
            $model->$name = 0;
            $model->save();

        } else {
            $model->$name = 1;
            $model->save();
        }

        return true;
    }

    /**
     * @param $key
     * @param string $value
     * @return string
     */
//    public
//    static function settingValue($key, $value = '')
//    {
//        $value = Setting::where('key', $key)->first() ? Setting::where('key', $key)->first()->value : $value;
//        return $value;
//    }
}
