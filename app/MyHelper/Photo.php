<?php


namespace App\MyHelper;


use App\Models\Attachment;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Mockery\Exception;


class Photo
{

    /**
     * @param $extension
     * @param string $destinationPath
     * @param mixed $file
     * @param int $size
     * @param int $quality
     * @return  string
     */
    public static function resizePhoto($extension, string $destinationPath, $file, int $size = 400 , $quality = 100): string
    {
        $image = $size . '-' . time() . '' . rand(11111, 99999) . '.' . $extension;

        $resize_image = Image::make($file);
        $resize_image->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image, $quality);

        return $image;
    }

    static function inArray($key, $array, $value)
    {
        $return = array_key_exists($key, $array) ? $array[$key] : $value;
        return $return;
    }


    static function editUpdatePhoto()
    {
        abort(404);
        $records = Attachment::where('path', 'not like', '%/400-%')
            ->where('path', 'not like', '%.docx%')
            ->where('path', 'not like', '%.jpg_480Wx480H%')
            ->where('path', 'not like', '%.jfif%')->where(function ($q) {
                $q->where('attachmentable_type', 'App\Models\Product')
                    ->orWhere('attachmentable_type', 'App\Models\Store')
                    ->orWhere('attachmentable_type', 'App\Models\Category');
            })
            ->take(500)->get();
        return $records;
        foreach ($records as $record) {


//            return $record;
            $path = public_path() . '/' . $record->path;
//return $path;
            if (is_file($path)) {
//                return $path;

                $array = explode('/', $record->path);
//        return $array;
                unset($array[count($array) - 1]);
//        return $array;
                $impolde = implode('/', $array);
//                return asset($record->path);
//        return $impolde;


                if (!in_array(explode('.', $path)[count(explode('.', $path)) - 1], ['docx', 'jfif', 'GD'])) {

                    $resize_image = Image::make($path);
//                            dd($resize_image);
                    $extension = $resize_image->extension;
//        return $extension;
                    $image = '400' . '-' . time() . '' . rand(11111, 99999) . '.' . $extension;
//return $image;


                    $destinationPath = public_path() . '/' . $impolde . '/' . $image;
//        return $destinationPath;
                    $resize_image->resize(400, null, function ($constraint) {

                        $constraint->aspectRatio();

                    })->save($destinationPath, 100);


                    File::delete(public_path() . '/' . $record->path);

                    $record->update([
                        'path' => $impolde . '/' . $image
                    ]);
//            return asset($impolde . '/'. $image);

                }

            } else {
//                return 'not photo';
//                $record->delete();
            }


        }
        return Attachment::where('path', 'not like', '%/400-%')
            ->where('path', 'not like', '%.docx%')
            ->where('path', 'not like', '%.jfif%')->count();
    }


    static function addPhoto($file, $model, $folder_name, $relation = 'photo', $usage = null, $type = 'image', $size = 400)
    {
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $file->getClientOriginalExtension(); // getting image extension

        if ($extension == 'svg') {

            $name = $file->getFilename() . '.' . $extension; // renaming image
            $file->move($destinationPath, $name); // uploading file to given
            $model->$relation()->create(
                [
                    'path' => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                    'type' => $type,
                    'usage' => $usage
                ]
            );

            return;
        }

        $imageResize = self::resizePhoto($extension, $destinationPath, $file, $size);

        $model->$relation()->create(
            [
                'path' => 'uploads/thumbnails/' . $folder_name . '/' . $imageResize,
                'type' => $type,
                'usage' => $usage
            ]
        );
    }


    static function addPhotoV2($file, $model, $folder_name, array $options = []): void
    {
        //ser options
        // relation
        //usage
        //type
        //size

        $relation = self::inArray('relation', $options, 'photo');
        $usage = self::inArray('usage', $options, null);
        $type = self::inArray('type', $options, 'image');
        $size = self::inArray('size', $options, 400);
        $quality = self::inArray('quality', $options, 100);

        ///////////////////////////////

        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $file->getClientOriginalExtension(); // getting image extension

        if ($extension == 'svg') {

            $name = $file->getFilename() . '.' . $extension; // renaming image
            $file->move($destinationPath, $name); // uploading file to given
            $model->$relation()->create(
                [
                    'path' => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                    'type' => $type,
                    'usage' => $usage
                ]
            );

            return;
        }

        $imageResize = self::resizePhoto($extension, $destinationPath, $file , $size , $quality);


        $model->$relation()->create(
            [
                'path' => 'uploads/thumbnails/' . $folder_name . '/' . $imageResize,
                'type' => $type,
                'usage' => $usage
            ]
        );
    }

    static function addFile($file, $oldFiles, $model,  $folder_name, $relation = 'photo', $usage = null, $type = 'image')
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->path);
        }

        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $file->move($destinationPath, $name); // uploading file to given

        $input = [
            'path' => 'uploads/thumbnails/' . $folder_name . '/' . $name,
            'type' => $type,
            'usage' => $usage
        ];

        if ($oldFiles) {
            $model->$relation()->where(['type' => $type])->update($input);
        } else {

            $model->$relation()->create($input);
        }
    }

    static function addOriginalPhoto($file, $model, $folder_name, $relation = 'photo', $usage = null, $type = 'image')
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given

        $model->$relation()->create(
            [
                'path' => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                'type' => $type,
                'usage' => $usage
            ]
        );
    }

    static function addPhotos($file, $model, $folder_name)
    {
        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
        $image->move($destinationPath, $name); // uploading file to given


        $image_400 = '400-' . time() . '' . rand(11111, 99999) . '.' . $extension;

        $resize_image = Image::make($destinationPath . $name);

        $resize_image->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath . $image_400, 100);

        $model->create(['photo_url' => 'uploads/thumbnails/' . $folder_name . '/' . $image_400,

            'type' => 'photo']);
    }


    static function updatePhoto($file, $oldFiles, $model, $folder_name, $relation = 'photo', $usage = null, $type = 'image', $size = 400)
    {
        if ($oldFiles) {
            File::delete(public_path() . '/' . $oldFiles->path);
        }

        $image = $file;
        $destinationPath = public_path() . '/uploads/thumbnails/' . $folder_name . '/';
        $extension = $image->getClientOriginalExtension(); // getting image extension

        if ($extension == 'svg') {

            $name = $file->getFilename() . '.' . $extension; // renaming image
            $file->move($destinationPath, $name); // uploading file to given

            $input =
                [
                    'path' => 'uploads/thumbnails/' . $folder_name . '/' . $name,
                    'type' => $type,
                    'usage' => $usage
                ];

        }else{

            $imageResize = self::resizePhoto($extension, $destinationPath, $file, $size);

            $input =
                [
                    'path' => 'uploads/thumbnails/' . $folder_name . '/' . $imageResize,
                    'type' => $type,
                    'usage' => $usage,
                ];
        }


        if ($oldFiles) {

            $oldFiles->update($input);

        } else {

            $model->$relation()->create($input);
        }
    }

    static function deletePhoto($model, $relation = 'photo', $multiple = false, $type = 'photo')
    {
        $photos = $model->$relation;

        if ($multiple == true) {
            foreach ($photos as $photo) {
                File::delete(public_path() . '/' . $photo->path);
                $photo->delete();
            }
            return true;
        } else {
            File::delete(public_path() . '/' . $photos->path);
        }

        $model->$relation()->where('type', $type)->delete();

    }

}
