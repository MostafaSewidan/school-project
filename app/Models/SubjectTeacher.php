<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    protected $table = 'subject_teacher';
    public $timestamps = true;
    protected $fillable = array('subject_id', 'teacher_id');
}