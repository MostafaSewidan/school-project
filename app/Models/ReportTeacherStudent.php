<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportTeacherStudent extends Model 
{

    protected $table = 'report_teacher_student';
    public $timestamps = true;
    protected $fillable = array('report_teacher_id', 'student_id');

}