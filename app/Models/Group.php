<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use LogTrait;

    protected $table = 'groups';
    public $timestamps = true;
    protected $fillable = array('name', 'level_id', 'subject_id', 'teacher_id', 'duration', 'break_time');

    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    public function absents()
    {
        return $this->hasMany('App\Models\Absent');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam');
    }
    public function StudySchedules()
    {
        return $this->hasMany('App\Models\StudySchedule');
    }
    public function getGroupNameAttribute($value)
    {
        return optional($this->level)->name . ' ' . optional($this->subject)->name . ' ' . ($this->name) ;
    }

    public function getStudentListAttribute()
    {
        return $this->students()->pluck('students.id')->toArray();
    }
}