<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;

class AnswerExamStudent extends Model 
{
    use GetAttribute;

    protected $table = 'answer_exams_students';
    public $timestamps = true;
    protected $fillable = array('answer', 'student_id', 'exam_id', 'result','teacher_comment');

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function exam()
    {
        return $this->belongsTo('App\Models\Exam');
    }

}