<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model 
{
    use LogTrait;

    protected $table = 'notifications';
    public $timestamps = true;
    protected $fillable = array('title','body','notifiable_type');
    protected $appends = array('type','resources');

    public function notifiable()
    {
        return $this->morphTo();
    }

    public function clients()
    {
        return $this->morphedByMany('App\Models\Client','notifiable')->withPivot('is_read','send_at','is_send');
    }

    public function stores()
    {
        return $this->morphedByMany('App\Models\Store','notifiable')->withPivot('is_read','send_at','is_send');
    }

    public function deliveries()
    {
        return $this->morphedByMany(Delivery::class,'notifiable')->withPivot('is_read','send_at','is_send');
    }

    public function image()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }

    public function getPhotoAttribute()
    {
        return $this->image ? asset($this->image->path) : null;
    }

    public function getTypeAttribute()
    {
        $type = '';

        switch ($this->notifiable_type){
            case 'App\Models\Order':
                $type = 'order';
                break;
            case 'App\Models\Review':
                $type = 'review';
                break;
            default :
                $type = 'order';
        }
        return $type;
    }

    public function getIsGeneralAttribute()
    {
        $type = '';

        switch ($this->notifiable_type){
            case 'App\Models\Order':
                $type = 0;
                break;
            case 'App\Models\Review':
                $type = 0;
                break;
            default :
                $type = 1;
        }
        return $type;
    }

    public function getResourcesAttribute()
    {
        $value = '';

        switch ($this->notifiable_type){
            case 'App\Models\Order':
                $value = 'App\Http\Resources\Order';
                break;

            case 'App\Models\Review':
                $value = 'App\Http\Resources\Review';
                break;
            default :
                $value = 'App\Http\Resources\GeneralNotification';
        }
        return $value;
    }

}