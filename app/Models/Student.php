<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use LogTrait,GetAttribute;
    protected $table = 'students';
    public $timestamps = true;
    protected $fillable = array('name', 'email', 'password', 'level_id', 'is_active', 'notes', 'last_active');
    protected $hidden = array('password');

    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Models\Group');
    }
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function absents()
    {
        return $this->belongsToMany(StudySchedule::class , 'absents')->withPivot('state');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }

    public function AnswerHomeworks()
    {
        return $this->hasMany('App\Models\AnswerHomework');
    }

    public function ReportTeachers()
    {
        return $this->belongsToMany('App\Models\ReportTeacher');
    }

    public function AnswerExamStudents()
    {
        return $this->hasMany('App\Models\AnswerExamStudent');
    }

    public function answers()
    {
        return $this->belongsToMany(Exam::class , 'answer_exams_students')->withPivot('answer','result');
    }

    public function getStudentStatusAttribute($key)
    {
        return $this->is_active > 0 ? 'فعال' : 'محظور';
    }
}