<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use LogTrait;
    protected $table = 'levels';
    public $timestamps = true;
    protected $fillable = array('name');

    public function groups()
    {
        return $this->hasMany('App\Models\Group');
    }

    public function students()
    {
        return $this->hasMany('App\Models\Student');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject');
    }
}