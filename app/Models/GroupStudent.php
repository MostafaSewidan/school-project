<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupStudent extends Model 
{

    protected $table = 'group_student';
    public $timestamps = true;
    protected $fillable = array('group_id', 'student_id');

}