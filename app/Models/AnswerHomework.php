<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Illuminate\Database\Eloquent\Model;

class AnswerHomework extends Model 
{
    use GetAttribute;

    protected $table = 'answer_homeworks';
    public $timestamps = true;
    protected $fillable = array('study_schedule_id', 'student_id', 'result', 'student_answer');

    public function StudySchedule()
    {
        return $this->belongsTo('App\Models\StudySchedule');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

}