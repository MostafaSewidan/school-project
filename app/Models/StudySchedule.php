<?php

namespace App\Models;

use App\Traits\GetAttribute;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StudySchedule extends Model
{
    use LogTrait;
    use GetAttribute;

    public static $user;
    public static $guard;
    protected $table = 'study_schedule';
    public $timestamps = true;
    protected $fillable = array('group_id', 'datetime', 'zoom_link', 'description', 'homework');
    protected $dates = array('datetime');

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->multiple_attachment = true;
        $this->multiple_attachment_usage = ['default', 'bdf-file'];
    }

    public function AnswerHomeworks()
    {
        return $this->hasMany('App\Models\AnswerHomework');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function absents()
    {
        return $this->belongsToMany(Student::class, 'absents')->withPivot('state');
    }

    public function getGroupNameAttribute($value)
    {
        return ($this->group)->level->name . ' ' . ($this->group)->subject->name . ' ' . ($this->group)->name;
    }

    public function getHomeWorkAttribute()
    {
        $file = $this->attachmentRelation()->where('usage', 'homework_pdf')->first();

        return $file ? asset($file->path) : null;
    }

    public function getEndTimeAttribute()
    {
        return Carbon::parse($this->datetime)->addMinutes(($this->group)->duration)->timestamp;
    }

    public function getStartTimeAttribute()
    {
        return strtotime($this->datetime);
    }

    public function getHomeWorkAnswerAttribute()
    {
        $homeWork = $this->AnswerHomeworks()->where('student_id', auth('student')->user()->id)->first();

        return $homeWork ? $homeWork->attachment : null;
    }

    public function getAuthStudentAbsentAttribute()
    {
        $absent = $this->absents()->where('student_id', auth('student')->user()->id)->first();

        if ($absent) {
            return $absent->pivot->state;
        }

        return null;
    }

    static function getLastSession($guard = 'teacher' , $user = null)
    {
        self::$user = $user ? $user : auth('teacher')->user();
        self::$guard = $guard;

        return self::whereHas('group', function ($q) {
            $q->whereHas('teacher', function ($q) {

                if(self::$guard == 'teacher') {
                    $q->where('teacher_id' , self::$user->id);

                }else {

                    $q->whereHas('manager', function ($q) {

                        if(self::$guard == 'manager') {
                            $q->where('user_id' , self::$user->id)->hasRole('manager');
                        }
                    });
                }
            });
        })->fiest();
    }
}