<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Absent extends Model 
{

    protected $table = 'absents';
    public $timestamps = true;
    protected $fillable = array('student_id', 'study_schedule_id', 'state');


    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }


    public function study_schedule()
    {
        return $this->belongsTo(StudySchedule::class);
    }

}