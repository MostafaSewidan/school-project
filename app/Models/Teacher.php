<?php

namespace App\Models;

use App\Traits\GetAttribute;
use App\Models\Attachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Teacher extends Authenticatable
{
    use LogTrait, GetAttribute;
    protected $table = 'teachers';
    public $timestamps = true;
    protected $fillable = array('name', 'phone', 'password', 'email', 'last_active', 'd_o_b', 'address', 'contract_ending_date', 'contract_starting_date', 'contract_hours');
    protected $hidden = array('password');

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->multiple_attachment = true;
        $this->multiple_attachment_usage = ['default', 'bdf-file'];
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\Group');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject');
    }


    public function getUserListAttribute()
    {
        return $this->users()->pluck('users.id')->toArray();
    }

    public function getSubjectListAttribute()
    {
        return $this->subjects()->pluck('subjects.id')->toArray();
    }


    public function getNationalIdAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'national_id')->first() ?
            asset($this->attachmentRelation()->where('usage', 'national_id')->first()->path) : null;
    }

    public function getCriminalStatementAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'criminal_statement')->first() ?
            asset($this->attachmentRelation()->where('usage', 'criminal_statement')->first()->path) : null;
    }

    public function getContractAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'contract')->first() ?
            asset($this->attachmentRelation()->where('usage', 'contract')->first()->path) : null;
    }

    public function getGraduationCertificateAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'graduation_certificate')->first() ?
            asset($this->attachmentRelation()->where('usage', 'graduation_certificate')->first()->path) : null;
    }

    public function getCurrentStatusAttribute()
    {
        return ($this->contract_ending_date) > date("Y-m-d") ? 'ساري' : 'منتهي';
    }

    public function getCurrentHoursAttribute()
    {
        return (($this->groups()->sum('duration')) + (($this->groups()->sum('duration')) * 0.25)) / 60;
    }

    public function getAvailableHoursAttribute()
    {
        return ($this->contract_hours - $this->current_hours) > 0 ? 'متوفر' : 'غير متوفر';
    }
}