<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    public $timestamps = true;
    protected $fillable = array('user_id', 'student_id', 'teacher_id', 'message');

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }
}