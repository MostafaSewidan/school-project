<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelSubject extends Model 
{

    protected $table = 'level_subject';
    public $timestamps = true;
    protected $fillable = array('level_id', 'subject_id');

}