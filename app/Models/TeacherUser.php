<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherUser extends Model
{
    protected $table = 'teacher_user';
    public $timestamps = true;
    protected $fillable = array('user_id', 'teacher_id');
}