<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportTeacher extends Model
{
    use LogTrait ;
    protected $table = 'reports_teachers';
    public $timestamps = true;
    protected $fillable = array('overall_evaluation', 'technical_problem', 'technical_problem_note', 'Problem_with_students', 'student_proplem_note', 'notes');

    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    public function getReportStatuesAttribute()
    {
        $toString = '' ;
        switch ($this->overall_evaluation)
        {
            case 'Excellent' :
                return 'ممتاز' ;
                break ;
            case 'very_good' :
                return 'جيد جدا' ;
                break ;
            case 'good' :
                return 'جيد' ;
                break ;
            case 'acceptable' :
                return 'مقبول' ;
                break ;
            case 'Bad' :
                return 'سيء' ;
                break ;
            case 'very_bad' :
                return 'سيئ جدا' ;
                break ;

        }
        return $toString ;

    }

}
