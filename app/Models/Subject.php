<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use LogTrait;

    protected $table = 'subjects';
    public $timestamps = true;
    protected $fillable = array('name');

    public function groups()
    {
        return $this->hasMany('App\Models\Group');
    }

    public function levels()
    {
        return $this->belongsToMany('App\Models\Level');
    }

    public function teachers()
    {
        return $this->belongsToMany('App\Models\Teacher');
    }

    public function getLevelListAttribute()
    {
        return $this->levels()->pluck('levels.id')->toArray();
    }

    static function studySchedule($subject = null , $with_student = false)
    {
        return StudySchedule::whereHas('group' , function ($q) use($subject,$with_student){

            if($subject) {

                $q->whereIn('subject_id' , (array)$subject);
            }

            if($with_student){

                $q->whereHas('students' , function ($q) {

                    $q->where('student_id' , auth('student')->user()->id);
                });
            }
        });
    }

    public function studentQuery() {
        return $this->whereHas('groups', function ($q) {
            $q->whereHas('students' , function ($q) {

                $q->where('student_id' , auth('student')->user()->id);
            });
        });
    }

    public function studentGroup() {
        return $this->groups()->whereHas('students' , function ($q) {

                $q->where('student_id' , auth('student')->user()->id);
            })->first();
    }
}