<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MangerProfile extends Model
{
    protected $table = 'users_profiles';
    public $timestamps = true;
    protected $fillable = array('user_id', 'd_o_b', 'address', 'contract_ending_date', 'contract_starting_date');

    public function manger()
    {
        return $this->belongsTo('App\User');
    }
}