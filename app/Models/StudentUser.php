<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentUser extends Model
{
    protected $table = 'student_user';
    public $timestamps = true;
    protected $fillable = array('user_id', 'student_id');
}