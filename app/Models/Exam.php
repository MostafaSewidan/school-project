<?php

namespace App\Models;

use App\Traits\GetAttribute;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use LogTrait, GetAttribute;
    protected $table = 'exams';
    public $timestamps = true;
    protected $fillable = array('name', 'group_id', 'start_datetime', 'end_datetime', 'description');
    protected $dates = ['start_datetime', 'end_datetime'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->multiple_attachment = true;
        $this->multiple_attachment_usage = ['default', 'bdf-file'];
    }

    public function AnswerExamStudents()
    {
        return $this->hasMany('App\Models\AnswerExamStudent');
    }

    public function answers()
    {
        return $this->belongsToMany(Student::class, 'answer_exams_students')->withPivot('answer', 'result');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function getAuthStudentAnswerAttribute()
    {
        return $this->AnswerExamStudents()->where('student_id', auth('student')->user()->id)->first();
    }

    public function getAuthStudentAnswerStatusAttribute()
    {
        $answer = $this->getAuthStudentAnswerAttribute();

        if ($answer) {

            if ($answer->result) {
                $res = [
                    'status' => 'corrected',
                    'label_class' => 'label label-success',
                ];
            } else {
                $res = [
                    'status' => 'waiting_correct',
                    'label_class' => 'label label-warning',
                ];
            }

        } elseif ($this->end_datetime < Carbon::now()->toDateTimeString()) {

            $res = [
                'status' => 'not_answered',
                'label_class' => 'label label-danger',
            ];

        } else {
            if($this->start_datetime < Carbon::now()->toDateTimeString()) {

                $res = [
                    'status' => 'started',
                    'label_class' => 'label label-primary',
                ];

            }else {
                $res = [
                    'status' => 'not_start',
                    'label_class' => 'label label-primary',
                ];
            }
        }

        return (object)$res;
    }

    public function getExams($subject = null, $with_student = false)
    {
        return $this->whereHas('group', function ($q) use ($subject, $with_student) {

            if ($subject) {

                $q->whereIn('subject_id', (object)$subject);
            }

            if ($with_student) {

                $q->whereHas('students', function ($q) {

                    $q->where('student_id', auth('student')->user()->id);
                });
            }
        });
    }
}