<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model 
{
    use LogTrait;

    protected $table = 'tokens';
    public $timestamps = true;
    protected $fillable = array('token','os', 'tokenable_id', 'tokenable_type');

    public function tokenable()
    {
        return $this->morphTo();
    }



    public function scopeCheckType($query,$relation)
    {
        $type = '';

        switch ($relation)
        {
            case 'clients':
                $type = 'App\Models\Client';
                break;
            case 'stores':
                $type = 'App\Models\Store';
                break;
            case 'deliveries':
                $type = 'App\Models\Delivery';
                break;
        }

        $query->where('tokenable_type' , $type);
    }
}