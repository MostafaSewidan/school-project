<?php

namespace App\Providers;

use App;
use App\Models\Setting;
use Carbon\Carbon;
use Helper\Helper;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('settings', function () {
            if (Schema::hasTable('settings')) {

                return optional(Setting::first());

            } else {
                return new Setting;
            }
        });
        App::singleton('helper', function () {
            return new Helper;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        Schema::defaultStringLength(191);

            Carbon::setLocale(app()->getLocale());

//        view()->composer('*', function ($view) {
//            $allSettings = Setting::all();
//            $settingArray = [];
//            foreach ($allSettings as $set) {
//             if ($set->data_type == 'fileWithPreview' && $set->photo) {
//                    $settingArray +=
//                        [
//                            $set['key'] => asset($set->photo->path)
//                        ];
//
//                } else {
//                    $settingArray +=
//                        [
//                            $set['key'] => $set['value']
//                        ];
//                }
//
//            }
//
//            return $view->with('settings', $settingArray);
//        });

    }
}
