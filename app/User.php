<?php

namespace App;

use App\Models\LogTrait;
use App\Traits\GetAttribute;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use LogTrait,GetAttribute,HasRoles;
    protected $table = 'users';
    public $timestamps = true;
    protected $fillable = array('name', 'password', 'email', 'phone', 'parent_id', 'last_active','d_o_b','address','contract_ending_date','contract_starting_date');
    protected $hidden = array('password');
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->multiple_attachment = true;
        $this->multiple_attachment_usage = ['default', 'bdf-file'];
    }
    // public function supervisors()
    // {
    //     return $this->hasMany('App\User', 'parent_id');
    // }

    // public function supervisor()
    // {
    //     return $this->belongsTo('App\User', 'parent_id');
    // }

    // public function visors()
    // {
    //     return $this->hasMany('App\User', 'parent_id');
    // }

    public function visor()
    {
        return $this->belongsTo('App\User', 'parent_id');
    }

    // public function director()
    // {
    //     return $this->belongsTo('App\User', 'parent_id');
    // }

    // public function directors()
    // {
    //     return $this->hasMany('App\User', 'parent_id');
    // }

    public function teachers()
    {
        return $this->belongsToMany('App\Models\Teacher');
    }

    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }

    public function getNationalIdAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'national_id')->first() ?
        asset($this->attachmentRelation()->where('usage', 'national_id')->first()->path) : null;
    }

    public function getCriminalStatementAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'criminal_statement')->first() ?
        asset($this->attachmentRelation()->where('usage', 'criminal_statement')->first()->path) : null;
    }

    public function getContractAttribute()
    {
        return $this->attachmentRelation()->where('usage', 'contract')->first() ?
        asset($this->attachmentRelation()->where('usage', 'contract')->first()->path) : null;
    }
    public function getCurrentStatusAttribute()
    {
        return ($this->contract_ending_date) > date("Y-m-d") ? 'ساري' : 'منتهي';
    }
}