<?php

namespace App\Console\Commands;

use App\Models\Maintenance;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateWorkOrderForMaintenance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:create-work-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating a work order for a newly due maintenance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listOfMaintenanceDoesntHaveWorkOrder = Maintenance::doesntHave('WorkingOrder')
            ->whereDate('maintenance_date','<=',Carbon::now()->toDateString())->get();
        foreach ($listOfMaintenanceDoesntHaveWorkOrder as $maintenance)
        {
            $workOrder = $maintenance->workingOrder()->create([
                'type' => 'maintenance',
                'due_to' => $maintenance->maintenance_date,
                'device_id' => $maintenance->device_id,
            ]);
            $workOrder->comments()->create(['body' => __('صيانة دورية وقائية'), 'user_id' => User::first()->id]);
        }
    }
}
