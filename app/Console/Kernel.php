<?php

namespace App\Console;

use App\Console\Commands\CreateWorkOrderForMaintenance;
use App\Http\Resources\NotificationAddReview;
use App\Models\Client;
use App\Models\Notifiable;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Store;
use App\Models\Token;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // to be called daily (later)
        $schedule->command(CreateWorkOrderForMaintenance::class)->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
