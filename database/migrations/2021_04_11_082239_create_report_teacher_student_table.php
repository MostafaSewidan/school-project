<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportTeacherStudentTable extends Migration {

	public function up()
	{
		Schema::create('report_teacher_student', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('report_teacher_id');
			$table->integer('student_id');
		});
	}

	public function down()
	{
		Schema::drop('report_teacher_student');
	}
}