<?php

use App\Models\Student;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('password', 255);
            $table->integer('level_id');
            $table->string('phone', 255);
            $table->ipAddress('ip')->nullable();
            $table->boolean('is_active')->default(0);
            $table->string('notes', 255)->nullable();
            $table->timestamp('last_active');
            $table->string('address', 255)->nullable();
        });



        \App\Models\Student::create([
                'name' => 'student',
                'email' => 'student@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
                                'level_id' => 1 ,

            ]);
        \App\Models\Student::create([
                'name' => 'student2',
                'email' => 'student2@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
                'level_id' => 1 ,
            ]);
        \App\Models\Student::create([
                'name' => 'student3',
                'email' => 'student3@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
                                'level_id' => 1 ,

            ]);
        \App\Models\Student::create([
                'name' => 'student4',
                'email' => 'student4@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
                                'level_id' => 1 ,

            ]);

        \App\Models\Student::create([
                'name' => 'student5',
                'email' => 'student5@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
                                'level_id' => 1 ,

            ]);
    }

    public function down()
    {
        Schema::drop('students');
    }
}