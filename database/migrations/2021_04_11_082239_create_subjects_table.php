<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubjectsTable extends Migration
{
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 255);
        });
        \App\Models\Subject::create([
                'name' => 'اللغة العربية',
            ]);
        \App\Models\Subject::create([
                'name' => 'اللغة الانجليزية',
            ]);
        \App\Models\Subject::create([
                'name' => 'اللغة الفرنسية',
            ]);
    }

    public function down()
    {
        Schema::drop('subjects');
    }
}