<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('d_o_b')->nullable();
            $table->text('address')->nullable();
            $table->date('contract_ending_date')->nullable();
            $table->date('contract_starting_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string('phone', 255);
            $table->integer('parent_id')->nullable();
            $table->timestamp('last_active')->nullable();
        });


        \App\User::create([
                'name' => 'admin',
                'email' => 'admin@school.com',
                'password' => bcrypt(123),
                'email_verified_at' => Carbon\Carbon::now(),
                'phone' => 01011111 ,

            ]);

        \App\User::create([
                'name' => 'director',
                'email' => 'director@school.com',
                'password' => bcrypt(123),
                'email_verified_at' => Carbon\Carbon::now(),
                'phone' => 01011111 ,
            ]);

        \App\User::create([
                'name' => 'supervisor',
                'email' => 'supervisor@school.com',
                'password' => bcrypt(123),
                'email_verified_at' => Carbon\Carbon::now(),
                'phone' => 01011111 ,
                'parent_id' => 2,
            ]);

        \App\User::create([
                'name' => 'visor',
                'email' => 'visor@school.com',
                'password' => bcrypt(123),
                'email_verified_at' => Carbon\Carbon::now(),
                'parent_id' => 3,
                'phone' => 01011111 ,
                'parent_id' => 3,

            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}