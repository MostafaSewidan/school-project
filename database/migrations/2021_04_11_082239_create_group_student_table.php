<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupStudentTable extends Migration
{
    public function up()
    {
        Schema::create('group_student', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('group_id');
            $table->integer('student_id');
        });

        \App\Models\GroupStudent::create([
                'group_id' => 1,
                'student_id' => 1,
            ]);
        \App\Models\GroupStudent::create([
                'group_id' => 1,
                'student_id' => 2,
            ]);

        \App\Models\GroupStudent::create([
                'group_id' => 1,
                'student_id' => 3,
            ]);

        \App\Models\GroupStudent::create([
                'group_id' => 1,
                'student_id' => 4,
            ]);

        \App\Models\GroupStudent::create([
                'group_id' => 1,
                'student_id' => 5,
            ]);
    }

    public function down()
    {
        Schema::drop('group_student');
    }
}