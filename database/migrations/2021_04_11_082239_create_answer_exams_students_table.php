<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswerExamsStudentsTable extends Migration {

	public function up()
	{
		Schema::create('answer_exams_students', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('answer')->nullable();
			$table->integer('student_id');
			$table->integer('exam_id');
			$table->text('teacher_comment')->nullable();
			$table->integer('result')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('answer_exams_students');
	}
}