<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatsTable extends Migration {

	public function up()
	{
		Schema::create('chats', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->nullable();
			$table->integer('student_id')->nullable();
			$table->integer('teacher_id')->nullable();
			$table->text('message');
		});
	}

	public function down()
	{
		Schema::drop('chats');
	}
}