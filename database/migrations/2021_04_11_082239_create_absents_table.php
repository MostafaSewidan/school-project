<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAbsentsTable extends Migration {

	public function up()
	{
		Schema::create('absents', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('student_id');
			$table->integer('study_schedule_id');
			$table->boolean('state')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('absents');
	}
}