<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudyScheduleTable extends Migration
{
    public function up()
    {
        Schema::create('study_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('group_id');
            $table->datetime('datetime');
            $table->string('zoom_link', 255);
            $table->text('description')->nullable();
            $table->text('homework')->nullable();
        });

        \App\Models\StudySchedule::create([
                'group_id' => '1',
                'datetime' => date('Y-m-d H:i:s'),
                'zoom_link' => 'http://zoom.com/join/aa',
                'description' => 'plaplapal',
                'homework' => 'بل بلا بلا',
                'phone' => 01011111 ,
            ]);
    }

    public function down()
    {
        Schema::drop('study_schedule');
    }
}