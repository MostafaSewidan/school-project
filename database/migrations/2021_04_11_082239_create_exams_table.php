<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamsTable extends Migration {

	public function up()
	{
		Schema::create('exams', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->dateTime('start_datetime');
			$table->dateTime('end_datetime');
			$table->string('name', 255)->nullable();
			$table->text('description')->nullable();
			$table->integer('group_id')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('exams');
	}
}