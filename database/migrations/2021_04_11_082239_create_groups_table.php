<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('name')->unique();
            $table->integer('level_id');
            $table->integer('subject_id');
            $table->integer('teacher_id');
            $table->integer('duration');
            $table->time('break_time');
        });

        \App\Models\Group::create([
                'name' => '1',
                'level_id' => 1,
                'subject_id' => 1,
                'teacher_id' => 1,
                'duration' => 90,
                'break_time' => date("H:i:s"),
            ]);
    }

    public function down()
    {
        Schema::drop('groups');
    }
}