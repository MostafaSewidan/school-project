<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportsTeachersTable extends Migration {

	public function up()
	{
		Schema::create('reports_teachers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->enum('overall_evaluation', array('Excellent', 'very_good', 'good', 'acceptable', 'Bad', 'very_bad'));
		//	$table->boolean('technical_problem');
			$table->text('technical_problem_note')->nullable();
		//	$table->boolean('Problem_with_students');
//			$table->text('student_proplem_note')->nullable();
			$table->string('student_name')->nullable();
			$table->string('parent_name')->nullable();
			$table->text('notes')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('reports_teachers');
	}
}
