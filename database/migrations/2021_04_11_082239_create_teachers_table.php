<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersTable extends Migration
{
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 255);
            $table->string('phone', 255);
            $table->string('password');
            $table->string('email', 255);
            $table->timestamp('last_active')->nullable();
            $table->date('d_o_b')->nullable();
            $table->string('address', 255)->nullable();
            $table->date('contract_starting_date')->nullable();
            $table->date('contract_ending_date')->nullable();
            $table->integer('contract_hours')->nullable();
        });


        \App\Models\Teacher::create([
                'name' => 'teacher',
                'email' => 'teacher@school.com',
                'password' => bcrypt(123),
                'phone' => 01011111 ,
            ]);
    }

    public function down()
    {
        Schema::drop('teachers');
    }
}