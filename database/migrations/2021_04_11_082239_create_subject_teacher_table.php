<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubjectTeacherTable extends Migration
{
    public function up()
    {
        Schema::create('subject_teacher', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('subject_id');
            $table->integer('teacher_id');
        });

        \App\Models\SubjectTeacher::create([
                'subject_id' => '1',
                'teacher_id' => '1',
            ]);
    }

    public function down()
    {
        Schema::drop('subject_teacher');
    }
}