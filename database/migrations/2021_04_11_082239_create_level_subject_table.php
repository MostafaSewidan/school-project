<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLevelSubjectTable extends Migration
{
    public function up()
    {
        Schema::create('level_subject', function (Blueprint $table) {
            $table->timestamps();
            $table->increments('id');
            $table->integer('level_id');
            $table->integer('subject_id');
        });
        \App\Models\LevelSubject::create([
                'level_id' => '1',
                'subject_id' => '1',
            ]);

        \App\Models\LevelSubject::create([
                'level_id' => '1',
                'subject_id' => '2',
            ]);

        \App\Models\LevelSubject::create([
                'level_id' => '2',
                'subject_id' => '2',
            ]);
        \App\Models\LevelSubject::create([
                'level_id' => '2',
                'subject_id' => '1',
            ]);
        \App\Models\LevelSubject::create([
                'level_id' => '2',
                'subject_id' => '3',
            ]);

        \App\Models\LevelSubject::create([
                'level_id' => '3',
                'subject_id' => '2',
            ]);
        \App\Models\LevelSubject::create([
                'level_id' => '3',
                'subject_id' => '1',
            ]);
        \App\Models\LevelSubject::create([
                'level_id' => '3',
                'subject_id' => '3',
            ]);
    }

    public function down()
    {
        Schema::drop('level_subject');
    }
}