<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentUserTable extends Migration
{
    public function up()
    {
        Schema::create('student_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->integer('student_id');
        });


        \App\Models\StudentUser::create([
                'user_id' => '4',
                'student_id' => '1',
            ]);
        \App\Models\StudentUser::create([
                'user_id' => '4',
                'student_id' => '2',
            ]);

        \App\Models\StudentUser::create([
                'user_id' => '4',
                'student_id' => '3',
            ]);

        \App\Models\StudentUser::create([
                'user_id' => '4',
                'student_id' => '4',
            ]);

        \App\Models\StudentUser::create([
                'user_id' => '4',
                'student_id' => '5',
            ]);
    }

    public function down()
    {
        Schema::drop('student_user');
    }
}