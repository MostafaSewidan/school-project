<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswerHomeworksTable extends Migration {

	public function up()
	{
		Schema::create('answer_homeworks', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('study_schedule_id');
			$table->string('student_id');
			$table->integer('result')->nullable();
			$table->string('student_answer', 255)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('answer_homeworks');
	}
}