<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Subject;
use Faker\Generator as Faker;
use Mockery\Matcher\Subset;

$factory->define(Subject::class, function (Faker $faker) {
    return [
       'name' => $faker->name
    ];
});