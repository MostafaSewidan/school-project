<?php

use Illuminate\Database\Seeder;

class SubjectSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Subject::class, rand(20, 30))->create()->each(function ($level) {
            $level->levels()->createMany(factory(\App\Models\Level::class, rand(20, 30))->make()->toArray());
        });
    }
}