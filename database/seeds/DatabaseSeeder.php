<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::beginTransaction();
        $this->call(LevelSeed::class);
        $this->call(SubjectSeed::class);

//        $this->call(Categories::class);
//        $this->call(Stores::class);
        // $this->call(Products::class);
        \Illuminate\Support\Facades\DB::commit();
    }
}