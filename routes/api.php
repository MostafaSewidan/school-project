<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::get('subjects', 'MainController@subjects');
    Route::get('teachers', 'MainController@teachers');
    Route::get('students', 'MainController@students');




    Route::get('device-info', 'ApiController@deviceInfo')->name('api.device-info');
    Route::get('employee-info', 'ApiController@employeeInfo')->name('api.employee-info');
    Route::get('hijri-to-georgian', 'ApiController@hijriToGeorgian');
    Route::get('georgian-to-hijri', 'ApiController@georgianToHijri');
    Route::get('employee', 'ApiController@getEmployee');
    Route::get('check-salary-month', 'ApiController@checkSalaryMonth');
});