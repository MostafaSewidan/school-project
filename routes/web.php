<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Setting;
use App\MyHelper\Helper;

/// to clear Cache
Route::get('/clear-cache', function () {
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    return 'done';
});
///
// local session
Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});
// end session

Route::any('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::redirect('/', 'student/login');


/// area auth manger
Route::group(['prefix' => 'manager','middleware'=>'guest:web','namespace'=>'User'], function () {
    Route::get('login', 'UserAuthController@viewLogin');
    Route::post('login', 'UserAuthController@login');
});
// end area auth manger

// area auth teacher
Route::group(['prefix' => 'teacher','middleware'=>'guest:teacher','namespace'=>'Teacher'], function () {
    Route::get('login', 'TeacherAuthController@viewLogin');
    Route::post('login', 'TeacherAuthController@login');
});

// start Teacher Dashboard
Route::group(['middleware' => ['auth:teacher','localization','auto-check-permission','last-active'], 'prefix' => 'teacher','namespace'=>'Teacher'], function () {
    Route::get('home', 'TeacherController@home')->name('teacher.home') ;
    Route::resource('group', 'GroupController');
    Route::resource('study-schedule', 'StudyScheduleController');
    Route::resource('exam', 'ExamController');
});
// end Teacher Dashboard




/// start area manger dashboard
Route::group(['middleware' => ['auth:web','localization','auto-check-permission','last-active'], 'prefix' => 'manager','namespace'=>'User'], function () {
    Route::get('home', 'UserController@home')->name('manager.home') ;

    Route::resource('level', 'LevelController');
    Route::resource('subject', 'SubjectController');
    Route::resource('visor', 'UserController');
    Route::resource('teacher', 'TeacherController');
    Route::resource('student', 'StudentController');
    Route::resource('exam', 'ExamController');
    Route::resource('group', 'GroupController');
    Route::resource('study-schedule', 'StudyScheduleController');
    Route::resource('logs', 'LogController')->only('index');
    Route::get('student/toggle-boolean/{id}/{action}', 'StudentController@toggleBoolean')->name('student.toggleBoolean');
    Route::resource('reports', 'ReportController');
    Route::get('teacher/{id}/editvisor', 'TeacherController@editvisor')->name('teacher.editvisor');
    Route::post('teacher/editvisor', 'TeacherController@updateVisor')->name('teacher.updateVisor');
});

/// end area manger