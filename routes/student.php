<?php


Route::group(['middleware'=>'guest:student'], function () {
    Route::get('login', 'StudentAuthController@viewLogin')->name('student.login');
    Route::post('login', 'StudentAuthController@login');
    Route::get('register', 'StudentAuthController@viewRegister');
    Route::post('register', 'StudentAuthController@register');
});




Route::group(['middleware' => ['auth:student','localization','auto-check-permission']], function () {

    Route::get('home', 'StudentController@home')->name('student.home') ;
    Route::get('profile', 'StudentController@profileView');
    Route::post('profile', 'StudentController@updateProfile');

    //schedule
    Route::post('schedule/{subject}/append-home-work/{session}', 'ScheduleController@appendHomeWork');
    Route::get('schedule/get-sessions', 'ScheduleController@getSessions');
    Route::resource('schedule', 'ScheduleController');

    //groups
    Route::resource('groups', 'GroupController');

    //exams
    Route::post('exams/append-exam/{exam}', 'ExamController@appendExam');
    Route::get('exams/get-sessions', 'ExamController@getExams');
    Route::resource('exams', 'ExamController');
});