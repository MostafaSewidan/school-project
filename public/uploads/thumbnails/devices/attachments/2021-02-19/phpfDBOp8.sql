

CREATE TABLE `attachments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('image','video') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'image',
  `usage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachmentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachmentable_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int DEFAULT NULL,
  `is_active` tinyint NOT NULL DEFAULT '1',
  `order` int DEFAULT NULL,
  `store_id` int DEFAULT NULL,
  `can_write_order` tinyint DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `contacts` (
  `client_id` int DEFAULT NULL,
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint NOT NULL DEFAULT '0',
  `contactable_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactable_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `contracts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `contract_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_type` enum('guaranteed','inclusive','maintenance_only','nothing','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_start` date NOT NULL,
  `hijri_contract_start` date NOT NULL,
  `contract_end` date NOT NULL,
  `hijri_contract_end` date NOT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` decimal(8,2) NOT NULL,
  `cut_per_day_down` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contracts_contract_number_unique` (`contract_number`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('1','con_num15','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_6','1013.00','1012.00','2021-02-19 22:36:47','2021-02-19 22:36:47');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('2','con_num20','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_4','1020.00','1023.00','2021-02-19 22:36:47','2021-02-19 22:36:47');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('3','con_num34','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_1','1030.00','1030.00','2021-02-19 22:36:47','2021-02-19 22:36:47');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('4','con_num41','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_2','1042.00','1042.00','2021-02-19 22:36:47','2021-02-19 22:36:47');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('5','con_num51','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_0','1053.00','1051.00','2021-02-19 22:36:47','2021-02-19 22:36:47');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('6','con_num66','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_0','1063.00','1062.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('7','con_num74','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_6','1071.00','1070.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('8','con_num84','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_6','1083.00','1082.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('9','con_num91','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_6','1092.00','1092.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO contracts (id, contract_number, contract_type, contract_start, hijri_contract_start, contract_end, hijri_contract_end, notes, cost, cut_per_day_down, created_at, updated_at) VALUES ('10','con_num105','guaranteed','2021-02-19','2021-02-19','2021-02-19','2021-02-19','not_1','10103.00','10101.00','2021-02-19 22:36:48','2021-02-19 22:36:48');


CREATE TABLE `departments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shorten_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','DIALYSIS','DIALYSIS');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','AMBULANCE','AMBULANCE');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','AMBULANCE','4957-SDB');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','BIOMEDICAL OFFICE','BIOMEDICAL OFFICE');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','CSSD','CSSD');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47',' Diabetic Center',' Diabetic Center');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','EEG ROOM','EEG ROOM');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','FEMALE MEDICAL WARD','FEMALE MEDICAL WARD');

INSERT INTO departments (id, created_at, updated_at, name, shorten_name) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','OB','OB');


CREATE TABLE `device_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو1');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو2');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو3');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو4');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو5');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو6');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو7');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو8');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو9');

INSERT INTO device_types (id, created_at, updated_at, name) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','راديو10');


CREATE TABLE `devices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int DEFAULT NULL,
  `manufacturer_id` int NOT NULL,
  `category` enum('a','b','c','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type_id` int NOT NULL,
  `importer_id` int DEFAULT NULL,
  `serial_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('up','down','under_construction','in_stock','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `constructing_date` date DEFAULT NULL,
  `hijri_constructing_date` date DEFAULT NULL,
  `room_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `devices_serial_number_unique` (`serial_number`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU1286','1','1','a','1','1','SNS20230095HA','B650','down','No_1','2021-02-19','2021-02-19','DIALYSIS','1');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU1420','2','2','b','1','1','2VCAH659','4008B','down','No_0','2021-02-19','2021-02-19','4008B','1');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU199','3','3','a','3','3','8VCAKY95','4008S','down','No_5','2021-02-19','2021-02-19','DIALYSIS','3');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU1286','4','4','a','4','4','SNS20230095H','B650','down','No_5','2021-02-19','2021-02-19','DIALYSIS','4');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU202','5','5','a','5','5','9VCANU18','4008S','down','No_5','2021-02-19','2021-02-19','DIALYSIS','5');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','AKU839','6','6','a','6','6','VSB14B0039','CARE VISION VS110','down','No_2','2021-02-19','2021-02-19','DIALYSIS','6');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','AMB115','7','7','a','7','7','E70135','LTV1200','down','No_3','2021-02-19','2021-02-19','DIALYSIS','7');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','AMB363','8','8','a','8','8','OMNI2-1208-32969-A','OMNI II','down','No_4','2021-02-19','2021-02-19','DIALYSIS','8');

INSERT INTO devices (id, created_at, updated_at, code, department_id, manufacturer_id, category, device_type_id, importer_id, serial_number, model, status, notes, constructing_date, hijri_constructing_date, room_number, contract_id) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','AMB367','9','9','a','9','9','X12G560846','AED PLUS','down','No_2','2021-02-19','2021-02-19','DIALYSIS','9');


CREATE TABLE `expenses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `expenses_data` date NOT NULL,
  `hijri_expenses_date` date NOT NULL,
  `item_id` int NOT NULL,
  `cost` decimal(8,2) NOT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `extracts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `extracts_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('1','نموذج رقم 1','شهادة إنجاز','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('2','نموذج رقم 2','جدول الاستحقاق الشهري ','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('3','نموذج رقم 3','جدول الحسميات علي الأجهزة المتعطلة','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('4','نموذج رقم 4','تكاليف تطبيق جدول حسميات التقصير في أعمال الصيانة الوقائية','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('5','نموذج رقم 5أ/ب','تكاليف مقاولي الباطن للأجهزة الطبية للعقود التي تصرف بنظام الزيارات','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('6','نموذج رقم 6','التقرير الشهري لإصلاح وصيانة الأجهزة الطبية بالموقع','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('7','نموذج رقم 7','بيان أوامر العمل التي تم إنجازها خلال فترة المستخلص','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('8','نموذج رقم 8','بيان أوامر العمل التي لم يتم إنجازها خلال فترة المستخلص','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('9','نموذج رقم 9','بيان قطع الغيار التي وصلت الموقع حسب التعاميد وتم تركيبها بالأجهزة','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('10','نموذج رقم 10','بيان قطع الغيار التي لم يتم تأمينها أو توريدها','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('11','نموذج رقم 11','مبررات المتعهد والإجراءات التي اتخذها بشأن أوامر العمل التي لم يتم
                                        إنجازها','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('12','نموذج رقم 12','بيان أوامر العمل المفتوحة والتي لم يتم انجازها خلال الفترات الماضية','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('13','نموذج رقم 13','بيان بأعمال الصيانة الوقائية لأجهزة فئة أ+ب خلال فترة المستخلص','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('14','نموذج رقم 14','بيان بأعمال الصيانة الوقائية للأجهزة فئة أ+ب التي لم تتم خلال الفترات
                                        السابقة','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('15','نموذج رقم 15','شهادة إنجاز الحاسب الآلي','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('16','نموذج رقم 16','شهادة أداء أعمال الصيانة الوقائية المخططة من قبل المقاول','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('17','نموذج رقم 17','مشهد بصرف الرواتب','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('18','نموذج رقم 18','شهادة أداء صيانة / إصلاح لجهاز تم فحصه من قبل مقاول الباطن ','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('19','نموذج رقم 19','زيارات مقاولي الباطن للأجهزة التخصصية أ-ب','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO extracts (id, code, name, created_at, updated_at) VALUES ('20','نموذج رقم 20','الاستحقاق الشهري لرواتب العاملين','2021-02-19 22:36:48','2021-02-19 22:36:48');


CREATE TABLE `importers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica2','019','3','medica_2@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica2','018','4','medica_1@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica0','017','5','medica_1@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica2','018','0','medica_2@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica1','015','1','medica_1@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica1','018','3','medica_1@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica0','018','2','medica_0@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica0','012','1','medica_2@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica0','015','2','medica_1@import.com');

INSERT INTO importers (id, created_at, updated_at, name, phone, fax, email) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','Medica2','013','1','medica_1@import.com');


CREATE TABLE `items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` decimal(8,2) NOT NULL,
  `current_balance` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('1','BAND0','0.00','0.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('2','BAND1','100.00','100.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('3','BAND2','200.00','200.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('4','BAND3','300.00','300.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('5','BAND4','400.00','400.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('6','BAND5','500.00','500.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('7','BAND6','600.00','600.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('8','BAND7','700.00','700.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('9','BAND8','800.00','800.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('10','BAND9','900.00','900.00','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO items (id, name, total_amount, current_balance, created_at, updated_at) VALUES ('11','BAND10','1000.00','1000.00','2021-02-19 22:36:48','2021-02-19 22:36:48');


CREATE TABLE `maintenances` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `device_id` int NOT NULL,
  `maintenance_date` date NOT NULL,
  `hijri_maintenance_date` date DEFAULT NULL,
  `cost` decimal(8,2) NOT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','1','2021-02-19','','111.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','2','2021-02-19','','112.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','3','2021-02-19','','113.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','4','2021-02-19','','114.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','5','2021-02-19','','115.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','6','2021-02-19','','116.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','7','2021-02-19','','117.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','8','2021-02-19','','118.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','9','2021-02-19','','119.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','10','2021-02-19','','1110.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('11','2021-02-19 22:37:11','2021-02-19 22:37:11','7','2021-01-01','','100.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('12','2021-02-19 22:37:11','2021-02-19 22:37:11','7','2021-02-01','','100.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('13','2021-02-19 22:37:11','2021-02-19 22:37:11','7','2021-09-01','','100.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('14','2021-02-19 23:03:14','2021-02-19 23:03:14','3','2021-02-08','','100.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('15','2021-02-19 23:14:24','2021-02-19 23:14:24','1','2021-02-16','','100.00','0');

INSERT INTO maintenances (id, created_at, updated_at, device_id, maintenance_date, hijri_maintenance_date, cost, is_done) VALUES ('16','2021-02-19 23:14:24','2021-02-19 23:14:24','1','2021-06-01','','100.00','0');


CREATE TABLE `manufacturers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_1');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_1');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_0');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_0');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_0');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_1');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_0');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_0');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_2');

INSERT INTO manufacturers (id, created_at, updated_at, name) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','Siemens_2');


CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO migrations (id, migration, batch) VALUES ('1','2014_10_12_000000_create_users_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('2','2014_10_12_100000_create_password_resets_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('3','2016_06_01_000001_create_oauth_auth_codes_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('4','2016_06_01_000002_create_oauth_access_tokens_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('5','2016_06_01_000003_create_oauth_refresh_tokens_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('6','2016_06_01_000004_create_oauth_clients_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('7','2016_06_01_000005_create_oauth_personal_access_clients_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('8','2020_04_22_070025_create_permission_tables','1');

INSERT INTO migrations (id, migration, batch) VALUES ('9','2020_05_21_164252_create_attachments_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('10','2020_05_21_164252_create_categories_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('11','2020_05_21_164252_create_contacts_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('12','2020_05_21_164252_create_notifiables_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('13','2020_05_21_164252_create_notifications_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('14','2020_05_21_164252_create_regions_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('15','2020_05_21_164252_create_settings_categories_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('16','2020_05_21_164252_create_tokens_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('17','2021_02_08_212854_create_departments_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('18','2021_02_08_212854_create_device_names_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('19','2021_02_08_212854_create_device_types_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('20','2021_02_08_212854_create_devices_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('21','2021_02_08_212854_create_importers_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('22','2021_02_08_212854_create_maintenances_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('23','2021_02_08_212854_create_manufacturers_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('24','2021_02_08_212854_create_purchase_orders_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('25','2021_02_08_212854_create_settings_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('26','2021_02_08_212854_create_working_orders_comments_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('27','2021_02_09_113044_create_working_orders_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('28','2021_02_12_211546_create_contracts_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('29','2021_02_13_200000_create_extracts_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('30','2021_02_13_215723_create_spare_parts_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('31','2021_02_13_234326_create_items_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('32','2021_02_19_231835_create_expenses_table','1');


CREATE TABLE `model_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `model_has_roles` (
  `role_id` bigint unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `notifiables` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int NOT NULL,
  `is_read` tinyint NOT NULL DEFAULT '0',
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `notifications` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `routes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `purchase_orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `po_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `starts_at` date NOT NULL,
  `valid_months` int NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `is_done` enum('outlet','not_enforced','under_procedure','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `part_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_order_id` int NOT NULL,
  `offered_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` int NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_imported` tinyint(1) NOT NULL DEFAULT '0',
  `bill_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `import_date` date NOT NULL,
  `hijri_import_date` date DEFAULT NULL,
  `payment_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ministry_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `export_date` date NOT NULL,
  `hijri_export_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','po_4','Cl_1','Jo_0','2021-02-19','1','0.00','outlet','par_2','1','off_1','2','0','0','bill_2','2021-02-19','','pay_','ministry_2','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','po_4','Cl_0','Jo_3','2021-02-19','2','0.00','outlet','par_4','2','off_4','3','0','0','bill_1','2021-02-19','','pay_','ministry_3','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','po_2','Cl_1','Jo_1','2021-02-19','3','3.00','outlet','par_3','3','off_1','2','0','0','bill_3','2021-02-19','','pay_','ministry_3','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','po_2','Cl_1','Jo_4','2021-02-19','4','0.00','outlet','par_5','4','off_4','2','0','0','bill_0','2021-02-19','','pay_','ministry_1','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','po_1','Cl_0','Jo_4','2021-02-19','5','3.00','outlet','par_2','5','off_3','2','0','0','bill_0','2021-02-19','','pay_','ministry_4','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','po_1','Cl_2','Jo_1','2021-02-19','6','2.00','outlet','par_0','6','off_3','2','0','0','bill_3','2021-02-19','','pay_','ministry_3','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','po_2','Cl_1','Jo_2','2021-02-19','7','1.00','outlet','par_4','7','off_2','3','0','0','bill_4','2021-02-19','','pay_','ministry_2','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','po_1','Cl_1','Jo_0','2021-02-19','8','4.00','outlet','par_2','8','off_0','1','0','0','bill_4','2021-02-19','','pay_','ministry_4','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','po_2','Cl_3','Jo_4','2021-02-19','9','0.00','outlet','par_3','9','off_0','0','0','0','bill_0','2021-02-19','','pay_','ministry_2','2021-02-19','');

INSERT INTO purchase_orders (id, created_at, updated_at, po_num, by, to, starts_at, valid_months, price, is_done, part_name, working_order_id, offered_by, period, is_paid, is_imported, bill_num, import_date, hijri_import_date, payment_num, ministry_num, export_date, hijri_export_date) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','po_0','Cl_4','Jo_0','2021-02-19','10','3.00','outlet','par_1','10','off_2','3','0','0','bill_1','2021-02-19','','pay_','ministry_0','2021-02-19','');


CREATE TABLE `regions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int NOT NULL,
  `delivery_search_area_in_app_cost` double(8,2) DEFAULT NULL,
  `store_search_area` double(8,2) DEFAULT NULL,
  `extra_km_price` double(8,2) DEFAULT NULL,
  `one_store_order_cost` double(8,2) DEFAULT NULL,
  `increase_factor` double(8,2) DEFAULT NULL,
  `sub_locality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `role_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_supervisor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_start_date` date NOT NULL,
  `total_budget` decimal(8,2) NOT NULL DEFAULT '0.00',
  `hijri_project_start_date` date NOT NULL,
  `project_end_date` date NOT NULL,
  `hijri_project_end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `settings_categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `spare_parts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int NOT NULL,
  `serial_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type_id` int NOT NULL,
  `available_quantity` int NOT NULL,
  `manufacturer_id` int NOT NULL,
  `importer_id` int NOT NULL,
  `statues` enum('valid','not_good','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `Validity_date` date NOT NULL,
  `hijri_Validity_date` date NOT NULL,
  `entry_date` date NOT NULL,
  `hijri_entry_date` date NOT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('1','na_11471342505','1','sr_10','1','11','1','1','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','jNNHCFTrNBGsyj2HoWcyN2Jaenvbmk3IR4u2LQKUquxdpgtDl6QyGJk2t7gz','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('2','na_2194296647','2','sr_27','2','2','2','2','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','1shCyDoutJ0iXBsJ6EMaa0ksdzt6vKjg2OslZBUWp9VP7Q9nJKQVrCeOYHea','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('3','na_31218335655','3','sr_37','3','33','3','3','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','RLq7cXalizhSOz4TxPTX7OGvcWWGBNJg5zgIltrrvhvhlM6VtVh3pjAXyrLV','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('4','na_41476576797','4','sr_40','4','54','4','4','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','33WoNSXm4xJ5CbVU3mxIOns4MGawfGqKN6L4wMopUjtsxRGo02ohhJ2cQL9l','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('5','na_5279097289','5','sr_54','5','5','5','5','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','73JfTWK0dummamLRkmD5Yrcr3eSH5s6fB0QsZdRSaFpOKqYUvWqAEoD3LTSa','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('6','na_6391061518','6','sr_66','6','66','6','6','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','1kNivUOJAa0b9ZXDqPulmjv7Mnva65q8QX4XuHGaxiyNo1xBzHjSRLBtJpWC','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('7','na_7993215321','7','sr_78','7','7','7','7','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','IZVYRcYfBaOCfpegGNnNkxoDQGrqvQ39oA7dzggDD4KmnVrgeGVtEjC0Bv53','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('8','na_8910304825','8','sr_82','8','38','8','8','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','ETSrKd6qGSuhVmjUR9yjYcDqMBfMGHnKzt1ltHC8ZcEdnDcGqRqL9XfptHE5','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('9','na_9923647818','9','sr_92','9','69','9','9','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','t9gD1Xrq2gBVDlaoeh0a7zdfWM35vAuCyzY8IZOVri29VqoBPESwFfU2cccv','2021-02-19 22:36:48','2021-02-19 22:36:48');

INSERT INTO spare_parts (id, name, device_id, serial_number, device_type_id, available_quantity, manufacturer_id, importer_id, statues, Validity_date, hijri_Validity_date, entry_date, hijri_entry_date, notes, created_at, updated_at) VALUES ('10','na_101447709742','10','sr_108','10','410','10','10','other','2021-02-19','2021-02-19','2021-02-19','2021-02-19','E7DO3YjF2C3l08dITFqNPXOBZAux8OAyg74FOvghn6BmrBfRhYlAgXg69qCt','2021-02-19 22:36:48','2021-02-19 22:36:48');


CREATE TABLE `tokens` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `os` enum('') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tokenable_id` int NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) VALUES ('1','مدير','admin@hospital.com','','$2y$10$.2gHEF4eUxaySY59arn0Pesj7/UE4xahr0J/POWWA0SCzX2K6PsR6','','2021-02-19 22:36:45','2021-02-19 22:36:45');


CREATE TABLE `working_orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `maintenance_id` int DEFAULT NULL,
  `device_id` int NOT NULL,
  `due_to` date NOT NULL,
  `hijri_due_to` date NOT NULL,
  `status` enum('open','done','not_done') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `reporter_id` int DEFAULT NULL,
  `type` enum('maintenance','repair') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','','1','2020-11-01','1442-02-01','open','1','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','','2','2020-11-01','1442-02-01','open','2','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','','3','2020-11-01','1442-02-01','open','3','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','','4','2020-11-01','1442-02-01','open','4','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','','5','2020-11-01','1442-02-01','open','5','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','','6','2020-11-01','1442-02-01','open','6','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','','7','2020-11-01','1442-02-01','open','7','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','','8','2020-11-01','1442-02-01','open','8','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','','9','2020-11-01','1442-02-01','open','9','maintenance');

INSERT INTO working_orders (id, created_at, updated_at, maintenance_id, device_id, due_to, hijri_due_to, status, reporter_id, type) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','','10','2020-11-01','1442-02-01','open','10','maintenance');


CREATE TABLE `working_orders_comments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_order_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('1','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText3','1');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('2','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText6','2');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('3','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText0','3');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('4','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText4','4');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('5','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText1','5');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('6','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText5','6');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('7','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText2','7');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('8','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText1','8');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('9','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText3','9');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('10','2021-02-19 22:36:47','2021-02-19 22:36:47','1','largText0','10');

INSERT INTO working_orders_comments (id, created_at, updated_at, user_id, body, working_order_id) VALUES ('11','2021-02-19 23:04:07','2021-02-19 23:04:07','1','تتشش','1');
