-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 18, 2021 at 04:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `routes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `routes`, `group`, `created_at`, `updated_at`) VALUES
(1, 'عرض المرحلة الدراسية', 'web', 'level.index', 'المراحل الدراسية', NULL, NULL),
(2, 'إضافة المرحلة الدراسية', 'web', 'level.create,level.store', 'المراحل الدراسية', NULL, NULL),
(3, 'تعديل المرحلة الدراسية', 'web', 'level.edit,level.update', 'المراحل الدراسية', NULL, NULL),
(4, 'حذف المرحلة الدراسية', 'web', 'level.destroy', 'المراحل الدراسية', NULL, NULL),
(5, 'عرض المواد الدراسية', 'web', 'subject.index', 'المواد الدراسية', NULL, NULL),
(6, 'إضافة المواد الدراسية', 'web', 'subject.create,subject.store', 'المواد الدراسية', NULL, NULL),
(7, 'تعديل المواد الدراسية', 'web', 'subject.edit,subject.update', 'المواد الدراسية', NULL, NULL),
(8, 'حذف المواد الدراسية', 'web', 'subject.destroy', 'المواد الدراسية', NULL, NULL),
(9, 'عرض المشرف', 'web', 'visor.index', 'المشرفين', NULL, NULL),
(10, 'إضافة المشرف', 'web', 'visor.create,visor.store', 'المشرفين', NULL, NULL),
(11, 'تعديل المشرف', 'web', 'visor.edit,visor.update', 'المشرفين', NULL, NULL),
(12, 'حذف المشرف', 'web', 'visor.destroy', 'المشرفين', NULL, NULL),
(13, 'عرض المدرس', 'web', 'teacher.index', 'المدرسين', NULL, NULL),
(14, 'إضافة المدرس', 'web', 'teacher.create,teacher.store', 'المدرسين', NULL, NULL),
(15, 'تعديل المدرس', 'web', 'teacher.edit,teacher.update', 'المدرسين', NULL, NULL),
(16, 'حذف المدرس', 'web', 'teacher.destroy', 'المدرسين', NULL, NULL),
(17, 'عرض الطلاب', 'web', 'student.index', 'الطلاب', NULL, NULL),
(18, 'إضافة طالب', 'web', 'student.create,student.store', 'الطلاب', NULL, NULL),
(19, 'تعديل الطالب', 'web', 'student.edit,student.update', 'الطلاب', NULL, NULL),
(20, 'حذف الطالب', 'web', 'student.destroy', 'الطلاب', NULL, NULL),
(21, 'عرض الامتحانات', 'web', 'exam.index', 'الامتحانات', NULL, NULL),
(22, 'إضافة امتحان', 'web', 'exam.create,exam.store', 'الامتحانات', NULL, NULL),
(23, 'تعديل الامتحانات', 'web', 'exam.edit,exam.update', 'الامتحانات', NULL, NULL),
(24, 'حذف الامتحان', 'web', 'exam.destroy', 'الامتحانات', NULL, NULL),
(25, 'عرض المجموعات', 'web', 'group.index', 'المجموعات', NULL, NULL),
(26, 'إضافة مجموعة', 'web', 'group.create,group.store', 'المجموعات', NULL, NULL),
(27, 'تعديل المجموعة', 'web', 'group.edit,group.update', 'المجموعات', NULL, NULL),
(28, 'حذف المجموعة', 'web', 'group.destroy', 'المجموعات', NULL, NULL),
(29, 'عرض الجدول الدراسي', 'web', 'study-schedule.index', 'الجداول الدراسية', NULL, NULL),
(30, 'إضافة الجدول الدراسي', 'web', 'study-schedule.create,study-schedule.store', 'الجداول الدراسية', NULL, NULL),
(31, 'تعديل الجدول الدراسي', 'web', 'study-schedule.edit,study-schedule.update', 'الجداول الدراسية', NULL, NULL),
(32, 'حذف الجدول الدراسي', 'web', 'study-schedule.destroy', 'الجداول الدراسية', NULL, NULL),
(33, 'عرض التقرير', 'web', 'reports.index', 'التقارير', NULL, NULL),
(34, 'إضافة التقرير', 'web', 'reports.create,reports.store', 'التقارير', NULL, NULL),
(35, 'تعديل التقرير', 'web', 'reports.edit,reports.update', 'التقارير', NULL, NULL),
(36, 'حذف التقرير', 'web', 'reports.destroy', 'التقارير', NULL, NULL),
(37, 'عرض الرسائل', 'web', 'message.index', 'الرسائل', NULL, NULL),
(38, 'حذف الرسائل', 'web', 'message.destroy', 'الرسائل', NULL, NULL),
(39, 'عرض السجلات', 'web', 'logs.index', 'السجلات', NULL, NULL),
(40, 'حذف السجلات', 'web', 'logs.destroy', 'السجلات', NULL, NULL),
(41, 'حظر الطالب', 'web', 'student.toggleBoolean', 'الطلاب', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
